package com.actiTime.UserTest;

import java.io.IOException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.actiTime.businessLib.UserLib;
import com.actiTime.genericLib.Driver;
import com.actiTime.genericLib.ExcelLib;
import com.actiTime.genericLib.WebDriverCommonUtil;
import com.actiTime.objectRepository.EditUserSettingsPage;

public class UserTest {
	
	
	//Object initialization
	ExcelLib eLib=new ExcelLib();
	WebDriverCommonUtil wLib=new WebDriverCommonUtil();
	UserLib bLib=new UserLib();
	EditUserSettingsPage editUserSettingsPage=PageFactory.initElements(Driver.driver, EditUserSettingsPage.class);
	
 
	
	@Test(groups={"SmokeTest"})
     public void createUserAndVerifyDetailsTest() throws InvalidFormatException, IOException {
		//Test Data
		String userName=eLib.getExcelData("UserTestData",1 ,2);
		String password=eLib.getExcelData("UserTestData",1 ,3);
		String newUserName=eLib.getExcelData("UserTestData",1 ,4);
		String firstName=eLib.getExcelData("UserTestData",1 ,6);
		String lastName=eLib.getExcelData("UserTestData",1 ,7);
		String userPassword=eLib.getExcelData("UserTestData",1 ,8);
		
		//Step 1:Login To Application
		bLib.loginToAppliction(userName, password);
		wLib.waitForPageToLoad();
		
		//Step 2:Navigate To User Page
		bLib.navigateToUserPage();
		wLib.waitForPageToLoad();
		
		//Step 3:Create User
		bLib.createUser(newUserName, userPassword, firstName, lastName);
		wLib.waitForPageToLoad();
		
		//Step 4:Verify User Details
		Driver.driver.findElement(By.partialLinkText(newUserName)).click();
		wLib.waitForPageToLoad();
		String actualUserName=editUserSettingsPage.getUserNameEditBox().getAttribute("value");
		String actualFirstName=editUserSettingsPage.getFirstNameEditBox().getAttribute("value");
		String actualLastName=editUserSettingsPage.getLastNameEditBox().getAttribute("value");
		Assert.assertEquals(actualUserName, newUserName,"User Name Not Matched");
		Assert.assertEquals(actualFirstName, firstName,"User Name Not Matched");
		Assert.assertEquals(actualLastName, lastName,"User Name Not Matched");
		
		//Step 5:Logout
		bLib.logout();
  }
	
	@Test
	  public void modifyUserAndVerifyDetailsTest() throws InvalidFormatException, IOException {
		
		//Test Data
				String userName=eLib.getExcelData("UserTestData",2 ,2);
				String password=eLib.getExcelData("UserTestData",2 ,3);
				String newUserName=eLib.getExcelData("UserTestData",2 ,4);
				String modifiedUserName=eLib.getExcelData("UserTestData",2 ,5);
				String firstName=eLib.getExcelData("UserTestData",2,6);
				String lastName=eLib.getExcelData("UserTestData",2,7);
				String userPassword=eLib.getExcelData("UserTestData",2,8);
				
				//Step 1:Login To Application
				bLib.loginToAppliction(userName, password);
				wLib.waitForPageToLoad();
				//Step 2:Navigate To User Page
				bLib.navigateToUserPage();
				wLib.waitForPageToLoad();
				
				//Step 3:Create User
				bLib.createUser(newUserName, userPassword, firstName, lastName);
				wLib.waitForPageToLoad();
				
				//Step 4:Modify User Details
				Driver.driver.findElement(By.partialLinkText(newUserName)).click();
				editUserSettingsPage.getUserNameEditBox().clear();
				editUserSettingsPage.getUserNameEditBox().sendKeys(modifiedUserName);
				editUserSettingsPage.getSaveChangesBtn().click();
				wLib.waitForPageToLoad();
				
				//Step 5:Verify User Details
				Driver.driver.findElement(By.partialLinkText(modifiedUserName)).click();
				wLib.waitForPageToLoad();
				String actualUserName=editUserSettingsPage.getUserNameEditBox().getAttribute("value");
				String actualFirstName=editUserSettingsPage.getFirstNameEditBox().getAttribute("value");
				String actualLastName=editUserSettingsPage.getLastNameEditBox().getAttribute("value");
				Assert.assertEquals(actualUserName, modifiedUserName,"User Name Not Matched");
				Assert.assertEquals(actualFirstName, firstName,"User Name Not Matched");
				Assert.assertEquals(actualLastName, lastName,"User Name Not Matched");
				
				//Step 6:Logout
				bLib.logout();
	  }
	
	@Test
	  public void deleteUserTest() throws InvalidFormatException, IOException {
		
		//Test Data
				String userName=eLib.getExcelData("UserTestData",3,2);
				String password=eLib.getExcelData("UserTestData",3,3);
				String newUserName=eLib.getExcelData("UserTestData",3,4);
				String firstName=eLib.getExcelData("UserTestData",3,6);
				String lastName=eLib.getExcelData("UserTestData",3,7);
				String userPassword=eLib.getExcelData("UserTestData",3,8);
				
				//Step 1:Login To Application
				bLib.loginToAppliction(userName, password);
				wLib.waitForPageToLoad();
				
				//Step 2:Navigate To User Page
				bLib.navigateToUserPage();
				wLib.waitForPageToLoad();
				
				//Step 3:Create User
				bLib.createUser(newUserName, userPassword, firstName, lastName);
				wLib.waitForPageToLoad();
				
				//Step 4:Delete User
				Driver.driver.findElement(By.partialLinkText(newUserName)).click();
				wLib.waitForPageToLoad();
				editUserSettingsPage.getDeleteThisuserBtn().click();
				wLib.acceptAlert();
				wLib.waitForPageToLoad();
				
				//Step 5:Logout
				bLib.logout();
		  
	  }
}
