package com.actiTime.businessLib;

import org.openqa.selenium.support.PageFactory;

import com.actiTime.genericLib.Driver;
import com.actiTime.genericLib.WebDriverCommonUtil;
import com.actiTime.objectRepository.ActiveProjectAndCustomerPage;
import com.actiTime.objectRepository.AddNewCustomerPage;
import com.actiTime.objectRepository.CommonPage;
import com.actiTime.objectRepository.LoginPage;
import com.actiTime.objectRepository.OpenTasksPage;


//Generic Business reusable Methods For ProjectAndTasks Module
public class ProjectAndTasksLib extends WebDriverCommonUtil {
	
	LoginPage loginpage=PageFactory.initElements(Driver.driver, LoginPage.class);
	OpenTasksPage openTasksPage=PageFactory.initElements(Driver.driver, OpenTasksPage.class);
	ActiveProjectAndCustomerPage activeProjectAndCustomerpage=PageFactory.initElements(Driver.driver, ActiveProjectAndCustomerPage.class);
	AddNewCustomerPage addnewCustomerPage=PageFactory.initElements(Driver.driver, AddNewCustomerPage.class);
	CommonPage commonPage=PageFactory.initElements(Driver.driver, CommonPage.class);
	
	public void loginToAppliction(String userName,String password)
	{
		Driver.driver.get("http://127.0.0.1:81/login.do");
		loginpage.getUserNameEditBox().sendKeys(userName);
		loginpage.getPasswordEditBox().sendKeys(password);
		loginpage.getLoginBtn().click();
	}
	
	public void navigateToProjectAndCustomerPage() 
	{
		openTasksPage.getProjectAndCustomeLink().click();
		
	}
	
	public void createCustomer(String customerName)
	{
	
		activeProjectAndCustomerpage.getAddNewCustomerBtn().click();
		waitForPageToLoad();
		addnewCustomerPage.getCustomerNameEditBox().sendKeys(customerName);
		addnewCustomerPage.getCreateCustomerBtn().click();
	
	
	}
	
	public void logout()
	{
		commonPage.getLogoutImage().click();
	}
	
	
	
		


}
