package com.actiTime.businessLib;

import org.openqa.selenium.support.PageFactory;

import com.actiTime.genericLib.Driver;
import com.actiTime.genericLib.WebDriverCommonUtil;
import com.actiTime.objectRepository.AddNewUserPage;
import com.actiTime.objectRepository.CommonPage;
import com.actiTime.objectRepository.LoginPage;
import com.actiTime.objectRepository.UserListPage;


//Generic Business reusable Methods For User Module
public class UserLib extends WebDriverCommonUtil {
	
	LoginPage loginpage=PageFactory.initElements(Driver.driver, LoginPage.class);
	UserListPage userListPage=PageFactory.initElements(Driver.driver, UserListPage.class);
	AddNewUserPage addNewUserPage=PageFactory.initElements(Driver.driver, AddNewUserPage.class);
	CommonPage commonPage=PageFactory.initElements(Driver.driver, CommonPage.class);
	
	
	public void loginToAppliction(String userName,String password)
	{
		Driver.driver.get("http://127.0.0.1:81/login.do");
		loginpage.getUserNameEditBox().sendKeys(userName);
		loginpage.getPasswordEditBox().sendKeys(password);
		loginpage.getLoginBtn().click();
	}
	
	public void navigateToUserPage() 
	{
		commonPage.getUserLink().click();
	}
	
	public void createUser(String newUserName, String userPassword, String firstName, String lastName)
	{
		userListPage.getAddNewUserBtn().click();
		waitForPageToLoad();
		addNewUserPage.getUserNameEditBox().sendKeys(newUserName);
		addNewUserPage.getFirstNameEditBox().sendKeys(firstName);
		addNewUserPage.getLastNameEditBox().sendKeys(lastName);
		addNewUserPage.getPasswordEditBox().sendKeys(userPassword);
		addNewUserPage.getRetypePasswordEditBox().sendKeys(userPassword);
		addNewUserPage.getCreateUserBtn().click();
	}
	
	
	public void logout()
	{
		commonPage.getLogoutImage().click();
	}

}
