package com.actiTime.objectRepository;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class UserListPage {
	
	@FindBy(xpath="//input[@value='Add New User']")
	private WebElement addNewUserBtn;

	public WebElement getAddNewUserBtn() {
		return addNewUserBtn;
	}
	

}
