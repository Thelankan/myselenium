package com.actiTime.objectRepository;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CommonPage {
	
	@FindBy(linkText="Users")
	private WebElement userLink;
	
	@FindBy(linkText="Projects & Tasks")
	private WebElement projectAndTasksLink;
	
	@FindBy(linkText="Reports")
	private WebElement reportsLink;
	
	@FindBy(linkText="Time-Track")
	private WebElement timeTrackLink;

	@FindBy(className="logoutImg")
	private WebElement logoutImage;

	public WebElement getUserLink() {
		return userLink;
	}

	public WebElement getProjectAndTasksLink() {
		return projectAndTasksLink;
	}

	public WebElement getReportsLink() {
		return reportsLink;
	}

	public WebElement getTimeTrackLink() {
		return timeTrackLink;
	}

	public WebElement getLogoutImage() {
		return logoutImage;
	}
}
