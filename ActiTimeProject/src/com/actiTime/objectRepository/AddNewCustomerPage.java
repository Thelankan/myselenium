package com.actiTime.objectRepository;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class AddNewCustomerPage {
	
	
	@FindBy(name="name")
	private WebElement customerNameEditBox;
	
	@FindBy(xpath="//input[@value='Create Customer']")
	private WebElement createCustomerBtn;

	public WebElement getCustomerNameEditBox() {
		return customerNameEditBox;
	}

	public WebElement getCreateCustomerBtn() {
		return createCustomerBtn;
	}

}
