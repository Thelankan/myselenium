package com.actiTime.objectRepository;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage {
	
	@FindBy(name="username")
	private WebElement userNameEditBox;
	
	@FindBy(name="pwd")
	private WebElement passwordEditBox;
	
	
	@FindBy(xpath="//input[@type='submit']")
	private WebElement loginBtn;
	
	public WebElement getUserNameEditBox() {
		return userNameEditBox;
	}

	public WebElement getPasswordEditBox() {
		return passwordEditBox;
	}

	public WebElement getLoginBtn() {
		return loginBtn;
	}

	
}
