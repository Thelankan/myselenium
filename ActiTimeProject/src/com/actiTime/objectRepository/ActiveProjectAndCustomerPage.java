package com.actiTime.objectRepository;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ActiveProjectAndCustomerPage {
	
	@FindBy(xpath="//input[@value='Add New Customer']")
	private WebElement addNewCustomerBtn;

	public WebElement getAddNewCustomerBtn() {
		return addNewCustomerBtn;
	}

}
