package com.actiTime.objectRepository;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class EditUserSettingsPage {
	
	@FindBy(name="username")
	private WebElement userNameEditBox;

	@FindBy(name="passwordText")
	private WebElement passwordEditBox;
	
	@FindBy(name="passwordTextRetype")
	private WebElement retypePasswordEditBox;
	
	@FindBy(name="firstName")
	private WebElement firstNameEditBox;
	
	@FindBy(name="lastName")
	private WebElement lastNameEditBox;
	
	@FindBy(xpath="//input[@type='submit']")
	private WebElement saveChangesBtn;
	
	@FindBy(xpath="//input[@value='Delete this User']")
	private WebElement deleteThisUserBtn;

	public WebElement getDeleteThisuserBtn() {
		return deleteThisUserBtn;
	}

	public WebElement getUserNameEditBox() {
		return userNameEditBox;
	}

	public WebElement getPasswordEditBox() {
		return passwordEditBox;
	}

	public WebElement getRetypePasswordEditBox() {
		return retypePasswordEditBox;
	}

	public WebElement getFirstNameEditBox() {
		return firstNameEditBox;
	}

	public WebElement getLastNameEditBox() {
		return lastNameEditBox;
	}

	public WebElement getSaveChangesBtn() {
		return saveChangesBtn;
	}

}
