package com.actiTime.objectRepository;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class EditCustomerInformationPage {
	
	@FindBy(name="name")
	private WebElement customerNameEditBox;
	
	@FindBy(xpath="//input[@value='Delete This Customer']")
	private WebElement deleteThisCustomerBtn;
	
	
	@FindBy(xpath="//input[@value='Save Changes']")
	private WebElement saveChangesBtn;
	
	@FindBy(xpath="//td[contains(text(),'You have selected customer')]/following-sibling::td/span")
	private WebElement customerNameInformation;


	public WebElement getCustomerNameInformation() {
		return customerNameInformation;
	}


	public WebElement getCustomerNameEditBox() {
		return customerNameEditBox;
	}


	public WebElement getDeleteThisCustomerBtn() {
		return deleteThisCustomerBtn;
	}


	public WebElement getSaveChangesBtn() {
		return saveChangesBtn;
	}
	
	

}
