package com.actiTime.objectRepository;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class OpenTasksPage {
	
	@FindBy(linkText="Projects & Customers")
	private WebElement projectAndCustomeLink;
	
	@FindBy(xpath="//input[@type='button']")
	private WebElement addNewTaskBtn;
	
	public WebElement getAddNewTaskBtn() {
		return addNewTaskBtn;
	}

	public WebElement getProjectAndCustomeLink() {
		return projectAndCustomeLink;
	}
}
