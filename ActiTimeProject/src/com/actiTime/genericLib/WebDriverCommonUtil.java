package com.actiTime.genericLib;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

//WebDriver Common Reusable Custom Methods
public class WebDriverCommonUtil {
	
	boolean flag=false;
	
	public void waitForPageToLoad()
	{
		Driver.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	}
	
	public boolean verifyText(String wbXpath, String expectedString)
	{
		String actualString=Driver.driver.findElement(By.xpath(wbXpath)).getText();
		if(actualString.equals(expectedString))
		{
			System.out.println(expectedString+ "is displayed on UI");
			flag=true;
		}
		else
		{
			System.out.println(expectedString+ "is not displayed on UI");
		}
		
		return flag;
	}

	public boolean verifyTextPresent(String expectedString)
	{
		String entairPageSource=Driver.driver.getPageSource();
		if(entairPageSource.contains(expectedString))
		{
			System.out.println(expectedString+ "is displayed on UI");
			flag=true;
		}
		else
		{
			System.out.println(expectedString+ "is not displayed on UI");
		}
		
		return flag;
	}

	public void waitForLinkPresent(String wbLinkText)
	{
		WebDriverWait wait=new WebDriverWait(Driver.driver, 20);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText(wbLinkText)));
	}
	
	public void waitForXpathPresent(String wbXpath)
	{
		WebDriverWait wait=new WebDriverWait(Driver.driver, 20);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(wbXpath)));
	}
	
	public boolean acceptAlert()
	{
		try
		{
			Alert alt=Driver.driver.switchTo().alert();
			System.out.println(alt.getText());
			alt.accept();
			flag=true;
		}
		catch(NoAlertPresentException ex)
		{
			
		}
		return flag;
	}
	
	public boolean dismissAlert()
	{
		try
		{
			Alert alt=Driver.driver.switchTo().alert();
			System.out.println(alt.getText());
			alt.dismiss();
			flag=true;
		}
		catch(NoAlertPresentException ex)
		{
			
		}
		return flag;
	}
	
	public List<WebElement> getRowData(String rowXpath)
	{
		List<WebElement> list=Driver.driver.findElements(By.xpath(rowXpath));
		return list;
	}
}

