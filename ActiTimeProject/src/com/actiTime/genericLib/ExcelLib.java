package com.actiTime.genericLib;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

public class ExcelLib {
	
	String excelPath="E:\\FrameWorkData\\TestData.xlsx";
	
	//Get Test Data From Excel Sheet
	public String getExcelData(String sheetName, int rowNum, int columnNum) throws InvalidFormatException, IOException
	{
		FileInputStream fileIn =new FileInputStream(excelPath);
		Workbook wrkbk=WorkbookFactory.create(fileIn);
		Sheet sh=wrkbk.getSheet(sheetName);
		Row row=sh.getRow(rowNum);
		String data=row.getCell(columnNum).getStringCellValue();
		return data;
		
	}
	
	//Write Result Back To Excel Sheet
	public void setExcelData(String sheetName, int rowNum, int columnNum, String StringData) throws InvalidFormatException, IOException
	{
		FileInputStream fileIn =new FileInputStream(excelPath);
		Workbook wrkbk=WorkbookFactory.create(fileIn);
		Sheet sh=wrkbk.getSheet(sheetName);
		Row row=sh.getRow(rowNum);
		Cell cell=row.createCell(columnNum);
		cell.setCellType(cell.CELL_TYPE_STRING);
		cell.setCellValue(StringData);
		
		FileOutputStream fileOut=new FileOutputStream(excelPath);
		wrkbk.write(fileOut);
	
	}
	
	//Get Row Count
	public int getRowCount(String sheetName) throws InvalidFormatException, IOException
	{
		FileInputStream fileIn =new FileInputStream(excelPath);
		Workbook wrkbk=WorkbookFactory.create(fileIn);
		Sheet sh=wrkbk.getSheet(sheetName);
		int rowCount=sh.getLastRowNum();
		return rowCount;
	}
}
