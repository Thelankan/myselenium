package com.actiTime.ProjectAndTasksTest;

import java.io.IOException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.actiTime.businessLib.ProjectAndTasksLib;
import com.actiTime.genericLib.Driver;
import com.actiTime.genericLib.ExcelLib;
import com.actiTime.genericLib.WebDriverCommonUtil;
import com.actiTime.objectRepository.EditCustomerInformationPage;

public class ProjectAndTasksTest {
  
	
	//Object initialization
		ExcelLib eLib=new ExcelLib();
		WebDriverCommonUtil wLib=new WebDriverCommonUtil();
		ProjectAndTasksLib bLib=new ProjectAndTasksLib();
		EditCustomerInformationPage editCustomerInformationPage=PageFactory.initElements(Driver.driver, EditCustomerInformationPage.class);
	
	
	 @Test(groups={"SmokeTest"})
  public void createCustomerAndVerifyDetailsTest() throws InvalidFormatException, IOException {
		//Test Data
			String userName=eLib.getExcelData("ProjectAndTaskTestData",1 ,2);
			String password=eLib.getExcelData("ProjectAndTaskTestData",1 ,3);
			String customerName=eLib.getExcelData("ProjectAndTaskTestData",1 ,4);
			
			
			//Step 1:Login To Application
			bLib.loginToAppliction(userName, password);
			wLib.waitForPageToLoad();
			
			//Step 2:Navigate to Project And Customer Page
			bLib.navigateToProjectAndCustomerPage();
			wLib.waitForPageToLoad();
			
			//Step 3:Create Customer
			bLib.createCustomer(customerName);
			wLib.waitForPageToLoad();
			
			//Step 4:Verify Customer Details
			Driver.driver.findElement(By.linkText(customerName)).click();
			wLib.waitForPageToLoad();
			String actualCustomer=editCustomerInformationPage.getCustomerNameInformation().getText();
			Assert.assertEquals(actualCustomer, customerName,"Customer Name Not matched");
			
			//Step 5:Logout
			bLib.logout();
	  
  }
	
	@Test
	  public void modifyCustomerAndVerifyDetailsTest() throws InvalidFormatException, IOException {
		
		//Test Data
		String userName=eLib.getExcelData("ProjectAndTaskTestData",2 ,2);
		String password=eLib.getExcelData("ProjectAndTaskTestData",2,3);
		String customerName=eLib.getExcelData("ProjectAndTaskTestData",2,4);
		String modifiedCustomerName=eLib.getExcelData("ProjectAndTaskTestData",2,5);
		
		//Step 1:Login To Application
		bLib.loginToAppliction(userName, password);
		wLib.waitForPageToLoad();
		
		//Step 2:Navigate to Project And Customer Page
		bLib.navigateToProjectAndCustomerPage();
		wLib.waitForPageToLoad();
		
		//Step 3:Create Customer
		bLib.createCustomer(customerName);
		wLib.waitForPageToLoad();
		
		//Step 4:Modify Customer 
		Driver.driver.findElement(By.linkText(customerName)).click();
		wLib.waitForPageToLoad();
		editCustomerInformationPage.getCustomerNameEditBox().clear();
		editCustomerInformationPage.getCustomerNameEditBox().sendKeys(modifiedCustomerName);
		editCustomerInformationPage.getSaveChangesBtn().click();
		wLib.waitForPageToLoad();
		
		//Step 5:Verify Customer Details
		Driver.driver.findElement(By.linkText(modifiedCustomerName)).click();
		wLib.waitForPageToLoad();
		String actualCustomer=editCustomerInformationPage.getCustomerNameInformation().getText();
		Assert.assertEquals(actualCustomer, modifiedCustomerName,"Customer Name Not matched");
		
		//Step 6:Logout
		bLib.logout();
  
		  
	  }
	
	@Test
	  public void deleteCustomeTest() throws InvalidFormatException, IOException {
		
		//Test Data
		String userName=eLib.getExcelData("ProjectAndTaskTestData",3 ,2);
		String password=eLib.getExcelData("ProjectAndTaskTestData",3,3);
		String customerName=eLib.getExcelData("ProjectAndTaskTestData",3,4);
		
		
		//Step 1:Login To Application
		bLib.loginToAppliction(userName, password);
		wLib.waitForPageToLoad();
		
		//Step 2:Navigate to Project And Customer Page
		bLib.navigateToProjectAndCustomerPage();
		wLib.waitForPageToLoad();
		
		//Step 3:Create Customer
		bLib.createCustomer(customerName);
		wLib.waitForPageToLoad();
		
		//Step 4:Delete Customer
		Driver.driver.findElement(By.linkText(customerName)).click();
		wLib.waitForPageToLoad();
		editCustomerInformationPage.getDeleteThisCustomerBtn().click();
		wLib.acceptAlert();
		
		//Step 5:Logout
		bLib.logout();
		  
	  }
}
