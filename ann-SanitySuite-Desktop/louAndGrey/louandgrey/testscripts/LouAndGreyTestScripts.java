package louandgrey.testscripts;

import java.lang.reflect.Method;
import  java.util.Collections;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.ascena.extentreport.ExtentTestManager;
import com.ascena.utilities.Utils;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

import louandgrey.pageobjects.CartPage;
import louandgrey.pageobjects.HomePage;
import louandgrey.pageobjects.MyAccountPage;
import louandgrey.pageobjects.PLPPage;
import louandgrey.pageobjects.PaymentPage;
import louandgrey.pageobjects.ReviewOrderPage;
import louandgrey.pageobjects.SearchResultPage;
import louandgrey.pageobjects.ShippingPage;
import louandgrey.pageobjects.ThankYouPage;


public class LouAndGreyTestScripts extends Utils {
	WebDriver driver;
	TestSoftAssert softAssert;
	HomePage homepage;
	PLPPage plp;
	CartPage cart;
	ShippingPage ship;
	PaymentPage payment;
	SearchResultPage srpage;
	ReviewOrderPage review;
	ThankYouPage thanks;
	MyAccountPage profile;
	ExtentTest test;
	public static final String searchkeyword="searchkeyword";
	
	@Parameters("brand")
	@BeforeMethod
	public void beforeMethod(String brand, Method method) {
		driver = invokeBrowser(getURL(env));
		ExtentTestManager.createTest(method.getName(), method.getAnnotation(Test.class).testName());
		ExtentTestManager.log("Test started on " + brand+" "+env);
		homepage = new HomePage(driver);
		plp = new PLPPage(driver);
		ship = new ShippingPage(driver);
		cart = new CartPage(driver);
		payment = new PaymentPage(driver);
		review = new ReviewOrderPage(driver);
		thanks = new ThankYouPage(driver);
		profile = new MyAccountPage(driver);
		srpage= new SearchResultPage(driver);
		softAssert = new TestSoftAssert();
		commFunc = new CommonFunctions(driver);
	}

	
	@Test(testName = "Guest checkout with quick shop and create account while placing order", enabled=true)
	public void verifyGuestCheckOutPlaceOrderCreateAccount()
	{	
		String randomEmail=commFunc.generateRandomEmail();
		
		homepage.clickClothingSubLink();
		
		/**Validate the Category of Launched Page**/
		softAssert.softAssertTrue(plp.getCurrentPageCategory().equalsIgnoreCase("New Arrivals - Clothing"), 
				"Validation : Launched Page is not the 'New Arrivals' Page.");
	
		String prdctName = plp.getFirstProductName();
		
		plp.clickSizeQS().
		selectInStockSizeQS().clickAddToBagQS();
		homepage.clickShopBag();

		/**Validate the count of added item count which is 1 from Shopping Page visible items count **/
		softAssert.softAssertEquals(cart.totalItemsInCart(), 1, 
				"Validation : Added Items and visible items count not matched in Cart.");

		/**Validate the Name of Added single item from Shopping Page**/
		softAssert.softAssertTrue(cart.getItemName(1).equalsIgnoreCase(prdctName), 
				"Validation : Added Product and visible product not matched in Cart");
		
		cart.clickProceedToCheckout().enterEmailGuestChkout(randomEmail).clickContinueGuestCheckout()
		.enterShippingDetails().clickContinueToPayment();
		
		String cardType = payment.getCardTypeGuestUser();

		payment.enterGuestUserCreditCardData(cardType).clickReviewOrder().enterPasswordAndConfirm();
		
		if (!env.equalsIgnoreCase("prod"))
		{
			review.clickPlaceOrder();
			
			/**Validate the email showing correctly on Thank you Page**/
			softAssert.assertTrue(thanks.getEmail().equals(randomEmail),
					"Validation : Given email for Guest and email on Thank you Page not matched");
			
		}
	}
	
	@Test(testName = "SortBy Price Low to High and Filter for Size and Color", enabled=true)
	public void verifyFiltersAndSortByPLP()
	{			
		homepage.clickClothingSubLink().selectSortByPriceLowToHigh();
		
		/**Validate Sort By 'Price Low to High' is selected on page**/
		softAssert.softAssertTrue(plp.getAppliedSortingName().contains("PRICE LOW"),
				"Validation : Sort By 'Price Low to High' not Selected.");
		
		List<String> webProductsPriceList = plp.getPricesOfAllProducts();
		List<String> expectedSortedPriceList = webProductsPriceList;
		Collections.sort(expectedSortedPriceList);
		
		/**Validate Sort By 'Price Low to High' is applied on products**/
		softAssert.softAssertTrue(webProductsPriceList.equals(expectedSortedPriceList),
				"Validation : Sort By 'Price Low to High' not Applied on Products.");
				
		plp.removeAppliedSorting().clickSizeFilter().selectAnySizeToFilter().clickApplySizeFilter();

		/**Validate Size Filter applied on page**/
		softAssert.softAssertTrue(plp.appliedFilter.isDisplayed(),
				"Validation : Size Filter not Applied.");

		plp.clickClearAllFilter().clickColorFilter().selectAnyColorToFilter().clickApplyColorFilter();
		
		/**Validate Color Filter applied on page**/
		softAssert.softAssertTrue(plp.appliedFilter.isDisplayed(),
				"Validation : Color Filter not Applied.");
	}
	
	@Test(testName = "SortBy Price Low to High and Filter for Size and Color", enabled=true)
	public void verifySearchByKeyword()
	{
		String keyword=prop.getProperty(searchkeyword);
		homepage.searchByKeyword(keyword);
		
		/**Validate the Category of Launched Page**/
		softAssert.softAssertTrue(srpage.getCurrentPageCategory().equalsIgnoreCase("Search results"), 
				"Validation : Launched Page is not the 'Search Result' Page.");
		
		/**Validate the name of first product has searched keyword **/
		softAssert.softAssertTrue(srpage.getFirstProductName().contains(keyword.toLowerCase()), 
				"Validation : Searched products are not correct as per the searched keyword.");
		
	}
	

	// PayPal express checkout start
		@Test(testName = "Registered account express place order using Paypal account", enabled = true)
		public void VerifyPaypalCheckOutPlaceOrderExpress() 
		{
			homepage.loginWithRegUser();
			commFunc.waitForSeconds(3);
			homepage.clickShopBag();
			cart.clearShoppingBag();
			String keyword = prop.getProperty("searchkeyword");
			homepage.searchByKeyword(keyword);
			
			plp.clickFirstProduct().selectInStockSize().clickAddToBag();
			homepage.clickShopBag().clickPayPalCheckout();

			String ecomSiteWindow = commFunc.getWindowId();
			payment.navigateToPaypalWindow();

			/** Verify the PayPal window opended and PayPal logo is visible */
			softAssert.softAssertTrue(payment.isPaypalWindowWithLogoDisplayed(),
					"Validation : PayPal Login window not Launched in "+env+" environment");

			if (!env.equalsIgnoreCase("prod")) {
				
				payment.loginToPayPalAccount().
				clickPayPalContinueBtn();
				commFunc.switchToWindow(ecomSiteWindow);
				review.clickPlaceOrder();

				/** Validate the email showing correctly on Thank you Page **/
				ExtentTestManager.getTest().log(Status.PASS,
						"Order Number " + thanks.getOrderId() + " placed successfully.");
			}
		}	


		@Test(testName = "Registered account regular checkout place order using Paypal account", enabled = true)
		public void VerifyPaypalCheckOutPlaceOrderRegular()
		{
			homepage.loginWithRegUser();
			//profile.removeAllCreditCards();
			homepage.clickShopBag();
			cart.clearShoppingBag();
			
			String keyword = prop.getProperty("searchkeyword");
			homepage.searchByKeyword(keyword);

			plp.clickFirstProduct().selectInStockSize().clickAddToBag();
			homepage.clickShopBag();

			cart.clickProceedToCheckout();
			
			review.paypalCheckoutReg();
	
			String ecomSiteWindow = commFunc.getWindowId();
			payment.navigateToPaypalWindow();
			
			/** Verify the PayPal window opended and PayPal logo is visible */
			softAssert.softAssertTrue(payment.isPaypalWindowWithLogoDisplayed(),
					"Validation : PayPal Login window not Launched in "+env+" environment");

			if (!env.equalsIgnoreCase("prod")) {
				
				payment.loginToPayPalAccount().
				clickPayPalContinueBtn();
				commFunc.switchToWindow(ecomSiteWindow);
				review.clickPlaceOrder();

				/** Validate the email showing correctly on Thank you Page **/
				ExtentTestManager.getTest().log(Status.PASS,
						"Order Number " + thanks.getOrderId() + " placed successfully.");
			}
			
		}
		
	@Test(testName = "Registerd User Order Submission  ", enabled = true)
	public void verifyRegCheckOutPlaceOrder() {

		homepage.loginWithRegUser();
		profile.removeAllCreditCards();
		homepage.clickShopBag();
		cart.clearShoppingBag();


		String keyword = prop.getProperty(searchkeyword);
		homepage.searchByKeyword(keyword);
		
		plp.clickFirstProduct().selectInStockSize().clickAddToBag();
		homepage.clickShopBag();

		cart.clickProceedToCheckout();

		String cardType = payment.getCardTypeRegUser();
		payment.enterRegUserCreditCardData(cardType).clickReviewOrder()
		.enterCVV();

		/** Validate the visibility of Place Order button on WebPage */
		softAssert.softAssertTrue(commFunc.isDisplayed(review.placeOrderBtn, 3),
				"Validation : Place Order Buton Displayed");

		if (!env.equalsIgnoreCase("prod")) {
			review.clickPlaceOrder();
			ExtentTestManager.getTest().log(Status.PASS,
					"Order Number '" + thanks.getOrderId() + "' placed successfully.");
		}
	}
	
	@AfterMethod
	public void afterMethod() {
		try{
			driver.quit();
			}
			catch(AssertionError e)	{
				throw e;
			}
	}

}
