package louandgrey.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.ascena.extentreport.ExtentTestManager;
import com.ascena.utilities.Utils;
import com.aventstack.extentreports.ExtentTest;

public class ThankYouPage extends Utils {

	WebDriver driver;
	ExtentTest test;
	TestSoftAssert softAssert;
	
	public ThankYouPage(WebDriver driver) {
		this.test = ExtentTestManager.getTest();
		this.driver = driver;
		PageFactory.initElements(driver, this);
		commFunc = new CommonFunctions(driver);
	}
	
	String Thanksmessage="Thank you for your order.";
	
	@FindBy(css = "#thankyou-email")
	public WebElement userEmail;
	
	@FindBy(css = "#thankyou-orderid")
	public WebElement orderId;
	
	@FindBy(xpath = "//*[@class='confirmation component']/h1")
	public WebElement thankYouMesage;
	
	/*Getting placed orderId from Thank you Page*/
	public String getOrderId()
	{
		return commFunc.getTextWhenReady(orderId,5);
	}
	
	/*Getting user email from Thank you Page*/
	public String getEmail()
	{
		return commFunc.getTextWhenReady(userEmail, 5);
	}
	
}
