package louandgrey.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.asserts.SoftAssert;

import com.ascena.extentreport.ExtentTestManager;
import com.ascena.utilities.ExcelUtils;
import com.ascena.utilities.Utils;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;




	

public class ReviewOrderPage extends Utils{
	
	WebDriver driver;
	ExtentTest test;
	SoftAssert softassert;
	TestSoftAssert softAssert;
	
	public ReviewOrderPage(WebDriver driver) {
		this.test = ExtentTestManager.getTest();
		this.driver = driver;
		PageFactory.initElements(driver, this);
		commFunc = new CommonFunctions(driver);
	}
	
	@FindBy(css = "#billing-cvv")
	public WebElement cvvBox;
	
	@FindBy(css = "#registerPassword")
	public WebElement createPwdBox;
	
	@FindBy(css = "[name='confirmPassword']")
	public WebElement confirmPwdBox;
	
	@FindBy(xpath = "//*[@class='summary billing-summary component']/div//a[contains(text(),'Add New')]")
	public WebElement clickAddNewCC;
	
	@FindBy(xpath = "//*[@class='summary billing-summary component']/div//a[contains(text(),'edit')]")
	public WebElement clickEditCC;
	
	@FindBy(css = "#order-confirmation-continue")
	public WebElement placeOrderBtn;
	
	//Paypal button
	
		@FindBy(xpath = "(//*[@class='custom-radio'])[1]")
		public WebElement paypalButton;
		
		@FindBy(css = "#paypal-button")
		public WebElement paypalcheckoutButton;
		
		public ReviewOrderPage paypalCheckoutReg()
		{
			if(commFunc.isElementPresent(driver, clickEditCC))
			{
				clickEditCC.click();

			}
			
					paypalButton.click();
					//commFunc.waitForSeconds(4);
			//paypalcheckoutButton.click();
			
			commFunc.clickWhenReady(paypalcheckoutButton, 5);
					
			return this;
		}
		
		//Paypal button end
	
	/*Clicking Place Order Button*/
	public ReviewOrderPage clickPlaceOrder()
	{
		try {
			commFunc.clickWhenReady(placeOrderBtn, 6);	
			test.log(Status.PASS, "Place Order button on review page clicked successfully.");
		} catch (Exception e) {
			test.log(Status.FAIL, "Clicking Place Order button on review page Failed.");
			throw e;
		}
		return this;	
	}
	
	/*Ennter password and confirm the same to create the account*/
	public ReviewOrderPage enterPasswordAndConfirm()
	{
		enterPasswordCreateAccount().confirmPasswordCreateAccount();
		return this;	
	}
	
	/*Entering HarCode Password as Ascena@123 in Create Password input box*/
	public ReviewOrderPage enterPasswordCreateAccount()
	{
		try {
			commFunc.sendWhenReady(createPwdBox, "Ascena@123", 5);
			test.log(Status.PASS, "Entering password to create account.");
		} catch (Exception e) {
			test.log(Status.FAIL, "Entering password to create account Failed.");
			throw e;
		}
		return this;	
	}
	
	/*Confirming HarCode Password as Ascena@123 in Confirm Password input box*/
	public ReviewOrderPage confirmPasswordCreateAccount()
	{
		try {
			commFunc.sendWhenReady(confirmPwdBox, "Ascena@123", 5);
			test.log(Status.PASS, "Entering password again to confirm and create account.");
		} catch (Exception e) {
			test.log(Status.FAIL, "Entering password for Confirmation Failed.");
			throw e;
		}
		return this;	
	}
	
	public ReviewOrderPage addNewCard()
	{
		try {
			clickAddNewCC.click();
			
			test.log(Status.PASS, "Add New card on review page clicked successfully.");
		} catch (Exception e) {
			test.log(Status.FAIL, "Clicking Add New link on review page failed.");
			throw e;
		}
		return this;	
	}
	public ReviewOrderPage editCard()
	{
		try {
			clickEditCC.click();
			
			test.log(Status.PASS, "EDIT CC details on review page clicked successfully.");
		} catch (Exception e) {
			test.log(Status.FAIL, "Clicking EDIT link on review page failed.");
			throw e;
		}
		return this;	
	}
	
	/*Entering CVV as 123 hardcode in Review order page for existing card*/
	public ReviewOrderPage enterCVV()
	{
		try {
			commFunc.sendWhenReady(cvvBox, "123", 5);
			test.log(Status.PASS, "Entered CVV Number successfully for already added card");
		} catch (Exception e) {
			test.log(Status.FAIL, "Entering CVV number for already added card Failed.");
			throw e;
		}
		return this;		
	}
	
	
}