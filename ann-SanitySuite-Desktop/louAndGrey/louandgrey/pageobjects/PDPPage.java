package louandgrey.pageobjects;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.asserts.SoftAssert;

import com.ascena.extentreport.ExtentTestManager;
import com.ascena.utilities.Utils;
import com.ascena.utilities.Utils.CommonFunctions;
import com.ascena.utilities.Utils.TestSoftAssert;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;


public class PDPPage extends Utils
{
	
	WebDriver driver;
	ExtentTest test;
	SoftAssert softassert;
	TestSoftAssert softAssert;
	
	public PDPPage(WebDriver driver) {
		this.test = ExtentTestManager.getTest();
		this.driver = driver;
		PageFactory.initElements(driver, this);
		commFunc = new CommonFunctions(driver);
	}
	
	//WebElements for bread crum on PDP for L2 PDP
	@FindBy(xpath = "(//span [@itemprop='name'])[3]")
	public WebElement bread_crumL2;
	
	//WebElements fpr breadcrum on PDP for L3 PDP
		@FindBy(xpath = "(//span [@itemprop='name'])[4]")
		public WebElement bread_crumL3;
	
	// Web Element for Product title on PDP Page
		@FindBy(xpath = "//h1[@itemprop='name']")
		public WebElement product_Title;

	// Web Element for Product Image on PDP Page
		@FindBy(css = "span.is-ready")
		public WebElement pDP_ProdImage;
		
	//WebElements for write review link  on PDP
		@FindBy(xpath = "//*[@class='bv_button_buttonMinimalist']")
		public WebElement writereview;	
		
	//WebElements for review count on PDP
		@FindBy(xpath = "//*[@class='bv_numReviews_text']")
		public WebElement reviewcount;

	//WebElements for promo msg on PDP
		@FindBy(xpath = "//*[@class='promos']")
		public WebElement Promomsg;		
		//div[2]/section/div[2] --sakshi
		
		//WebElements for FP on PDP
		@FindBy(xpath = "(//*[@class='price'])[1]")
		public WebElement FP;	
		
		//WebElements for NFP on PDP
		@FindBy(xpath = "(//*[@class='price sale'])[1]")
		public WebElement NFP;
	
		// Web Element for Product Size on PDP Page
		@FindBy(xpath = "(//*[@class='submit mouse-focus-input'])")
		public WebElement product_Size;

		// Web Element for Product colour on PDP Page
		@FindBy(xpath = "//*[@class='colors']/div/a")
		public WebElement product_colour;

		// Web Element for Product Add to Bag Button on PDP Page
		@FindBy(id = "pdp-add-to-bag")
		public WebElement add_ToBag_Button;

		// Web Element for Pick up store Button on PDP Page
		@FindBy(xpath = "(//*[@class='cnt-pick-up mouse-focus-input'])")
		public WebElement storebutton;
		
		// Web Element for Product Add to Bag Button on PDP Page
		@FindBy(xpath = "(//*[@class='selected error'])")
		public WebElement errorMsgSelectSizeColour;

		
		// Web Element for Enabled Product Size on PDP Page
		@FindBy(xpath = "(//*[@class='wishlist-add'])")
		public WebElement WishList;

		//**********************************************************
		// WebElement for product colours
		@FindBy(xpath = "//input[contains(@id,'pdp-color')]/following-sibling::label")
		public List<WebElement> productcolours;
		//
		// WebElement for product colours status
		@FindBy(xpath = "//input[contains(@id,'pdp-color')]")
		public List<WebElement> prodcolourstatus;

		// WebElement for product size
		@FindBy(xpath = "//input[contains(@data-option-key,'size')]/following-sibling::label")
		public List<WebElement> productsize;

		// WebElement for product sizestatus
		@FindBy(xpath = "//input[contains(@data-option-key,'size')]")
		public List<WebElement> productsizestatus;

		// Web Element for product image in CLP
		@FindBy(xpath = "//div[@class='mar-product-item-image-holder']")
		public WebElement productimg;

		// Web Element for addToFav_Link
		@FindBy(xpath = "//a[contains(@class,'cmn-add-to-favs asc-add-to-favs-btn')]")
		public WebElement addToFav_Link;

		// Web Element for addedToFav_Link
		@FindBy(xpath = "//a[contains(@class,'cmn-add-to-favs asc-add-to-favs-btn active')]")
		public WebElement addedToFav_Link;
		
		// Web Element for Favorites Header at Shopping Bag
		@FindBy(xpath = "//h2[@class='mar-module-title h3 gamma']")
		public WebElement faveHeader;
		
		// Web Element for Favorites Product Title at Shopping Bag
		@FindBy(xpath = "//h3[@class='product-title']")
		public WebElement faveProductTitle;
		
		// Web Element for Favorites Product Price at Shopping Bag
		@FindBy(xpath = "//span[@class='mar-price mar-sale-price']")
		public WebElement faveProductPrice;
		
		// Web Element for Favorites Product Move To Cart Button at Shopping Bag
		@FindBy(xpath = "//a[@class='mar-quickview-move-btn cmn-move-cart-btn']")
		public WebElement faveProductMoveToCart;
		
		// Web Element for Favorites Product Image at Shopping Bag
		@FindBy(xpath = "//img[contains(@src,'mauricesprodatg.scene7.com')]")
		public WebElement faveProductImg;

		// Web Element for Favorites Product Modal header at Shopping Bag
		@FindBy(xpath = "//span[@class='h1 mar-product-title']")
		public WebElement faveProductModalHeader;
		
		// Web Element for Favorites Product Modal header at Shopping Bag
		@FindBy(xpath = "//button[@class='modal-close mar-icon icon-x']")
		public WebElement faveProductModalClose;
		
		// Web Element for go to Favorites Link at Shopping Bag
		@FindBy(xpath = "//a[@class='mar-module-header-link btn mar-btn inverted']")
		public WebElement goToMyFaveLink;
		
		// Web Element for My Favorites Page Header 
		@FindBy(xpath = "//h1[@class='cmn-module-title h3 gamma']")
		public WebElement myFaveHeader;
		
		// Web Element for My Favorites Page Product Title
		@FindBy(xpath = "//h2[@class='product-title three-quarters xs-one-whole h5']")
		public WebElement myFaveProductName;
		
		// Web Element for My Favorites Page Product Price 
		@FindBy(xpath = "//*[@id='cmn-account-favorites']//span[@class='mar-price mar-sale-price']")
		public WebElement myFaveProductPrice;

		// Web Element for My Favorites Page Remove Button
		@FindBy(xpath = "//*[@id='cmn-account-favorites']//button[text()='Remove']")
		public WebElement myFaveProductRemove;
		
		// Web Element for My Favorites Page ShareList Link 
		@FindBy(xpath = "//a[@title='Share List']")
		public WebElement myFaveShareList;
		
		// Web Element for My Favorites Page Product Image 
		@FindBy(xpath = "//*[@id='cmn-account-favorites']//img")
		public WebElement myFaveProductImage;
		
		// Web Element for My Favorites Page Move to Cart Button
		@FindBy(xpath = "//*[@id='cmn-account-favorites']//button[text()='Move to Cart']")
		public WebElement myFaveProductMovetoCart;
		
		// Web Element for My Favorites Page Product Promotional Msg
		@FindBy(xpath = "//*[@class='cmn-promo-message']")
		public WebElement myFaveProductPromotionalMsg;
		
		//WebElement for product title of Favorite
		@FindBy(xpath="//div[@class='mar-cart-item-title']/h2/a")
		public WebElement favProductTitle;
		
		//Web Element for firstprod_title
		@FindBy(xpath = "(//h1[@class='mar-product-title'])[2]")
		public WebElement firstprod_title;
			
		// Web Element for My Favorites Page Remove Button
		@FindBy(xpath = "//*[@id='cmn-account-favorites']//button[text()='Remove']")
		public List<WebElement> removefav;
		
		//WebElement for remove favorites
		@FindBy(xpath="//button[@class='btn invert btn--small mar-btn-half asc-remove-btn']")
		public WebElement removefavlink;
		
		// WebElement for remove button favorites
		@FindBy(xpath="//button[@class='cta-btn btn btn--full asc-remove-conf-btn']")
		public WebElement removebtn;
		
		/*
		 * Web Element for Product_Price
		 */
		@FindBy(xpath = "//div[@class='title-price-wrapper-mobile']/div[2]/p/span")
		public WebElement productPrice;

		/*
		 * Web Element for writeaReview_Link
		 */
		@FindBy(xpath = "//button[contains(@class,'bv-write-review')]")
		public WebElement writeaReview_Link;

		/*
		 * Web Element for selectSize_dropdown
		 */
		@FindBy(xpath = "//select[contains(@class,'mar-product-sizes')]")
		public WebElement selectSize_dropdown;

		/*
		 * Web Element for quantityTextBox
		 */
		@FindBy(xpath = "//input[@id='mar-item-qty']")
		public WebElement quantityTextBox;

		/*
		 * Web Element for quantiyPlusIcon
		 */
		@FindBy(xpath = "//button[@class='increase']")
		public WebElement quantiyPlusIcon;

		/*
		 * Web Element for quantiyMinusIcon
		 */
		@FindBy(xpath = "//button[@class='reduce']")
		public WebElement quantiyMinusIcon;

		/*
		 * Web Element for addToBagBtn
		 */

		@FindBy(xpath = "//div[contains(@class,'mar-product-actions')]/input")
		public WebElement addToBagBtn;

		/*
		 * Web Element for findInStore_Link
		 */
		@FindBy(xpath = "//a[contains(@class,'find-in-store-link enabled')]")
		public WebElement findInStore_Link;

		/*
		 * Web Element for overView_Link
		 */
		@FindBy(xpath = "//a[contains(text(),'Overview')]")
		public WebElement overView_Link;

		/*
		 * Web Element for care_Link
		 */
		@FindBy(xpath = "//a[contains(text(),'Care')]")
		public WebElement care_Link;

		/*
		 * Web Element for productDescription
		 */
		@FindBy(xpath = "//div[contains(@class,'mar-product-description-content')]")
		public WebElement productDescription;

		/*
		 * Web Element for seeDetails_Link
		 */
		@FindBy(xpath = "//a[contains(text(),'See Details')]")
		public WebElement seeDetails_Link;

		/*
		 * Web Element for showReview_Link
		 */
		@FindBy(xpath = "//a[@class='icon-plus']")
		public WebElement showReview_Link;

		/*
		 * Web Element for hideReview_Link
		 */
		@FindBy(xpath = "//a[@class='icon-minus']")
		public WebElement hideReview_Link;

		/*
		 * Web Element for beTheFirstToReview_Link
		 */
		@FindBy(xpath = "//a[contains(@class,'bv-write-review')]")
		public WebElement beTheFirstToReview_Link;

		/*
		 * Web Element for giftCardTitle
		 */
		@FindBy(xpath = "//*[@title='gift cards']")
		public WebElement giftCardTitle;

		/*
		 * Web Element for sizevalue
		 */
		@FindBy(xpath = "//option[@class='cmn-radio-container' and @data-size-value][1]")
		public WebElement sizeval;

		/*
		 * Web Element for sizevalue
		 */
		@FindBy(xpath = "//input[@id='mar-item-qty']")
		public WebElement itemquantity;

		/*
		 * Web Element for sizevalue
		 */
		@FindBy(xpath = "//label[text()='ITEM ADDED TO BAG']")
		public WebElement itemadddedmsg;

		/*
		 * Web Element for firstproduct
		 */
		@FindBy(xpath = "(//div[@class='mar-product-item-image-holder']/img)[1]")
		public WebElement firstproduct;

		/*
		 * Web Element for Facebook link in footer menu
		 */
		@FindBy(xpath = "//a[contains(@class,'mar-checkout-btn btn cta-btn') and text()='Checkout']")
		public WebElement checkoutpdp;

		/*
		 * Web Element for product title header text in Shopping cart
		 */
		@FindBy(xpath = "//h2[@class='product-title three-quarters h5']")
		public WebElement addbagitem;

		@FindBy (xpath = "//*[@name='size' and not(contains(@class,'disabled'))]")
		public List<WebElement> sizeQS;
		
		/*Selecting Random Size which is available for product*/
		public PDPPage selectInStockSize()
		{
			try {
				int randomInt=0;
				if(sizeQS.size()==1) {	sizeQS.get(0).click(); 	}
				else {
				  randomInt = commFunc.getRandomInteger(sizeQS.size()-1);
				  sizeQS.get(randomInt).click();
				}
				test.log(Status.PASS, "In-Stock Size selected successfully");
			} catch (Exception e) {
				test.log(Status.FAIL, "In-Stock Size selection Failed.");
				throw e;
			}
			return this;	
		}
		
		public PDPPage clickAddToBag()
		{
			try {
				commFunc.clickWhenReady(add_ToBag_Button, 6);
				commFunc.refresh();
				test.log(Status.PASS, "Add to Bag button in PDPPage clicked successfuly.");
			} catch (Exception e) {
				test.log(Status.FAIL, "Click on Add to Bag button in PDPPage Failed.");
				System.out.println(e);
				throw e;
			}
			return this;	
		}
}

