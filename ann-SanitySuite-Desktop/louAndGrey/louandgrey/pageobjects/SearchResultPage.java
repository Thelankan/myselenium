package louandgrey.pageobjects;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.asserts.SoftAssert;

import com.ascena.extentreport.ExtentTestManager;
import com.ascena.utilities.Utils;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

public class SearchResultPage extends Utils {
	
	WebDriver driver;
	ExtentTest test;
	SoftAssert softassert;
	TestSoftAssert softAssert;

	public SearchResultPage(WebDriver driver) {
		this.test = ExtentTestManager.getTest();
		this.driver = driver;
		PageFactory.initElements(driver, this);
		commFunc = new CommonFunctions(driver);
	}
	
	@FindBy(xpath = "//span[@itemprop='name']")
	public List<WebElement> breadcrumbList;
	
	@FindBy(xpath = "//strong[@itemprop='name']")
	public WebElement firstPrdctName;
	
	
	/*Getting Current Page Category*/
	public String getCurrentPageCategory()
	{
		int index = breadcrumbList.size()-1;
		return commFunc.getTextWhenReady(breadcrumbList.get(index), 5);
	}
	
	/*Getting First Product Name from searched results*/
	public String getFirstProductName()
	{
		String name=null;
		try {
			name= commFunc.getTextWhenReady(firstPrdctName,5);
			test.log(Status.PASS, "First Prduct name : "+name);
		} catch (Exception e) {
			test.log(Status.FAIL, "Getting First Product Name Failed.");
			System.out.println(e);
			throw e;
		}	
		return name.toLowerCase();
	}
	
	/*Getting First Product Name from searched results*/
	public boolean isSearchResultHasProduct()
	{
		try {
			int index = breadcrumbList.size()-1;
			return breadcrumbList.get(index).isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}
	
	

}
