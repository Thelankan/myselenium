package louandgrey.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.ascena.extentreport.ExtentTestManager;
import com.ascena.utilities.ExcelUtils;
import com.ascena.utilities.Utils;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

public class HomePage extends Utils {

	WebDriver driver;
	ExtentTest test;
	TestSoftAssert softAssert;

	public HomePage(WebDriver driver) {
		this.test = ExtentTestManager.getTest();
		this.driver = driver;
		PageFactory.initElements(driver, this);
		commFunc = new CommonFunctions(driver);
	}

	@FindBy(xpath = "//div[contains(@class,'login')]/a")
	public WebElement signInLink;
	
	@FindBy(css = "[class='login create']")
	public WebElement signInSubLink;
	
	@FindBy(css = "#email")
	public WebElement emailBox;
	
	@FindBy(css = "#login-password")
	public WebElement pwdBox;
	
	@FindBy(css = "#login-submit")
	public WebElement signInBtn;
	
	@FindBy(xpath = "//*[@id='main-nav']/descendant::a[@role='menuitem']")
	public WebElement newArrivalsL1Link;
	
	@FindBy(xpath = "//*[@class='sub-nav active']//a[@role='menuitem']")
	public WebElement clothingL2Link;
	
	@FindBy(css = "#my-bag-icon")
	public WebElement shopBag;
	
	@FindBy(id = "search-toggle")
	public WebElement searchLink;
	
	@FindBy(xpath = "//*[@id='endeca-search-bar-frame']/span/div/input")
	public WebElement searchBox;
	
	@FindBy(css = "[id='endeca-search-bar-frame'] a")
	public WebElement searchIconInBox;

	/*Clicking top right side SignIn link  */
	public HomePage clickSignInLink()
	{
		try {
			commFunc.hoverAndSelect(signInLink, signInSubLink);
			test.log(Status.PASS, "Hovering SignIn on Page Top and selecting SignIn Sub Link.");
		} catch (Exception e) {
			test.log(Status.FAIL, "Sign In Link on page top was not clicked.");
			throw e;
		}
		return this;	
	}
	
	/*Login in with registered user*/
	public HomePage loginWithRegUser()
	{
		int row = getCredentialsRow(brand);
		String email = ExcelUtils.getCellData("AnnTaylor_TestData", row, 3);
		String pwd = ExcelUtils.getCellData("AnnTaylor_TestData", row, 4);
		clickSignInLink().enterEmail(email).enterPassword(pwd).clickSignInBtn();

		return this;	
	}
	
	/*Getting Registered user credential row in Excel file according to the brand*/
	public int getCredentialsRow(String brand)
	{
		int rowNum = 0;
		switch(brand)
		{	
			case "AT" : rowNum= 1; break;
			case "LT" :	rowNum= 2; break;
			case "LG" : rowNum= 3; break;
		}
		return rowNum;
	}
	
	/*Entering email in login page*/
	public HomePage enterEmail(String email)
	{
		try {
			commFunc.sendWhenReady(emailBox, email, 5);
			test.log(Status.PASS, "Email '"+email+"' entered successfully.");
		} catch (Exception e) {
			test.log(Status.FAIL, "Email was not entered.");
			throw e;
		}
		return this;	
	}
	
	/*Entering password in login page*/
	public HomePage enterPassword(String pwd)
	{
		try {
			commFunc.sendWhenReady(pwdBox, pwd, 5);
			test.log(Status.PASS, "Password '"+pwd+"' entered successfully.");
		} catch (Exception e) {
			test.log(Status.FAIL, "Password was not entered.");
			throw e;
		}
		return this;	
	}
	
	/*Clicking SignIn button in login page*/
	public HomePage clickSignInBtn()
	{
		try {
			commFunc.clickWhenReady(signInBtn, 5);
			test.log(Status.PASS, "SignIn Button at login page clicked successfully.");
		} catch (Exception e) {
			test.log(Status.FAIL, "SignIn Button at login page was not clicked.");
			throw e;
		}
		return this;	
	}
	
	/*Hovering Clothing-L1 and clicking New-Arrivals-L2*/
	public PLPPage clickClothingSubLink()
	{
		try {
			commFunc.hoverAndSelect(newArrivalsL1Link,clothingL2Link);
			test.log(Status.PASS, "Hovering L1-Clothing and Selecting L2-New-Arrivals.");
		} catch (Exception e) {
			test.log(Status.FAIL, "New Arrivals was not clicked.");
			throw e;
			}
		return new PLPPage(driver);
	}
	
	/*Clicking on Shopping Bag*/
	public CartPage clickShopBag()
	{
		try {
			commFunc.clickWhenReady(shopBag, 7);
			test.log(Status.PASS, "Shopping Bag clicked successfully.");
		} catch (Exception e) {
			test.log(Status.FAIL, "Click to Shopping Bag Failed.");
			throw e;
			}
		return new CartPage(driver);
	}
	
	/*Clicking search link, enter keyword and search for the given keyword icon on top right corner on webpage  */
	public HomePage searchByKeyword(String keyword)
	{
		commFunc.waitForSeconds(2);
		clickSearchLink().enterKeywordToSearch(keyword).clickSearchIconInSearchBox();
		return this;	
	}
	
	/*Clicking on search link on top right corner on webpage */
	public HomePage clickSearchLink()
	{
		try {
			commFunc.clickWhenReady(searchLink, 3);
			test.log(Status.PASS, "Clicking on Search Link.");
		} catch (Exception e) {
			test.log(Status.FAIL, "Click on Search Link on top right corner Failed.");
			throw e;
		}
		return this;	
	}
	
	/*Entering keyword in Search box to search */
	public HomePage enterKeywordToSearch(String keyword)
	{
		try {
			commFunc.waitForSeconds(2);
			commFunc.sendWhenReady(searchBox, keyword, 6);
			test.log(Status.PASS, "Entered Text '"+keyword+"' in search box to search");
		} catch (Exception e) {
			test.log(Status.FAIL, "Entering Text '"+keyword+"' in search box Failed.");
			throw e;
		}
		return this;	
	}
	
	/*Clicking on search icon in search input box */
	public HomePage clickSearchIconInSearchBox()
	{
		try {
			commFunc.clickWhenReady(searchIconInBox, 4);
			test.log(Status.PASS, "Clicked on Search icon in Search Box Successfully");
		} catch (Exception e) {
			test.log(Status.FAIL, "Click on Search icon in Search Box Failed.");
			throw e;
		}
		return this;	
	}
	
}
