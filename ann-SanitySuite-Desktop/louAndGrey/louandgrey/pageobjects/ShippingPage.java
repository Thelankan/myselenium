package louandgrey.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.asserts.SoftAssert;

import com.ascena.extentreport.ExtentTestManager;
import com.ascena.utilities.ExcelUtils;
import com.ascena.utilities.Utils;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

public class ShippingPage extends Utils {

	WebDriver driver;
	ExtentTest test;
	SoftAssert softassert;
	TestSoftAssert softAssert;

	public ShippingPage(WebDriver driver) {
		this.test = ExtentTestManager.getTest();
		this.driver = driver;
		PageFactory.initElements(driver, this);
		commFunc = new CommonFunctions(driver);
	}

	@FindBy(css = "#checkout-field-firstname")
	public WebElement shipToFirstName;

	@FindBy(css = "#checkout-field-lastname")
	public WebElement shipToLastName;
	
	@FindBy(css = "#checkout-field-address1")
	public WebElement shipToAddress1;
	
	@FindBy(css = "#checkout-field-zipcode")
	public WebElement shipToZipcode;
	
	@FindBy(css = "#locality")
	public WebElement shipToCity;
	
	@FindBy(css = "[name='state']")
	public WebElement shipToState;
	
	@FindBy(css = "#billing-phonenumber")
	public WebElement shipToPhone;
	
	@FindBy(css = "#shipping-continue")
	public WebElement continueToPaymentBtn;
	
	
	/*Entering Details for Shipping Address*/
	public ShippingPage enterShippingDetails()
	{
		try {
			commFunc.sendWhenReady(shipToFirstName, ExcelUtils.getCellData("Address", 7, 1), 8);
			commFunc.sendWhenReady(shipToLastName, ExcelUtils.getCellData("Address", 7, 2), 5);
			commFunc.sendWhenReady(shipToAddress1, ExcelUtils.getCellData("Address", 7, 3), 5);
			commFunc.sendWhenReady(shipToZipcode, ExcelUtils.getCellData("Address", 7, 8), 5);
			commFunc.waitForSeconds(2);
			commFunc.sendWhenReady(shipToPhone, ExcelUtils.getCellData("Address", 7, 9), 5);
			test.log(Status.PASS, "Details entered for Shipping Address successfully");
		} catch (Exception e) {
			test.log(Status.FAIL, "Entering details for Shipping Address Failed");
			throw e;
			}
		return this;
	}
	
	/*Clicking on Continue to Payment Button of Shipping Page*/
	public PaymentPage clickContinueToPayment()
	{
		try {
			commFunc.clickWhenReady(continueToPaymentBtn,5);
			test.log(Status.PASS, "Clicked on 'Proceed to checkout' on Shopping Bag successfully.");
		} catch (Exception e) {
			test.log(Status.FAIL, "Click on 'Proceed to checkout' on Shopping Bag Failed.");
			throw e;
		}
		return new PaymentPage(driver);
	}
	
}
