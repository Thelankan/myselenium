package louandgrey.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
/**
 * Created by Amritha
 **/
public class AccountsPage {

	WebDriver driver;
	
	public AccountsPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	//left navigation starts
	
	@FindBy(xpath="//a[contains(text(),'Orders & Returns')]")
	public WebElement ordersAndReturnslink;
	
	@FindBy(xpath="//a[contains(text(),'Profiles & Preferences')]")
	public WebElement profilesAndPreferenceslink;
	
	@FindBy(xpath="//a[contains(text(),'Address Book')]")
	public WebElement addresslink;
	
	@FindBy(xpath="//a[contains(text(),'Payment Methods')]")
	public WebElement paymentMethodlink;
	
	
	@FindBy(xpath="//*[@class='wishlist-nav activated']")
	public WebElement wishlistlink;
	
	//left navigation ends
	
	@FindBy(xpath="//form//input[@name='number']")
	public WebElement orderSearch;
	
	@FindBy(xpath="//input[@value='GO']")
	public WebElement orderSearchGo;
	
	
	@FindBy(xpath="//a[contains(@class,'view profile-prefs')]")
	public WebElement viewlink;
	
	@FindBy(xpath="//a[contains(@class,'edit-email-password')]")
	public WebElement editEmailPasswordlink;
	
	@FindBy(xpath="//a[contains(@class,'edit-email-pref')]")
	public WebElement editEmaillink;
	
	@FindBy(xpath="//*[@id='password-edit-mode']/div/a")
	public WebElement editPasswordlink;
	
	@FindBy(xpath="//form//input[@name='newEmail']")
	public WebElement editEmail;
	
	@FindBy(xpath="//form//input[@name='confirmEmail']")
	public WebElement editConfirmEmail;
	
	//save email address
	@FindBy(xpath="//*[@id='email-edit-mode']/descendant::input[@name='save']")
	public WebElement saveEmail;
	
	@FindBy(xpath="//*[@id='email-edit-mode']/descendant::input[@name='cancel']")
	public WebElement cancelEmail;
	
	//save password
	@FindBy(xpath="//input[@name='currentPassword']")
	public WebElement editCurrentPassword;
	
	@FindBy(xpath="//input[@name='newPassword']")
	public WebElement editNewPassword;
	
	@FindBy(xpath="//input[@name='confirmNewPassword']")
	public WebElement editConfirmNewPassword;
	
		
	@FindBy(xpath="//*[@id='password-edit-mode']/descendant::input[@name='save']")
	public WebElement savePassword;
	
	@FindBy(xpath="//*[@id='password-edit-mode']/descendant::input[@name='cancel']")
	public WebElement cancelPassword;
	
	//Details of preferences
	
	@FindBy(xpath="//*[@id='FNAME']")
	public WebElement editFname;
	
	@FindBy(xpath="//*[@id='COUNTRY']")
	public WebElement editCountry;
	
	@FindBy(xpath="//*[@id='ZIP']")
	public WebElement editZipCode;
	
	@FindBy(xpath="//*[@id='bd0']")
	public WebElement birthdayMonth;
	
	@FindBy(xpath="//*[@id='bd1']")
	public WebElement birthdayDay;
	
	@FindBy(xpath="//*[@id='imageField']")
	public WebElement myPreferencesSave;
	
	//Add new address book
		
	@FindBy(xpath="//a[contains(text(),'Add New')]")
	public WebElement addressNewButton;
	
	@FindBy(xpath="//div[contains(@class,'slot')]//form[contains(@class,'address-book')]//input[@name='firstName']")
	public WebElement addressNewFName;
	
	@FindBy(xpath="//div[contains(@class,'slot')]//form[contains(@class,'address-book')]//input[@name='lastName']")
	public WebElement addressNewLName;
	
	@FindBy(xpath="//div[contains(@class,'slot')]//form[contains(@class,'address-book')]//input[@id='shipping-address-1']")
	public WebElement addressShippingAdd1;
	
	@FindBy(xpath="//div[contains(@class,'slot')]//form[contains(@class,'address-book')]//input[@id='shipping-zip-code']")
	public WebElement addressZipCode;
	
	@FindBy(xpath="//div[contains(@class,'slot')]//form[contains(@class,'address-book')]//input[@name='city']")
	public WebElement addressCity;
	
	@FindBy(xpath="//div[contains(@class,'slot')]//form[contains(@class,'address-book')]//*[@id='dd-label-3']")
	public WebElement addressState;
	
	@FindBy(xpath="//div[contains(@class,'slot')]//form[contains(@class,'address-book')]//input[@name='phone']")
	public WebElement addressPhone;
	
	
	@FindBy(xpath="//div[contains(@class,'slot')]//form[contains(@class,'address-book')]//div[contains(@class,'custom-checkbox mouse-focus-input')]//input[@type='checkbox']")
	public WebElement addressDefault;
	
	@FindBy(xpath="//div[contains(@class,'slot')]//form[contains(@class,'address-book')]//input[@name='submit']")
	public WebElement addressSave;
	
	//Add Payment 
	
	
	
	@FindBy(xpath="//form[contains(@class,'address-book wallet')]//a[contains(text(),'Add New')]")
	public WebElement paymentMethods_New;
	
	

	@FindBy(xpath="//form//input[@id='cardNumber']")
	public WebElement newCardNumber;
	
	
	@FindBy(xpath="//select[@id='exp_month']")
	public WebElement newCardMonth;
	
	@FindBy(xpath="//select[@id='exp_year']")
	public WebElement newCardYear;
	
	//Payment method billing address
	
	@FindBy(xpath="//form//input[@id='billingFirstName']")
	public WebElement billingFname;
	
	@FindBy(xpath="//form//input[@id='billingLastName']")
	public WebElement billingLname;
	
	
	@FindBy(xpath="//form//input[@id='billingAddress']")
	public WebElement billingAddress;
	

	@FindBy(xpath="//form//input[@id='billingZip']")
	public WebElement billingZip;
	
	@FindBy(xpath="//form//input[@id='billingCity']")
	public WebElement billingCity;
	
	
	//select state for billing
	@FindBy(xpath="//select[@id='billingState']")
	public WebElement billingState;
	
	
	@FindBy(xpath="//form//input[@id='billingPhone']")
	public WebElement billingPhone;
	
	@FindBy(xpath="//form//input[@id='billing-btn-revieworder']")
	public WebElement billingSave;	
	
	
}
