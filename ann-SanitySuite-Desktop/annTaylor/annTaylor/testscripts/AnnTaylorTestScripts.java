package annTaylor.testscripts;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.ascena.database.DBUtils;
import com.ascena.extentreport.ExtentTestManager;
import com.ascena.utilities.Utils;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

import annTaylor.pageobjects.CartPage;
import annTaylor.pageobjects.HomePage;
import annTaylor.pageobjects.MyAccountPage;
import annTaylor.pageobjects.PDPPage;
import annTaylor.pageobjects.PLPPage;
import annTaylor.pageobjects.PaymentPage;
import annTaylor.pageobjects.ReviewOrderPage;
import annTaylor.pageobjects.SearchResultPage;
import annTaylor.pageobjects.ShippingPage;
import annTaylor.pageobjects.ShopRunnerPage;
import annTaylor.pageobjects.ThankYouPage;
import edu.emory.mathcs.backport.java.util.Collections;

public class AnnTaylorTestScripts extends Utils {
	WebDriver driver;
	TestSoftAssert softAssert;
	HomePage homepage;
	PLPPage plp;
	PDPPage pdp;
	CartPage cart;
	ShippingPage ship;
	PaymentPage payment;
	SearchResultPage srpage;
	ReviewOrderPage review;
	ThankYouPage thanks;
	ExtentTest test;
	MyAccountPage profile;
	ShopRunnerPage shoprunner;

	@Parameters("brand")
	@BeforeMethod
	public void beforeMethod(String brand, Method method) {
		driver = invokeBrowser(getURL(env));
		ExtentTestManager.createTest(method.getName(), method.getAnnotation(Test.class).testName());
		ExtentTestManager.log("Test started on " + brand + " " + env);
		homepage = new HomePage(driver);
		plp = new PLPPage(driver);
		pdp = new PDPPage(driver);
		ship = new ShippingPage(driver);
		cart = new CartPage(driver);
		payment = new PaymentPage(driver);
		review = new ReviewOrderPage(driver);
		thanks = new ThankYouPage(driver);
		profile = new MyAccountPage(driver);
		srpage = new SearchResultPage(driver);
		softAssert = new TestSoftAssert();
		commFunc = new CommonFunctions(driver);
		shoprunner = new ShopRunnerPage(driver);
	}

	// S!D
	@Test(testName = "Guest checkout with quick shop and create account while placing order", enabled = true)
	public void verifyGuestCheckOutPlaceOrderCreateAccount() {
		String randomEmail = commFunc.generateRandomEmail();

		homepage.clickNewArrivalsSubLink();

		/** Validate the Category of Launched Page **/
		softAssert.softAssertTrue(plp.getCurrentPageCategory().equalsIgnoreCase("New Arrivals"),
				"Validation : Launched Page is not the 'New Arrivals' Page.");

		String prdctName = plp.getFirstProductName();

		plp.clickFirstProductQS().selectInStockSizeQS().clickAddToBagQS();
		homepage.clickShopBag();

		/**
		 * Validate the count of added item count which is 1 from Shopping Page visible
		 * items count
		 **/
		softAssert.softAssertEquals(cart.totalItemsInCart(), 1,
				"Validation : Added Items and visible items count not matched in Cart.");

		/** Validate the Name of Added single item from Shopping Page **/
		softAssert.softAssertTrue(cart.getItemName(1).equalsIgnoreCase(prdctName),
				"Validation : Added Product and visible product not matched in Cart");

		cart.clickProceedToCheckout().enterEmailGuestChkout(randomEmail).clickContinueGuestCheckout()
				.enterShippingDetails().clickContinueToPayment();

		String cardType = payment.getCardTypeGuestUser();

		payment.enterGuestUserCreditCardData(cardType).clickReviewOrder().enterPasswordAndConfirm();

		softAssert.softAssertTrue(commFunc.isDisplayed(review.placeOrderBtn, 3),
				"Validation : Place Order Buton Displayed");

		if (!env.equalsIgnoreCase("prod")) {
			review.clickPlaceOrder();

			/** Validate the email showing correctly on Thank you Page **/
			softAssert.assertTrue(thanks.getEmail().equals(randomEmail), "Validation : Given email for Guest and email on Thank you Page not matched");
			ExtentTestManager.getTest().log(Status.PASS, "Order Number " + thanks.getOrderId() + " placed successfully.");
		}
	}

	@Test(testName = "SortBy Price Low to High and Filter for Size and Color", enabled = true)
	public void verifyFiltersAndSortByPLP() {
		homepage.clickNewArrivalsSubLink().selectSortByPriceLowToHigh();

		/** Validate Sort By 'Price Low to High' is selected on page **/
		softAssert.softAssertTrue(plp.getAppliedSortingName().equalsIgnoreCase("Price Low"),
				"Validation : Sort By 'Price Low to High' not Selected.");

		List<String> webProductsPriceList = plp.getPricesOfAllProducts();
		List<String> expectedSortedPriceList = webProductsPriceList;
		Collections.sort(expectedSortedPriceList);

		/** Validate Sort By 'Price Low to High' is applied on products **/
		softAssert.softAssertTrue(webProductsPriceList.equals(expectedSortedPriceList),
				"Validation : Sort By 'Price Low to High' not Applied on Products.");

		plp.removeAppliedSorting().clickSizeFilter().selectAnySizeToFilter().clickApplySizeFilter();

		/** Validate Size Filter applied on page **/
		softAssert.softAssertTrue(plp.appliedFilter.isDisplayed(), "Validation : Size Filter not Applied.");

		plp.clickClearAllFilter().clickColorFilter().selectAnyColorToFilter().clickApplyColorFilter();

		/** Validate Color Filter applied on page **/
		softAssert.softAssertTrue(plp.appliedFilter.isDisplayed(), "Validation : Color Filter not Applied.");
	}

	@Test(testName = "Search with Keyword and validate the search page", enabled = true)
	public void verifySearchByKeyword() {
		String keyword = prop.getProperty("searchkeyword");
		homepage.searchByKeyword(keyword);

		/** Validate the Category of Launched Page **/
		softAssert.softAssertTrue(srpage.getCurrentPageCategory().equalsIgnoreCase("Search results"),
				"Validation : Launched Page is not the 'Search Result' Page.");

		/** Validate the name of first product has searched keyword **/
		softAssert.softAssertTrue(srpage.getFirstProductName().contains(keyword),
				"Validation : Searched products are correct as per the searched keyword.");
	}

	@Test(testName = "Adding Item into wishlist and validation for Item details", enabled = true)
	public void verifyWishlistforRegisteredUser() {
		homepage.loginWithRegUser().clickOnWishList().clearWishList();

		String keyword = prop.getProperty("searchkeyword");
		homepage.searchByKeyword(keyword);

		srpage.clickFirstProductName().selectInStockSize();
		HashMap<String, String> productDetailsPDP = pdp.getProductDetailsPDP();

		pdp.clickAddToWishList();
		commFunc.refresh();
		homepage.clickOnWishList();

		/** Validate the Category of Launched Page **/
		softAssert.softAssertTrue(profile.getCurrentPageCategory().equalsIgnoreCase("WishList"),
				"Validation : Launched Page is not the 'Wish List' Page.");

		/**
		 * Validate the count of added items count in wish list which is expected to be
		 * 1
		 */
		softAssert.softAssertEquals(profile.totalItemsInWishList(), 1,
				"Validation : Added Items and visible items count not matched in WishList.");

		/**
		 * Validate the Name of Added single item in wishlist with the name taken from
		 * SearchPage
		 **/
		softAssert.softAssertTrue(profile.getProductDetailsWishList().equals(productDetailsPDP),
				"Validation : Added Product in PDP and visible product details not matched in wishlist");
	}


	@Test(testName = "Guest Express SR checkout and place order", enabled = true)
	public void verifySRExpressCheckout() {

		String keyword = prop.getProperty("searchkeyword");
		homepage.searchByKeyword(keyword);
		
		plp.clickFirstProduct().selectInStockSize().clickAddToBag();
		homepage.clickShopBag();

		cart.clickExpressShoprunner();

		/** Verify the Shop Runner Modal visible on the page */
		softAssert.softAssertTrue(shoprunner.isSRmodalVisible(),
				"Validation : Shop Runner Modal not Launched successfully");

		shoprunner.shopRunnerLogin();

		if (!env.equalsIgnoreCase("prod")) {
			shoprunner.completeSRExpress();
			ExtentTestManager.getTest().log(Status.PASS, "Order Number " + thanks.getOrderId() + " placed successfully.");
		}
	}

	@Test(testName = "Guest Regular SR checkout and place order", enabled = true)
	public void verifySRRegularCheckout() 
	{	
		String randomEmail = commFunc.generateRandomEmail();

		String keyword = prop.getProperty("searchkeyword");
		homepage.searchByKeyword(keyword);
		
		plp.clickFirstProduct().selectInStockSize().clickAddToBag();

		homepage.clickShopBag();

		cart.clickProceedToCheckout().enterEmailGuestChkout(randomEmail).clickContinueGuestCheckout()
				.enterShippingDetails();

		/** Validate Shop Runner is disabled as it requires Sign In **/
		softAssert.softAssertTrue(ship.isSRshipMethodDisabled(),
				"Validation : Selection of Ship Method Shop runner is not disabled which was expected.");

		ship.clickSRsignInLink();

		/** Verify the Shop Runner Modal visible on the page */
		softAssert.softAssertTrue(shoprunner.isSRmodalVisible(),
				"Validation : Shop Runner Modal not Launched successfully");

		shoprunner.shopRunnerLogin().clickContinueShoping();
		String cardType = payment.getCardTypeGuestUser();

		ship.clickContinueToPayment();
		payment.enterGuestUserCreditCardData(cardType).clickReviewOrder();

		/** Validate the visibility of Place Order button on WebPage */
		softAssert.softAssertTrue(commFunc.isDisplayed(review.placeOrderBtn, 3),
				"Validation : Place Order Buton not Displayed");

		if (!env.equalsIgnoreCase("prod")) {
			shoprunner.completeSRExpress();

			/** Validate the email showing correctly on Thank you Page **/
			softAssert.assertTrue(thanks.getEmail().equals(randomEmail),
					"Validation : Given email for Guest and email on Thank you Page not matched");

			ExtentTestManager.getTest().log(Status.PASS, "Order Number " + thanks.getOrderId() + " placed successfully.");
		}
	}

	// PayPal express checkout start
	@Test(testName = "Registered account express place order using Paypal account", enabled = true)
	public void verifyPaypalCheckOutPlaceOrderExpress() 
	{
		homepage.loginWithRegUser();
		commFunc.waitForSeconds(3);
		homepage.clickShopBag();
		cart.clearShoppingBag();
		String keyword = prop.getProperty("searchkeyword");
		homepage.searchByKeyword(keyword);

		plp.clickFirstProduct().selectInStockSize().clickAddToBag();
		homepage.clickShopBag().clickPayPalCheckout();

		String ecomSiteWindow = commFunc.getWindowId();
		payment.navigateToPaypalWindow();
		
		/** Verify the PayPal window opended and PayPal logo is visible */
		softAssert.softAssertTrue(payment.isPaypalWindowWithLogoDisplayed(),
				"Validation : PayPal Login window not Launched in "+env+" environment");

		if (!env.equalsIgnoreCase("prod")) {
			
			payment.loginToPayPalAccount().
			clickPayPalContinueBtn();
			commFunc.switchToWindow(ecomSiteWindow);
			review.clickPlaceOrder();

			ExtentTestManager.getTest().log(Status.PASS, "Order Number " + thanks.getOrderId() + " placed successfully.");
		}
	}

	@Test(testName = "Registered account regular checkout place order using Paypal account", enabled = true)
	public void verifyPaypalCheckOutPlaceOrderRegular()
	{
		homepage.loginWithRegUser();
		profile.removeAllCreditCards();
		homepage.clickShopBag();
		cart.clearShoppingBag();
		
		String keyword = prop.getProperty("searchkeyword");
		homepage.searchByKeyword(keyword);

		plp.clickFirstProduct().selectInStockSize().clickAddToBag();
		homepage.clickShopBag();

		cart.clickProceedToCheckout();
		
		/*payment.selectPaypalPaymentOption()
		.clickPaypalCheckout();*/
		
		review.paypalCheckoutReg();
	
		String ecomSiteWindow = commFunc.getWindowId();
		payment.navigateToPaypalWindow();
		
		/** Verify the PayPal window opended and PayPal logo is visible */
		softAssert.softAssertTrue(payment.isPaypalWindowWithLogoDisplayed(),
				"Validation : PayPal Login window not Launched in "+env+" environment");

		if (!env.equalsIgnoreCase("prod")) {
			
			payment.loginToPayPalAccount().
			clickPayPalContinueBtn();
			commFunc.switchToWindow(ecomSiteWindow);
			review.clickPlaceOrder();
			ExtentTestManager.getTest().log(Status.PASS, "Order Number " + thanks.getOrderId() + " placed successfully.");
		}
	}

	@Test(testName = "Guest User BAC Order Submission  ", enabled = false)
	public void verifyBACCheckout() throws Exception {

		String randomEmail = commFunc.generateRandomEmail();

		homepage.clickDress();
		String prdctName = plp.getFirstProductName();

		plp.clickFirstProduct();
		pdp.selectInStockSize().serachAndSelectBACStore("Newyark");
		homepage.clickShopBag();

		/** Validate the count of added items from Shopping Page **/
		softAssert.softAssertEquals(cart.totalItemsInCart(), 1,
				"Validation : Added Items and visible items count not matched in Cart.");

		/** Validate the Name of Added single item from Shopping Page */
		softAssert.softAssertTrue(cart.getItemName(1).equalsIgnoreCase(prdctName),
				"Validation : Added Product and visible product not matched in Cart");

		cart.clickProceedToCheckout().enterEmailGuestChkout(randomEmail).clickContinueGuestCheckout();

		String cardType = payment.getCardTypeRegUser();

		payment.enterGuestUserCreditCardData(cardType).enterBillingAddress().clickReviewOrder();

		if (!env.equalsIgnoreCase("prod")) {
			review.clickPlaceOrder().getOrderId();

		}
	}

	@Test(testName = "Registerd User Order Submission  ", enabled = true)
	public void verifyRegCheckOutPlaceOrder() {

		homepage.loginWithRegUser();
		profile.removeAllCreditCards();
		homepage.clickShopBag();
		cart.clearShoppingBag();
		homepage.clickNewArrivalsSubLink();

		plp.clickFirstProduct().selectInStockSize().clickAddToBag();
		homepage.clickShopBag();

		cart.clickProceedToCheckout();

		String cardType = payment.getCardTypeRegUser();

		payment.enterRegUserCreditCardData(cardType).clickReviewOrder().enterCVV();

		/** Validate the visibility of Place Order button on WebPage */
		softAssert.softAssertTrue(commFunc.isDisplayed(review.placeOrderBtn, 3),
				"Validation : Place Order Buton Displayed");

		if (!env.equalsIgnoreCase("prod")) {
			review.clickPlaceOrder();
			ExtentTestManager.getTest().log(Status.PASS, "Order Number " + thanks.getOrderId() + " placed successfully.");
		}
	}

	@AfterMethod
	public void afterMethod() {
		try {
			driver.quit();
		} catch (AssertionError e) {
			throw e;
		}
	}

}
