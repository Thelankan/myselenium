package annTaylor.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.asserts.SoftAssert;

import com.ascena.extentreport.ExtentTestManager;
import com.ascena.utilities.ExcelUtils;
import com.ascena.utilities.Utils;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;


public class ShopRunnerPage extends Utils{
	
	WebDriver driver;
	ExtentTest test;
	SoftAssert softassert;
	TestSoftAssert softAssert;
	
	public ShopRunnerPage(WebDriver driver) {
		this.test = ExtentTestManager.getTest();
		this.driver = driver;
		PageFactory.initElements(driver, this);
		commFunc = new CommonFunctions(driver);
	}
	
	@FindBy(id = "sr_content_header_title")
	public WebElement srWelcomeMsg;
	
	@FindBy(id = "sr_signin_email")
	public WebElement srModalEmailBox;
	
	@FindBy(id = "sr_signin_password")
	public WebElement srModalPwdBox;
	
	@FindBy(id = "sr_sign_in_button")
	public WebElement srModalSignInBtn;
	
	@FindBy(id = "sr_header_logo")
	public WebElement srModalLogo;
	
	@FindBy(id = "pr_complete_btn")
	public WebElement CompleteSRExpressPurchase;
	
	@FindBy(id = "sr_close")
	public WebElement srContinueShopBtn;

	public ShopRunnerPage srEmailPwd() throws InterruptedException {
		try {

			String userName = ExcelUtils.getCellData("ShopRunner", 1, 0);
			String passWd = ExcelUtils.getCellData("ShopRunner", 1, 1);
			Thread.sleep(3000);
			driver.findElement(By.id("sr_signin_email")).sendKeys(userName);

			// commFunc.sendWhenReady(ExpressSRSignIn, userName, 3);
			Thread.sleep(3000);
			driver.findElement(By.id("sr_signin_password")).sendKeys(passWd);
			// commFunc.sendWhenReady(ExpressSRPassword, passWd, 3);
			Thread.sleep(3000);
			test.log(Status.PASS, "Email and password entered in shoprunner Modal");

		} catch (Exception e) {
			test.log(Status.FAIL, "Email and password not entered in shoprunner Modal");
			throw e;
		}
		return new ShopRunnerPage(driver);

	}

	/*Login to Shop Runner Modal with registered user*/
	public ShopRunnerPage shopRunnerLogin()
	{
		//commFunc.sid1WaitForPageLoaded();
		String email = ExcelUtils.getCellData("ShopRunner", 1, 0);
		String password = ExcelUtils.getCellData("ShopRunner", 1, 1);
		enterSRemail(email).enterSRpassword(password).clickSRsignInBtn();
		commFunc.waitForPageLoaded();
		return this;
	}
	
	/*Entering email to Shop Runner Modal login page*/
	public ShopRunnerPage enterSRemail(String email)
	{
		try {
			commFunc.sendWhenReady(srModalEmailBox, email, 10);
			test.log(Status.PASS, "Shop Runner  User Email '"+email+"' entered successfully.");
		} catch (Exception e) {
			test.log(Status.FAIL, "Shop Runner User Email was not entered.");
			throw e;
		}
		return this;	
	}
	
	/*Entering password to Shop Runner Modal login page*/
	public ShopRunnerPage enterSRpassword(String password)
	{
		try {
			commFunc.sendWhenReady(srModalPwdBox, password, 5);
			test.log(Status.PASS, "Shop Runner  User Password '"+password+"' entered successfully.");
		} catch (Exception e) {
			test.log(Status.FAIL, "Shop Runner User Password was not entered.");
			throw e;
		}
		return this;	
	}
	
	/*Clicking SignIn button at Shop Runner Modal*/
	public ShopRunnerPage clickSRsignInBtn()
	{
		try {
			commFunc.clickWhenReady(srModalSignInBtn, 5);
			test.log(Status.PASS, "SignIn Button at Shop Runner Modal clicked successfully.");
		} catch (Exception e) {
			test.log(Status.FAIL, "Click at SignIn Button at Shop Runner Modal Failed.");
			throw e;
		}
		return this;	
	}
	
	/*Clicking on Continue Shopping in Shop Runner Modal*/
	public ShopRunnerPage clickContinueShoping()
	{
		try {
			commFunc.clickWhenReady(srContinueShopBtn, 8);
			test.log(Status.PASS, "Clicked at Continue Shopping in Shop runner modal successfully.");
		} catch (Exception e) {
			test.log(Status.FAIL, "Click at Continue Shopping in Shop runner modal Failed.");
			throw e;
		}
		return this;	
	}
	
	public ShopRunnerPage completeSRExpress() {
	try {
		commFunc.clickWhenReady(CompleteSRExpressPurchase, 3);
		test.log(Status.PASS, "Complete your purchase button in shorunner Modal is clicked.");
		
		} catch (Exception e) {
			test.log(Status.FAIL, "Complete your purchase button in shorunner Modal is not clicked.");
			throw e;
			}
		return new ShopRunnerPage(driver);
	}
	
	/*Checking whether SR Modal opened or not */
	public boolean isSRmodalVisible()
	{
		return commFunc.isDisplayed(srModalLogo, 5);	
	}
	
	
}
	
	
	



