package annTaylor.pageobjects;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.asserts.SoftAssert;

import com.ascena.extentreport.ExtentTestManager;
import com.ascena.utilities.Constants;
import com.ascena.utilities.ExcelUtils;
import com.ascena.utilities.Utils;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

public class ShippingPage extends Utils {

	WebDriver driver;
	ExtentTest test;
	SoftAssert softassert;
	TestSoftAssert softAssert;

	public ShippingPage(WebDriver driver) {
		this.test = ExtentTestManager.getTest();
		this.driver = driver;
		PageFactory.initElements(driver, this);
		commFunc = new CommonFunctions(driver);
	}

	@FindBy(css = "#checkout-field-firstname")
	public WebElement shipToFirstName;

	@FindBy(css = "#checkout-field-lastname")
	public WebElement shipToLastName;
	
	@FindBy(css = "#checkout-field-address1")
	public WebElement shipToAddress1;
	
	@FindBy(css = "#checkout-field-zipcode")
	public WebElement shipToZipcode;
	
	@FindBy(css = "#locality")
	public WebElement shipToCity;
	
	@FindBy(css = "[name='state']")
	public WebElement shipToState;
	
	@FindBy(css = "#billing-phonenumber")
	public WebElement shipToPhone;
	
	@FindBy(css = "#shipping-continue")
	public WebElement continueToPaymentBtn;
	
	@FindBy(xpath = "//input[@value='SRThirdBusinessDay' and @type='radio']")
	public WebElement shipMethodSR;
	
	@FindBy(css = "#sr_ec_at_signInSignOut")
	public WebElement signInLinkSR;
		
	/*Entering Details for Shipping Address*/
	public ShippingPage enterShippingDetails()
	{
		try {
			commFunc.sendWhenReady(shipToFirstName, ExcelUtils.getCellData("Address", 7, 1), 8);
			commFunc.sendWhenReady(shipToLastName, ExcelUtils.getCellData("Address", 7, 2), 5);
			commFunc.sendWhenReady(shipToAddress1, ExcelUtils.getCellData("Address", 7, 3), 5);
			commFunc.sendWhenReady(shipToZipcode, ExcelUtils.getCellData("Address", 7, 8), 5);
			commFunc.waitForSeconds(2);
			commFunc.sendWhenReady(shipToPhone, ExcelUtils.getCellData("Address", 7, 9), 5);
			test.log(Status.PASS, "Details entered for Shipping Address successfully");
		} catch (Exception e) {
			test.log(Status.FAIL, "Entering details for Shipping Address Failed");
			throw e;
			}
		return this;
	}
	
	/*Clicking on Continue to Payment Button of Shipping Page*/
	public PaymentPage clickContinueToPayment()
	{
		try {
			commFunc.waitForSeconds(2);
			commFunc.clickWhenReady(continueToPaymentBtn,7);
			test.log(Status.PASS, "Clicked on 'Continue to payment' on Shopping Bag successfully.");
		} catch (Exception e) {
			test.log(Status.FAIL, "Click on 'Continue to payment' on Shopping Bag Failed.");
			throw e;
		}
		return new PaymentPage(driver);
	}

	/*Checking whether SR ship method is disabled or not */
	public boolean isSRshipMethodDisabled()
	{
		return !(shipMethodSR.isEnabled());
	}	
	
	/*Clicking Sign In shop runner ship method*/
	public ShopRunnerPage clickSRsignInLink()
	{
		try {
			commFunc.clickWhenReady(signInLinkSR,5);
			commFunc.waitForPageLoaded();
			test.log(Status.PASS, "Clicked on Shop Runner Ship method sign in link");
		} catch (Exception e) {
			test.log(Status.FAIL, "Click on Shop Runner Ship method sign in link Failed");
			throw e;
		}
		return new ShopRunnerPage(driver);
	}
	
}
