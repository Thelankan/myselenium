package annTaylor.pageobjects;

import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.asserts.SoftAssert;

import com.ascena.extentreport.ExtentTestManager;
import com.ascena.utilities.Utils;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

public class PDPPage extends Utils {
	
	WebDriver driver;
	ExtentTest test;
	SoftAssert softassert;
	TestSoftAssert softAssert; 

	public PDPPage(WebDriver driver) {
		this.test = ExtentTestManager.getTest();
		this.driver = driver;
		PageFactory.initElements(driver, this);
		commFunc = new CommonFunctions(driver);
	}

	
	//WebElements for bread crum on PDP for L2 PDP
	@FindBy(xpath = "(//span [@itemprop='name'])[3]")
	public WebElement bread_crumL2;

	//WebElements fpr breadcrum on PDP for L3 PDP
	@FindBy(xpath = "(//span [@itemprop='name'])[4]")
	public WebElement bread_crumL3;

	// Web Element for Product title on PDP Page
	@FindBy(xpath = "//h1[@itemprop='name']")
	public WebElement product_Title;

	// Web Element for Product Image on PDP Page
	@FindBy(css = "span.is-ready")
	public WebElement pDP_ProdImage;

	// Web Element for Product fit on PDP Page
	@FindBy(xpath = "//*[@class='size-types']//a")
	public WebElement product_fit;
	
	// Web Element for Product size on PDP Page
	@FindBy(xpath = "//*[@class='sizes']//a")
	public WebElement product_Size;
	

	// Web Element for Product colour on PDP Page
	@FindBy(xpath = "//*[@class='colors']/div/a")
	public WebElement product_colour;

	// Web Element for Product Add to Bag Button on PDP Page
	@FindBy(id ="pdp-add-to-bag")
	public WebElement add_ToBag_Button;

	// Web Element for Pick up store Button on PDP Page
	@FindBy(id = "pdp-pick-up")
	public WebElement storebutton;
		
	//Web Element for Shoprunner Sign in link on PDP Page
	@FindBy(id = "sr_ec_at_signInSignOut")
	public WebElement PDPShoprunnerLogin;

	
	// Web Element for Enabled Product Size on PDP Page
	@FindBy(css = "a.wishlist-add")
	public WebElement addToWishList;
	
	///sudarshan
	@FindBy(css = "input.pick-up-address-box")
	public WebElement searchZipBox;
	
	@FindBy(css = "input.js-custom-location")
	public WebElement searchZipBtn;
	
	@FindBy (xpath = "//*[@name='size' and not(contains(@class,'disabled'))]")
	List<WebElement> sizeQS;
	
	@FindBy(xpath = "//a[@name='size' and contains(@class,'active')]")
	public WebElement selectedSize;
	
	@FindBy(css = ".size-types .active")
	public WebElement selectedSizeType;
	
	@FindBy(css = ".colors .selected")
	public WebElement selectedColor;
	
	@FindBy(css = "h1[itemprop='name']")
	public WebElement productName;
	
	public String StoresList= "//div[contains(@class,'pick-up-in-store')]//ol[@class='store-list']/li";
	public String PickupInStoreMessages="//div[contains(@class,'pick-up-in-store')]//span[contains(@class,'availability')]";
	public String StoreBtns="//a[@class='pick-up-confirm']";
	public String StoreNames="//ol[@class='store-list']/li//div[@class='address-details']/strong[@class='store-name']";
	public String sizesInPDP="//*[@class='sizes']//a";
	public String colorsInPDP="//*[@class='colors']//a";

	/*Select InstorePickup option in PDP page*/
	public PDPPage selectPickupInstore() {
		try {
			commFunc.clickWhenReady(storebutton, 3);

			test.log(Status.PASS, "User selected instore pickup option successfully.");
		} catch (Exception e) {
			test.log(Status.FAIL, "Selecting instore pickup option in PDP page failed.");
			throw e;
		}
		return this;
	}
	
	/*Selecting Random Size which is available for product*/
	public PDPPage selectInStockSize()
	{
		try {
			int randomInt=0;
			if(sizeQS.size()==1) {	sizeQS.get(0).click(); 	}
			else {
			  randomInt = commFunc.getRandomInteger(sizeQS.size()-1);
			  sizeQS.get(randomInt).click();
			}
			test.log(Status.PASS, "In-Stock Size selected successfully");
		} catch (Exception e) {
			test.log(Status.FAIL, "In-Stock Size selection Failed.");
			throw e;
		}
		return this;	
	}
	
	public PDPPage serachAndSelectBACStore(String Zipcode) throws Exception
	{
	String SelectedStoreName=null;

	try {

	commFunc.clickWhenReady(storebutton, 10);
	commFunc.sendWhenReady(searchZipBox, Zipcode, 3);
	commFunc.clickWhenReady(searchZipBtn, 3);
	Thread.sleep(5000);
	List<WebElement> listOfStores = driver.findElements(By.xpath(StoresList));
	System.out.println("Size is-----"+listOfStores.size());
	int k=0;
	for(int i = 0; i < listOfStores.size(); i++)
	{
	List<WebElement> listOfPSMessage = driver.findElements(By.xpath(PickupInStoreMessages));
System.out.println("listOfPSMessage size is ----"+listOfPSMessage.size());
	Thread.sleep(3000);
	String st=listOfPSMessage.get(i).getText();
	if (listOfPSMessage.get(i).getText().contains("today") || listOfPSMessage.get(i).getText().contains("days"))
	{
	Thread.sleep(2000);
	List<WebElement> listOfPickUpInStore = driver.findElements(By.xpath(StoreBtns));
	List<WebElement> listOfPStoreName = driver.findElements(By.xpath(StoreNames));
	listOfPickUpInStore.get(k);
	SelectedStoreName=listOfPStoreName.get(i).getText();
	Thread.sleep(3000);
	listOfPickUpInStore.get(k).click();
	break;
	} 
	else if (listOfPSMessage.get(i).getText().contains("Available within"))
	{
	k = k + 1;
	}

	}

	test.log(Status.PASS, "User selected instore pickup option successfully.");
	} catch (Exception e) {
	test.log(Status.FAIL, "Selecting instore pickup option in PDP page failed.");
	throw e;
	}
	return this; 
	}
	
	/*Clicking on Add to wishList link*/
	public PDPPage clickAddToWishList()
	{
		try {
			commFunc.clickWhenReady(addToWishList, 3);
			test.log(Status.PASS, "Add to WishList on PDP Page clicked successfuly.");
		} catch (Exception e) {
			test.log(Status.FAIL, "Click on Add to wishlist in PDP Page Failed.");
			System.out.println(e);
			throw e;
		}
		return this;	
	}
	
	/*Getting color, size type, size, name of product before clicking on AddToBag or AddToWishlist*/
	public HashMap<String,String> getProductDetailsPDP()
	{
		HashMap<String,String> hmp = new HashMap<String,String>();
		//hmp.put("Color", commFunc.getTextWhenReady(selectedColor, 4));
		//hmp.put("Size", commFunc.getTextWhenReady(selectedSize, 4));
		hmp.put("Name", commFunc.getTextWhenReady(productName, 4).toLowerCase());
		//hmp.put("SizeType", commFunc.getTextWhenReady(selectedSizeType, 4));
		return hmp;
	}
	
	
	/*public PDPPage clickZipSearchBtn()
	{
		try {
			commFunc.clickWhenReady(zipSearchBtn, 3);
			test.log(Status.PASS, "In-Stock Size selected successfully");
		} catch (Exception e) {
			test.log(Status.FAIL, "In-Stock Size selection Failed.");
			throw e;
		}
		return this;	
	}*/
		public PDPPage clickAddToBag()
		{
			try {
				commFunc.clickWhenReady(add_ToBag_Button, 6);
				commFunc.refresh();
				test.log(Status.PASS, "Add to Bag button in PDPPage clicked successfuly.");
			} catch (Exception e) {
				test.log(Status.FAIL, "Click on Add to Bag button in PDPPage Failed.");
				System.out.println(e);
				throw e;
			}
			return this;	
		}
		
		
		public PDPPage lnk_SRSignIn()
		{
			
			{
				
				try {
					
					commFunc.clickWhenReady(PDPShoprunnerLogin, 3);
								
					test.log(Status.PASS, "User clicked on shoprrunner sign in link successfully.");
				} catch (Exception e) {
					test.log(Status.FAIL, "Clicking on shopprunner sign in link failed.");
					throw e;
				}
			return this;
			
		}
		}
			
			public PDPPage addToBagFromPDP()
			{

				{
					
					try {
						
						commFunc.clickWhenReady(add_ToBag_Button, 3);
									
						test.log(Status.PASS, "User clicked on add to cart on PDP successfully.");
					} catch (Exception e) {
						test.log(Status.FAIL, "Clicking on add to cart on PDP failed.");
						throw e;
					}
				return this;

				}
			}
			
			
		
}

