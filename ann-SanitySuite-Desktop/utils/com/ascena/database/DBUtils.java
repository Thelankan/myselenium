package com.ascena.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.ascena.extentreport.ExtentManager;

/**
 * DB connection and data extract method
 * @author SSaxena
 *
 */

public class DBUtils {
	
	private static final String jdbcURL = "jdbc:oracle:thin://@192.168.130.204:9101/ORQATG01";
	private static Connection conn = null;
	//private static Statement stmt = null;
	private static PreparedStatement preStmt = null;
	private static ResultSet rs =null;
	private static String user = null;
	private static String password = null;

	/**
	 * DB connection and data extract method
	 * @author SSaxena
	 *
	 */	
	
	public static String getUserName(String core_cata)
	{
		return "ATGDB_ANNT_"+core_cata.toUpperCase()+"_"+ExtentManager.getTestEnvName().toUpperCase();
	}
	
	
	/**
	 * DB connection and data extract method
	 * @author SSaxena
	 *
	 */
	public static void dbConnectCORE()
	{
		try {
			user=getUserName ("core");
			password=user;
			Class.forName("oracle.jdbc.driver.OracleDriver").newInstance();
			 conn = DriverManager.getConnection(jdbcURL,user,password);
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	
	/**
	 * DB connection and data extract method
	 * @author SSaxena
	 *
	 */
	
	public static void dbConnectCATA()
	{
		try {
			user=getUserName ("cata");
			password=user;
			Class.forName("oracle.jdbc.driver.OracleDriver").newInstance();
			 conn = DriverManager.getConnection(jdbcURL,user,password);
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	
	/**
	 * Data extraction methods from DB
	 * @author SSaxena
	 *
	 */
	
	public static String getOrderStatus(String orderId)
	{
		dbConnectCORE();
		String status = null;
		try {
			preStmt = conn.prepareStatement(SqlQuery.SQL_getOrderStatus);
			preStmt.setString(1, orderId);
	
			rs=preStmt.executeQuery();			
			rs.next();
			status = rs.getString("state");
			
		} catch (SQLException e) {
			System.out.println(e);
		}
		return status;
	}
	
}
