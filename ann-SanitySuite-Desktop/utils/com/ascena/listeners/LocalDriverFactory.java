package com.ascena.listeners;

import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.ascena.extentreport.ExtentManager;
import com.ascena.extentreport.ExtentTestManager;

import io.appium.java_client.remote.MobileCapabilityType;

/**
 * This call contains methods to initiate extent report and web driver
 * initialization for different browsers
 * 
 * @author HMarkam
 *
 */
public class LocalDriverFactory {
	 

	static WebDriverListener utilWebDriver;

	/**
	 * 1. Method to create instance of extent report 2. Create driver instance
	 * and return driver
	 * 
	 * @param browserName
	 * @return driver
	 * @throws Exception
	 */
	public static WebDriver createInstance(String browserName) throws Exception {
		RemoteWebDriver driver = null;
		ChromeOptions chromeOptions;
		DesiredCapabilities capabilities = new DesiredCapabilities();

		try {
			/*************** Extent Report *****************/

			if (ExtentManager.getInstance() == null)
				ExtentManager.createInstance(System.getProperty("user.dir") + "\\extentreports\\"
						+ ExtentManager.getTestSuiteName() + "AutoExecutionReport" + ".html");

			/********************** Driver Initialization *******************/

			System.out.printf("Opening %s browser.\n", browserName);

			switch (browserName.toLowerCase()) {
			
			case "androidemulator": {
				capabilities.setCapability("device", "Android Emulator");
				capabilities.setCapability(CapabilityType.BROWSER_NAME, "Chrome");
				capabilities.setCapability(CapabilityType.VERSION, "8.1.0");
				capabilities.setCapability(CapabilityType.PLATFORM, "Android");
				capabilities.setCapability("platformName", "Android");
				capabilities.setCapability("deviceName", "Nexus_6_API_27:5554");
				//driver = new RemoteWebDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
				break;
			}

			case "android": {
				capabilities.setCapability("device", "Android");
				capabilities.setCapability(CapabilityType.BROWSER_NAME, "Chrome");
				capabilities.setCapability(CapabilityType.VERSION, "7.0");
				capabilities.setCapability(CapabilityType.PLATFORM, "Android");
				capabilities.setCapability("platformName", "Android");
				capabilities.setCapability("deviceName", "ce12171c394f3e1d05");
				//driver = new RemoteWebDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
				break;
			}
			case "iphone": {
				
				
				 capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, "iOS");
				  capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "iPhone");
				  capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, "11.2");
				  capabilities.setCapability(MobileCapabilityType.BROWSER_NAME, "Safari");
				  capabilities.setCapability("automationName","XCUITest");
				  capabilities.setCapability("bundleId","com.apple.mobilesafari");
				  capabilities.setCapability("xcodeOrgId","Ascena GIC");
				  capabilities.setCapability("xcodeSigningId","iPhone Developer");
				  capabilities.setCapability(MobileCapabilityType.AUTO_WEBVIEW, true);
				  
				  capabilities.setCapability("udid", "91b15bbec4a33daba25c489752874d35394d626c");
				  driver = new RemoteWebDriver(new URL("http://127.0.0.1:4723/wd/hub"),capabilities);
				 
			
				break;
			}
			case "androidcloud": {
				capabilities.setCapability("realMobile", "true");
				capabilities.setCapability("device", "Galaxy S3");
				capabilities.setCapability("os_version", "4.1");
				capabilities.setCapability("browserstack.appium_version", "1.4.16");
				capabilities.setCapability("acceptSslCerts", "true");
				capabilities.setCapability("browserstack.debug", "true");
				/*driver = new RemoteWebDriver(
						new URL("https://" + utilWebDriver.prop.getProperty("BROWSERSTACK_USERNAME") + ":"
								+ utilWebDriver.prop.getProperty("BROWSERSTACK_ACCESS_KEY")
								+ "@hub-cloud.browserstack.com/wd/hub"),
						capabilities);*/
				break;
			}
			case "iphonecloud": {
				capabilities.setCapability("realMobile", "true");
				capabilities.setCapability("device", "iPhone 8");
				capabilities.setCapability("os_version", "11.0");
				capabilities.setCapability("browserstack.appium_version", "1.7.0");
				capabilities.setCapability("acceptSslCerts", "true");
				capabilities.setCapability("browserstack.debug", "true");
				/*driver = new RemoteWebDriver(
						new URL("https://" + utilWebDriver.prop.getProperty("BROWSERSTACK_USERNAME") + ":"
								+ utilWebDriver.prop.getProperty("BROWSERSTACK_ACCESS_KEY")
								+ "@hub-cloud.browserstack.com/wd/hub"),
						capabilities);*/
				break;
			}
			
			
			case "chrome": { 
				System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "\\drivers\\chromedriver.exe");
				ChromeOptions options = new ChromeOptions();
				options.addArguments("disable-infobars");
				driver = new ChromeDriver(options);
				driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
				driver.manage().timeouts().pageLoadTimeout(100, TimeUnit.SECONDS);
				driver.manage().window().maximize();
				break;
			}
				
			default: {
				ExtentTestManager.log(browserName + " Incorrect/No browsers defined");
				break;
			}
			}

			if (browserName.equalsIgnoreCase("firefox") || browserName.equalsIgnoreCase("chrome")
					|| browserName.equalsIgnoreCase("ie")) {
				driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
				driver.manage().timeouts().pageLoadTimeout(100, TimeUnit.SECONDS);
				driver.manage().window().maximize();
			} else {
				driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
				driver.manage().timeouts().pageLoadTimeout(120, TimeUnit.SECONDS);
			}

		} catch (Exception e) {
			//ExtentTestManager.log(browserName + " driver initialization failed");
			throw e;
		}
		return driver;
	}

}