package loft.pageobjects;

import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.ascena.extentreport.ExtentTestManager;
import com.ascena.utilities.Utils;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

public class MyAccountPage extends Utils {

	WebDriver driver;
	ExtentTest test;
	TestSoftAssert softAssert;

	public MyAccountPage(WebDriver driver) {
		this.test = ExtentTestManager.getTest();
		this.driver = driver;
		PageFactory.initElements(driver, this);
		commFunc = new CommonFunctions(driver);
	}
	
	//left navigation starts
	
	@FindBy(xpath="//a[contains(text(),'Orders & Returns')]")
	public WebElement ordersAndReturns_link;
	
	@FindBy(xpath="//a[contains(text(),'Profiles & Preferences')]")
	public WebElement profilesAndPreferences_link;
	
	@FindBy(xpath="//a[contains(text(),'Address Book')]")
	public WebElement address_link;
	
	@FindBy(xpath="//a[contains(text(),'Payment Methods')]")
	public WebElement paymentMethod_link;
	
	
	@FindBy(xpath="//*[@class='wishlist-nav activated']")
	public WebElement wishlist_link;
	
	//left navigation ends
	
	@FindBy(xpath="//form//input[@name='number']")
	public WebElement orderSearch;
	
	@FindBy(xpath="//input[@value='GO']")
	public WebElement orderSearchGo;
	
	
	@FindBy(xpath="//a[contains(@class,'view profile-prefs')]")
	public WebElement view_link;
	
	@FindBy(xpath="//a[contains(@class,'edit-email-password')]")
	public WebElement editEmailPassword_link;
	
	@FindBy(xpath="//a[contains(@class,'edit-email-pref')]")
	public WebElement editEmail_link;
	
	@FindBy(xpath="//*[@id='password-edit-mode']/div/a")
	public WebElement editPassword_link;
	
	@FindBy(xpath="//form//input[@name='newEmail']")
	public WebElement editEmail;
	
	@FindBy(xpath="//form//input[@name='confirmEmail']")
	public WebElement editConfirmEmail;
	
	//save email address
	@FindBy(xpath="//*[@id='email-edit-mode']/descendant::input[@name='save']")
	public WebElement saveEmail;
	
	@FindBy(xpath="//*[@id='email-edit-mode']/descendant::input[@name='cancel']")
	public WebElement cancelEmail;
	
	//save password
	@FindBy(xpath="//input[@name='currentPassword']")
	public WebElement editCurrentPassword;
	
	@FindBy(xpath="//input[@name='newPassword']")
	public WebElement editNewPassword;
	
	@FindBy(xpath="//input[@name='confirmNewPassword']")
	public WebElement editConfirmNewPassword;
	
		
	@FindBy(xpath="//*[@id='password-edit-mode']/descendant::input[@name='save']")
	public WebElement savePassword;
	
	@FindBy(xpath="//*[@id='password-edit-mode']/descendant::input[@name='cancel']")
	public WebElement cancelPassword;
	
	//Details of preferences
	
	@FindBy(xpath="//*[@id='FNAME']")
	public WebElement editFname;
	
	@FindBy(xpath="//*[@id='COUNTRY']")
	public WebElement editCountry;
	
	@FindBy(xpath="//*[@id='ZIP']")
	public WebElement editZipCode;
	
	@FindBy(xpath="//*[@id='bd0']")
	public WebElement birthdayMonth;
	
	@FindBy(xpath="//*[@id='bd1']")
	public WebElement birthdayDay;
	
	@FindBy(xpath="//*[@id='imageField']")
	public WebElement myPreferencesSave;
	
	//Add new address book
		
	@FindBy(xpath="//a[contains(text(),'Add New')]")
	public WebElement addressNewButton;
	
	@FindBy(xpath="//div[contains(@class,'slot')]//form[contains(@class,'address-book')]//input[@name='firstName']")
	public WebElement addressNewFName;
	
	@FindBy(xpath="//div[contains(@class,'slot')]//form[contains(@class,'address-book')]//input[@name='lastName']")
	public WebElement addressNewLName;
	
	@FindBy(xpath="//div[contains(@class,'slot')]//form[contains(@class,'address-book')]//input[@id='shipping-address-1']")
	public WebElement addressShippingAdd1;
	
	@FindBy(xpath="//div[contains(@class,'slot')]//form[contains(@class,'address-book')]//input[@id='shipping-zip-code']")
	public WebElement addressZipCode;
	
	@FindBy(xpath="//div[contains(@class,'slot')]//form[contains(@class,'address-book')]//input[@name='city']")
	public WebElement addressCity;
	
	@FindBy(xpath="//div[contains(@class,'slot')]//form[contains(@class,'address-book')]//*[@id='dd-label-3']")
	public WebElement addressState;
	
	@FindBy(xpath="//div[contains(@class,'slot')]//form[contains(@class,'address-book')]//input[@name='phone']")
	public WebElement addressPhone;
	
	
	@FindBy(xpath="//div[contains(@class,'slot')]//form[contains(@class,'address-book')]//div[contains(@class,'custom-checkbox mouse-focus-input')]//input[@type='checkbox']")
	public WebElement addressDefault;
	
	@FindBy(xpath="//div[contains(@class,'slot')]//form[contains(@class,'address-book')]//input[@name='submit']")
	public WebElement addressSave;
	
	//Add Payment 
	
	
	
	@FindBy(xpath="//form[contains(@class,'address-book wallet')]//a[contains(text(),'Add New')]")
	public WebElement paymentMethods_New;
	
	

	@FindBy(xpath="//form//input[@id='cardNumber']")
	public WebElement NewCardNumber;
	
	
	@FindBy(xpath="//select[@id='exp_month']")
	public WebElement NewCardMonth;
	
	@FindBy(xpath="//select[@id='exp_year']")
	public WebElement NewCardYear;
	
	//Payment method billing address
	
	@FindBy(xpath="//form//input[@id='billingFirstName']")
	public WebElement BillingFname;
	
	@FindBy(xpath="//form//input[@id='billingLastName']")
	public WebElement BillingLname;
	
	
	@FindBy(xpath="//form//input[@id='billingAddress']")
	public WebElement BillingAddress;
	

	@FindBy(xpath="//form//input[@id='billingZip']")
	public WebElement BillingZip;
	
	@FindBy(xpath="//form//input[@id='billingCity']")
	public WebElement BillingCity;
	
	
	//select state for billing
	@FindBy(xpath="//select[@id='billingState']")
	public WebElement BillingState;
	
	@FindBy(xpath = "//nav[@class='breadcrumb']//a")
	public List<WebElement> breadcrumbList;
	
	@FindBy(xpath="//form//input[@id='billingPhone']")
	public WebElement BillingPhone;
	
	@FindBy(xpath="//form//input[@id='billing-btn-revieworder']")
	public WebElement BillingSave;

	@FindBy(css = "h1.name a")
	public List<WebElement> wishListItems;
	
	@FindBy(css = "[class='product'] a.remove")
	public List<WebElement> wishListRemoveBtns;
	
	@FindBy(xpath = "//a[contains(@href,'billing') and contains(.,'Wallet')]")
	public WebElement paymentMethods;
	
	@FindBy(css="[class='item checked']")
	public List<WebElement> existingCards;
	
	@FindBy(css="[class='remove']")
	public List<WebElement> ccRemoveBtns;
	
	@FindBy(css = "input[value='confirm']")
	public WebElement removeCardConfirm;
	
	/*clear wish list it it has any added item*/
	public MyAccountPage clearWishList()
	{
		try {
			if (wishListItems.get(0).isDisplayed())
			{
				for (int index = 0 ; index < wishListItems.size() ; index++)
				{
					wishListRemoveBtns.get(0).click();
				}
			}
			test.log(Status.INFO, "Wish List Cleared successfully");
		} catch (Exception e) {
			test.log(Status.INFO, "Wish List was Empty");
		}
		
		return this;
	}
	
	/*Getting Current Page Category*/
	public String getCurrentPageCategory()
	{
		int index = breadcrumbList.size()-1;
		return commFunc.getTextWhenReady(breadcrumbList.get(index), 5);
	}
	
	/*Getting the count of item which is added into wishlist*/
	public int totalItemsInWishList()
	{
		return wishListItems.size();
	}
	
	/*/*Getting the name of first product available in wishlist*/
	public String getFirstProductName()
	{
		try {
			return commFunc.getTextWhenReady(wishListItems.get(0), 4);
		} catch (Exception e) {
			test.log(Status.FAIL, "Getting First product Name from WishList Failed");
			throw e;
		}
	}
	
	/*Getting color, size type, size, name of product from WishList*/
	public HashMap<String,String> getProductDetailsWishList()
	{
		HashMap<String,String> hmp = new HashMap<String,String>();
		//hmp.put("Color", commFunc.getTextWhenReady(selectedColor, 4));
		//hmp.put("Size", commFunc.getTextWhenReady(selectedSize, 4));
		hmp.put("Name", commFunc.getTextWhenReady(wishListItems.get(0), 4).toLowerCase());
		//hmp.put("SizeType", commFunc.getTextWhenReady(selectedSizeType, 4));
		return hmp;
	}
	
	/*Removing all added credit cards from registered user account*/
	public MyAccountPage removeAllCreditCards() 
	{
		try {
			commFunc.clickWhenReady(paymentMethods, 3);
			commFunc.waitForSeconds(3);
			if (existingCards.get(0).isDisplayed()) 
			{
				int size = existingCards.size();
				for (int index = 0; index < size; index++) {
					ccRemoveBtns.get(0).click();
					removeCardConfirm.click();
					commFunc.waitForSeconds(2);
				}
				test.log(Status.INFO, "All credit cards from wallet removed successfuly.");
			}
		} catch (Exception e) {
			test.log(Status.INFO, "There is no Payment Cards saved with user account ");
		}
		return this;
	}
}
