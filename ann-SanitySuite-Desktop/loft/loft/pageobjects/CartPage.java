package loft.pageobjects;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.asserts.SoftAssert;

import com.ascena.extentreport.ExtentTestManager;
import com.ascena.utilities.Utils;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;


public class CartPage extends Utils {

	WebDriver driver;
	ExtentTest test;
	SoftAssert softassert;
	TestSoftAssert softAssert;

	public CartPage(WebDriver driver) {
		this.test = ExtentTestManager.getTest();
		this.driver = driver;
		PageFactory.initElements(driver, this);
		commFunc = new CommonFunctions(driver);
	}

	@FindBy(css = ".cartItem")
	public By lineItem;

	@FindBy(xpath = "//*[@class='cartItem']/a")
	public WebElement itemName;
	
	@FindBy(id = "checkout-proceed")
	public WebElement prcdToChkoutBtn;
	
	@FindBy(xpath = "//*[@class='slot']//input[@id='modal-email-guest']")
	public WebElement chkoutModalGuestEmailBox;
	
	@FindBy(xpath = "//*[@class='slot']//input[@id='modal-continue-button']")
	public WebElement chkoutModalcontinueBtn;
	
	@FindBy(xpath = ("//a[@id='checkout-proceed']"))
	public WebElement checkOutElement;

	@FindBy(xpath = "//button[@id='srd_XC']")
	public WebElement btn_ExpressShoprunner;
	
	@FindBy(xpath = "//button[@id='paypal-button']")
	public WebElement btn_ExpressPaypal;

	@FindBy	(xpath = "//div[4]/section/ol/li[1]/a[contains(text(),'edit')]")
	public WebElement Shipping_bag_Edit_Icon;
	
	@FindBy (xpath = "//div[4]/section/ol/li[1]/a[text()='remove']")
	public WebElement Shipping_bag_Remove_Icon;
	
	@FindBy(css = "input[id=code]")
	public WebElement promoCodeInputBox;

	@FindBy(css = "i#promo-submit")
	public WebElement promoCodeApplyBtn;
	
	@FindBy(xpath = "//span[@class='error']")
	public WebElement invalidPromoCodeMsg;
	
	
	@FindBy(xpath = "//a[@href='#gift-box0']")
	public WebElement giftBoxCheck;
	
	//clearing cart items start
	@FindBy(xpath ="//a[contains(text(),'remove')]")
	public WebElement removeItems;
		
	@FindBy (css = ".cartItem")
	public List<WebElement> itemsInCart;
		
	@FindBy (css = ".cartItem")
	public WebElement itemInCart;
	
	@FindBy(css = "a.remove")
	public List<WebElement> cartItemRemoveBtns;
	
	@FindBy(xpath = "//button[@id='paypal-button']")
	public WebElement paypalCheckoutBtn;
		
		//clearing cart items end
	
	public CartPage click_Shipping_bag_Edit_Icon() {
		Shipping_bag_Edit_Icon.click();
		return this;
	}
	
	/*Clicking on Add to Bag in Quick Shop Modal*/
	public int totalItemsInCart()
	{
		return driver.findElements(By.cssSelector(".cartItem")).size();
	}
	
	/*Getting Item Name from Shopping Bag based on index*/
	public String getItemName(int lineItem)
	{
		return driver.findElement(By.xpath("(//*[@class='details']/h1/a)"+"["+lineItem+"]")).getText().trim();
	}
	
	/*Clicking on Proceed to Checkout Button*/
	public CartPage clickProceedToCheckout()
	{
		try {
			commFunc.clickWhenReady(prcdToChkoutBtn,5);
			test.log(Status.PASS, "Clicked on 'Proceed to checkout' on Shopping Bag successfully.");
		} catch (Exception e) {
			test.log(Status.FAIL, "Click on 'Proceed to checkout' on Shopping Bag Failed.");
			throw e;
		}
		return this;

	}
	/*Entering email in guest checkout modal*/
	public CartPage enterEmailGuestChkout(String email)
	{
		try {
			if (commFunc.isDisplayed(chkoutModalGuestEmailBox, 5))
			{
				for(int index = 0; index<email.length(); index++)
				{
					chkoutModalGuestEmailBox.sendKeys(Character.toString(email.charAt(index)));
				}
			}
			test.log(Status.PASS, "Email '"+email+"' entered successfully for Guest Checkout");
		} catch (Exception e) {
			test.log(Status.FAIL, "Entering Email for Guest Checkout Failed");
			throw e;
		}
		return this;
	}
	
	/*Clicking Continue button in guest checkout modal*/
	public ShippingPage clickContinueGuestCheckout()
	{
		try {
			commFunc.clickWhenReady(chkoutModalcontinueBtn, 5);
			test.log(Status.PASS, "Clicked on Continue button at Guest Checkout Modal successfully");
		} catch (Exception e) {
			test.log(Status.FAIL, "Click on Continue button at Guest Checkout Modal Failed");
			throw e;
		}
		return new ShippingPage(driver);
	} 
	
	/*//Clearing shopping bag items start 
		public void clearShoppingBag()  throws Exception{
			{
				try {
					
					if (itemInCart.isDisplayed())
					{
						ExtentTestManager.getTest().log(Status.INFO, "cart size 1" +itemsInCart.size());
					for(int j =0; j<= itemsInCart.size()-1; j++)	
					 {
						ExtentTestManager.getTest().log(Status.INFO, "cart size" +itemsInCart.size());
						commFunc.clickWhenReady(removeItems, 3);
						
						
					 }
					}
					
					test.log(Status.PASS, "Items removed from cart");
				} catch (Exception e) {
					test.log(Status.FAIL, "No items removed");
				}
					
			}

		}
		//Clearing shopping bag items end
*/		
	/* clear wish list it it has any added item */
	public CartPage clearShoppingBag() {
		try {
			if (itemInCart.isDisplayed()) {
				int size = itemsInCart.size();
				for (int index = 0; index < size; index++) {
					cartItemRemoveBtns.get(0).click();
					commFunc.waitForSeconds(2);
				}
			}
			test.log(Status.INFO, "Items removed from cart");
		} catch (Exception e) {
			test.log(Status.INFO, "Cart was already empty");
		}
		return this;
	}

	/*Clicking on PayPal Checkout Button*/
	public CartPage clickPayPalCheckout()
	{
		try {
			commFunc.waitForSeconds(3);
			if(commFunc.isElementPresent(driver, paypalCheckoutBtn))
			{
			commFunc.clickWhenReady(paypalCheckoutBtn, 7);
			test.log(Status.PASS, "Clicked on 'PayPal Checkout' on Shopping Bag successfully.");
			}
			else
				test.log(Status.PASS, "Not Clicked on 'PayPal Checkout' on Shopping Bag successfully.");
		} catch (Exception e) {
			test.log(Status.FAIL, "Click on 'PayPal Checkout' on Shopping Bag Failed.");
			throw e;
		}
		return this;
	}
	
}
