package loft.pageobjects;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.asserts.SoftAssert;

import com.ascena.extentreport.ExtentTestManager;
import com.ascena.utilities.Utils;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

public class PLPPage extends Utils {

	WebDriver driver;
	ExtentTest test;
	SoftAssert softassert;
	TestSoftAssert softAssert;

	public PLPPage(WebDriver driver) {
		this.test = ExtentTestManager.getTest();
		this.driver = driver;
		PageFactory.initElements(driver, this);
		commFunc = new CommonFunctions(driver);
	}

	@FindBy(css = "a.shop-now")
	public WebElement firstProductQS;

	@FindBy(css = ".submit button")
	public WebElement addToBagQS;
	
	@FindBy(xpath = "//*[@name='size' and not(contains(@class,'disabled'))]")
	List<WebElement> sizeQS;
	
	@FindBy(xpath = "//a[@class='shop-now']/..//a/strong")
	public WebElement firstProductName;
	
	@FindBy(css = "[class='sort-by initialized']")
	public WebElement sortByDD;
	
	@FindBy(xpath = "//select[@class='sort-by initialized']/following-sibling::span")
	public WebElement sortByArrow;
	
	@FindBy(xpath = "//*[@class='sort-by initialized']/following-sibling::span")
	public WebElement appliedSort;
	
	@FindBy(xpath = "//*[@class='custom-dropdown size']/span")
	public WebElement sizeFilter;
	
	@FindBy(xpath = "//*[@class='custom-dropdown color']/span")
	public WebElement colorFilter;
	
	@FindBy(xpath = "//*[@class='scrollable active']/div/a")
	List<WebElement> sizeFiltersOptions;
	
	@FindBy(xpath = "//*[@class='scrollable active']//img")
	List<WebElement> colorFiltersOptions;
	
	@FindBy(css = "span.price")
	List<WebElement> allPrices;
	
	@FindAll ({
		@FindBy(xpath = "//*[@class='check-list size ']/a[@class='apply']"),
		@FindBy(xpath = "//*[@class='check-list size']/a[@class='apply']")
		})
	public WebElement applySizeFilterBtn;
	
	@FindAll ({
		@FindBy(xpath = "//*[@class='check-list color ']/a[@class='apply']"),
		@FindBy(xpath = "//*[@class='check-list color']/a[@class='apply']")
		})
	public WebElement applyColorFilterBtn;
	
	@FindBy(css = ".filters-selected")
	public WebElement appliedFilter;
	
	@FindBy(xpath = "//*[@class='filters-selected']/a[@class='clear-all']")
	public WebElement clearAllFilter;
	
	@FindBy(xpath = "//span[@itemprop='name']")
	public List<WebElement> breadcrumbList;
	
	/*Getting Current Page Category*/
	public String getCurrentPageCategory()
	{
		int index = breadcrumbList.size()-1;
		return commFunc.getTextWhenReady(breadcrumbList.get(index), 5);
	}
	
	/*Clicking on First Product Quick Shop option*/
	public PLPPage clickFirstProductQS()
	{
		try {
			commFunc.mousehover(firstProductQS);
			commFunc.waitForSeconds(2);
			firstProductQS.click();
			test.log(Status.PASS, "First Prduct Quick shop clicked successfuly.");
		} catch (Exception e) {
			test.log(Status.FAIL, "Click on First Product Qucik Shop Failed.");
			System.out.println(e);
			throw e;
		}
		return this;	
	}
	
	/*Getting First Product name from current open PLP page*/
	public String getFirstProductName()
	{
		String name=null;
		try {
			name= commFunc.getTextWhenReady(firstProductName,5);
			test.log(Status.PASS, "First Prduct name : "+name);
		} catch (Exception e) {
			test.log(Status.FAIL, "Getting First Product Name Failed.");
			System.out.println(e);
			throw e;
		}	
		return name;
	}
	
	/*Selecting Random Size which is available for product in Quick Shop Modal*/
	public PLPPage selectInStockSizeQS() {
		try {
			int randomInt = 0;
			if (sizeQS.size() == 1) {
				System.out.println("sizeQS" + sizeQS.size());
				sizeQS.get(0).click();
			} else {
				randomInt = commFunc.getRandomInteger(sizeQS.size() - 1);
			}
			sizeQS.get(randomInt).click();
			test.log(Status.PASS, "In-Stock Size selected successfully");
		} catch (Exception e) {
			test.log(Status.FAIL, "In-Stock Size selection Failed.");
			throw e;
		}
		return this;
	}
	
	/*Clicking on Add to Bag in Quick Shop Modal*/
	public PLPPage clickAddToBagQS()
	{
		try {
			commFunc.clickWhenReady(addToBagQS, 6);
			commFunc.refresh();// After 
			test.log(Status.PASS, "Add to Bag in Quick Shop Modal clicked successfuly.");
		} catch (Exception e) {
			test.log(Status.FAIL, "Click on Add to Bag in Quick Shop Modal Failed.");
			System.out.println(e);
			throw e;
		}
		return this;	
	}

	/*Selecting sort by with Price Low to High*/
	public PLPPage selectSortByPriceLowToHigh()
	{
		try {
			commFunc.waitForSeconds(2);
			commFunc.selectUsingVisibleText(sortByDD, "Price Low to High");
			commFunc.waitForSeconds(3);
			test.log(Status.PASS, "Sort By 'Price Low to High' selected successfully");
		} catch (Exception e) {
			test.log(Status.FAIL, "Sort By 'Price Low to High' selection Failed");
			throw e;
		}
		return this;
	}
	
	/*Clicking on Applied Sorting to remove the sorting*/
	public PLPPage removeAppliedSorting()
	{
		try {
			commFunc.clickWhenReady(appliedSort, 6);
			commFunc.waitForSeconds(2);
			test.log(Status.PASS, "Applied Sorting Removed successfuly.");
		} catch (Exception e) {
			test.log(Status.FAIL, "Removing applied sorting Failed.");
			System.out.println(e);
			throw e;
		}
		return this;	
	}
	
	/*Getting Applied Sort Category Name*/
	public String getAppliedSortingName()
	{
		try {
			return commFunc.getTextWhenReady(appliedSort, 6);
		} catch (Exception e) {
			throw e;
		}
	}
	
	/*Getting prices for all products visible on the page*/
	public List<String> getPricesOfAllProducts()
	{
		try {
			List<String> priceList = new ArrayList<String>();
			for(WebElement price : allPrices)
			{
				priceList.add(commFunc.getTextWhenReady(price, 5));
			}
			return priceList;
		} catch (Exception e) {
			throw e;
		}
	}
	
	/*Clicking on Size Filter Option*/
	public PLPPage clickSizeFilter()
	{
		try {
			commFunc.clickWhenReady(sizeFilter, 6); 
			test.log(Status.PASS, "Size Filter clicked successfuly.");
		} catch (Exception e) {
			test.log(Status.FAIL, "Click on size filter Failed.");
			System.out.println(e);
			throw e;
		}
		return this;	
	}
	
	/*Clicking on Color Filter Option*/
	public PLPPage clickColorFilter()
	{
		try {
			commFunc.clickWhenReady(colorFilter, 6); 
			test.log(Status.PASS, "Color Filter clicked successfuly.");
		} catch (Exception e) {
			test.log(Status.FAIL, "Click on color filter Failed.");
			System.out.println(e);
			throw e;
		}
		return this;	
	}
	
	/*Selecting any random option from opened size filter options to apply*/
	public PLPPage selectAnySizeToFilter()
	{
		String option = null;
		try {
			int index = commFunc.getRandomInteger(sizeFiltersOptions.size());
			sizeFiltersOptions.get(index).click();
			option = sizeFiltersOptions.get(index).getText().trim();
			test.log(Status.PASS, "selected filter option '"+option+"' successfuly.");
		} catch (Exception e) {
			test.log(Status.FAIL, "Selection of filter option '"+option+"' Failed.");
			System.out.println(e);
			throw e;
		}
		return this;	
	}
	
	/*Selecting any random option from opened color filter options to apply*/
	public PLPPage selectAnyColorToFilter()
	{
		String option = null;
		try {
			int index = commFunc.getRandomInteger(colorFiltersOptions.size());
			colorFiltersOptions.get(index).click();
			option = colorFiltersOptions.get(index).getText().trim();
			test.log(Status.PASS, "selected filter option '"+option+"' successfuly.");
		} catch (Exception e) {
			test.log(Status.FAIL, "Selection of filter option '"+option+"' Failed.");
			System.out.println(e);
			throw e;
		}
		return this;	
	}
	
	/*Clicking on Apply Filter Button*/
	public PLPPage clickApplySizeFilter()
	{
		try {
			commFunc.clickWhenReady(applySizeFilterBtn, 3); 
			commFunc.waitForSeconds(2);
			test.log(Status.PASS, "Clicked on Apply button for Size Filter successfuly.");
		} catch (Exception e) {
			test.log(Status.FAIL, "Click on Apply Button for Size Filter Failed.");
			System.out.println(e);
			throw e;
		}
		return this;	
	}
	
	/*Clicking on Apply Color Filter Button*/
	public PLPPage clickApplyColorFilter()
	{
		try {
			commFunc.clickWhenReady(applyColorFilterBtn, 6); 
			commFunc.waitForSeconds(2);
			test.log(Status.PASS, "Clicked on Apply button for Color Filter successfuly.");
		} catch (Exception e) {
			test.log(Status.FAIL, "Click on Apply Button for Color Filter Failed.");
			System.out.println(e);
			throw e;
		}
		return this;	
	}
	
	/*Clicking on Clear all option to remove all filters*/
	public PLPPage clickClearAllFilter()
	{
		try {
			commFunc.clickWhenReady(clearAllFilter, 6);
			commFunc.waitForSeconds(2);
			test.log(Status.PASS, "Clear All clicked successfuly.");
		} catch (Exception e) {
			test.log(Status.FAIL, "Click on Clear all Failed.");
			System.out.println(e);
			throw e;
		}
		return this;	
	}
	
	/*Clicking on First Product to navigate to PDP Page*/
	public PDPPage clickFirstProduct()
	{
		try {
			commFunc.clickWhenReady(firstProductName, 5);
			test.log(Status.PASS, "First Prduct on PLP clicked successfuly.");
		} catch (Exception e) {
			test.log(Status.FAIL, "Click on First Product on PLP Failed.");
			System.out.println(e);
			throw e;
		}
		return new PDPPage(driver);	
	}
	
}
