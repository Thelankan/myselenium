package loft.pageobjects;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Set;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.asserts.SoftAssert;

import com.ascena.extentreport.ExtentTestManager;
import com.ascena.utilities.ExcelUtils;
import com.ascena.utilities.Utils;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

public class PaymentPage extends Utils {

	WebDriver driver;
	ExtentTest test;
	SoftAssert softassert;
	TestSoftAssert softAssert;

	public PaymentPage(WebDriver driver) {
		this.test = ExtentTestManager.getTest();
		this.driver = driver;
		PageFactory.initElements(driver, this);
		commFunc = new CommonFunctions(driver);
	}

	@FindBy(xpath = "//*[@name='payment-type' and @value='cc']")
	public WebElement selectCCOption;

	@FindBy(xpath = "//*[@name='payment-type' and @value='paypal']")
	public WebElement selectPayPalOption;

	@FindBy(xpath = "//*[@id='paypal-button']//*[contains(text(),'Check out')]")
	public WebElement clickPayPalCheckout;

	@FindBy(xpath = "//*[@name='payment-type' and @value='masterpass']")
	public WebElement selectMatserpassOption;

	@FindBy(xpath = "//*[@class='masterpass payment-section active']/a[1]/img")
	public WebElement clickMatserpassCheckout;

	@FindBy(id = "cardNumber")
	public WebElement cardNumBox;

	@FindBy(id = "cvvCode")
	public WebElement cvvBox;

	@FindBy(xpath = "//*[contains(text(),'Wallet')]")
	public WebElement selectWalletLink;

	@FindBy(xpath = "//*[@class='address-book wallet']//ul/li/label[contains(text(),'Visa')]")
	public WebElement selectCardFromWalletOverlay;

	@FindAll({ @FindBy(id = "exp_month"), @FindBy(id = "billing-exp-month-dropdown") })
	public WebElement expMonthDD;

	@FindAll({ @FindBy(id = "exp_year"), @FindBy(id = "billing-exp-year-dropdown") })
	public WebElement expYearDD;

	@FindBy(xpath = "//div[@class='custom-checkbox mouse-focus-input']")
	public WebElement selectSameAsShippingAddress;

	@FindBy(css = "#billingFirstName")
	public WebElement enterBillingFirstName;

	@FindBy(css = "#billingLastName")
	public WebElement enterBillingLastName;

	@FindBy(css = "#billingAddress")
	public WebElement enterBillingAddress;

	@FindBy(xpath = "//*[@id='payment-info-form1]//ul/li[2]/label[2]/span")
	public WebElement poBoxAddress;

	@FindBy(css = "#address_2")
	public WebElement enterBillingAddressOptional;

	@FindBy(css = "#billingZip")
	public WebElement enterBillingZip;

	@FindBy(css = "#billingCity")
	public WebElement enterBillingCity;

	@FindBy(css = "#billingState-span")
	public WebElement enterBillingState;

	@FindBy(css = "#billingPhone")
	public WebElement enterBillingPhone;

	@FindBy(xpath = "//*[@class='mouse-focus-input']/input")
	public WebElement reviewOrderBtn;

	@FindBy(xpath = "//a[@class='gc-text']")
	public WebElement selectGiftCardOption;

	@FindBy(xpath = "//*[@id='input-gift-card']/label[1]/input")
	public WebElement enterGiftCard;

	@FindBy(xpath = "//*[@id='input-gift-card']/label[2]/input")
	public WebElement enterGCPinOrEmail;

	@FindBy(xpath = "//*[@id='input-gift-card']//input[@value='Apply']")
	public WebElement clickGCApply;

	@FindBy(xpath = "//*[contains(text(),'Check Gift Card Balance')]")
	public WebElement checkGiftCardBalance;

	@FindBy(id = "sparkred-iframe")
	public WebElement ccFrame;
	
	@FindBy(id = "paypalLogo")
	public WebElement paypalLogo;

	// Paypal start
	@FindBy(xpath = "//*[@id='loginSection']//a")
	public WebElement paypalSignin;

	@FindBy(css = "input#email")
	public WebElement paypalEmail;

	@FindBy(css = "input#password")
	public WebElement paypalPassword;

	@FindBy(id = "btnNext")
	public WebElement paypalNext;

	@FindBy(id = "btnLogin")
	public WebElement paypalLogin;
	
	@FindBy(css = ".paypal-option")
	public WebElement paypalRadioBtn;
	
	@FindBy(css = "#paypal-button")
	public WebElement paypalcheckoutButton;

	@FindBy(id = "confirmButtonTop")
	public WebElement paypalConfirm;
	// Paypal end

	public PaymentPage selectCreditCard() {
		try {
			commFunc.clickWhenReady(selectCCOption, 3);

			test.log(Status.PASS, "PayPal option on billing page selected successfully.");
		} catch (Exception e) {
			test.log(Status.FAIL, "PayPal option on page not selected.");
			throw e;
		}
		return this;
	}

	public PaymentPage selectPayPal() {
		try {
			commFunc.clickWhenReady(selectPayPalOption, 3);

			test.log(Status.PASS, "PayPal option on billing page selected successfully.");
		} catch (Exception e) {
			test.log(Status.FAIL, "PayPal option on page not selected.");
			throw e;
		}
		return this;
	}

	public PaymentPage clickPayPalCheckout() {
		try {
			commFunc.clickWhenReady(clickPayPalCheckout, 3);

			test.log(Status.PASS, "PayPal option on billing page selected successfully.");
		} catch (Exception e) {
			test.log(Status.FAIL, "PayPal option on page not selected.");
			throw e;
		}
		return this;
	}

	public PaymentPage selectMasterpass() {
		try {
			commFunc.clickWhenReady(selectMatserpassOption, 3);

			test.log(Status.PASS, "Masterpass option on billing page selected successfully.");
		} catch (Exception e) {
			test.log(Status.FAIL, "Masterpass option on page not selected.");
			throw e;
		}
		return this;
	}

	public PaymentPage clickMasterpassCheckout() {
		try {
			commFunc.clickWhenReady(clickMatserpassCheckout, 3);

			test.log(Status.PASS, "Masterpass option on billing page selected successfully.");
		} catch (Exception e) {
			test.log(Status.FAIL, "Masterpass option on page not selected.");
			throw e;
		}
		return this;
	}

	public PaymentPage clickWallet() {
		try {
			commFunc.clickWhenReady(selectWalletLink, 3);

			test.log(Status.PASS, "Wallet link on billing page clicked successfully.");
		} catch (Exception e) {
			test.log(Status.FAIL, "Wallet link on page not clicked.");
			throw e;
		}
		return this;
	}

	public PaymentPage selectCardFromOverlay() {
		try {

			commFunc.clickWhenReady(selectCardFromWalletOverlay, 2);
			test.log(Status.PASS, "Card on wallet overlay selected successfully.");
		} catch (Exception e) {
			test.log(Status.FAIL, "Card on wallet overlay not seelcted.");
			throw e;
		}
		return this;
	}

	/* Clicking on Review order button */
	public ReviewOrderPage clickReviewOrder() {
		try {
			commFunc.clickWhenReady(reviewOrderBtn, 6);
			commFunc.waitForSeconds(3);
			commFunc.switchToDefaultContent();
			test.log(Status.PASS, "Review Your Order button clicked successfully.");
		} catch (Exception e) {
			test.log(Status.FAIL, "Click on Review Your Order button Failed.");
			throw e;
		}
		return new ReviewOrderPage(driver);
	}

	/*Entering Card details, exipry month, expiry year and CVV for Guest user*/
	public PaymentPage enterGuestUserCreditCardData(String cardType)
	{
		if(env.equalsIgnoreCase("prod"))
		{	commFunc.switchToFrame(ccFrame); }
		else {
			if (commFunc.isDisplayed(ccFrame, 5))
			{ commFunc.switchToFrame(ccFrame); }
		}
		enterCreditCardNum(cardType).selectExpiryMonth().selectExpiryYear().enterCVV();
		return this;	
	}
	
	/*Entering Card details, exipry month, expiry year for Registered user*/
	public PaymentPage enterRegUserCreditCardData(String cardType)
	{
		if(env.equalsIgnoreCase("prod"))
		{	commFunc.switchToFrame(ccFrame); }
		else {
			if (commFunc.isDisplayed(ccFrame, 5))
			{ commFunc.switchToFrame(ccFrame); }
		}
		enterCreditCardNum(cardType).selectExpiryMonth().selectExpiryYear();
		return this;	
	}
	
	/*Fetching card number from Excel based on card type and entering the same*/
	public PaymentPage enterCreditCardNum(String cardType)
	{
		int rowNum = getCardTypeRowNumber(cardType);
		String ccNum = ExcelUtils.getCellData("CreditCard", rowNum, 1);
		try {
			if (commFunc.isDisplayed(cardNumBox, 5))
			{
				for(int index = 0; index<ccNum.length(); index++)
				{
					cardNumBox.sendKeys(Character.toString(ccNum.charAt(index)));
				}
			}
			commFunc.waitForSeconds(2);
			test.log(Status.PASS, "Entered Card Number successfully");
		} catch (Exception e) {
			test.log(Status.FAIL, "Entering card numver Failed.");
			throw e;
		}
		return this;	
	}

	/* Selecting Random Expiry Month */
	public PaymentPage selectExpiryMonth() {
		try {
			int index = commFunc.getRandomInteger(commFunc.getAllDropDownValues(expMonthDD).size());
			commFunc.selectUsingIndex(expMonthDD, index);
			test.log(Status.PASS, "Expiry Month selected successfully");
		} catch (Exception e) {
			test.log(Status.FAIL, "Expiry month selection Failed");
			throw e;
		}
		return this;
	}

	/* Selecting Expiry Year with Logic current year +1 */
	public PaymentPage selectExpiryYear() {
		try {
			Calendar now = Calendar.getInstance();
			int year = now.get(Calendar.YEAR) + 1;
			commFunc.selectUsingVisibleText(expYearDD, Integer.toString(year));
			test.log(Status.PASS, "Expiry Year selected successfully");
		} catch (Exception e) {
			test.log(Status.FAIL, "Expiry Year selection Failed");
			throw e;
		}
		return this;
	}

	/* Entering CVV number as : 123 everytime */
	public PaymentPage enterCVV() {
		try {
			commFunc.sendWhenReady(cvvBox, "123", 5);
			test.log(Status.PASS, "Entered CVV Number successfully");
		} catch (Exception e) {
			test.log(Status.FAIL, "Entering CVV numver Failed.");
			throw e;
		}
		return this;
	}

	public PaymentPage enterBillingAddress() {

		String BFName = ExcelUtils.getCellData("Address", 1, 1);
		String BLName = ExcelUtils.getCellData("Address", 1, 2);
		String BAddress = ExcelUtils.getCellData("Address", 1, 3);
		String BAddressOpt = ExcelUtils.getCellData("Address", 1, 5);
		String BZip = ExcelUtils.getCellData("Address", 1, 6);
		String BState = ExcelUtils.getCellData("Address", 1, 7);
		String BCity = ExcelUtils.getCellData("Address", 1, 8);
		String BPhone = ExcelUtils.getCellData("Address", 1, 9);
		try {
			enterBillingFirstName.click();
			enterBillingFirstName.sendKeys(BFName);
			enterBillingLastName.click();
			enterBillingLastName.sendKeys(BLName);
			enterBillingAddress.click();
			enterBillingAddress.sendKeys(BAddress);
			enterBillingAddressOptional.click();
			enterBillingAddressOptional.sendKeys(BAddressOpt);
			enterBillingZip.click();
			enterBillingZip.sendKeys(BZip);
			enterBillingCity.click();
			enterBillingCity.sendKeys(BCity);
			enterBillingState.click();
			enterBillingState.sendKeys(BState);
			enterBillingPhone.click();
			enterBillingPhone.sendKeys(BPhone);

			test.log(Status.PASS, "User able to enter billing address.");
		} catch (Exception e) {
			test.log(Status.FAIL, "Entering billing address on payment page failed.");
			throw e;
		}
		return this;
	}

	public PaymentPage selectPOAddress() {
		try {
			poBoxAddress.click();

			test.log(Status.PASS, "PO Address chebox on payment page clicked successfully.");
		} catch (Exception e) {
			test.log(Status.FAIL, "PO Address chebox on payment page not clicked.");
			throw e;
		}
		return this;
	}

	public PaymentPage selectGiftCardOption() {
		try {
			selectGiftCardOption.click();

			test.log(Status.PASS, "GiftCard option on payment page clicked successfully.");
		} catch (Exception e) {
			test.log(Status.FAIL, "GiftCard option on payment page not clicked.");
			throw e;
		}
		return this;
	}

	public PaymentPage enterGiftCard() {

		String GCardNum = ExcelUtils.getCellData("GC", 2, 0);
		String GCPin = ExcelUtils.getCellData("GC", 2, 1);
		try {
			enterGiftCard.click();
			enterGiftCard.sendKeys(GCardNum);
			enterGCPinOrEmail.click();
			enterGCPinOrEmail.sendKeys(GCPin);
			test.log(Status.PASS, "User able to enter GiftCard details on payment page.");
		} catch (Exception e) {
			test.log(Status.FAIL, "Entering GiftCard details on payment page failed.");
			throw e;
		}
		return this;
	}

	public int getCardTypeRowNumber(String cardType) {
		int rowNum = 0;
		switch (cardType) {
		case "Visa":
			rowNum = 2;
			break;
		case "MasterCard":
			rowNum = 1;
			break;
		case "PLCC":
			rowNum = 3;
			break;
		case "CBCC":
			rowNum = 4;
			break;
		case "American Express":
			rowNum = 5;
			break;
		case "Discover":
			rowNum = 6;
			break;
		case "Diners":
			rowNum = 7;
			break;
		case "JCB":
			rowNum = 8;
			break;
		}
		return rowNum;
	}

	/*
	 * public PaymentPage enterCreditCardDetails(String cardType) {
	 * ExcelUtils.getRowNumber("CreditCard", cardType);
	 * commFunc.sendWhenReady(cardNumBox, Excel, timeout); return this; }
	 */

	// To enter Paypal details
	public void enterPaypalDetailsAndProceed(WebDriver driver) throws Exception {
		
		
		ExtentTestManager.getTest().log(Status.INFO, "*********Enter PayPal Payment***********");
		
		Set<String> PayPalWindow =driver.getWindowHandles();
		
		if(PayPalWindow.size()>1){
			//shippingpage.paypalLogin.click();
			if (!env.equalsIgnoreCase("prod"))
			{

			String paypalUserEmail= ExcelUtils.getCellData("Paypal", 2, 0);
			String paypalUserPassword =ExcelUtils.getCellData("Paypal", 2, 1);
			
			ExtentTestManager.getTest().log(Status.INFO, "PayPal Window Displayed with email" +  paypalUserEmail);
			ExtentTestManager.getTest().log(Status.INFO, "PayPal Window Displayed with password" +  paypalUserPassword);
			}
			ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
			driver.switchTo().window(tabs.get(1));
			driver.manage().window().maximize();
			//Utils.Desktop.waitForPageLoaded(driver);
//			
			commFunc.waitForPageLoaded();
			
			String PaypalWindowTitle =driver.getTitle();
			if (PaypalWindowTitle.contains("PayPal Checkout")==true){
				ExtentTestManager.getTest().log(Status.INFO, "PayPal Window Displayed");

				commFunc.waitForSeconds(3);
//				//PayPal User Email Entry
				softAssert.softAssertTrue(PaypalWindowTitle.equalsIgnoreCase(PaypalWindowTitle),
						"Paypal window is not displayed"); 
		
}
			
			if(Utils.env.equalsIgnoreCase("prod")){
			ExtentTestManager.getTest().log(Status.INFO, "PayPal Window for Prod is Displayed");
			}

						
			
			if(!env.equalsIgnoreCase("prod")){
			
			paypalSignin.click();
			commFunc.waitForSeconds(4);
			paypalEmail.sendKeys(ExcelUtils.getCellData("Paypal", 2, 0));
			paypalNext.click();
			paypalPassword.sendKeys(ExcelUtils.getCellData("Paypal", 2, 1));
			//paypalContLogin.click();
			//paypalNext.click();
			
			paypalLogin.click();
			commFunc.waitForSeconds(4);
			paypalConfirm.click();
			//commFunc.waitForSeconds(4);
			//driver.switchTo().defaultContent();
			driver.switchTo().window(tabs.get(0));
			commFunc.waitForSeconds(3);
			}
			
		}
	
}
	// To enter Paypal details end

	public String getCardTypeGuestUser()
	{
		String cardType= null;
		if(brand.equalsIgnoreCase("AT")){ cardType="Visa";}
		else if(brand.equalsIgnoreCase("LT")){ cardType="Discover";}
		else if(brand.equalsIgnoreCase("LG")){ cardType="American Express";}
		return cardType;
	}
	
	public String getCardTypeRegUser()
	{
		String cardType= null;
		if(brand.equalsIgnoreCase("AT")){ cardType="Visa";}
		else if(brand.equalsIgnoreCase("LT")){ cardType="Diners";}
		else if(brand.equalsIgnoreCase("LG")){ cardType="MsterCard";}
		return cardType;
	}

	
	/*Switching to PayPal window to access PayPal window*/
	public PaymentPage navigateToPaypalWindow()
	{
		String mainWindow = commFunc.getWindowId();
		Set<String> windows =  commFunc.getAllOpenedWindows();
		
		for (String window : windows)
		{
			if(!window.equalsIgnoreCase(mainWindow)) {
			commFunc.switchToWindow(window); 
			driver.manage().window().maximize();}
		}
		return this;
	}
	
	/*checking whether the card is already associated with user account*/
	public boolean isPaypalWindowWithLogoDisplayed()
	{
		commFunc.waitForSeconds(2);
		return commFunc.isDisplayed(paypalLogo,6);	
	}
	
	/*Login to PayPal account*/
	public PaymentPage loginToPayPalAccount()
	{
		String email = ExcelUtils.getCellData("Paypal", 2, 0);
		String pwd = ExcelUtils.getCellData("Paypal", 2, 1);
		clickLogInBtnToEnterEmail().enterEmailToLoginPayPal(email).clickNextBtnToEnterPwd()
		.enterPwdToLoginPaypal(pwd).clickLogInBtnToSubmit();
		return this;
	}
	
	/*Clicking on PayPal Log In button to start entering credentials */
	public PaymentPage clickLogInBtnToEnterEmail()
	{
		try {
			commFunc.clickWhenReady(paypalSignin, 6);
			test.log(Status.PASS, "Log In Button to enter Email in Pay Pal window clicked successfully.");
		} catch (Exception e) {
			test.log(Status.FAIL, "Click on Log In Button to enter Email in Pay Pal window Failed.");
			throw e;
		}
		return this;	
	}
	
	/*Fetching Email to enter in PayPal login window from Excel and entering the same*/
	public PaymentPage enterEmailToLoginPayPal(String email)
	{
		try {
			commFunc.waitForSeconds(2);
			commFunc.sendWhenReady(paypalEmail, email, 6);		
			test.log(Status.PASS, "Entered Email in Pay Pal window successfully");
		} catch (Exception e) {
			test.log(Status.FAIL, "Entering Email in Pay Pal window Failed.");
			throw e;
		}
		return this;	
	}
	
	/*Clicking on PayPal Next Button to enter password*/
	public PaymentPage clickNextBtnToEnterPwd()
	{
		try {
			commFunc.clickWhenReady(paypalNext, 6);
			test.log(Status.PASS, "Next Button to enter password in Pay Pal window clicked successfully.");
		} catch (Exception e) {
			test.log(Status.FAIL, "Click on Next Button to enter Password in Pay Pal window Failed.");
			throw e;
		}
		return this;	
	}
	
	/*Fetching Password to enter in PayPal login window from Excel and entering the same*/
	public PaymentPage enterPwdToLoginPaypal(String pwd)
	{
		try {
			commFunc.sendWhenReady(paypalPassword, pwd, 4);		
			test.log(Status.PASS, "Entered Password in Pay Pal window successfully");
		} catch (Exception e) {
			test.log(Status.FAIL, "Entering Password in Pay Pal window Failed.");
			throw e;
		}
		return this;	
	}
	
	/*Clicking on PayPal Log In button to submit credentials and get into the PayPal account */
	public PaymentPage clickLogInBtnToSubmit()
	{
		try {
			commFunc.clickWhenReady(paypalLogin, 6);
			test.log(Status.PASS, "Clicked on Log In Button to submit password successfully.");
		} catch (Exception e) {
			test.log(Status.FAIL, "Click on Log In Button to submit password Failed.");
			throw e;
		}
		return this;	
	}
	
	/*Clicking on Continue button to close the PayPal window */
	public PaymentPage clickPayPalContinueBtn()
	{
		try {
			commFunc.isDisplayed(paypalConfirm, 20);
			commFunc.waitForSeconds(5);
			commFunc.clickWhenReady(paypalConfirm, 4);
			test.log(Status.PASS, "Clicked on continue button to close the paypal window successfully.");
		} catch (Exception e) {
			test.log(Status.FAIL, "Click on continue button to close the paypal window Failed.");
			throw e;
		}
		return this;	
	}
	
	public PaymentPage selectPaypalPaymentOption()
	{
		try {
			commFunc.waitForSeconds(2);
			commFunc.clickWhenReady(paypalRadioBtn, 7);
			test.log(Status.PASS, "Selected Paypal as payment option successfully");
		} catch (Exception e) {
			test.log(Status.FAIL, "Selection for Paypal as payment option Failed.");
			throw e;
		}
		
		return this;
	}
	
	public PaymentPage clickPaypalCheckout()
	{
		try {
			commFunc.waitForSeconds(2);
			commFunc.clickWhenReady(paypalcheckoutButton, 5);
			test.log(Status.PASS, "Clicked on PayPal checkout on Payment page successfully.");
		} catch (Exception e) {
			test.log(Status.FAIL, "Click on PayPal checkout on Payment page Failed.");
			throw e;
		}	
		return this;
	}
}
