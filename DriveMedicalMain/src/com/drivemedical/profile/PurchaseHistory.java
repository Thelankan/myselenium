package com.drivemedical.profile;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import com.drivemedical.utilgeneric.BaseTest;
import com.drivemedical.utilgeneric.GetData;

public class PurchaseHistory extends BaseTest{

	String orderNum;
	Float actPrices[];
	boolean decide;
	
	@Test(groups="reg")
	public void TC00_plcaeOrder() throws Exception
	{
		log.info("log out from application");
		p.logout(xmlTest);
		log.info("Log in to the application");
		p.navigateToLoginPage();
		p.logIn(xmlTest);
		
		log.info("navigate to Order Review page");
		checkout.navigateToOrderReviewPage();
		log.info("click on place order");
		l1.getWebElements("OrderReview_PlaceOrder_Btn", "Checkout//OrderReview.properties").get(0).click();
		log.info("fetch order number");
		orderNum=l1.getWebElement("OrderConfir_OrderNum", "Checkout//OrderConfirmation.properties").getText().split("#")[1];
	}
	
	@Test(groups="reg",description="DMED-010 DRM-1248")
	public void displayOrder() throws Exception
	{
		log.info("navigate to frequently ordered items page");
		act.moveToElement(l1.getWebElement("Header_Reg_MyAct_Link", "Shopnav//header.properties")).perform();
		log.info("click on drop ship address");
		l1.getWebElement("Header_FrequentlyOrders_Link", "Shopnav//header.properties").click();
		log.info("verify first order");
		sa.assertTrue(gVar.assertVisible("PH_Heading", "Profile//PurchaseHisotry.properties"),"Frequently Ordered items heading");
		sa.assertEquals(gVar.assertEqual("PH_ProductNames", "Profile//PurchaseHisotry.properties",0),s.prodnames.get(0),"Frequently Ordered first item name");
		sa.assertAll();
	}
	
	@Test(groups="reg",description="DMED-010 DRM-963,964,965")
	public void verifyUI() throws Exception
	{
		log.info("verify heading");
		sa.assertTrue(gVar.assertVisible("PH_Heading", "Profile//PurchaseHisotry.properties"),"Frequently Ordered items heading");
		log.info("verify filter by date drop down");
		sa.assertTrue(gVar.assertVisible("PH_Date_Filetr", "Profile//PurchaseHisotry.properties"),"Filter by date");
		log.info("verify price filter");
		sa.assertTrue(gVar.assertVisible("PH_Price_Filter", "Profile//PurchaseHisotry.properties"),"price filter");
		log.info("verify Nothing Slected Drop Down");
		sa.assertTrue(gVar.assertVisible("PH_NothingSelected_DD", "Profile//PurchaseHisotry.properties"),"Nothing Selected Drop Down");
		log.info("verify Serach box");
		sa.assertTrue(gVar.assertVisible("PH_SearchBox", "Profile//PurchaseHisotry.properties"),"Search Box");
		log.info("verify Serach button");
		sa.assertTrue(gVar.assertVisible("PH_Search_Btn", "Profile//PurchaseHisotry.properties"),"Search Button");
		log.info("verify Sort By DD Top");
		sa.assertTrue(gVar.assertVisible("PH_SortBy_DD", "Profile//PurchaseHisotry.properties"),"Sort By DD TOP");
		log.info("verify Sort By DD bottom");
		sa.assertTrue(gVar.assertVisible("PH_SortBY_DD_Bottom", "Profile//PurchaseHisotry.properties"),"Sort By DD Bottom");
		
		log.info("Sort by LabelsPagination,Pagination Bar Results");
		for(int i=0;i<2;i++) {
			sa.assertTrue(gVar.assertVisible("PH_SortBy_Label", "Profile//PurchaseHisotry.properties",i),"Sort By Label");
			sa.assertTrue(gVar.assertVisible("PH_Pagination_Bar_Results_Text", "Profile//PurchaseHisotry.properties",i),"Search bar Results");
			sa.assertTrue(gVar.assertVisible("PH_Pagination", "Profile//PurchaseHisotry.properties",i),"Pagination");
		}
		
		List<WebElement> totProds=l1.getWebElements("PH_Tot_Products", "Profile//PurchaseHisotry.properties");
		for(int i=0;i<totProds.size();i++) {
			log.info("Item Image");
			sa.assertTrue(gVar.assertVisible("PH_Item_Image", "Profile//PurchaseHisotry.properties",i),"Item Image");
			log.info("Item Name");
			sa.assertTrue(gVar.assertVisible("PH_Item_Name", "Profile//PurchaseHisotry.properties",i),"Item Name");
			log.info("Item Code");
			sa.assertTrue(gVar.assertVisible("PH_Item_Codes", "Profile//PurchaseHisotry.properties",i),"Item Codes");
			log.info("UOM");
			sa.assertTrue(gVar.assertVisible("PH_Item_Unit", "Profile//PurchaseHisotry.properties",i),"UOM");
			log.info("Varients");
			sa.assertTrue(gVar.assertVisible("PH_Variant_Opt", "Profile//PurchaseHisotry.properties",i),"Varients");
			log.info("Prices");
			sa.assertTrue(gVar.assertVisible("PH_Prod_Prices", "Profile//PurchaseHisotry.properties",i),"Prices");
		}
		
		sa.assertAll();
	}
	
	@Test(groups="reg",description="DMED-010 DRM-955")
	public void sortPriceASC() throws Exception
	{
		log.info("select price Ascending option");
		new Select(l1.getWebElement("PH_Sort_DD", "Profile//PurchaseHisotry.properties")).selectByValue("priceAsc");
		log.info("fetch all the prices");
		List<WebElement> prices=l1.getWebElements("PH_Prod_Prices", "Profile//PurchaseHisotry.properties");
		
		int i=0;
		for(WebElement ele:prices) {
			
			actPrices[i]=Float.parseFloat(ele.getText().split("$")[1]);
			i++;
		}
		
		for(int j=0;j<actPrices.length;j++) {
			for(int k=1;k<actPrices.length;k++) {
			if(actPrices[j]<=actPrices[k]) {
				decide=true;
			} else {
				decide=false;
			}
			}
		}
		
		sa.assertTrue(decide);
		sa.assertAll();
		
	}
	
	@Test(groups="reg",description="DMED-010 DRM-956")
	public void sortPriceDSC() throws Exception
	{
		log.info("select price Ascending option");
		new Select(l1.getWebElement("PH_Sort_DD", "Profile//PurchaseHisotry.properties")).selectByValue("priceAsc");
		log.info("fetch all the prices");
		List<WebElement> prices=l1.getWebElements("PH_Prod_Prices", "Profile//PurchaseHisotry.properties");
		
		int i=0;
		for(WebElement ele:prices) {
			
			actPrices[i]=Float.parseFloat(ele.getText().split("$")[1]);
			i++;
		}
		
		for(int j=0;j<actPrices.length;j++) {
			for(int k=1;k<actPrices.length;k++) {
			if(actPrices[j]>=actPrices[k]) {
				decide=true;
			} else {
				decide=false;
			}
			}
		}
		
		sa.assertTrue(decide);
		sa.assertAll();
		
	}
	
	@Test(groups="reg",description="DMED-010 DRM-1045")
	public void searchByOrderNum() throws Exception
	{
		log.info("Enter order number");
		l1.getWebElement("PH_SearchBox","Profile//PurchaseHisotry.properties").sendKeys(orderNum);
		l1.getWebElement("PH_Search_Btn","Profile//PurchaseHisotry.properties").click();
		log.info("verify results");
		sa.assertEquals(gVar.assertEqual("PH_ProductNames", "Profile//PurchaseHisotry.properties",0),s.prodnames.get(0),"search results products");
		sa.assertAll();
	}
	
	@Test(groups="reg",description="DMED-010 DRM-967")
	public void addPurchaseHistoryItemToCart() throws Exception
	{
		log.info("clear cart items");
		cart.clearCart();
		log.info("navigate to frequently ordered items page");
		act.moveToElement(l1.getWebElement("Header_Reg_MyAct_Link", "Shopnav//header.properties")).perform();
		log.info("click on drop ship address");
		l1.getWebElement("Header_FrequentlyOrders_Link", "Shopnav//header.properties").click();
		log.info("fetch first product name");
		List<WebElement> prodNames=l1.getWebElements("PH_Item_Name", "Profile//PurchaseHisotry.properties");
		log.info("fetch first product name");
		String firstProdName=prodNames.get(0).getText();
		log.info("click on first product name");
		prodNames.get(0).click();
		log.info("click on add to cart");
		l1.getWebElement("PDP_AddToCart_button", "Shopnav//PDP.properties").click();
		log.info("click on cart link from header");
		l1.getWebElement("Header_Cart_Link", "Shopnav//header.properties").click();
		log.info("verify added product");
		l1.getWebElement("Cart_Prod_Name", "Cart//Cart.properties").click();
		sa.assertTrue(gVar.assertVisible("Cart_Prod_Name", "Cart//Cart.properties"),"product name");
		sa.assertEquals(firstProdName, gVar.assertEqual("Cart_Prod_Name", "Cart//Cart.properties"));
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="DMED-010 DRM-961")
	public void productNameAscendingOrder() throws Exception
	{
		log.info("navigate to frequently ordered items page");
		act.moveToElement(l1.getWebElement("Header_Reg_MyAct_Link", "Shopnav//header.properties")).perform();
		l1.getWebElement("Header_FrequentlyOrders_Link", "Shopnav//header.properties").click();
		
		//For both Ascending and Descending order
		for (int k = 3; k <= 4; k++) 
		{
			System.out.println("LOOP K:-"+ k);
			ArrayList<String> proNamesList = new ArrayList<String>();

			log.info("Select the sort by NAme options");
			WebElement sortByDD = l1.getWebElement("PH_Sort_DD", "Profile//PurchaseHisotry.properties");
			Select sel = new Select(sortByDD);
			String sortByOptionText = GetData.getDataFromExcel("//data//GenericData.xls", "PurchaseHistory", k, 0);
			sel.selectByVisibleText(sortByOptionText);
			Thread.sleep(3000);
			
			log.info("Collect the Product names from all page");
			List<WebElement> proNames = l1.getWebElements("PH_ProductNames", "Profile//PurchaseHisotry.properties");
			System.out.println("Total number of Products :- " + proNames.size());
			for (int i = 0; i < proNames.size(); i++) 
			{
				System.out.println("LOOP i:-"+ i);
				proNamesList.add(proNames.get(i).getText());
			}
			
			System.out.println("proNamesList:- " + proNamesList);
			
			log.info("Create a copy of product names list");
			ArrayList<String> copyOfproNamesList = new ArrayList<String>();
			copyOfproNamesList = (ArrayList<String>) proNamesList.clone();
			
			log.info("Sort the copyOfproNamesList");
			if (k==3) 
			{
				//For Ascending order
				Collections.sort(copyOfproNamesList);
			}
			else 
			{
				//For Descending order
				Collections.reverse(copyOfproNamesList);
			}
			
			System.out.println("proNamesList after Sort:- " + proNamesList);
			
			
			log.info("Compair the product names");
			sa.assertEquals(proNames, copyOfproNamesList, "Compair product names");
			
		}
		
		sa.assertAll();
	}
}
