package com.drivemedical.profile;

import org.testng.annotations.Test;
import org.testng.xml.XmlTest;

import com.drivemedical.utilgeneric.BaseTest;

public class MyAccountHome extends BaseTest{
	
	@Test(groups={"reg"},description="OOTB-055-DRM-596")
	public void leftNavSignOut_1(XmlTest xmlTest) throws Exception
	{
		log.info("log in to the application");
		p.navigateToLoginPage();
		p.logIn(xmlTest);
		log.info("navigate to my account page");
		if(xmlTest.getParameter("executingIn").equalsIgnoreCase("mobile") || xmlTest.getParameter("executingIn").equalsIgnoreCase("Tablet")) {
			log.info("Click on Hamburger menu");
			l1.getWebElement("Header_Hamburger_Menu","Shopnav\\header.properties").click();
			log.info("Click on contact details link");
			l1.getWebElement("Hamburger_ContactDetails_Link","Shopnav\\header.properties").click();
			
		} else {
		act.moveToElement(l1.getWebElement("Header_Reg_MyAct_Link", "Shopnav\\header.properties")).perform();
		l1.getWebElement("Header_ContactDetails_Link", "Shopnav\\header.properties").click();
		
		}
		log.info("sign out from left nav");
		l1.getWebElement("Leftnav_SignOut_Link", "Profile\\leftnav.properties").click();
		log.info("Verify navigation back to home page");
		sa.assertTrue(l1.getWebElement("Home_Identifier", "Shopnav\\header.properties").isDisplayed());
		sa.assertTrue(l1.getWebElement("Header_SignIn_LINK", "Shopnav\\header.properties").isDisplayed());
	}

}
