package com.drivemedical.profile;

import java.util.List;

//DRM-838,841 - can not be automated.
//DRM-834,835

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;
import org.testng.xml.XmlTest;

import com.drivemedical.utilgeneric.BaseTest;
import com.drivemedical.utilgeneric.GetData;
//DRM-834,835
public class QuickOrder extends BaseTest{

	@Test(groups={"reg"},description="DMED-521 DRM-829")
	public void meterialNumFun(XmlTest xmlTest) throws Exception
	{
		log.info("Log in to application");
		p.navigateToLoginPage();
		p.logIn(xmlTest);
		log.info("click on quick order link from header");
		l1.getWebElement("Header_Reg_QuickOrder_Link","Shopnav//header.properties").click();
		List<WebElement> meterialNum;
		for(int i=0;i<25;i++) {
			log.info("get meterial number text boxes");
			meterialNum=l1.getWebElements("QuickOrder_MeterialNum", "Profile//QuickOrder.properties");
			log.info("Enter meterial number");
			meterialNum.get(i).sendKeys(GetData.getDataFromExcel("data//GenericData.xls", "QuickOrder", 1, 0));
		}
		log.info("fetch total meterial boxes");
		meterialNum=l1.getWebElements("QuickOrder_MeterialNum", "Profile//QuickOrder.properties");
		sa.assertEquals("25", meterialNum.size());
		log.info("click on clear form");
		l1.getWebElement("MeterialNum_ClearForm_Top", "Profile//QuickOrder.properties").click();
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="DMED-521 DRM-830,836")
	public void validAndInvalidMeterialNumbers() throws Exception
	{
		log.info("Enter valid and invalid meterial numbers");
		for(int i=0;i<3;i++) {
			
			log.info("Enter meterial numbers");
			int j=i+1;
			l1.getWebElements("QuickOrder_MeterialNum", "Profile//QuickOrder.properties").get(i).sendKeys(GetData.getDataFromExcel("data//GenericData.xls", "QuickOrder", j, 0));
			act.moveToElement(l1.getWebElements("QuickOrder_MeterialNum", "Profile//QuickOrder.properties").get(j)).perform();
			Thread.sleep(5000);
			if(i==0) {
			sa.assertTrue(gVar.assertVisible("QuickOrder_StockDetails", "Profile//QuickOrder.properties"),"Displayed product details");
			log.info("verify price");
			sa.assertTrue(gVar.assertVisible("QuickOrder_ProductPrice", "Profile//QuickOrder.properties",i),"verify price");
			
			
			} else if(i==1) {
				sa.assertTrue(gVar.assertVisible("QuickOrder_StockDetails", "Profile//QuickOrder.properties",i),"Displayed product details");
				sa.assertEquals(j, l1.getWebElements("QuickOrder_StockDetails", "Profile//QuickOrder.properties").size(),"total displayed products");
				log.info("verify price");
				sa.assertTrue(gVar.assertVisible("QuickOrder_ProductPrice", "Profile//QuickOrder.properties",i),"verify price");
				
			} else {
				log.info("Verify Error message");
				sa.assertTrue(gVar.assertVisible("QuickOrder_Error_Msg", "Profile//QuickOrder.properties"),"Error messages");
			}
			
			sa.assertAll();
		}
	}
	
	@Test(groups={"reg"},description="DMED-521 DRM-837")
	public void qytUpdate() throws Exception
	{
		log.info("verify quantity update");
		for(int i=0;i<2;i++) {
			List<WebElement> itemPrice=l1.getWebElements("QuickOrder_ProductPrice", "Profile//QuickOrder.properties");
			String actPrice=itemPrice.get(i).getText().split("$")[1];
			l1.getWebElements("QuickOrder_Qty","Profile//QuickOrder.properties").get(i).clear();
			l1.getWebElements("QuickOrder_Qty","Profile//QuickOrder.properties").get(i).sendKeys("2");
			act.moveToElement(l1.getWebElements("QuickOrder_MeterialNum", "Profile//QuickOrder.properties").get(i)).perform();
			int actpriceInt=new Integer(actPrice);
			actpriceInt=actpriceInt*2;
			String expTotPrice=l1.getWebElement("QuickOrder_TotalItemPrice", "Profile//QuickOrder.properties").getText();
			expTotPrice=expTotPrice.split("$")[1];
			log.info("verify price after quantity update");
			sa.assertEquals(actpriceInt+"", expTotPrice);
		}
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="DMED-521 DRM-831,832")
	public void UOMCheck() throws Exception
	{
		log.info("verify UOM");
		sa.assertTrue(gVar.assertVisible("QuickOrder_Unit_Select", "Profile//QuickOrder.properties"),"UOM drop down");
		log.info("verify default UOM measurement");
		String UOM=new Select(l1.getWebElement("QuickOrder_Unit_DD", "Profile//QuickOrder.properties")).getFirstSelectedOption().getText();
		sa.assertEquals(GetData.getDataFromExcel("data//GenericData.xls", "QuickOrder", 2, 2),UOM);
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="DMED-521 DRM-833")
	public void removeFun() throws Exception
	{
		for(int i=0;i<2;i++) {
			log.info("fetch total displayed products");
			int totProds=l1.getWebElements("QuickOrder_StockDetails", "Profile//QuickOrder.properties").size();
			log.info("click on remove link");
			l1.getWebElements("QuickOrder_Remove", "Profile//QuickOrder.properties").get(i).click();
			if(i==0) {
			log.info("verify total products");
			sa.assertEquals(totProds-1,l1.getWebElements("QuickOrder_StockDetails", "Profile//QuickOrder.properties").size());
			} else if(i==1) {
				log.info("verify products should not visible");
				sa.assertTrue(gVar.assertNotVisible("QuickOrder_StockDetails", "Profile//QuickOrder.properties"),"Quick order products");
				break;
			}
		}
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="DMED-521 DRM-839,840")
	public void addToCart() throws Exception
	{
		l1.getWebElements("QuickOrder_MeterialNum", "Profile//QuickOrder.properties").get(i).sendKeys(GetData.getDataFromExcel("data//GenericData.xls", "QuickOrder", 1, 0));
		act.moveToElement(l1.getWebElements("QuickOrder_MeterialNum", "Profile//QuickOrder.properties").get(1)).perform();
		Thread.sleep(5000);
		l1.getWebElement("QuickOrder_AddToCart_Top","Profile//QuickOrder.properties").click();
		sa.assertTrue(false,"issue in application");
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="DMED-521 DRM-834,835")
	public void validate() throws Exception
	{
		log.info("click on quick order link from header");
		l1.getWebElement("Header_Reg_QuickOrder_Link","Shopnav//header.properties").click();
		log.info("Enter SKU value");
		l1.getWebElements("QuickOrder_MeterialNum", "Profile//QuickOrder.properties").get(0).sendKeys(GetData.getDataFromExcel("data//GenericData.xls", "QuickOrder", 5, 0));
		act.moveToElement(l1.getWebElements("QuickOrder_MeterialNum", "Profile//QuickOrder.properties").get(1)).perform();
		Thread.sleep(5000);
		log.info("verify error message");
		sa.assertTrue(gVar.assertVisible("QuickOrder_Error_Msg", "Profile//QuickOrder.properties"),"Error message should display");
		l1.getWebElements("QuickOrder_MeterialNum", "Profile//QuickOrder.properties").get(0).sendKeys(GetData.getDataFromExcel("data//GenericData.xls", "QuickOrder", 4, 0));
		act.moveToElement(l1.getWebElements("QuickOrder_MeterialNum", "Profile//QuickOrder.properties").get(1)).perform();
		Thread.sleep(5000);
		sa.assertTrue(gVar.assertVisible("QuickOrder_PeoductsName", "Profile//QuickOrder.properties"),"Verify display of product name");
		sa.assertAll();
	}
	
}
