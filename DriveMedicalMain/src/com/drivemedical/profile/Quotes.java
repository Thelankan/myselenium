package com.drivemedical.profile;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import org.testng.xml.XmlTest;

import com.drivemedical.utilgeneric.BaseTest;
import com.drivemedical.utilgeneric.GetData;

public class Quotes extends BaseTest{

	/* Can not be automated Test Cases are DRM-1100*/
	
	String quoteID;
	static String reason;
	String quoteNameInCheckout;
	String proNameInPDP;
	String productPriceInPdp;
	
	@Test(groups={"reg"},description="DMED-535 DRM-1095,1101")
	public void TC00_quote(XmlTest xmlTest) throws Exception
	{
		log.info("log out from application");
		p.logout(xmlTest);
		
		log.info("Log in to the application");
		p.navigateToLoginPage();
		p.logIn(xmlTest);
		
		log.info("navigate to Order Review page");
		checkout.navigateToShipToPage();
		proNameInPDP = s.prodnames.get(0);
		productPriceInPdp = s.prodPrice.get(0);
		log.info("proNameInPDP:- "+ proNameInPDP);
		log.info("productPriceInPdp:- "+ productPriceInPdp);
		
		log.info("Request quote button should be disabled");
		sa.assertEquals(gVar.assertEqual("ShipTo_RequestQuote_Disabled", "Checkout//ShipTo.properties","disabled"), "true", "Verify the Request quote button");
		
		log.info("enter new address");
		l1.getWebElement("ShipTo_AddAddress_Link","Checkout//ShipTo.properties").click();
		checkout.addAddress(1);
		l1.getWebElement("ShipTo_Continue_button","Checkout//ShipTo.properties").click();
		log.info("click on next button");
		l1.getWebElement("ShippingMtd_Next_Btn","Checkout//ShippingMethod.properties").click();
		log.info("enter PO boc number");
		int randomNum = gVar.generateRandomNum();
		String PONumberVal=randomNum+"";
		log.info("PONumberVal:-"+PONumberVal);
		l1.getWebElement("PT_POBox_Textbox","Checkout//PaymentType.properties").clear();
		l1.getWebElement("PT_POBox_Textbox","Checkout//PaymentType.properties").sendKeys(PONumberVal);
		log.info("click on next button");
		l1.getWebElement("Next_Button","Checkout//PaymentType.properties").click();
		
		log.info("click on quote a request");
		l1.getWebElement("OrderReview_RequestQuote_Btn", "Checkout//OrderReview.properties").click();
		log.info("verify opened quote pop up");
		sa.assertTrue(gVar.assertVisible("OrderReview_Quote_Heading", "Checkout//OrderReview.properties"),"create quote heading");
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="DMED-535 DRM-1096,1099,1104,1107 DMED-545 DRM-1182")
	public void TC01_quoteReason() throws Exception 
	{
		log.info("verify max length of reason text box");
		sa.assertEquals(gVar.assertEqual("Quote_Reason_Textbox", "Checkout//OrderReview.properties", "maxlength"), "255");
		log.info("Collect the prepopulated QuoteId");
		quoteNameInCheckout = l1.getWebElement("CreateQuotePopup_QuoteId", "Checkout//OrderReview.properties").getAttribute("value");
		String quoteName= quoteNameInCheckout.split(" ")[1].trim();
		log.info("quoteName:- "+ quoteName);
		
		log.info("enter reason");
		reason = "Reason Entered";
		l1.getWebElement("Quote_Reason_Textbox", "Checkout//OrderReview.properties").sendKeys(reason);
		log.info("click on submit");
		l1.getWebElement("Quote_Submit_Btn", "Checkout//OrderReview.properties").click();
		log.info("fetch quote code");
		quoteID=l1.getWebElement("Quote_Id", "Checkout//OrderReview.properties").getText().replace("#", "").trim();
		log.info("quoteID:- "+ quoteID);
		
		sa.assertEquals(quoteName, quoteID, "Verify the Quote id value in both the popups");
		log.info("click yes on quote confirmation pop up");
		l1.getWebElement("Quote_Yes_Btn", "Checkout//OrderReview.properties").click();
		log.info("verify navigation to quotes page");
		sa.assertTrue(gVar.assertVisible("Quotes_Heading", "Profile//Quotes.properties"),"Quotes heading");
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="DMED-535 DRM-1098,1105")
	public void TC02_quoteStatus() throws Exception 
	{
		log.info("Verify recently submitted quotes code");
		sa.assertEquals(gVar.assertEqual("Quote_Code", "Profile//Quotes.properties"), 
				quoteID, "Verify the Quote ID");
		
		log.info("Verify recently submitted quotes status");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("Quote_Status", "Profile//Quotes.properties"), "Submitted", "Verify the Quote status");
		
		sa.assertAll();
	}
	
	/* Can not be automated it DMED-537 DRM-1166 */
	
	@Test(groups={"reg"},description="DMED-537 DRM-1163,1164,1165")
	public void TC03_quotesListingUI(XmlTest xmlTest) throws Exception
	{
		
		log.info("verify Quote Listing page UI");
		log.info("bread crumb");
		sa.assertTrue(gVar.assertVisible("Quote_Home_BreadCrumb_link", "Profile//Quotes.properties"),"Home bread crumb link");
		log.info("quotes bread crumb text");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("Breadcrumb_ActiveElement", "Generic//Generic.properties"), 
				GetData.getDataFromExcel("//data//GenericData.xls", "Quotes", 1, 2),"quotes bread crumb text");
		
		log.info("quotes Heading");
		sa.assertTrue(gVar.assertVisible("Quote_Heading", "Profile//Quotes.properties"),"quotes heading");
		log.info("sort by label");
		sa.assertTrue(gVar.assertVisible("Quote_SortBy_Top_Label", "Profile//Quotes.properties"),"sort by label top");
		log.info("sort by drop down top");
		sa.assertTrue(gVar.assertVisible("Quote_SortBy_DropDown_Top", "Profile//Quotes.properties"),"sort by drop down top");
		log.info("pagination results top");
		sa.assertTrue(gVar.assertVisible("Quote_Pagination_Results_Top", "Profile//Quotes.properties"),"pagination results top");
		log.info("quote listing tabel headings");
		
		if (BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("mobile") || BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("Tablet")) 
		{
			log.info("MOBILE VIEW");
			List<WebElement> quotesRows = l1.getWebElements("QuoteDetails_Rows_Mob", "Profile//Quotes.properties");
			
			for (int i = 0; i < quotesRows.size(); i++) 
			{
				log.info("Loop i:- "+ i);
				
				By byRefLabel = l1.getByReference("QuoteDetails_Labels_Mob", "Profile//Quotes.properties");
				By byRefQuoteId = l1.getByReference("QuoteDetails_QuoteLink", "Profile//Quotes.properties");
				
				WebElement rowElement = quotesRows.get(i);
				List<WebElement> labels = rowElement.findElements(byRefLabel);
				log.info("labels count:- "+ labels.size());
				int jrowNum =1;
				for (int j = 0; j < labels.size(); j++) 
				{
					log.info("Loop j:- "+ j);
					String labelElement = labels.get(j).getText();
					log.info("Labels:- "+labelElement);
					String expectedLAbel = GetData.getDataFromExcel("//data//GenericData.xls", "Quotes", jrowNum,0);
					sa.assertEquals(labelElement, expectedLAbel,"Verify the labels");
					jrowNum++;
				}
			}
		
		} 
		else 
		{
			List<WebElement> quoteHeadings=l1.getWebElements("Quote_Listing_Table", "Profile//Quotes.properties");
			int i=1;
			for(WebElement ele:quoteHeadings) 
			{
				sa.assertTrue(ele.isDisplayed(),"quotes table headings");
				String expectedLAbel = GetData.getDataFromExcel("//data//GenericData.xls", "Quotes", i,0);
				gVar.assertequalsIgnoreCase(ele.getText(),expectedLAbel,"verify with EXCEL data");
				i++;
			}
		}
		
		List<WebElement> nameLinks=l1.getWebElements("Quote_Name_Links", "Profile//Quotes.properties");
		i=0;
		for(WebElement ele:nameLinks) 
		{
			log.info("name links");
			sa.assertTrue(gVar.assertVisible("Quote_Name_Links", "Profile//Quotes.properties",i),"name link");
			log.info("quote Id");
			sa.assertTrue(gVar.assertVisible("Quote_Code", "Profile//Quotes.properties",i),"Quote Code");
			log.info("Date Updated");
			sa.assertTrue(gVar.assertVisible("Quote_Date_Updated", "Profile//Quotes.properties",i),"Date Updated");
			log.info("Status");
			sa.assertTrue(gVar.assertVisible("Quote_Status", "Profile//Quotes.properties",i),"Quote status");
			log.info("Documents");
			sa.assertTrue(gVar.assertVisible("Quote_Documnets", "Profile//Quotes.properties",i),"Documents");
		}
		
		log.info("sort by label");
		sa.assertTrue(gVar.assertVisible("Quote_SortBy_bottom_Label", "Profile//Quotes.properties"),"sort by label bottom");
		log.info("sort by drop down top");
		sa.assertTrue(gVar.assertVisible("Quote_SortBy_DropDown_bottom", "Profile//Quotes.properties"),"sort by drop down bottom");
		log.info("pagination results top");
		sa.assertTrue(gVar.assertVisible("Quote_Pagination_Results_bottom", "Profile//Quotes.properties"),"pagination results bottom");
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="DMED-537 DRM-1161")
	public void TC04_quotesStatus() throws Exception
	{
		log.info("verify quotes status");
		List<WebElement> status=l1.getWebElements("Quote_Status", "Profile//Quotes.properties");
		
		for (int i = 0; i < status.size(); i++) 
		{
			log.info("Loop:- " +i);
			String ele = status.get(i).getText();
			if(ele.equalsIgnoreCase("Pending") || ele.equalsIgnoreCase("rejected") || ele.equalsIgnoreCase("accepted")) 
			{
				sa.assertTrue(true);
			} 
			else 
			{
				sa.assertTrue(false,"Status is not mentianed in Testcase");
			}
		}
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="DMED-538 DRM-1167")
	public void TC05_quoteDetails() throws Exception 
	{
		log.info("click on Quote link");
		l1.getWebElement("Quote_Links", "Profile//Quotes.properties").click();
		log.info("verify quote status in Details page");
		sa.assertEquals(gVar.assertEqual("QuoteDetails_Status", "Profile//Quotes.properties"),"Submitted","Quote status in details page");
		
		log.info("Verify the Quote code in Breadcrumb");
		String actualBreadCrumb = gVar.assertEqual("Breadcrumb_ActiveElement", "Generic//Generic.properties");
		log.info("actualBreadCrumb:- "+ actualBreadCrumb);
		sa.assertTrue(actualBreadCrumb.contains(quoteID),"Verify the Quote number in breadcrumb");
		
		log.info("Verify the Quote name");
		gVar.assertequalsIgnoreCase(l1.getWebElement("QuoteDetails_QuoteName", "Profile//Quotes.properties").getText(), quoteNameInCheckout,"Verify the quote name");
		
		log.info("Verify the Description label and value");
		gVar.assertequalsIgnoreCase(l1.getWebElement("QuoteDetails_DescriptionLabel", "Profile//Quotes.properties").getText()
				, GetData.getDataFromExcel("//data//GenericData.xls", "Quotes", 2, 2),"Verifyt Description label");
		gVar.assertequalsIgnoreCase(l1.getWebElement("QuoteDetails_DescriptionValue", "Profile//Quotes.properties").getText(), reason, "Verify the reason");
		
		log.info("Verify the updated date and date placed");
		String actualUpdatedDate = l1.getWebElement("QuoteDetails_UpdatedDate", "Profile//Quotes.properties").getText();
		log.info("actualUpdatedDate:- "+ actualUpdatedDate);
		String systemDate = gVar.getSystemDate("MMM dd, yyyy");
		log.info("systemDate:- "+ systemDate);
		
		sa.assertTrue(actualUpdatedDate.contains(systemDate), "Verify the updated date");
		String actualDAte = actualUpdatedDate.substring(0,systemDate.length());
		sa.assertEquals(actualDAte, systemDate,"Verify the updated date..");
		String actualDatePlaced = l1.getWebElement("QuoteDetails_PlacedDate", "Profile//Quotes.properties").getText();
		log.info("actualDatePlaced:- "+ actualDatePlaced);
		sa.assertTrue(actualDatePlaced.contains(systemDate),"Verify the Date placed");
		sa.assertEquals(actualDAte, systemDate,"Verify the Date placed..");
		
		
		log.info("Verify the product name");
		String proNAmeInQuoteDetails = gVar.assertEqual("QuoteDetails_ProductName", "Profile//Quotes.properties");
		sa.assertTrue(proNameInPDP.contains(proNAmeInQuoteDetails), "Verify the Product name");
		sa.assertEquals(proNAmeInQuoteDetails, proNameInPDP.substring(0, proNAmeInQuoteDetails.length()), "Verify the Product name..");
		
		log.info("Verify the item table heading");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("QuoteDetails_ItemHeading", "Profile//Quotes.properties"), 
				GetData.getDataFromExcel("//data//GenericData.xls", "Quotes", 1, 3),"Item heading");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("QuoteDetails_PriceHeading", "Profile//Quotes.properties"), 
				GetData.getDataFromExcel("//data//GenericData.xls", "Quotes", 2, 3),"Item heading");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("QuoteDetails_QuantityHeading", "Profile//Quotes.properties"), 
				GetData.getDataFromExcel("//data//GenericData.xls", "Quotes", 3, 3),"Item heading");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("QuoteDetails_UnitHeading", "Profile//Quotes.properties"), 
				GetData.getDataFromExcel("//data//GenericData.xls", "Quotes", 4, 3),"Item heading");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("QuoteDetails_DeliveryHeading", "Profile//Quotes.properties"), 
				GetData.getDataFromExcel("//data//GenericData.xls", "Quotes", 5, 3),"Item heading");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("QuoteDetails_TotalHeading", "Profile//Quotes.properties"), 
				GetData.getDataFromExcel("//data//GenericData.xls", "Quotes", 6, 3),"Item heading");
		
		String proPrice = l1.getWebElement("QuoteDetails_ProductPrice", "Profile//Quotes.properties").getText();
		String proPriceInQuoteDetailsPage  = proPrice.substring(proPrice.indexOf("$"), proPrice.length());
		log.info("proPriceInQuoteDetailsPage:- "+ proPriceInQuoteDetailsPage);
		sa.assertEquals(proPriceInQuoteDetailsPage, productPriceInPdp, "Verify the price");
		
		log.info("Collect the Quantity");
		String qtyValText = l1.getWebElement("QuoteDetails_QuantityValue", "Profile//Quotes.properties").getText();
		int quantityVal = Integer.parseInt(qtyValText);
		double productPriceInPdpVal = Double.parseDouble(productPriceInPdp.replace("$", ""));
				
		double expectedTotalPrice = productPriceInPdpVal*quantityVal;
		log.info("expectedTotalPrice:- "+ expectedTotalPrice);
		String actualTotalPrice = l1.getWebElement("QuoteDetails_ProductTotalPrice", "Profile//Quotes.properties").getText().replace("$", "");
		double actualTotalPriceVal = Double.parseDouble(actualTotalPrice);
		sa.assertEquals(actualTotalPriceVal, expectedTotalPrice,"Verify the total price");
		
		sa.assertFalse(true, "negotiation details, comments and quote properties need to check manually(these things can't automated)");

		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="DMED-535 DRM-1106 DMED-537 DRM-1162")
	public void TC06_quoteExpireDate() throws Exception 
	{
		SimpleDateFormat sf = new SimpleDateFormat("MMM dd, yyyy");
		Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.DATE, 90);
        Date date = cal.getTime();
        String expExpireDate = sf.format(date);
        log.info("Expected ExpireDate:- "+ expExpireDate);
		
		log.info("verify expiration date");
		String actualExpaireDate = gVar.assertEqual("Quote_Expire_Date", "Profile//Quotes.properties").trim();
		log.info("expectedDate:- "+ actualExpaireDate);
		
		sa.assertTrue(actualExpaireDate.contains(expExpireDate),"Verify the expiry date");
		sa.assertAll();
	}
	
	
	
	@Test(groups={"reg"},description="DMED-535 DRM-1097")
	public void TC07_quoteWithoutReason() throws Exception 
	{
		log.info("Navigate to Order Review page");
		checkout.navigateToOrderReviewPage();
		
		log.info("click on quote a request");
		l1.getWebElement("OrderReview_RequestQuote_Btn", "Checkout//OrderReview.properties").click();
		log.info("verify opened quote pop up");
		sa.assertTrue(gVar.assertVisible("OrderReview_Quote_Heading", "Checkout//OrderReview.properties"),"create quote heading");
		
		Thread.sleep(3000);
		log.info("click on submit button without entering Reason");
		l1.getWebElement("Quote_Submit_Btn", "Checkout//OrderReview.properties").click();
		
		log.info("fetch quote code");
		quoteID=l1.getWebElement("Quote_Id", "Checkout//OrderReview.properties").getText().replace("#", "");
		log.info("quoteID:- "+ quoteID);
		
		log.info("click yes on quote confirmation pop up");
		l1.getWebElement("Quote_Yes_Btn", "Checkout//OrderReview.properties").click();
		
		log.info("verify navigation to quotes page");
		sa.assertTrue(gVar.assertVisible("Quotes_Heading", "Profile//Quotes.properties"),"Quotes heading");
		
		log.info("Verify recently submitted quotes code");
		sa.assertEquals(gVar.assertEqual("Quote_Code", "Profile//Quotes.properties"),quoteID,"Quote number");
		
		
		sa.assertAll();
	}
	
	
	@Test(groups={"reg"}, description="DMED-535 DRM-1098")
	public void TC08_verifyTheQuoteStatus() throws Exception
	{
		log.info("Verify the status");
		sa.assertEquals(gVar.assertEqual("Quote_Status", "Profile//Quotes.properties"), 
				GetData.getDataFromExcel("//data//GenericData.xls", "Quotes", 2, 1), "Verify the status");
		
		sa.assertAll();
	}
	
}
