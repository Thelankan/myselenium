package com.drivemedical.profile;

import java.lang.reflect.Method;

import org.apache.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.xml.XmlTest;

import com.drivemedical.utilgeneric.BaseTest;
import com.drivemedical.utilgeneric.Data;
import com.drivemedical.utilgeneric.GetData;

public class SignIn extends BaseTest{
	
	String emailId;
	
	
/*	@Test(groups={"reg"})
	public void TC00_Signin(XmlTest xmlTest) throws Exception
	{
		log.info("Click on Sign In Link");
		p.navigateToLoginPage();
		//verify UI
		log.info("Verify heading");
		sa.assertTrue(l1.getWebElement("Login_Heading", "Profile\\login.properties").isDisplayed());
		log.info("Verify required text");
		sa.assertTrue(l1.getWebElement("Login_ReqField_Text", "Profile\\login.properties").isDisplayed());
		log.info("Verify username label");
		sa.assertTrue(l1.getWebElement("Login_UserName_Text", "Profile\\login.properties").isDisplayed());
		log.info("Verify username textbox");
		sa.assertTrue(l1.getWebElement("Login_Email_TextBox", "Profile\\login.properties").isDisplayed());
		log.info("Verify password label");
		sa.assertTrue(l1.getWebElement("Login_Password_Text", "Profile\\login.properties").isDisplayed());
		log.info("Verify password textbox");
		sa.assertTrue(l1.getWebElement("Login_Password_Box", "Profile\\login.properties").isDisplayed());
		log.info("Verify sign in button");
		sa.assertTrue(l1.getWebElement("Login_Login_Btn", "Profile\\login.properties").isDisplayed());
		log.info("Verify forgot password link");
		sa.assertTrue(l1.getWebElement("Login_ForgotPwd_Link", "Profile\\login.properties").isDisplayed());
		sa.assertAll();
	}
	
	@Test(groups={"reg"},dataProvider="loginVal",dataProviderClass=Data.class,description="OOTB_055-DRM-593,594 OOTB_059_1_2_3 OOTB-059 DRM-543")
	public void TC01_SignInValidation(String un,String pwd) throws Exception
	{
		log.info("Verify validations");
		l1.getWebElement("Login_Email_TextBox", "Profile\\login.properties").clear();
		l1.getWebElement("Login_Email_TextBox", "Profile\\login.properties").sendKeys(un);
		l1.getWebElement("Login_Password_Box", "Profile\\login.properties").clear();
		l1.getWebElement("Login_Password_Box", "Profile\\login.properties").sendKeys(pwd);
		l1.getWebElement("Login_Login_Btn", "Profile\\login.properties").click();
		
		if(i==0 || i==1 || i==2 || i==3 || i==4)
		{
			log.info("Verify error message");
			sa.assertTrue(l1.getWebElement("Login_ErrorMsg_Text", "Profile\\login.properties").isDisplayed());
			String expText=GetData.getDataFromProperties("//data//Validations.properties", "SignIn_Error_Msg");
			sa.assertTrue(l1.getWebElement("Login_ErrorMsg_Text", "Profile\\login.properties").getText().contains(expText));
		} else if(i==5) {
			log.info("User should be logged in");
			sa.assertTrue(l1.getWebElement("Header_Reg_MyAct_Link", "Shopnav\\header.properties").isDisplayed());
			log.info("Log out from application");
			act.moveToElement(l1.getWebElement("Header_Reg_MyAct_Link", "Shopnav\\header.properties")).perform();
			l1.getWebElement("Header_SignOut_Link","Shopnav\\header.properties").click();
		}
		
		++i;
		System.out.println(i);
		sa.assertAll();
	}*/
	
	@Test(groups={"reg"},description="OOTB_054_1_2_3")
	public void TC02_ForgotPWD_UI(XmlTest xmlTest) throws Exception 
	{
		log.info("Click on Sign In Link");
		p.navigateToLoginPage();
		log.info("click on forgot password link");
		l1.getWebElement("Login_ForgotPwd_Link", "Profile\\login.properties").click();
		log.info("verify overlay is opened or not and UI");
		log.info("Reset password overlay heading");
		sa.assertTrue(l1.getWebElement("ForgotPwd_ResetPwd_Heading", "Profile\\login.properties").isDisplayed());
		log.info("Reset password text");
		sa.assertTrue(l1.getWebElement("ForgotPwd_Text", "Profile\\login.properties").isDisplayed());
		log.info("Reset password email label text");
		sa.assertTrue(l1.getWebElement("ForgotPwd_EmailLbl_Text", "Profile\\login.properties").isDisplayed());
		log.info("Reset password email textbox");
		sa.assertTrue(l1.getWebElement("ForgotPwd_Email_Textbox", "Profile\\login.properties").isDisplayed());
		log.info("Reset password submit button");
		sa.assertTrue(l1.getWebElement("ForgotPwd_ResetPwd_Btn", "Profile\\login.properties").isDisplayed());
		log.info("Reset password close link");
		sa.assertTrue(l1.getWebElement("ForgotPwd_Close_icon", "Profile\\login.properties").isDisplayed());
	}
	
	@Test(groups={"reg"},description="OOTB_054")
	public void TC03_forgot_Pwd_Close() throws Exception 
	{
		log.info("click on close link");
		Thread.sleep(3000);
		l1.getWebElement("ForgotPwd_Close_icon", "Profile\\login.properties").click();
		log.info("Verify forgot password overay is closed");
		
		if(xmlTest.getParameter("executingIn").equalsIgnoreCase("Tablet") || xmlTest.getParameter("executingIn").equalsIgnoreCase("mobile")) {
			Thread.sleep(3000);
		}
		
		try
		{
		sa.assertFalse(l1.getWebElement("ForgotPwd_ResetPwd_Heading", "Profile\\login.properties").isDisplayed(),"forgot password Overlay closed");
		}
		catch(Exception e){
			sa.assertTrue(true);
		}
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="OOTB_054_5")
	public void TC04_Forgot_Pwd_Success_UI(XmlTest xmlTest) throws Exception
	{
		log.info("open fogot password overlay");
		l1.getWebElement("Login_ForgotPwd_Link", "Profile\\login.properties").click();
		log.info("enter valid email id");
		emailId=GetData.getDataFromExcel("//data//GenericData.xls", "Login",1,0);
		l1.getWebElement("ForgotPwd_Email_Textbox", "Profile\\login.properties").sendKeys(emailId);
		log.info("click on reset password button");
		l1.getWebElement("ForgotPwd_ResetPwd_Btn","Profile\\login.properties").click();
		log.info("verisy success pop up and UI");
		sa.assertTrue(l1.getWebElement("ForgotPwd_ResetPwd_Heading", "Profile\\login.properties").isDisplayed());
		log.info("Success Text");
		sa.assertTrue(l1.getWebElement("ForgotPwd_Success_Text", "Profile\\login.properties").isDisplayed());
		log.info("Success Text1");
		sa.assertTrue(l1.getWebElement("ForgotPwd_Success1_Text", "Profile\\login.properties").isDisplayed());
		log.info("close button");
		sa.assertTrue(l1.getWebElement("ForgotPwd_Close_btn", "Profile\\login.properties").isDisplayed());
		log.info("close icon");
		sa.assertTrue(l1.getWebElement("ForgotPwd_Close_icon", "Profile\\login.properties").isDisplayed());
		sa.assertAll();
	}
	
	@Test(groups={"reg"})
	public void TC05_Forgot_Success_Close(XmlTest xmlTest) throws Exception
	{
		log.info("click on close button");
		l1.getWebElement("ForgotPwd_Close_btn", "Profile\\login.properties").click();
		if(xmlTest.getParameter("executingIn").equalsIgnoreCase("Tablet") || xmlTest.getParameter("executingIn").equalsIgnoreCase("mobile")) {
			Thread.sleep(3000);
		}
		log.info("Reset Password Over should close");
		sa.assertFalse(l1.getWebElement("ForgotPwd_ResetPwd_Heading", "Profile\\login.properties").isDisplayed(),"forgot password Overlay closed");
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="OOTB_054_4_7")
	public void TC06_Forgot_Pwd_InvalidEmail() throws Exception
	{
		log.info("Click on Sign In Link");
		p.navigateToLoginPage();
		log.info("Open forgot password overlay");
		l1.getWebElement("Login_ForgotPwd_Link", "Profile\\login.properties").click();
		log.info("Enter Invalid Email");
		emailId=GetData.getDataFromExcel("//data//GenericData.xls", "Login",2,0);
		l1.getWebElement("ForgotPwd_Email_Textbox", "Profile\\login.properties").sendKeys(emailId);
		log.info("click on reset password button");
		l1.getWebElement("ForgotPwd_ResetPwd_Btn","Profile\\login.properties").click();
		log.info("verify error message");
		sa.assertTrue(gVar.assertVisible("ForgotPwd_Error_Msg", "Profile\\login.properties"),"forgot pwd error message");
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="OOTB-058 DRM-542")
	public void TC07_signInToTheApplication(XmlTest xmlTest) throws Exception
	{
		log.info("Navigate to Login page");
		p.navigateToLoginPage();
		
		log.info("Login with valid credentials");
		p.logIn(xmlTest);
		
		sa.assertTrue(gVar.assertVisible("Header_Reg_MyAct_Link", "Shopnav//header.properties"),"Verify the My Account heading in header");
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="OOTB-058 DRM-1043 DRM-1044 DRM-544")
	public void TC08_UiOFMyaccountDetails() throws Exception
	{
		log.info("Navigate to Account Overview page");
		act.moveToElement(l1.getWebElement("Header_Reg_MyAct_Link", "Shopnav//header.properties")).perform();
		l1.getWebElement("Header_AccountOverview_Link", "Shopnav//header.properties").click();
		
		log.info("Verify the Account Overview page");
		String accountOverviewHeading = GetData.getDataFromExcel("//data//GenericData.xls", "AccountOverview", 1, 0);
		sa.assertTrue(gVar.assertEqual("AccountOverview.properties", "Profile//AccountOverview.properties").contains(accountOverviewHeading),"Verify the Heading");
		sa.assertTrue(gVar.assertVisible("AccountOverview_DownloadPriceSheet_Link", "Profile//AccountOverview.properties"), "Verify the Donload link");
		String downloadLinkText = GetData.getDataFromExcel("//data//GenericData.xls", "AccountOverview", 1, 1);
		gVar.assertequalsIgnoreCase(gVar.assertEqual("AccountOverview_DownloadPriceSheet_Link", "Profile//AccountOverview.properties"), 
				downloadLinkText, "Download Price Sheet link text");
		

		String OrderHistoryText = GetData.getDataFromExcel("//data//GenericData.xls", "AccountOverview", 2, 0);
		gVar.assertequalsIgnoreCase(gVar.assertEqual("AccountOverview_OrderHistory_Heading", "Profile//AccountOverview.properties"), 
				OrderHistoryText,"Verify the Order History heading");
		
		String viewOrdersLinkText = GetData.getDataFromExcel("//data//GenericData.xls", "AccountOverview", 2, 2);
		sa.assertEquals(gVar.assertEqual("AccountOverview_ViewOrders_Link", "Profile//AccountOverview.properties"), 
				viewOrdersLinkText,"Verify the View orders link");
		
		sa.assertEquals(l1.getWebElements("AccountOverview_OrderItemsRow", "Profile//AccountOverview.properties").size(), 3,"Verify the number of order items row");
		
		sa.assertTrue(false,"Page is not completely developed, refer wireframe 'https://pfsweb.invisionapp.com/share/ECDBD6PQ6#/screens/248299570' ");
		sa.assertAll();
	}
	
	
}
