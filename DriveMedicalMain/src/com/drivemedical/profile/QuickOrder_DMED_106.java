package com.drivemedical.profile;

import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import org.testng.xml.XmlTest;

import com.drivemedical.utilgeneric.BaseTest;
import com.drivemedical.utilgeneric.GetData;
import com.google.common.math.Quantiles;

public class QuickOrder_DMED_106 extends BaseTest
{
	
	String QuickOrderProperties = "Profile//QuickOrder.properties";
	String placeHolderText; 
	
	
	@Test(groups={"reg"}, description="DMED-106 DRM-870")
	public void TC00_quickOrderLink_Functionality(XmlTest xmlTest) throws Exception
	{
		log.info("Login to the application");
		p.navigateToLoginPage();
		p.logIn(xmlTest);
		
		log.info("Click on QUICK ORDER link in header");
		if (xmlTest.getParameter("executingIn").equalsIgnoreCase("mobile") || xmlTest.getParameter("executingIn").equalsIgnoreCase("Tablet")) 
		{
			driver.get("https://stage.medicaldepot.com:9002/drivestorefront/driveUS/en/USD/quickOrder");
		} 
		else 
		{
			l1.getWebElement("Header_Reg_QuickOrder_Link", "Shopnav//header.properties").click();
		}
		
		log.info("Verify the QUICK ORDER page");
		String quickOrderText = GetData.getDataFromExcel("//data//GenericData.xls", "QuickOrder", 1, 4);
		gVar.assertequalsIgnoreCase(gVar.assertEqual("Breadcrumb_ActiveElement", "Generic//Generic.properties"), 
				quickOrderText, "Verify the active breadcrumb");
		sa.assertTrue(gVar.assertEqual("QuickOrder_Heading", QuickOrderProperties).contains(quickOrderText),"Verify the Quick Order page heading");
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="DMED-106 DRM-871 DRM-872 DRM-873")
	public void TC01_uiOfQuickOrder_Page() throws Exception
	{
		log.info("Verify the Upload CSV button");
		String UploadCSVText = GetData.getDataFromExcel("//data//GenericData.xls", "QuickOrder", 1, 5);
		gVar.assertequalsIgnoreCase(gVar.assertEqual("QuickOrder_UploadCSV_Text", QuickOrderProperties), UploadCSVText,"UploadCSVText");
		sa.assertTrue(gVar.assertVisible("QuickOrder_UploadCSV_Button", QuickOrderProperties),"Verify the Quick order link");
		
		log.info("Download CSV link");
		String DownloadCSVLink = GetData.getDataFromExcel("//data//GenericData.xls", "QuickOrder", 2, 5);
		gVar.assertequalsIgnoreCase(gVar.assertEqual("QuickOrder_DownloadCSV_Link", QuickOrderProperties),
				DownloadCSVLink,"Verify the Download CSV link");
		
		String maxSKUsHeading = GetData.getDataFromExcel("//data//GenericData.xls", "QuickOrder", 2, 4);
		gVar.assertequalsIgnoreCase(gVar.assertEqual("QuickOrder_Max_SKUsHeading", QuickOrderProperties), 
				maxSKUsHeading,"Verify Add up to 25 Valid Skus heading");
		
		log.info("Verify the CLEAR FORM link");
		String clearFormLink = GetData.getDataFromExcel("//data//GenericData.xls", "QuickOrder", 3, 5);
		gVar.assertequalsIgnoreCase(gVar.assertEqual("QuickOrder_ClearForm_Top", QuickOrderProperties), 
				clearFormLink, "Clear form link top");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("QuickOrder_ClearForm_LinkBottom", QuickOrderProperties), 
				clearFormLink, "Clear form link bottom");
		
		log.info("Verify the ADD TO CART button");
		String addToCartText = GetData.getDataFromExcel("//data//GenericData.xls", "QuickOrder", 4, 5);
		gVar.assertequalsIgnoreCase(gVar.assertEqual("QuickOrder_AddToCart_Button_Top", QuickOrderProperties), 
				addToCartText,"Add To Cart button Top");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("QuickOrder_AddToCart_Button_Bottom", QuickOrderProperties), 
				addToCartText,"Add To Cart button bottom");
		
		log.info("Verify the Table heading");
		String MaterialNumberText = GetData.getDataFromExcel("//data//GenericData.xls", "QuickOrder", 3, 4);
		String PriceText = GetData.getDataFromExcel("//data//GenericData.xls", "QuickOrder", 4, 4);
		String QtyText = GetData.getDataFromExcel("//data//GenericData.xls", "QuickOrder", 5, 4);
		String UnitText = GetData.getDataFromExcel("//data//GenericData.xls", "QuickOrder", 6, 4);
		String TotalText = GetData.getDataFromExcel("//data//GenericData.xls", "QuickOrder", 7, 4);
		
		gVar.assertEqual(gVar.assertEqual("QuickOrder_MaterialNumber_Heading", QuickOrderProperties), 
				MaterialNumberText,"Material Number heading");
		gVar.assertEqual(gVar.assertEqual("QuickOrder_MaterialNumber_Heading", QuickOrderProperties), 
				PriceText,"Material Number heading");
		gVar.assertEqual(gVar.assertEqual("QuickOrder_MaterialNumber_Heading", QuickOrderProperties), 
				QtyText,"Material Number heading");
		gVar.assertEqual(gVar.assertEqual("QuickOrder_MaterialNumber_Heading", QuickOrderProperties), 
				UnitText,"Material Number heading");
		gVar.assertEqual(gVar.assertEqual("QuickOrder_MaterialNumber_Heading", QuickOrderProperties), 
				TotalText,"Material Number heading");
		
		log.info("Verify the SKU input field");
		sa.assertTrue(gVar.assertVisible("QuickOrder_SKUsInput_Field", QuickOrderProperties),"Verify the SKUs input field");
		List<WebElement> skuInputField = l1.getWebElements("QuickOrder_SKUsInput_Field", QuickOrderProperties);
		sa.assertEquals(skuInputField.size(), 3,"Verify the number of SKU input field");
		List<WebElement> removeIcons = l1.getWebElements("QuickOrder_Remove_Icons", QuickOrderProperties);
		sa.assertEquals(removeIcons.size(), 3,"Verify the number of remove icons");
		
		log.info("Verify the placeholder text and close button");
		placeHolderText = GetData.getDataFromExcel("//data//GenericData.xls", "QuickOrder", 8, 4);
		for (int i = 0; i < skuInputField.size(); i++) 
		{
			System.out.println("LOOP:- "+i);
			gVar.assertequalsIgnoreCase(skuInputField.get(i).getAttribute("placeholder"), 
					placeHolderText,"placeholder text");
			sa.assertTrue(removeIcons.get(i).isDisplayed(),"Verify the Remove icons");
		}
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="DMED-106 DRM-877")
	public void TC02_maximumProductsEnter_Functionality() throws Exception
	{
		int rowNum=1;
		log.info("Enter product IDs in SKUs field");
		for (int i = 0; i < 26; i++) 
		{
			System.out.println("LOOP:- "+i);
			List<WebElement> skuInputField = l1.getWebElements("QuickOrder_SKUsInput_Field", QuickOrderProperties);
			if (i<25) 
			{
				gVar.assertequalsIgnoreCase(skuInputField.get(i).getAttribute("placeholder"), placeHolderText,"Verify the place holder text");
				String productId = GetData.getDataFromExcel("//data//GenericData.xls", "QuickOrder", rowNum, 3);
				skuInputField.get(i).sendKeys(productId);
				Thread.sleep(3000);
				if (i==24) 
				{
					log.info("To remove the focus from the textbox");
					skuInputField.get(i).sendKeys(Keys.TAB);
				}
			}
			else
			{
				
				try 
				{
					sa.assertTrue(skuInputField.get(i).isDisplayed(),"Verify the SKU input field");
					sa.assertTrue(false,"26 SKU input field is displaying");;
				} 
				catch (Exception e) 
				{
					sa.assertTrue(true,"Only 25 sku input fields are displaying");
				}
			}
			
			rowNum++;
		}
		
		Thread.sleep(10000);
		log.info("Verify the number of product name and Close icons");
		List<WebElement> skuInputField = l1.getWebElements("QuickOrder_SKUsInput_Field", QuickOrderProperties);
		sa.assertEquals(skuInputField.size(), 25,"Verify the number of SKU input field");
		
		int productsNamesCount = l1.getWebElements("QuickOrder_PeoductsName", QuickOrderProperties).size();
		System.out.println("productsNamesCount:- "+productsNamesCount);
		sa.assertEquals(productsNamesCount, 25, "Veriufy the number of products");
		
		log.info("Add To CArt button should be enabled");
		sa.assertTrue(l1.getWebElement("QuickOrder_AddToCart_Button_Top", QuickOrderProperties).isEnabled(), "Verify the ADD TO CART button");
		sa.assertTrue(l1.getWebElement("QuickOrder_AddToCart_Button_Bottom", QuickOrderProperties).isEnabled(), "Verify the ADD TO CART button");
		
		sa.assertAll();
	}
	
	
	@Test(groups={"reg"},description="DMED-106 DRM-878")
	public void TC03_clearFormLink_Functionality() throws Exception
	{
		log.info("Click on CLEAR FORM link");
		l1.getWebElement("QuickOrder_ClearForm_Top", QuickOrderProperties).click();
		
		Alert alert = driver.switchTo().alert();
		String alertText = alert.getText();
		System.out.println("alertText:- "+ alertText);
		
		log.info("Select STAY ON PAGE in popup");
		alert.dismiss();
		
		log.info("Popup should disappear");
		try 
		{
			driver.switchTo().alert();
			sa.assertFalse(true,"Still popup is displaying. Popup should not display on selecting STAY ON PAGE");
		} 
		catch (Exception e) 
		{
			sa.assertTrue(true,"Popup is disappeared");
		}

		log.info("Click on CLEAR form link");
		l1.getWebElement("QuickOrder_ClearForm_Top", QuickOrderProperties).click();
		Alert alert1 = driver.switchTo().alert();
		
		log.info("Select LEAVE PAGE option in popup");
		alert1.accept();
		Thread.sleep(3000);
		log.info("Saved products should be deleted");
		sa.assertTrue(gVar.assertVisible("QuickOrder_SKUsInput_Field", QuickOrderProperties),"Verify the SKUs input field");
		List<WebElement> skuInputField = l1.getWebElements("QuickOrder_SKUsInput_Field", QuickOrderProperties);
		sa.assertEquals(skuInputField.size(), 3,"Verify the number of SKU input field");
		List<WebElement> removeIcons = l1.getWebElements("QuickOrder_Remove_Icons", QuickOrderProperties);
		sa.assertEquals(removeIcons.size(), 3,"Verify the number of remove icons");
		
		log.info("Add to Cart button should be disabled");
		sa.assertFalse(l1.getWebElement("QuickOrder_AddToCart_Button_Top", QuickOrderProperties).isEnabled(),"ADD TO CART button top should be disabled");
		sa.assertFalse(l1.getWebElement("QuickOrder_AddToCart_Button_Bottom", QuickOrderProperties).isEnabled(),"ADD TO CART button bottom should be disabled");
		
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="DMED-106 DRM-875")
	public void TC04_errorMessageForInvalidProductInfo() throws Exception
	{
		
		for (int i = 1; i <= 2; i++) 
		{
			System.out.println("LOOP:- "+ i);
			log.info("enter invalid product ID");
			String invalidProductId = GetData.getDataFromExcel("//data//GenericData.xls", "QuickOrder", i, 6);
			l1.getWebElement("QuickOrder_MeterialNum", QuickOrderProperties).sendKeys(invalidProductId);
//			act.sendKeys(Keys.TAB).perform();
			l1.getWebElement("QuickOrder_MeterialNum", QuickOrderProperties).sendKeys(Keys.TAB);
			
			Thread.sleep(3000);
			log.info("Verify the Error message");
			sa.assertTrue(gVar.assertVisible("QuickOrder_Error_Msg", QuickOrderProperties),"Verify the Error message");
			String expectedEm1 = GetData.getDataFromExcel("//data//GenericData.xls", "QuickOrder", i, 7);
			gVar.assertequalsIgnoreCase(gVar.assertEqual("QuickOrder_Error_Msg", QuickOrderProperties), 
					expectedEm1,"Verify the Error message");
			sa.assertEquals(gVar.getCssValue("QuickOrder_Error_Msg", QuickOrderProperties, "color"), 
					"rgb(255, 0, 0)","Verify the Error message text color");
			sa.assertEquals(gVar.getCssValue("QuickOrder_MeterialNum", QuickOrderProperties, "border-color"), 
					gVar.emTextColor,"Verify the SKU textbox border color");
			
			log.info("Click on CLOSE icon");
			l1.getWebElement("QuickOrder_Remove", QuickOrderProperties).click();
			log.info("Error message should disappear");
			sa.assertFalse(gVar.assertVisible("QuickOrder_Error_Msg", QuickOrderProperties),"Error message should disappear");
			Thread.sleep(2000);
		}
		
		sa.assertAll();
		
	}
	
//	QuickOrder_DuplicateID_ErrorMsg
	@Test(groups={"reg"}, description="Enter Duplicate Product ID")
	public void TC05_errorMEssageForDuplicateID() throws Exception
	{
		
		List<WebElement> idTextBox = l1.getWebElements("QuickOrder_MeterialNum", QuickOrderProperties);
		
		for (int i = 0; i < 2; i++) 
		{
			System.out.println("LOOP:- "+ i);
			log.info("enter Same Product ID");
			String invalidProductId = GetData.getDataFromExcel("//data//GenericData.xls", "QuickOrder", 1, 6);
//			l1.getWebElement("QuickOrder_MeterialNum", QuickOrderProperties).sendKeys(invalidProductId);
			idTextBox.get(i).sendKeys(invalidProductId);
			
			l1.getWebElement("QuickOrder_MeterialNum", QuickOrderProperties).sendKeys(Keys.TAB);
			Thread.sleep(3000);
		}
		
		
		log.info("Verify Invalid product ID Error message");
		sa.assertTrue(gVar.assertVisible("QuickOrder_Error_Msg", QuickOrderProperties),"Verify the Error message");
		String expectedEm1 = GetData.getDataFromExcel("//data//GenericData.xls", "QuickOrder", 1, 7);
		gVar.assertequalsIgnoreCase(gVar.assertEqual("QuickOrder_Error_Msg", QuickOrderProperties), 
				expectedEm1,"Verify the Error message");
		sa.assertEquals(gVar.getCssValue("QuickOrder_Error_Msg", QuickOrderProperties, "color"), 
				"rgb(255, 0, 0)","Verify the Error message text color");
		
		log.info("Verify the Duplicate ID error message");
		String duplicateSKUerrorMsg = GetData.getDataFromExcel("//data//GenericData.xls", "QuickOrder", 3, 7);
		String emText = l1.getWebElement("QuickOrder_DuplicateID_ErrorMsg", QuickOrderProperties).getText();
		System.out.println("emText:- "+ emText);
		gVar.assertequalsIgnoreCase(gVar.assertEqual("QuickOrder_DuplicateID_ErrorMsg", QuickOrderProperties), duplicateSKUerrorMsg, "Verify the DUPLICATE SKU ERROR message");
		sa.assertEquals(gVar.assertEqual("QuickOrder_DuplicateID_ErrorMsg", QuickOrderProperties, "color"), "rgb(255, 0, 0)", "Verify the Error message Color");

		log.info("Click on CLEAR form link");
		l1.getWebElement("QuickOrder_ClearForm_Top", QuickOrderProperties).click();
		Alert alert1 = driver.switchTo().alert();
		log.info("Select LEAVE PAGE option in popup");
		alert1.accept();
		Thread.sleep(3000);
		
		log.info("Error message should disappear");
		sa.assertTrue(gVar.assertNotVisible("QuickOrder_Error_Msg", QuickOrderProperties),"Error msg should not display");
		sa.assertTrue(gVar.assertNotVisible("QuickOrder_DuplicateID_ErrorMsg", QuickOrderProperties),"Duplicate Error msg should not display");
		
		sa.assertAll();
	}
	
}
