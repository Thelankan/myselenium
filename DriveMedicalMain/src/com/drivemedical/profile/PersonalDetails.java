package com.drivemedical.profile;

import org.openqa.selenium.By;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.xml.XmlTest;



//import com.drivemedical.projectspec.GenericFunctions;
import com.drivemedical.utilgeneric.BaseTest;
import com.drivemedical.utilgeneric.Data;
import com.drivemedical.utilgeneric.GetData;
import com.drivemedical.utilgeneric.TestData;

public class PersonalDetails extends BaseTest{
	
	
	private static final String String = null;
	String ErrorMessageColor = "rgb(208, 2, 27)";
	String ErrorTextboxBackgroundColor = "#fec3c3";
	String ErrorTextboxBorderColor = "#fd7b7b";
	int cnt;

	@Test(groups={"reg"},description="OOTB-059 DRM-632 AND OOTB-059 DRM-636")
	public void TC00_PD_Update_Test_1(XmlTest xmlTest) throws Exception
	{
		log.info("navigate to Sign In page");
		p.navigateToLoginPage();
		log.info("log in to the application");
		p.logIn(xmlTest);
		
		log.info("Navigate to Personal details page");
		s.navigateToPersonalDetailsPAge();
		log.info("Verify the prepoluted data of First name and Email ID");
		String fnExpected = l1.getWebElement("PD_FN_TextBox", "Profile//PersonalDetails.properties").getAttribute("value");
		System.out.println("fnExpected:-  "+ fnExpected);
		String emilExpected = l1.getWebElement("PD_NewEmail_Textbox", "Profile//PersonalDetails.properties").getAttribute("value");
		System.out.println("emilExpected:-  "+ emilExpected);
		sa.assertEquals("Mohan", fnExpected,"Verify prepopulated FIRST NAME");	//Verify 
		sa.assertEquals("mohan@gmail.com", emilExpected, "Verify prepopulated EMAIL ID");
		
		log.info("Clear the details");
		clearPD_Details();
		
		String fNameUpdt = "Adi";
		String lNameUpdt = "R";
		log.info("Enter User details and Click on Update button");
		l1.getWebElement("PD_FN_TextBox", "Profile//PersonalDetails.properties").sendKeys(fNameUpdt);
		l1.getWebElement("PD_LN_Textbox", "Profile//PersonalDetails.properties").sendKeys(lNameUpdt);
		l1.getWebElement("PD_NewEmail_Textbox", "Profile//PersonalDetails.properties").sendKeys("adityaupdated@gmail.com");
		l1.getWebElement("PD_PhoneNum_Textbox", "Profile//PersonalDetails.properties").sendKeys("3333333333");
		l1.getWebElement("PD_Update_Btn", "Profile//PersonalDetails.properties").click();
		
		log.info("Pgae should reload and update the details");
		sa.assertTrue(l1.getWebElement("PD_Heading", "Profile//PersonalDetails.properties").isDisplayed());	
		
		log.info("Verify the pre-poluted data");
		sa.assertEquals("Adi", l1.getWebElement("PD_FN_TextBox", "Profile//PersonalDetails.properties").getAttribute("value"));
		sa.assertEquals("R", l1.getWebElement("PD_LN_Textbox", "Profile//PersonalDetails.properties").getAttribute("value"));
		sa.assertEquals("adityaupdated@gmail.com", l1.getWebElement("PD_NewEmail_Textbox", "Profile//PersonalDetails.properties").getAttribute("value"));
		sa.assertEquals("3333333333", l1.getWebElement("PD_PhoneNum_Textbox", "Profile//PersonalDetails.properties").getAttribute("value"));
		
		sa.assertEquals(l1.getWebElement("PD_ShopperName", "Profile//PersonalDetails.properties").getText(), 
				"Hi,"+fNameUpdt+" "+lNameUpdt);
		
		//Change Password
		log.info("Clikc on Change Password link");
		l1.getWebElement("PD_ChangePWD_Link", "Profile//PersonalDetails.properties").click();
		
		log.info("Verify the password expanded section");
		sa.assertTrue(l1.getWebElement("PD_KeepPwd_Link", "Profile//PersonalDetails.properties").isDisplayed());
		sa.assertTrue(l1.getWebElement("PD_CurrentPwd_Textbox", "Profile//PersonalDetails.properties").isDisplayed());
		sa.assertTrue(l1.getWebElement("PD_NewPwd_Textbox", "Profile//PersonalDetails.properties").isDisplayed());
		sa.assertTrue(l1.getWebElement("PD_ConfirmPwd_Textbox", "Profile//PersonalDetails.properties").isDisplayed());
		
		log.info("Update Password");
		l1.getWebElement("PD_CurrentPwd_Textbox", "Profile//PersonalDetails.properties").sendKeys("Test@1234");
		l1.getWebElement("PD_NewPwd_Textbox", "Profile//PersonalDetails.properties").sendKeys("Mohan@1234");
		l1.getWebElement("PD_ConfirmPwd_Textbox", "Profile//PersonalDetails.properties").sendKeys("Mohan@1234");
		l1.getWebElement("PD_Update_Btn", "Profile//PersonalDetails.properties").click();	//Click on Update button
		
		log.info("After Updating, password section should collapse and user should be in same page");
		sa.assertTrue(l1.getWebElement("PD_ChangePWD_Link", "Profile//PersonalDetails.properties").isDisplayed());
		sa.assertTrue(l1.getWebElement("PD_Heading", "Profile//PersonalDetails.properties").isDisplayed());
		
		log.info("Logout from the application");
		p.logout(xmlTest);
	
		log.info("Navigate to login page");
		p.navigateToLoginPage();
		log.info("Login with old credentials");
		p.logIn(xmlTest);
		
		log.info("Verify the Error message");
		sa.assertTrue(l1.getWebElement("Login_ErrorMsg_Text", "Profile//login.properties").isDisplayed());

		log.info("Login with updated password");
		l1.getWebElement("Login_Email_TextBox", "Profile\\login.properties").clear();
		l1.getWebElement("Login_Email_TextBox", "Profile\\login.properties").sendKeys("mohan@gmail.com");
		l1.getWebElement("Login_Password_Box", "Profile\\login.properties").sendKeys("Mohan@1234");
		l1.getWebElement("Login_Login_Btn", "Profile\\login.properties").click();
		System.out.println("Clicked on LOGIN button");

		log.info("Navigate to contact details");
		s.navigateToPersonalDetailsPAge();
		
		log.info("Verify the pre-poluted data");
		sa.assertEquals("Adi", l1.getWebElement("PD_FN_TextBox", "Profile//PersonalDetails.properties").getAttribute("value"));
		sa.assertEquals("R", l1.getWebElement("PD_LN_Textbox", "Profile//PersonalDetails.properties").getAttribute("value"));
		sa.assertEquals("adityaupdated@gmail.com", l1.getWebElement("PD_NewEmail_Textbox", "Profile//PersonalDetails.properties").getAttribute("value"));
		sa.assertEquals("3333333333", l1.getWebElement("PD_PhoneNum_Textbox", "Profile//PersonalDetails.properties").getAttribute("value"));
		//email id
		
		log.info("Reset the details");
		l1.getWebElement("PD_ChangePWD_Link", "Profile//PersonalDetails.properties").click();
		clearPD_Details();
		fNameUpdt = "Mohan";
		lNameUpdt = "Pattar";
		l1.getWebElement("PD_FN_TextBox", "Profile//PersonalDetails.properties").sendKeys(fNameUpdt);
		l1.getWebElement("PD_LN_Textbox", "Profile//PersonalDetails.properties").sendKeys(lNameUpdt);
		l1.getWebElement("PD_NewEmail_Textbox", "Profile//PersonalDetails.properties").sendKeys("mohan@gmail.com");
		l1.getWebElement("PD_PhoneNum_Textbox", "Profile//PersonalDetails.properties").sendKeys("9999999999");
		l1.getWebElement("PD_CurrentPwd_Textbox", "Profile//PersonalDetails.properties").sendKeys("Mohan@1234");	
		l1.getWebElement("PD_NewPwd_Textbox", "Profile//PersonalDetails.properties").sendKeys("Test@1234");
		l1.getWebElement("PD_ConfirmPwd_Textbox", "Profile//PersonalDetails.properties").sendKeys("Test@1234");
		l1.getWebElement("PD_Update_Btn", "Profile//PersonalDetails.properties").click();
		
		log.info("Verify the updated Name in Contact Ditails page");
		sa.assertEquals(l1.getWebElement("PD_ShopperName", "Profile//PersonalDetails.properties").getText(), 
				"Hi,"+fNameUpdt+" "+lNameUpdt);
		
	}

	
	@Test(groups={"reg"},description="OOTB-059 DRM-634 AND DRM-635")
	public void TC01_PD_Update_Test_2(XmlTest xmlTest) throws Exception
	{
		log.info("CLear the details");
		clearPD_Details();
		
		log.info("Update the User Details");
		l1.getWebElement("PD_FN_TextBox", "Profile//PersonalDetails.properties").sendKeys("Adi");
		l1.getWebElement("PD_LN_Textbox", "Profile//PersonalDetails.properties").sendKeys("R");
		l1.getWebElement("PD_NewEmail_Textbox", "Profile//PersonalDetails.properties").sendKeys("adityaupdated@gmail.com");
		l1.getWebElement("PD_PhoneNum_Textbox", "Profile//PersonalDetails.properties").sendKeys("3333333333");
		
		log.info("Update the password");
		l1.getWebElement("PD_CurrentPwd_Textbox", "Profile//PersonalDetails.properties").sendKeys("Test@1234");	
		l1.getWebElement("PD_NewPwd_Textbox", "Profile//PersonalDetails.properties").sendKeys("Mohan@1234");
		l1.getWebElement("PD_ConfirmPwd_Textbox", "Profile//PersonalDetails.properties").sendKeys("Mohan@1234");
		log.info("Click on Update button");
		l1.getWebElement("PD_Update_Btn", "Profile//PersonalDetails.properties").click();
		
		log.info("Logout from the application");
		p.logout(xmlTest);
		
		log.info("Change the locail to CA");
		l1.getWebElement("Header_Lang_Selector", "Shopnav//header.properties").click();
		l1.getWebElement("Header_CandaEng_Link", "Shopnav//header.properties").click();
		
		log.info("Login with updated credentials");
		l1.getWebElement("Header_SignIn_LINK", "Shopnav//header.properties").click();
		l1.getWebElement("Login_Email_TextBox", "Profile\\login.properties").sendKeys("mohan@gmail.com");
		l1.getWebElement("Login_Password_Box", "Profile\\login.properties").sendKeys("Mohan@1234");
		l1.getWebElement("Login_Login_Btn", "Profile\\login.properties").click();
		
		log.info("User should not be logged-in with updated credentials");
		sa.assertTrue(l1.getWebElement("Login_ErrorMsg_Text", "Profile//login.properties").isDisplayed());
		
		l1.getWebElement("Login_Email_TextBox", "Profile\\login.properties").clear();
		log.info("Login to the application");
		p.logIn(xmlTest);
		
		log.info("Navigate to Personal Detail page");
		s.navigateToPersonalDetailsPAge();
		
		log.info("User Should be in Personal data page and details should not be updated");
		sa.assertTrue(l1.getWebElement("PD_Heading", "Profile//PersonalDetails.properties").isDisplayed());	
		sa.assertEquals("Aditya", l1.getWebElement("PD_CurrentPwd_Textbox", "Profile//PersonalDetails.properties").getAttribute("value"));
		sa.assertEquals("Reddy", l1.getWebElement("PD_LN_Textbox", "Profile//PersonalDetails.properties").getAttribute("value"));
		sa.assertEquals("9999999999", l1.getWebElement("PD_PhoneNum_Textbox", "Profile//PersonalDetails.properties").getAttribute("value"));
		
		log.info("Logout from the application");
		p.logout(xmlTest);
		
		log.info("Change Country to US");
		l1.getWebElement("Header_Lang_Selector", "Shopnav//header.properties").click();
		l1.getWebElement("Header_UsEng_Link", "Shopnav//header.properties").click();
		
		log.info("Login to the US application and reset the datails");
		l1.getWebElement("Header_SignIn_LINK", "Shopnav//header.properties").click();
		p.logIn(xmlTest);
		
		log.info("Navigate to Personal details page");
		s.navigateToPersonalDetailsPAge();
		
		log.info("Reset the details");
		l1.getWebElement("PD_ChangePWD_Link", "Profile//PersonalDetails.properties").click();
		clearPD_Details();
		l1.getWebElement("PD_FN_TextBox", "Profile//PersonalDetails.properties").sendKeys("Mohan");
		l1.getWebElement("PD_LN_Textbox", "Profile//PersonalDetails.properties").sendKeys("Pattar");
		l1.getWebElement("PD_NewEmail_Textbox", "Profile//PersonalDetails.properties").sendKeys("mohan@gmail.com");
		l1.getWebElement("PD_PhoneNum_Textbox", "Profile//PersonalDetails.properties").sendKeys("9999999999");
		
		l1.getWebElement("PD_CurrentPwd_Textbox", "Profile//PersonalDetails.properties").sendKeys("Mohan@1234");	
		l1.getWebElement("PD_NewPwd_Textbox", "Profile//PersonalDetails.properties").sendKeys("Test@1234");
		l1.getWebElement("PD_ConfirmPwd_Textbox", "Profile//PersonalDetails.properties").sendKeys("Test@1234");
		
		l1.getWebElement("PD_Update_Btn", "Profile//PersonalDetails.properties").click();

	}

	
	
@Test(groups={"reg"},description="OOTB-059 DRM-638")
	public void TC02_PD_Update_Test_4(XmlTest xmlTest) throws Exception
	{
		//Pre Condition:- Two account should be created with SAME EMAIL ID
		
		log.info("Login with email id Which is assosiated with multiple account");
		l1.getWebElement("Header_SignIn_LINK", "Shopnav//header.properties").click();
		l1.getWebElement("Login_Email_TextBox", "Profile\\login.properties").sendKeys("mohan@gmail.com");
		l1.getWebElement("Login_Password_Box", "Profile\\login.properties").sendKeys("Mohan@1234");
		l1.getWebElement("Login_Login_Btn", "Profile\\login.properties").click();
		
		
		log.info("Navigate to Personal detail page");
		s.navigateToPersonalDetailsPAge();
		
		log.info("Update the Email ID");
		l1.getWebElement("PD_NewEmail_Textbox", "Profile//PersonalDetails.properties").sendKeys("mohan@gmail.com");
		l1.getWebElement("PD_Update_Btn", "Profile//PersonalDetails.properties").click();
		
		log.info("Logout from the application");
		p.logout(xmlTest);
		
		log.info("Login to anathor account which is having same email ID");
		l1.getWebElement("Header_SignIn_LINK", "Shopnav//header.properties").click();
		l1.getWebElement("Login_Email_TextBox", "Profile\\login.properties").sendKeys("mohan@gmail.com");
		l1.getWebElement("Login_Password_Box", "Profile\\login.properties").sendKeys("Aditya@2017");
		l1.getWebElement("Login_Login_Btn", "Profile\\login.properties").click();
		
		log.info("Navigate to Personal detail page");
		s.navigateToPersonalDetailsPAge();
		
		log.info("Verify the Email Id");
		sa.assertEquals("mohan@gmail.com", l1.getWebElement("PD_NewEmail_Textbox", "Profile//PersonalDetails.properties").getAttribute("value"));
		
		log.info("Logout from the application");
		p.logout(xmlTest);
		
	}
	

	
@Test(groups="reg",dataProvider="personalDetailsVal",dataProviderClass=Data.class, description="OOTB-059 DRM-633 AND OOTB-059 DRM-637")
public void TC03_PD_val(TestData t) throws Exception
{

	log.info("Clear the details");
	clearPD_Details();
	
	log.info("enter validations values");
	l1.getWebElement("PD_FN_TextBox", "Profile//PersonalDetails.properties").sendKeys(t.get(0));
	l1.getWebElement("PD_LN_Textbox", "Profile//PersonalDetails.properties").sendKeys(t.get(1));
	l1.getWebElement("PD_NewEmail_Textbox", "Profile//PersonalDetails.properties").sendKeys(t.get(2));
	l1.getWebElement("PD_PhoneNum_Textbox", "Profile//PersonalDetails.properties").sendKeys(t.get(3));
	log.info("Click on Update Button");
	l1.getWebElement("PD_Update_Btn", "Profile//PersonalDetails.properties").click();
	Thread.sleep(5000);
	log.info("Verify the message for the iteration  cnt :- " + cnt);
	if(cnt==0)	//For Blank fields
	{
		sa.assertTrue(l1.getWebElement("PD_FN_Error_message", "Profile//PersonalDetails.properties").isDisplayed());
		//Text color FN
		String fnErrorMsgColor = l1.getWebElement("PD_FN_Error_message", "Profile//PersonalDetails.properties").getCssValue("color");
		sa.assertEquals(fnErrorMsgColor, ErrorMessageColor);
		
		
		sa.assertTrue(l1.getWebElement("PD_LN_Error_message", "Profile//PersonalDetails.properties").isDisplayed());
		//Text color LN
		String lnErrorMsgColor = l1.getWebElement("PD_LN_Error_message", "Profile//PersonalDetails.properties").getCssValue("color");
		sa.assertEquals(lnErrorMsgColor, ErrorMessageColor);
		
		
		sa.assertTrue(l1.getWebElement("PD_Email_Error_message", "Profile//PersonalDetails.properties").isDisplayed());
		//Text color EMAIL
		String emailErrorMsgColor = l1.getWebElement("PD_Email_Error_message", "Profile//PersonalDetails.properties").getCssValue("color");
		sa.assertEquals(emailErrorMsgColor, ErrorMessageColor);
		
		sa.assertTrue(l1.getWebElement("PD_Phone_Error_message", "Profile//PersonalDetails.properties").isDisplayed());
		//text color Phone
		String phoneErrorMsgColor = l1.getWebElement("PD_Phone_Error_message", "Profile//PersonalDetails.properties").getCssValue("color");
		sa.assertEquals(phoneErrorMsgColor, ErrorMessageColor);
		
	}
	else if(cnt==1)	// invalid password AND invalid Phone number AND blank LName
	{
		sa.assertTrue(l1.getWebElement("PD_LN_Error_message", "Profile//PersonalDetails.properties").isDisplayed());
		log.info("Verify the text color");
		String lnErrorMsgColor = l1.getWebElement("PD_LN_Error_message", "Profile//PersonalDetails.properties").getCssValue("color");
		sa.assertEquals(lnErrorMsgColor, ErrorMessageColor);
		System.out.println("lnErrorMsgColor:-  "+ lnErrorMsgColor);
		
		sa.assertTrue(l1.getWebElement("PD_Email_Error_message", "Profile//PersonalDetails.properties").isDisplayed());
		log.info("Verify the text color");
		String emailErrorMsgColor = l1.getWebElement("PD_Email_Error_message", "Profile//PersonalDetails.properties").getCssValue("color");
		sa.assertEquals(emailErrorMsgColor, ErrorMessageColor);
		System.out.println("emailErrorMsgColor:-  "+ emailErrorMsgColor);
		
		sa.assertTrue(l1.getWebElement("PD_Phone_Error_message", "Profile//PersonalDetails.properties").isDisplayed());
		log.info("Verify the text color");
		String phoneErrorMsgColor = l1.getWebElement("PD_Phone_Error_message", "Profile//PersonalDetails.properties").getCssValue("color");
		sa.assertEquals(phoneErrorMsgColor, ErrorMessageColor);
		System.out.println("phoneErrorMsgColor:-  "+ phoneErrorMsgColor);
	}
	
	else if(cnt==2)	// invalid Email Id AND blank FName
	{
		sa.assertTrue(l1.getWebElement("PD_FN_Error_message", "Profile//PersonalDetails.properties").isDisplayed());
		String fnErrorMsgColor = l1.getWebElement("PD_FN_Error_message", "Profile//PersonalDetails.properties").getCssValue("color");
		sa.assertEquals(fnErrorMsgColor, ErrorMessageColor);
		System.out.println("fnErrorMsgColor:-  "+ fnErrorMsgColor);
		
		sa.assertTrue(l1.getWebElement("PD_Email_Error_message", "Profile//PersonalDetails.properties").isDisplayed());
		String emailErrorMsgColor = l1.getWebElement("PD_Email_Error_message", "Profile//PersonalDetails.properties").getCssValue("color");
		sa.assertEquals(emailErrorMsgColor, ErrorMessageColor);
		System.out.println("emailErrorMsgColor:-  "+ emailErrorMsgColor);
		
		sa.assertTrue(l1.getWebElement("PD_Phone_Error_message", "Profile//PersonalDetails.properties").isDisplayed());
		String phoneErrorMsgColor = l1.getWebElement("PD_Phone_Error_message", "Profile//PersonalDetails.properties").getCssValue("color");
		sa.assertEquals(phoneErrorMsgColor, ErrorMessageColor);
		System.out.println("phoneErrorMsgColor:-  "+ phoneErrorMsgColor);
	}
	
	else if(cnt==3)	// For invalid password AND invalid Phone number
	{
		sa.assertTrue(l1.getWebElement("PD_Email_Error_message", "Profile//PersonalDetails.properties").isDisplayed());
		String emailErrorMsgColor = l1.getWebElement("PD_Email_Error_message", "Profile//PersonalDetails.properties").getCssValue("color");
		sa.assertEquals(emailErrorMsgColor, ErrorMessageColor);
		System.out.println("emailErrorMsgColor:-  "+ emailErrorMsgColor);
		
		
		sa.assertTrue(l1.getWebElement("PD_Phone_less_Error_message", "Profile//PersonalDetails.properties").isDisplayed());
		String phoneErrorMsgColor = l1.getWebElement("PD_Phone_less_Error_message", "Profile//PersonalDetails.properties").getCssValue("color");
		sa.assertEquals(phoneErrorMsgColor, ErrorMessageColor);
		System.out.println("phoneErrorMsgColor:-  "+ phoneErrorMsgColor);
	}
	else if(cnt==4)	// For Alphanumeric data
	{
		sa.assertTrue(l1.getWebElement("PD_Phone_dig_Error_message", "Profile//PersonalDetails.properties").isDisplayed());
		String phoneErrorMsgColor = l1.getWebElement("PD_Phone_dig_Error_message", "Profile//PersonalDetails.properties").getCssValue("color");
		sa.assertEquals(phoneErrorMsgColor, ErrorMessageColor);
		System.out.println("phoneErrorMsgColor:-  "+ phoneErrorMsgColor);
	}
	else if (cnt==5 && cnt==6) //For Existing email id and Valid data, error message should not display
	{
		try
		{
			sa.assertFalse(l1.getWebElement("PD_FN_Error_message", "Profile//PersonalDetails.properties").isDisplayed());
			sa.assertFalse(l1.getWebElement("PD_LN_Error_message", "Profile//PersonalDetails.properties").isDisplayed());
			sa.assertFalse(l1.getWebElement("PD_Email_Error_message", "Profile//PersonalDetails.properties").isDisplayed());
			sa.assertFalse(l1.getWebElement("PD_Phone_Error_message", "Profile//PersonalDetails.properties").isDisplayed());
		} catch (Exception e)
		{
			sa.assertTrue(true);
		}
	}
	
	i++;
	
	sa.assertAll();
}


@Test(groups="reg",dataProvider="personalDetailsPasswordVal",dataProviderClass=Data.class, description="OOTB-059 DRM-633 AND OOTB-059 DRM-637")
public void TC04_PD_val_Password(TestData t) throws Exception
{
	try 
	{
		l1.getWebElement("PD_ChangePWD_Link", "Profile//PersonalDetails.properties").click();
	} 
	catch (Exception e) 
	{
		sa.assertTrue(true);
	}

	log.info("Enter passwords in the fields");
	l1.getWebElement("PD_CurrentPwd_Textbox", "Profile//PersonalDetails.properties").sendKeys(t.get(0));
	l1.getWebElement("PD_NewPwd_Textbox", "Profile//PersonalDetails.properties").sendKeys(t.get(0));
	l1.getWebElement("PD_ConfirmPwd_Textbox", "Profile//PersonalDetails.properties").sendKeys(t.get(0));
	
	log.info("Click on Update Button");
	l1.getWebElement("PD_Update_Btn", "Profile//PersonalDetails.properties").click();

	Thread.sleep(3000);
	log.info("Verify the message for the iteration  i :- " +i);
	if(i==0)	//For Blank fields
	{
		sa.assertTrue(l1.getWebElement("PD_Current_PassEM", "Profile//PersonalDetails.properties").isDisplayed());
		sa.assertTrue(l1.getWebElement("PD_New_PassEM", "Profile//PersonalDetails.properties").isDisplayed());
		sa.assertTrue(l1.getWebElement("PD_Confm_PassEM", "Profile//PersonalDetails.properties").isDisplayed());
		
		log.info("Verify the Error message color");
		sa.assertEquals(l1.getWebElement("PD_Confm_PassEM", "Profile//PersonalDetails.properties").getCssValue("color"), 
				ErrorMessageColor);
		sa.assertEquals(l1.getWebElement("PD_New_PassEM", "Profile//PersonalDetails.properties").getCssValue("color"), 
				ErrorMessageColor);
		sa.assertEquals(l1.getWebElement("PD_Confm_PassEM", "Profile//PersonalDetails.properties").getCssValue("color"), 
				ErrorMessageColor);
	}
	else if(i==1 && i==2 && i==3)	// invalid passwords 
	{
		sa.assertTrue(l1.getWebElement("PD_Invalid_PassEM", "Profile//PersonalDetails.properties").isDisplayed());
		sa.assertTrue(l1.getWebElement("PD_Invalid_PassEM", "Profile//PersonalDetails.properties").isDisplayed());
		sa.assertTrue(l1.getWebElement("PD_Invalid_PassEM", "Profile//PersonalDetails.properties").isDisplayed());
		
		log.info("Verify the Error message color");
		sa.assertEquals(l1.getWebElement("PD_Invalid_PassEM", "Profile//PersonalDetails.properties").getCssValue("color"), 
				ErrorMessageColor);
		sa.assertEquals(l1.getWebElement("PD_Invalid_PassEM", "Profile//PersonalDetails.properties").getCssValue("color"), 
				ErrorMessageColor);
		sa.assertEquals(l1.getWebElement("PD_Invalid_PassEM", "Profile//PersonalDetails.properties").getCssValue("color"), 
				ErrorMessageColor);
	}
	
	else if(i==4 && i==5)	// For Less chars
	{
		sa.assertTrue(l1.getWebElement("PD_Invalid_Current_PassEM", "Profile//PersonalDetails.properties").isDisplayed());
		log.info("Verify the Error message color");
		sa.assertEquals(l1.getWebElement("PD_Invalid_Current_PassEM", "Profile//PersonalDetails.properties").getCssValue("color"), 
				ErrorMessageColor);
		log.info("Verify the textbox background color");
		sa.assertEquals(l1.getWebElement("PD_CurrentPwd_Textbox", "Profile//PersonalDetails.properties").getCssValue("background"), 
				ErrorMessageColor);
		
	}
	
	else if(i==6)	// For invalid password AND invalid Phone number
	{
		sa.assertTrue(l1.getWebElement("PD_Invalid_ConfPsdEM", "Profile//PersonalDetails.properties").isDisplayed());
		log.info("Verify the textbox background color");
		sa.assertEquals(l1.getWebElement("PD_Invalid_ConfPsdEM", "Profile//PersonalDetails.properties").getCssValue("background"), 
				ErrorMessageColor);
	}

	
	i++;
	
	sa.assertAll();
}

	public void clearPD_Details() throws Exception
	{
		l1.getWebElement("PD_FN_TextBox", "Profile//PersonalDetails.properties").clear();
		l1.getWebElement("PD_LN_Textbox", "Profile//PersonalDetails.properties").clear();;
		l1.getWebElement("PD_NewEmail_Textbox", "Profile//PersonalDetails.properties").clear();
		l1.getWebElement("PD_PhoneNum_Textbox", "Profile//PersonalDetails.properties").clear();
	}
}
