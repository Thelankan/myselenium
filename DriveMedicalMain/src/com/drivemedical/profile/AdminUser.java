package com.drivemedical.profile;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.html5.Location;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;
import org.testng.xml.XmlTest;

import com.drivemedical.projectspec.GlobalFunctions;
import com.drivemedical.utilgeneric.BaseTest;
import com.drivemedical.utilgeneric.GetData;

public class AdminUser extends BaseTest{
	
	boolean verify;
	int count;
	List<WebElement> nameLinks ;
	List<WebElement> editLinks;
	List<WebElement> status;
	List<WebElement> roles;
	String userStatus;
	
/*	@Test
	public void tc00_removeUser(XmlTest xmlTest) throws Exception
	{
		backOfc.deleteUser();
		log.info("navigate to application");
		new GlobalFunctions(driver).navigateToSite(xmlTest);
	}*/
	
	@Test(groups={"reg"},description="OOTB-072 DRM-608,609,610,615,616,617")
	public void tc01_UI_Test(XmlTest xmlTest) throws Exception
	{
		if(xmlTest.getParameter("executingIn").equalsIgnoreCase("mobile") || xmlTest.getParameter("executingIn").equalsIgnoreCase("Tablet"))
		{
			log.info("click on hamburger menu");
			l1.getWebElement("Header_Hamburger_Menu", "Shopnav//header.properties").click();
			log.info("click on hamburger sign in link");
			l1.getWebElement("Hamburger_SignIn_Link", "Shopnav//header.properties").click();
			p.logIn(xmlTest);
			log.info("click on hamburger menu");
			l1.getWebElement("Header_Hamburger_Menu", "Shopnav//header.properties").click();
			log.info("click on user link");
			l1.getWebElement("Hamburger_User_Link", "Shopnav//header.properties").click();
		}else {
		log.info("Log in to the application");
		l1.getWebElement("Header_SignIn_LINK", "Shopnav//header.properties").click();
		p.logIn(xmlTest);
		log.info("Navigate to users page");
		act.moveToElement(l1.getWebElement("Header_Reg_MyAct_Link", "Shopnav//header.properties")).perform();
		log.info("click on users link");
		l1.getWebElement("Header_User_Link", "Shopnav//header.properties").click();
		}
		
		log.info("verify heading");
		sa.assertTrue(l1.getWebElement("ManageUser_Heading", "Profile//AdminUser.properties").isDisplayed());
		System.out.println(l1.getWebElement("ManageUser_Heading", "Profile//AdminUser.properties").getText()+"manage");
		sa.assertEquals(GetData.getDataFromExcel("//data//GenericData.xls", "AdminUser", 1, 5).toString(), l1.getWebElement("ManageUser_Heading", "Profile//AdminUser.properties").getText().substring(0,12));
		log.info("create new user button");
		sa.assertTrue(l1.getWebElement("AdminUser_AddNewUser_Btn", "Profile//AdminUser.properties").isDisplayed());
		sa.assertEquals(GetData.getDataFromExcel("//data//GenericData.xls", "AdminUser", 2, 5), l1.getWebElement("AdminUser_AddNewUser_Btn", "Profile//AdminUser.properties").getText());
		log.info("sort by label");
		sa.assertTrue(l1.getWebElement("ManageUser_SortBy_Label_Top", "Profile//AdminUser.properties").isDisplayed(),"Sort By Label");
		log.info("sort by drop down");
		sa.assertTrue(l1.getWebElement("ManageUser_SortDropDown", "Profile//AdminUser.properties").isDisplayed(),"Sort By Drop Down");
		log.info("pagination text");
		sa.assertTrue(l1.getWebElement("ManageUser_PaginationText_Top", "Profile//AdminUser.properties").isDisplayed(),"Pagination top");
		if(xmlTest.getParameter("executingIn").equalsIgnoreCase("browser"))
		{
		log.info("name column");
		sa.assertTrue(l1.getWebElement("ManagerUser_Name_Column", "Profile//AdminUser.properties").isDisplayed(),"ManagerUser_Name_Column");
		log.info("roles column");
		sa.assertTrue(l1.getWebElement("ManagerUser_Roles_Column", "Profile//AdminUser.properties").isDisplayed(),"ManagerUser_Roles_Column");
		log.info("status column");
		sa.assertTrue(l1.getWebElement("ManagerUser_Status_Column", "Profile//AdminUser.properties").isDisplayed(),"ManagerUser_Status_Column");
		}
		log.info("fetch total name links");
		nameLinks=l1.getWebElements("NewUser_UN_Link", "Profile//AdminUser.properties");
		editLinks=l1.getWebElements("NewUser_Edit_Links","Profile//AdminUser.properties");
		status=l1.getWebElements("NewUser_Status_Links","Profile//AdminUser.properties");
		roles=l1.getWebElements("NewUser_Status_Links","Profile//AdminUser.properties");
		
		for(int i=0;i<nameLinks.size();i++)
		{
			log.info("verify name links");
			sa.assertTrue(nameLinks.get(i).isDisplayed(),"name Links"+i);
			log.info("verify edit links");
			sa.assertTrue(editLinks.get(i).isDisplayed(),"edit Links"+i);
			log.info("verify status");
			sa.assertTrue(status.get(i).isDisplayed(),"Status"+i);
		}
		
		log.info("sort by label bottm");
		sa.assertTrue(l1.getWebElement("ManageUser_SortBy_Label_Bottom", "Profile//AdminUser.properties").isDisplayed(),"ManageUser_SortBy_Label_Bottom");
		log.info("sort by drop down bottom");
		sa.assertTrue(l1.getWebElement("ManageUser_SortDropDown_Bottom", "Profile//AdminUser.properties").isDisplayed(),"ManageUser_SortDropDown_Bottom");
		log.info("pagination text bottom");
		sa.assertTrue(l1.getWebElement("ManageUser_PaginationText_Bottom", "Profile//AdminUser.properties").isDisplayed(),"ManageUser_PaginationText_Bottom");
		log.info("pagination bottom");
		sa.assertTrue(l1.getWebElement("ManageUser_Pagination_Bottom", "Profile//AdminUser.properties").isDisplayed(),"ManageUser_Pagination_Bottom");
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="OOTB-072 DRM-606")
	public void tc02_createUser_Test(XmlTest xmlTest) throws Exception
	{
		log.info("click on add new user button");
		l1.getWebElement("AdminUser_AddNewUser_Btn", "Profile//AdminUser.properties").click();
		log.info("Enter First Name");
		l1.getWebElement("NewUser_FN", "Profile//AdminUser.properties").sendKeys(GetData.getDataFromExcel("//data//GenericData.xls", "AdminUser", 1, 0));
		log.info("Enter Last Name");
		l1.getWebElement("NewUser_LN", "Profile//AdminUser.properties").sendKeys(GetData.getDataFromExcel("//data//GenericData.xls", "AdminUser", 2, 0));
		log.info("Enter Email Id");
		l1.getWebElement("NewUser_Email", "Profile//AdminUser.properties").sendKeys(GetData.getDataFromExcel("//data//GenericData.xls", "AdminUser", 3, 0));
		log.info("click on save button");
		l1.getWebElement("NewUser_Save_Btn", "Profile//AdminUser.properties").click();
		Thread.sleep(15000);
		log.info("verify user created or not");
		nameLinks = l1.getWebElements("NewUser_UN_Link", "Profile//AdminUser.properties");
		for(WebElement o:nameLinks)
		{
			log.info(""+o.getText());
			if(o.getText().equalsIgnoreCase(GetData.getDataFromExcel("//data//GenericData.xls", "AdminUser", 1, 1)))
			{
				verify=true;
				break;
			}
			++count;
		}
		System.out.println(count);
		sa.assertTrue(verify,"verifying created account");
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="OOTB-072 DRM-611")
	public void tc03_editUser(XmlTest xmlTest) throws Exception
	{
		//driver.navigate().refresh();
		Thread.sleep(5000);
		verify=false;
		log.info("click on edit link");
		editLinks=l1.getWebElements("NewUser_Edit_Links", "Profile//AdminUser.properties");
		for(int i=0;i<editLinks.size();i++)
		{
			Thread.sleep(5000);
/*			if(i==count)
			{*/
				editLinks.get(i).click();
				break;
				/*try
				{
					if(l1.getWebElement("EditUser_Popup", "Profile//AdminUser.properties").isDisplayed()){
						log.info("edit user pop up is displaying");
						break;
					}
				}catch(Exception e){
					driver.navigate().back();
					Thread.sleep(10000);
					editLinks.get(count).click();
					break;
				}
			}*/
		}
		log.info("edit user information");
		log.info("Enter First Name");
		l1.getWebElement("NewUser_FN", "Profile//AdminUser.properties").clear();
		l1.getWebElement("NewUser_FN", "Profile//AdminUser.properties").sendKeys(GetData.getDataFromExcel("//data//GenericData.xls", "AdminUser", 1, 2));
		log.info("Enter Last Name");
		l1.getWebElement("NewUser_LN", "Profile//AdminUser.properties").clear();
		l1.getWebElement("NewUser_LN", "Profile//AdminUser.properties").sendKeys(GetData.getDataFromExcel("//data//GenericData.xls", "AdminUser", 2, 2));
		log.info("Enter Email Id");
		l1.getWebElement("NewUser_Email", "Profile//AdminUser.properties").clear();
		l1.getWebElement("NewUser_Email", "Profile//AdminUser.properties").sendKeys(GetData.getDataFromExcel("//data//GenericData.xls", "AdminUser", 3, 2));
		log.info("click on save button");
		l1.getWebElement("NewUser_Save_Btn", "Profile//AdminUser.properties").click();
		Thread.sleep(10000);
		log.info("verify user details edited or not");
		nameLinks = l1.getWebElements("NewUser_UN_Link", "Profile//AdminUser.properties");
		count=0;
		for(WebElement o:nameLinks)
		{
			log.info(""+o.getText());
			if(o.getText().equalsIgnoreCase(GetData.getDataFromExcel("//data//GenericData.xls", "AdminUser", 2, 1)))
			{
				verify=true;
				break;
			}
			++count;
		}
		System.out.println(count);
		sa.assertTrue(verify);
		sa.assertAll();
	}

	@Test(groups={"reg"},description="OOTB-072 DRM-612")
	public void tc04_resetPwd() throws Exception
	{
		log.info("click on user name link");
		nameLinks=l1.getWebElements("NewUser_UN_Link", "Profile//AdminUser.properties");
		for(int i=0;i<nameLinks.size();i++)
		{
			if(i==count)
			{
				nameLinks.get(i).click();
			}
		}
		log.info("click on user Reset password link");
		l1.getWebElement("NewUser_ResetPwd_Btn", "Profile//AdminUser.properties").click();
		log.info("Enter new Password");
		l1.getWebElement("NewUser_NewPwd_Textbox", "Profile//AdminUser.properties").sendKeys(GetData.getDataFromExcel("//data//GenericData.xls", "AdminUser", 1, 3));
		log.info("Enter confirm Password");
		l1.getWebElement("NewUser_ConfirmPwd_Textbox", "Profile//AdminUser.properties").sendKeys(GetData.getDataFromExcel("//data//GenericData.xls", "AdminUser", 2, 3));
		log.info("click on update button");
		l1.getWebElement("NewUser_UpdatePwd_Btn", "Profile//AdminUser.properties").click();
		log.info("Should be in user details page");
		sa.assertTrue(l1.getWebElement("UserDetails_Heading", "Profile//AdminUser.properties").isDisplayed());
		log.info("verify success message");
		sa.assertTrue(l1.getWebElement("NewUser_SuccessMsg", "Profile//AdminUser.properties").isDisplayed());
		//sa.assertEquals(GetData.getDataFromExcel("//data//GenericData.xls", "AdminUser", 1, 4), l1.getWebElement("NewUser_SuccessMsg", "Profile//AdminUser.properties").getText());
		log.info("log out from application");
		if(xmlTest.getParameter("executingIn").equalsIgnoreCase("mobile") || xmlTest.getParameter("executingIn").equalsIgnoreCase("Tablet"))
		{
		log.info("click on hamburger menu");
		l1.getWebElement("Header_Hamburger_Menu", "Shopnav\\header.properties").click();
		log.info("click on sign out link");
		l1.getWebElement("Hamburger_SignOut_Link", "Shopnav\\header.properties").click();
		} else {
		act.moveToElement(l1.getWebElement("Header_Reg_MyAct_Link", "Shopnav\\header.properties")).perform();
		log.info("click on sign out link");
		l1.getWebElement("Header_SignOut_Link", "Shopnav\\header.properties").click();
		}
		log.info("log in with new credentials");
		p.navigateToLoginPage();
		l1.getWebElement("Login_Email_TextBox", "Profile\\login.properties").sendKeys(GetData.getDataFromExcel("//data//GenericData.xls", "AdminUser", 3, 2));
		l1.getWebElement("Login_Password_Box", "Profile\\login.properties").sendKeys(GetData.getDataFromExcel("//data//GenericData.xls", "AdminUser", 1, 3));
		l1.getWebElement("Login_Login_Btn", "Profile\\login.properties").click();
		
		if(xmlTest.getParameter("executingIn").equalsIgnoreCase("mobile") || xmlTest.getParameter("executingIn").equalsIgnoreCase("Tablet"))
		{
		log.info("Verify sign out Link");
		log.info("click on hamburger menu");
		l1.getWebElement("Header_Hamburger_Menu", "Shopnav\\header.properties").click();
		sa.assertTrue(gVar.assertVisible("Hamburger_SignOut_Link", "Shopnav\\header.properties"),"sign out link in mobile");
		log.info("click on sign out link");
		l1.getWebElement("Hamburger_SignOut_Link", "Shopnav\\header.properties").click();
		
		} else {
		log.info("Verify My Account Link");
		sa.assertTrue(l1.getWebElement("Header_Reg_MyAct_Link", "Shopnav\\header.properties").isDisplayed());
		log.info("click on sign out link");
		act.moveToElement(l1.getWebElement("Header_Reg_MyAct_Link", "Shopnav\\header.properties")).perform();
		log.info("click on sign out link");
		l1.getWebElement("Header_SignOut_Link", "Shopnav\\header.properties").click();
		}
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="OOTB-072 DRM-613,614")
	public void tc05_disableUser_Test(XmlTest xmlTest) throws Exception
	{
		log.info("Log in to the application");
		p.navigateToLoginPage();
		p.logIn(xmlTest);
		log.info("Navigate to users page");
		if(xmlTest.getParameter("executingIn").equalsIgnoreCase("mobile") || xmlTest.getParameter("executingIn").equalsIgnoreCase("Tablet"))
		{
			log.info("click on hamburger menu");
			l1.getWebElement("Header_Hamburger_Menu", "Shopnav//header.properties").click();
			log.info("click on user link");
			l1.getWebElement("Hamburger_User_Link", "Shopnav//header.properties").click();
		} else {
		act.moveToElement(l1.getWebElement("Header_Reg_MyAct_Link", "Shopnav//header.properties")).perform();
		log.info("click on users link");
		l1.getWebElement("Header_User_Link", "Shopnav//header.properties").click();
		}
		nameLinks = l1.getWebElements("NewUser_UN_Link", "Profile//AdminUser.properties");
		count=0;
		for(WebElement o:nameLinks)
		{
			if(o.getText().equalsIgnoreCase(GetData.getDataFromExcel("//data//GenericData.xls", "AdminUser", 2, 1)))
			{
				nameLinks.get(count).click();
				break;
			}
			++count;
		}
		log.info("Disable the user");
		l1.getWebElement("DisableUser_Link", "Profile//AdminUser.properties").click();
		log.info("click on disable button");
		l1.getWebElement("Disable_Btn","Profile//AdminUser.properties").click();
		log.info("Verify enable user button");
		sa.assertTrue(l1.getWebElement("EnableUser_Btn", "Profile//AdminUser.properties").isDisplayed(),"verify Enable user button");
		log.info("click on back to users button");
		l1.getWebElement("BackToUsers_Btn", "Profile//AdminUser.properties").click();
		log.info("verify user is disabled or not");
		status=l1.getWebElements("NewUser_Status_Links", "Profile//AdminUser.properties");
		nameLinks = l1.getWebElements("NewUser_UN_Link", "Profile//AdminUser.properties");
		count=0;
		for(WebElement o:nameLinks)
		{
			if(o.getText().equalsIgnoreCase(GetData.getDataFromExcel("//data//GenericData.xls", "AdminUser", 2, 1)))
			{
				userStatus=status.get(count).getText();
				break;
			}
			++count;
		}
		sa.assertEquals("Disabled", userStatus);
		log.info("Enable same user");
		nameLinks = l1.getWebElements("NewUser_UN_Link", "Profile//AdminUser.properties");
		count=0;
		for(WebElement o:nameLinks)
		{
			if(o.getText().equalsIgnoreCase(GetData.getDataFromExcel("//data//GenericData.xls", "AdminUser", 2, 1)))
			{
				nameLinks.get(count).click();
				break;
			}
			++count;
		}
		l1.getWebElement("EnableUser_Btn", "Profile//AdminUser.properties").click();
		log.info("verify disable user link");
		sa.assertTrue(l1.getWebElement("DisableUser_Link", "Profile//AdminUser.properties").isDisplayed(),"verify disable user button");
		log.info("click on back to users button");
		l1.getWebElement("BackToUsers_Btn", "Profile//AdminUser.properties").click();
		log.info("verify user is acivated or not");
		status=l1.getWebElements("NewUser_Status_Links", "Profile//AdminUser.properties");
		nameLinks = l1.getWebElements("NewUser_UN_Link", "Profile//AdminUser.properties");
		count=0;
		for(WebElement o:nameLinks)
		{
			if(o.getText().equalsIgnoreCase(GetData.getDataFromExcel("//data//GenericData.xls", "AdminUser", 2, 1)))
			{
				System.out.println(count);
				userStatus=status.get(count).getText();
				break;
			}
			++count;
		}
		sa.assertEquals("Active", userStatus);
		sa.assertAll();
		
	}
	
}
