package com.drivemedical.profile;

import java.util.Random;

import org.openqa.selenium.By;
import org.testng.annotations.Test;
import org.testng.xml.XmlTest;

import com.drivemedical.utilgeneric.BaseTest;
import com.drivemedical.utilgeneric.GetData;

public class PurchaseHistoryOld extends BaseTest
{
	
	String cartID;
	String productColor;
	String productSize;
	String productPrice;
	String productTotalPrice;
	int quantity;
	String productId;
	String shippToAddress;
	String selectedShippingMethod;
	String poNumber;
	String orderID;
	
	String OrderHistoryProperties = "Profile//OrderHistory.properties";
	
	@Test(groups={"reg"}, description="Place an Order")
	public void TC06_placeOrder(XmlTest xmlTest) throws Exception
	{
		log.info("Navigate to Login page");
		p.navigateToLoginPage();
		p.logIn(xmlTest);
		
		log.info("Place an Order");
		log.info("Navigate to CART page");
		productId = GetData.getDataFromExcel("//data//GenericData.xls", "Products", 1, 1);
		quantity = 1;
		s.navigateToCart(productId, 1, quantity);
		log.info("Verify the CART page");
		sa.assertTrue(gVar.assertVisible("Cart_Heading", "Cart//Cart.properties"),"Verify the CART page");
		
		log.info("Collect the Cart ID");
		cartID = l1.getWebElement("Cart_ID", "Cart//Cart.properties").getText();
		System.out.println("cartID:- "+ cartID);
		
		log.info("Collect the product Details");
		productColor = l1.getWebElement("Cart_ProductColor", "Cart//Cart.properties").getText().split(":")[1].trim();
		System.out.println("productColor:- "+ productColor);
		productSize = l1.getWebElement("Cart_ProductSize", "Cart//Cart.properties").getText().split(":")[1].trim();
		System.out.println("productSize:- "+ productSize);
		productPrice = l1.getWebElement("Cart_ItemPrice", "Cart//Cart.properties").getText();
		System.out.println("productPrice:- "+ productPrice);
		productTotalPrice = l1.getWebElement("Cart_ItemTotal", "Cart//Cart.properties").getText();
		System.out.println("productTotalPrice:- "+ productTotalPrice);
		
		log.info("Click on PROCEED TO CHECKOUT button");
		l1.getWebElement("Cart_CheckoutButton", "Cart//Cart.properties").click();
		
		log.info("Verify the Checkout page");
		sa.assertTrue(gVar.assertVisible("Checkout_Heading", "Checkout//Checkout.properties"),"Verify the CHECKOUT page");
		
		log.info("Click on Select Pick Up link");
		l1.getWebElement("Checkout_SelectPickUpLink", "Checkout//Checkout.properties").click();
		
		log.info("Click on radio button");
		l1.getWebElement("ShipTo_Pickup_Address_Radio", "Checkout//ShipTo.properties").click();
		
		log.info("Click on SELECT pickup address CONTINUE button");
		l1.getWebElement("ShipTo_Continue_button", "Checkout//ShipTo.properties").click();
		
		log.info("Collect the SHIPPTO Address");
		shippToAddress = l1.getWebElement("ShipTo_Shipping_Addr", "Checkout//ShipTo.properties").getText();
		System.out.println("shippToAddress:- "+ shippToAddress);
		
		if (gVar.assertVisible("ShippingMtd_Next_Btn", "Checkout//ShippingMethod.properties")) 
		{
			l1.getWebElement("ShippingMtd_Next_Btn", "Checkout//ShippingMethod.properties").click();
			
		}
		log.info("Collect the shipping method");
		selectedShippingMethod = l1.getWebElement("ShippingMtd_Selected_ShippingMethod", "Checkout//ShippingMethod.properties").getText();
		System.out.println("selectedShippingMethod:- " + selectedShippingMethod);
		
		log.info("Enter PO number");
		sa.assertTrue(gVar.assertVisible("PT_POBox_Textbox", "Checkout//PaymentType.properties"),"verify the PO number");
		
		log.info("Enter PO number");
		Random rand = new Random();
		int randomNumber = rand.nextInt(90000) + 10000;
		poNumber = Integer.toString(randomNumber);
		System.out.println("Random poNumber:- "+poNumber);
		l1.getWebElement("PT_POBox_Textbox", "Checkout//PaymentType.properties").sendKeys(poNumber);
		
		log.info("Click on CONTINUE button in payment section");
		l1.getWebElement("Next_Button", "Checkout//PaymentType.properties").click();
		
		log.info("Click on PLACE ORDER button");
		l1.getWebElement("OrderReview_PlaceOrder_Btn", "Checkout//OrderReview.properties").click();
		
		log.info("Verify the THANK YOU page");
		sa.assertTrue(gVar.assertVisible("OrderConfirmation_Heading", "Checkout//OrderConfirmation.properties"),"Verify the Order Confirmation page");
		
		log.info("Collect the Order ID");
		orderID = l1.getWebElement("collect the OrderID in Thank you page", OrderHistoryProperties).getText();
		System.out.println("Order ID:- "+ orderID);
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="DMED-010 DRM-952")
	public void defaultSortingOption() throws Exception
	{
		log.info("Navigate to Order History page");
		act.moveToElement(l1.getWebElement("Header_Reg_MyAct_Link", "Shopnav//header.properties")).perform();
		l1.getWebElement("Header_OrderHistory_Link", "Shopnav//header.properties").click();
		
		log.info("Verify the Order History page");
		String heading = GetData.getDataFromExcel("//data//GenericData.xls", "OrderHistory", 1, 0);
		sa.assertEquals(gVar.assertEqual("OrderHistory_Section_Header", OrderHistoryProperties), 
				heading,"Order History Page Heading");
		
		log.info("Verify the Default Sorting option");
		String exptSelectedVal = GetData.getDataFromExcel("//data//GenericData.xls", "OrderHistory", 1, 1);
		
		gVar.assertequalsIgnoreCase(gVar.assertEqual("OrderHistory_SortBy_SelectedOption_Top", OrderHistoryProperties), 
				exptSelectedVal,"Selected SortBy value TOP_1");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("OrderHistory_SortBy_Dropdown_Top", OrderHistoryProperties, "title"), 
				exptSelectedVal, "Selected SortBy value TOP_2");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("OrderHistory_SortBy_SelectedOption_Bottom", OrderHistoryProperties), 
				exptSelectedVal,"Selected SortBy value BOTTOM_1");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("OrderHistory_SortBy_Dropdown_Bottom", OrderHistoryProperties, "title"), 
				exptSelectedVal, "Selected SortBy value BOTTOM_2");
		
		sa.assertAll();
	}
	
	
	@Test(groups={"reg"}, description="DMED-010 DRM-948")
	public void verifyOrderedProductIn_PurchaseHistoryPage() throws Exception
	{
		
		sa.assertEquals(gVar.assertEqual("OrderHistory_AllOrderId", OrderHistoryProperties),
				orderID,"Verify the Order ID");
		
		log.info("Click on ORDER ID link in Order history page");
		l1.getWebElement("OrderHistory_AllOrderId", OrderHistoryProperties).click();
		
		log.info("Verify the ORDER DETAILS page");
		String activeBreadCrumb = l1.getWebElement("Breadcrumb_ActiveElement", "Generic//Generic.properties").getText().split(" ")[1];
		System.out.println("activeBreadCrumb:- "+ activeBreadCrumb);
		sa.assertEquals(activeBreadCrumb, orderID,"Verify the Order ID in breadcrumb");
		
		String orderIdText = l1.getWebElement("OrderDetails_OrderId", OrderHistoryProperties).getText();
		System.out.println("orderIdText:-  "+ orderIdText);
		orderIdText.replaceAll("[^0-9]", "");
		System.out.println("orderId in Order Details page:-  "+ orderIdText);
		sa.assertEquals(orderIdText, orderID,"Verify the OrderID in order details page");
		
		log.info("Collect the Product details in Order history page");
		String proDetail = l1.getWebElement("OrderDetails_OrderedProduct_Details", OrderHistoryProperties).getText();
		System.out.println("proDetail:- " + proDetail);
		sa.assertFalse(true,"Compair the product details with PDP");
		
		sa.assertTrue(gVar.assertVisible("OrderDetails_OrderedProduct_Image", OrderHistoryProperties),"Verify the Product image");
		
		
		sa.assertAll();
	}
	
	
	@Test(groups={"reg"},description="DMED-010 DRM-966")
	public void pagination_Functionality() throws Exception
	{
		log.info("Navigate to Order History page");
		act.moveToElement(l1.getWebElement("Header_Reg_MyAct_Link", "Shopnav//header.properties")).perform();
		l1.getWebElement("Header_OrderHistory_Link", "Shopnav//header.properties").click();
		
		log.info("Verify the Pagination section");
		sa.assertTrue(gVar.assertVisible("OrderHistory_Pagination_Top", OrderHistoryProperties), "Verify the Pagination TOP");
		sa.assertTrue(gVar.assertVisible("OrderHistory_Pagination_Bottom", OrderHistoryProperties), "Verify the Pagination Bottom");
		
		log.info("Verify the Default selected page");
		sa.assertEquals(gVar.assertEqual("OrderHistory_Pagination_Top_CurrentPage", OrderHistoryProperties), 
				"1","Verify the Default selected page in pagination");
		sa.assertEquals(gVar.assertEqual("OrderHistory_Pagination_Bottom_CurrentPage", OrderHistoryProperties), 
				"1","Verify the Default selected page in pagination");
		
		log.info("Preview link should be disabled");
		sa.assertFalse(l1.getWebElement("OrderHistory_PaginationPreview_Top", OrderHistoryProperties).isEnabled(),"Preview button top");
		sa.assertFalse(l1.getWebElement("OrderHistory_PaginationPreview_Bottom", OrderHistoryProperties).isEnabled(),"Preview button bottom");
		
		
//		String previewTextTop = l1.getWebElement("OrderHistory_PaginationPreview_Top", OrderHistoryProperties).getAttribute("class");
//		System.out.println("previewText:- "+previewTextTop);
//		sa.assertTrue(previewTextTop.contains("disabled"),"verify the disabled text");
//		String previewTextBottom = l1.getWebElement("OrderHistory_PaginationPreview_Top", OrderHistoryProperties).getAttribute("class");
//		System.out.println("previewText:- "+previewTextBottom);
//		sa.assertTrue(previewTextBottom.contains("disabled"),"verify the disabled text");
		
//							int cnt=1;
//							boolean booleanNext = true;
//							while (booleanNext) 
//							{
//								System.out.println("LOOP:-" +cnt);
//								if (gVar.assertVisible("OrderHistory_PaginationNext_Link", OrderHistoryProperties)) 
//								{
//									log.info("Click on Next link");
//									l1.getWebElement("OrderHistory_PaginationNext_Link", OrderHistoryProperties).click();
//									Thread.sleep(3000);
//									String currentPageTop = l1.getWebElement("OrderHistory_Pagination_Top_CurrentPage", OrderHistoryProperties).getText();
//									String currentPageBottom = l1.getWebElement("OrderHistory_Pagination_Bottom_CurrentPage", OrderHistoryProperties).getText();
//									sa.assertEquals(cnt+"", currentPageTop);
//									sa.assertEquals(currentPageTop, currentPageBottom,"page should be same in top and bottom pagination");
//									
//									
//								}
//								else 
//								{
//									System.out.println("User is at last page");
//									booleanNext=false;
//								}
//								cnt++;
//							}
//							System.out.println("Cnt:- "+cnt);
//							
//							log.info("Click on previous link in pagination section");
//							boolean booleanPrevious = true;
//							while (booleanPrevious) 
//							{
//								cnt--;
//								System.out.println("LOOP:-" +cnt);
//								if (gVar.assertVisible("OrderHistory_PaginationPreview_Top", OrderHistoryProperties)) 
//								{
//									log.info("Click on Next link");
//									l1.getWebElement("OrderHistory_PaginationPreview_Top", OrderHistoryProperties).click();
//									Thread.sleep(3000);
//									String currentPageTop = l1.getWebElement("OrderHistory_Pagination_Top_CurrentPage", OrderHistoryProperties).getText();
//									String currentPageBottom = l1.getWebElement("OrderHistory_Pagination_Bottom_CurrentPage", OrderHistoryProperties).getText();
//									sa.assertEquals(cnt+"", currentPageTop);
//									sa.assertEquals(currentPageTop, currentPageBottom,"page should be same in top and bottom pagination");
//									
//								}
//								else 
//								{
//									System.out.println("User is at last page");
//									booleanPrevious=false;
//								}
//								
//							}
//							
//							log.info("Click on page numbers");
		
					
		String ordersCountText = l1.getWebElement("OrderHistory_OrdersCount_Section", OrderHistoryProperties).getText();
		System.out.println("ordersCountText:- "+ ordersCountText);
		int firstLastIndex = ordersCountText.lastIndexOf(" ");
		String temp = ordersCountText.substring(0, firstLastIndex);
		System.out.println("temp:- "+ temp);
		String total = temp.substring(temp.lastIndexOf(" "), firstLastIndex).trim();
		System.out.println("total:- "+ total);
		int totalOrders = Integer.parseInt(total);
		System.out.println("totalOrders:- "+ totalOrders);
		
		int numberOfPages = totalOrders/5+1;
		int numberOfOrdersPerPage;
		int totalOrdersCount = 0;
		int r1 = 1;
		int r2 = 0;
		int pageNum =1;
		int pNum = 2;
		System.out.println("Number of pages:- "+numberOfPages);
		for (int j = 0; j < 3; j++) 
		{
			for (int i = 0; i < numberOfPages; i++) 
			{
				System.out.println("LOOP:- "+i);
				
				log.info("Verify the Current page");
				String currentPageTop = l1.getWebElement("OrderHistory_Pagination_Top_CurrentPage", OrderHistoryProperties).getText();
				String currentPageBottom = l1.getWebElement("OrderHistory_Pagination_Bottom_CurrentPage", OrderHistoryProperties).getText();
				sa.assertEquals(pageNum+"", currentPageTop);
				sa.assertEquals(currentPageTop, currentPageBottom,"page should be same in top and bottom pagination");
				
				log.info("Collect the number of orders per page");
				numberOfOrdersPerPage = l1.getWebElements("OrderHistory_AllOrderId", OrderHistoryProperties).size();
				numberOfOrdersPerPage= totalOrdersCount+numberOfOrdersPerPage;
				totalOrdersCount=numberOfOrdersPerPage;
				
				log.info("Verify the Order count message");
				String expected = r1+" - "+totalOrdersCount+" "+"of "+totalOrders+" "+"Orders";
				gVar.assertequalsIgnoreCase(gVar.assertEqual("OrderHistory_OrdersCount_Section", OrderHistoryProperties), 
						expected);
				
				if (j==0) 
				{
					log.info("Click on next button");
					l1.getWebElement("OrderHistory_PaginationNext_Link", OrderHistoryProperties).click();
					
					pageNum++;
				} 
				else if(j==1)
				{
					log.info("Click on previous button");
					l1.getWebElement("OrderHistory_PaginationPreview_Top", OrderHistoryProperties).click();
					
					pageNum--;
				}
				
				else if(j==2)
				{
					
					log.info("Click on PAge numbers");
					String xpath = "//div[contains(@class,'top')]//a[text()='"+pNum+"']";
					driver.findElement(By.xpath(xpath)).click();
					pNum++;
					pageNum++;
					
				}
				
				Thread.sleep(2000);
				r1=totalOrdersCount+1;	
			
			}
			System.out.println("totalOrdersCount:- "+totalOrdersCount); 
			sa.assertEquals(totalOrdersCount, totalOrders,"Verify the Total Orders");
			
		}
		
		sa.assertAll();
	}
	
	
	@Test(groups={"reg"}, description="DMED-010 DRM-1045")
	public void verify_Search_Functionality() throws Exception
	{
		String[]searchKeyWord = {orderID, poNumber, productId};
		
		for (int i = 0; i < searchKeyWord.length; i++) 
		{

			log.info("Search with material ID");
			System.out.println("searchKeyWord["+i+"]:- " + searchKeyWord);
			l1.getWebElement("OrderHistory_SearchBox", OrderHistoryProperties).sendKeys(searchKeyWord[i]);
			log.info("Click on Search button");
			l1.getWebElement("OrderHistory_Search_Button", OrderHistoryProperties).click();
			
			log.info("Verify the search result table");
			sa.assertTrue(gVar.assertVisible("OrderHistory_OrdersListTable", OrderHistoryProperties),"Verify the ");
			
			log.info("Verify the PO number");
			sa.assertEquals(gVar.assertEqual("OrderHistory_AllPONumber", OrderHistoryProperties), 
					poNumber,"Verify the PO Number");			
			
			log.info("Verify the Order ID");
			sa.assertEquals(gVar.assertEqual("OrderHistory_AllOrderId", OrderHistoryProperties), 
					orderID,"Verify the Order ID");
			
			if (searchKeyWord[i]==productId) 
			{
				log.info("Click on Order Id number");
				l1.getWebElement("OrderHistory_AllOrderId", OrderHistoryProperties).click();
				
				log.info("Verify the Material ID in Order Details page");
				String productDetail = l1.getWebElement("OrderDetails_OrderedProduct_Details", OrderHistoryProperties).getText();
				System.out.println("productDetail:- "+ productDetail);
				
				log.info("Verify the material number in product details section");
				sa.assertTrue(productDetail.contains(productId),"Verify the Material ID in order details page");
			}
		}
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="DMED-010 DRM-967")
	public void addProductToCartFromOrderHistoryPage() throws Exception
	{
		log.info("Navigate to Order Details page");
		l1.getWebElement("OrderHistory_AllOrderId", OrderHistoryProperties).click();
		
		log.info("Collect the Product details in Order Details page");
		String productName = l1.getWebElement("OrderDetails_ProductName", OrderHistoryProperties).getText();
		String productColor = l1.getWebElement("OrderDetails_ProductColor", OrderHistoryProperties).getText();
		String productSize = l1.getWebElement("OrderDetails_ProductSize", OrderHistoryProperties).getText();
		String OrderTotal = l1.getWebElement("OrderDetails_Total", OrderHistoryProperties).getText();
		
		System.out.println("productName:- "+ productName);
		System.out.println("productColor:- "+ productColor);
		System.out.println("productSize:- "+ productSize);
		System.out.println("OrderTotal:- "+ OrderTotal);
		
		log.info("Click on REORDER button in order details page");
		l1.getWebElement("OrderDetails_Reorder1", OrderHistoryProperties).click();
		
		log.info("Verify the Checkout page");
		sa.assertTrue(gVar.assertVisible("Checkout_Heading", OrderHistoryProperties),"Verify the Checkout page");
		
		log.info("Verify the Product details in Order Review section");
		String proName = l1.getWebElement("OrderReview_ProductName", OrderHistoryProperties).getText();
		String proId = l1.getWebElement("OrderReview_ProductId", OrderHistoryProperties).getText();
		String proColor = l1.getWebElement("OrderReview_ProductColor", OrderHistoryProperties).getText();
		String proSize = l1.getWebElement("OrderReview_ProductSize", OrderHistoryProperties).getText();
		
		System.out.println("proName:- "+proName);
		System.out.println("proId:- "+proId);
		System.out.println("proColor:- "+proColor);
		System.out.println("proSize:- "+proSize);
		
		sa.assertFalse(true,"Continue");
		
		sa.assertAll();
	}
	

	

}
