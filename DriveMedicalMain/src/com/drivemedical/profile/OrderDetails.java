package com.drivemedical.profile;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.TimeZone;

import javax.sound.midi.MidiDevice.Info;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;
import org.testng.xml.XmlTest;

import com.drivemedical.utilgeneric.BaseTest;
import com.drivemedical.utilgeneric.GetData;
import com.gargoylesoftware.htmlunit.javascript.background.JavaScriptExecutor;

public class OrderDetails extends BaseTest
{
	String OrderHistoryProperties = "Profile//OrderHistory.properties";
	
//	ArrayList<String> allDates=new ArrayList<String>();
//	ArrayList<String> allDatesBeforeSort=new ArrayList<String>();
//	ArrayList<Double> totalPrices = new ArrayList<Double>();
//	ArrayList<String> totalPricesBeforeSort = new ArrayList<String>();
//	ArrayList<String> allIds = new ArrayList<String>();
//	ArrayList<String> allIdsBeforeSort = new ArrayList<String>();
	
	
	String cartID;
	String productColor;
	String productSize;
	String productPrice;
	String productTotalPrice;
	int quantity;
	String searchKeyWord;
	String shippToAddress;
	String selectedShippingMethod;
	static String poNumber;
	static String orderID;
	static String systemDate;
	static int numberOfProducts;
	
	List<String> proNameList; 
	ArrayList<String> materialIdsList;

	
	@Test(groups={"reg"}, description="OOTB-061 DRM-1188")
	public void TC00_uiOfOrderHistoryPage() throws Exception
	{
		
		log.info("Navigate to LOGIN page");
		p.navigateToLoginPage();
		p.logIn(xmlTest);
		
		Thread.sleep(2000);
		log.info("Navigate to Order history page");
		s.navigateToOrderHistoryPage();
		log.info("Verify the Order History page");
		String orderHistoryText = GetData.getDataFromExcel("//data//GenericData.xls", "OrderHistory", 1, 0);
		sa.assertEquals(gVar.assertEqual("OrderHistory_Section_Header", OrderHistoryProperties), 
				orderHistoryText,"Order History Page Heading");
		log.info("Verify the Breadcrumb");
		String activeBreadcrumb = orderHistoryText;
		gVar.assertequalsIgnoreCase(gVar.assertEqual("Breadcrumb_ActiveElement", "Generic//Generic.properties"), 
				activeBreadcrumb, "Active breadcrumb");
		
		sa.assertTrue(gVar.assertVisible("PD_ShopperName", "Profile//PersonalDetails.properties"),"Verify the USer name");
		
		gVar.assertequalsIgnoreCase(gVar.assertEqual("Leftnav_Active_Component", "Profile//leftnav.properties"), 
				orderHistoryText,"Verify the active component in leftNAv section");
		
		String showAllDAtesText = GetData.getDataFromExcel("//data//GenericData.xls", "OrderHistory", 1, 2);
		gVar.assertequalsIgnoreCase(gVar.assertEqual("OrderHistory_SelectByDate_DropdownBox", OrderHistoryProperties,"title"), 
				showAllDAtesText,"Verify the default selected option in SortBy Date dropdown box");
		
		String showAllPriceText = GetData.getDataFromExcel("//data//GenericData.xls", "OrderHistory", 1, 3);
		gVar.assertequalsIgnoreCase(gVar.assertEqual("OrderHistory_SelectByPrice_DropdownBox", OrderHistoryProperties, "title"), 
				showAllPriceText,"Verify the default selected option in SortBy Price dropdown box");
		
		log.info("Search Section");
		String placeHolderText = GetData.getDataFromExcel("//data//GenericData.xls", "OrderHistory", 12, 0);
		sa.assertTrue(gVar.assertVisible("OrderHistory_SearchBox", OrderHistoryProperties),"Verify search box");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("OrderHistory_SearchBox", OrderHistoryProperties, "placeholder"), 
				placeHolderText,"Verify the Placeholder text");
		
		sa.assertTrue(gVar.assertVisible("OrderHistory_Search_Button", OrderHistoryProperties),"Verify the Search button");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("OrderHistory_Search_Button", OrderHistoryProperties), 
				"Search","verify the text on search button");
		
		log.info("Sort By Section");
		String sortByLabelText = GetData.getDataFromExcel("//data//GenericData.xls", "OrderHistory", 20, 0);
		gVar.assertequalsIgnoreCase(gVar.assertEqual("OrderHistory_SortBy_Label_Top", OrderHistoryProperties), 
				sortByLabelText,"Verify the SortBy LAbel Text TOP");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("OrderHistory_SortBy_Label_Bottom", OrderHistoryProperties), 
				sortByLabelText,"Verify the SortBy LAbel Text Bottom");		
		
		sa.assertTrue(gVar.assertVisible("OrderHistory_SortBy_SelectBox_Top", OrderHistoryProperties),"Verify the SortBy dropdown");
		sa.assertTrue(gVar.assertVisible("OrderHistory_SortBy_SelectBox_Bottom", OrderHistoryProperties),"Verify the SortBy dropdown");
		
		sa.assertTrue(gVar.assertVisible("OrderHistory_OrdersCount_Section", OrderHistoryProperties),"Verify the Result Count text");
		sa.assertFalse(true,"Verify the Result count bottom");
		
		String Heading1 = GetData.getDataFromExcel("//data//GenericData.xls", "OrderHistory", 13, 0);
		String Heading2 = GetData.getDataFromExcel("//data//GenericData.xls", "OrderHistory", 14, 0);
		String Heading3 = GetData.getDataFromExcel("//data//GenericData.xls", "OrderHistory", 15, 0);
		String Heading4 = GetData.getDataFromExcel("//data//GenericData.xls", "OrderHistory", 16, 0);
		String Heading5 = GetData.getDataFromExcel("//data//GenericData.xls", "OrderHistory", 17, 0);
		
		gVar.assertequalsIgnoreCase(gVar.assertEqual("OrderHistory_OrderNum_Heading", OrderHistoryProperties), 
				Heading1,"Verify the Order Number heading");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("OrderHistory_DatePlaced_Heading", OrderHistoryProperties), 
				Heading2,"Verify the Date Placed heading");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("OrderHistory_Status_Heading", OrderHistoryProperties), 
				Heading3,"Verify the Status heading");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("OrderHistory_PONumber_Heading", OrderHistoryProperties), 
				Heading4,"Verify the P.O. Number heading");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("OrderHistory_Total_Heading", OrderHistoryProperties), 
				Heading5,"Verify the Total heading");
		
		log.info("Verify the Pagination");
		sa.assertTrue(gVar.assertVisible("OrderHistory_Pagination_Top", OrderHistoryProperties),"Verify the Pagination TOP");
		sa.assertTrue(gVar.assertVisible("OrderHistory_Pagination_Bottom", OrderHistoryProperties),"Verify the Pagination Bottom");
		
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description = "DMED-209 DRM-748, OOTB-061 DRM-1187")
	public void TC01_mostRecentOption(XmlTest xmlTest) throws Exception
	{
		
		sa.assertTrue(gVar.assertVisible("OrderHistory_SortBy_SelectBox_Top", OrderHistoryProperties), "Verify SORTBY selectbox TOP");
		sa.assertTrue(gVar.assertVisible("OrderHistory_SortBy_SelectBox_Bottom", OrderHistoryProperties), "Verify SORTBY selectbox BOTTOM");
		
		log.info("Verify the default selected value");
		String exptSelectedVal = GetData.getDataFromExcel("//data//GenericData.xls", "OrderHistory", 1, 1);
		gVar.assertequalsIgnoreCase(gVar.assertEqual("OrderHistory_SortBy_SelectedOption_Top", OrderHistoryProperties), 
				exptSelectedVal,"Selected SortBy value TOP_1");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("OrderHistory_SortBy_Dropdown_Top", OrderHistoryProperties, "title"), 
				exptSelectedVal, "Selected SortBy value TOP_2");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("OrderHistory_SortBy_SelectedOption_Bottom", OrderHistoryProperties), 
				exptSelectedVal,"Selected SortBy value BOTTOM_1");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("OrderHistory_SortBy_Dropdown_Bottom", OrderHistoryProperties, "title"), 
				exptSelectedVal, "Selected SortBy value BOTTOM_2");
		
		log.info("Collect the Dates");
		List<WebElement> dates = l1.getWebElements("OrderHistory_AllDAtes", OrderHistoryProperties);
		log.info("allDates size:- "+ dates.size());
		ArrayList<String> allDates=new ArrayList<String>();
		ArrayList<String> allDatesBeforeSort=new ArrayList<String>();
		for (int i = 0; i < dates.size(); i++) 
		{
			StringBuffer sb = new StringBuffer(dates.get(i).getText());
			if (sb.charAt(4) !='0')
			{
				sb.insert(4, "0");
			}
			log.info("dateAdded:- "+ sb.toString());
			allDates.add(sb.toString());
		}
		
		allDatesBeforeSort = (ArrayList<String>) allDates.clone();
		log.info("allDates before Sort:- "+ allDates);
		log.info("Copy of allDates before Sort:- "+ allDatesBeforeSort);
		Collections.sort(allDates); //This will sort from low to High. But we need High to low
		log.info("allDates After Sort:- "+ allDates);	
		Collections.reverse(allDates);	//This will reverse the sorted array.
		log.info("allDates After Reverse:- "+ allDates);
		
		log.info("Verify the Sorted dates");
		sa.assertEquals(allDates, allDatesBeforeSort,"Verify the Sorted dates");
		
		sa.assertAll();
	}
	

	/*@Test(groups ={"reg"}, description="Collec date and price")
	public void TC02_collectOrderDetails() throws Exception
	{
		int loop = 0;
		boolean booleanVal = true;
		log.info("Collect the Dates and price in all the page");
		while (booleanVal) 
		{
			log.info("Loop:- " +loop);
			List<WebElement> dates = l1.getWebElements("OrderHistory_AllDAtes", OrderHistoryProperties);
			List<WebElement> totalPrice = l1.getWebElements("OrderHistory_AllTotalPrice", OrderHistoryProperties);
			List<WebElement> allOrderIds = l1.getWebElements("OrderHistory_AllOrderId", OrderHistoryProperties);
			log.info("allDates size:- "+ dates.size());
			
			for (int i = 0; i < dates.size(); i++) 
			{
				//Collecting the date
				String dateAdded = dates.get(i).getText();
				//converting date formate (ex:- "jan 1, 2018" to "jan 01, 2018")
				StringBuilder sb = new StringBuilder(dateAdded);
				if (sb.charAt(5) == ',') 
				{
					dateAdded = sb.insert(4, '0').toString();
				}

				//Collecting the price
				String totPrice = totalPrice.get(i).getText().replace("$", "").replace(",", "");
				
				//collecting the Order ID's
				String id = allOrderIds.get(i).getText();
				
				log.info("dateAdded:- "+ dateAdded);
				log.info("totPrice:- "+ totPrice);
				log.info("id:- "+ id);
				
				allDates.add(dateAdded);
				totalPrices.add(Double.parseDouble(totPrice));
				allIds.add(id);
				
			}
			log.info("Dates Placed per page -"+loop+"-"+ allDates);
			log.info("totalPrices per page -"+loop+"-"+ totalPrices);
			log.info("OrderIds per page -"+loop+"-"+allIds);
			
			if (gVar.assertVisible("OrderHistory_PaginationNext_Link", OrderHistoryProperties)) 
			{
				log.info("Click on NEXt button in pagination");
				l1.getWebElement("OrderHistory_PaginationNext_Link", OrderHistoryProperties).click();
			}
			else 
			{
				booleanVal = false;
			}
			
			Thread.sleep(4000);
			loop++;
		}	//End of WHILE loop
		
		log.info("Dates placed from all page:- "+allDates);	
		log.info("Total Price from all pages:- "+totalPrices);
		log.info("Order Ids from all pages:- "+allIds);
		
		log.info("Take a copy of OrderId, allDates and price arraylist");
		allDatesBeforeSort = (ArrayList<String>) allDates.clone();
		totalPricesBeforeSort = (ArrayList<String>) totalPrices.clone();
		allIdsBeforeSort = (ArrayList<String>) allIds.clone();
		
		log.info("Copy of Dates:- "+ allDatesBeforeSort);
		log.info("Copy of Total price:- "+ totalPricesBeforeSort);
		log.info("Copy of Order Ids:- "+ allIdsBeforeSort);
		
		log.info("Sort the all OrderIds, dates and Price");
		Collections.sort(allDates);
		Collections.sort(totalPrices);
		Collections.sort(allIds);
		
		log.info("allDates after Sort:- "+allDates);
		log.info("totalPrice after Sort:- "+totalPrices);
		log.info("OrderId's after sort:- "+ allIds);
		
		String ordersResult = l1.getWebElement("OrderHistory_OrdersCount_Section", OrderHistoryProperties).getText();
		log.info("ordersResult:- "+ordersResult);
		log.info("total orders Count:- "+ allIds.size());
		
		sa.assertAll();
	}
	*/
	
	@Test(groups={"reg"}, description  ="DMED-209 DRM-749, OOTB-061 DRM-1194")
	public void TC03_sortBy_BasedOn_OrderPlacedDate() throws Exception
	{
		ArrayList<String> tempDates = new ArrayList<String>();

		log.info("Select the Date options");
		int rowNum = 2;
		for (int i = 0; i < 2; i++) 
		{
			log.info("LOOP I:- "+i);
			ArrayList<String> datesAfterSortBy = new ArrayList<String>();
			
			WebElement selectBoxElement = l1.getWebElement("OrderHistory_SortBy_SelectBox_Top", OrderHistoryProperties);
			Select sel = new Select(selectBoxElement);
			String data = GetData.getDataFromExcel("//data//GenericData.xls", "OrderHistory", rowNum, 1);
			sel.selectByVisibleText(data);
			Thread.sleep(1000);
			
			log.info("collect the prices");
			List<WebElement> dates = l1.getWebElements("OrderHistory_AllDAtes", OrderHistoryProperties);
			log.info("dates:- "+ dates.size());
			for (int j = 0; j < dates.size(); j++) 
			{
				log.info("LOOP J:- "+j);
				String date = dates.get(j).getText();
				//converting date formate (ex:- "jan 1, 2018" to "jan 01, 2018")
				StringBuffer sb1 = new StringBuffer(date);
				if (sb1.charAt(4)!='0') 
				{
					date = sb1.insert(4, '0').toString();
				}
				
				datesAfterSortBy.add(date);
				
			}
			
			log.info("datesAfterSortBy:- "+ datesAfterSortBy);
			tempDates = (ArrayList<String>) datesAfterSortBy.clone();
			if (i==1) //for Date Placed - Newest To Oldest, Reverse the DATE ARRAY list
			{
				Collections.reverse(tempDates);	//to convert High to low
			}
			else 
			{
				Collections.sort(tempDates);	//to convert low to High
			}
			
			log.info("tempDates:- "+tempDates);

			log.info("Verify the SORTBY values");
			sa.assertEquals(datesAfterSortBy, tempDates, "Verify the dates");
			
			rowNum++;
		}
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="DMED-209 DRM-755")
	public void TC04_sortByDate_dropdown_Functionality() throws Exception
	{
		log.info("Verify the Select by DATE dropdownbox");
		sa.assertTrue(gVar.assertVisible("OrderHistory_SelectByDate", OrderHistoryProperties),"Select By Date select box");
		int rowNum = 1;
		for (int i = 0; i < 5; i++) 
		{
			log.info("LOOP:- "+i);
			WebElement dateDropDownElement = l1.getWebElement("OrderHistory_SelectByDate", OrderHistoryProperties);
			Select selectBox = new Select(dateDropDownElement);
			
			String actualOption = selectBox.getOptions().get(i).getText();
			log.info("actualOption:- "+ actualOption);
			
			String exptdOption = GetData.getDataFromExcel("//data//GenericData.xls", "OrderHistory", rowNum, 2);
			sa.assertEquals(actualOption, exptdOption,"verify the Dropdown options");
			
			log.info("Select the options");
			selectBox.selectByIndex(i);
			
			log.info("Verify the Slected options");
			Thread.sleep(3000);
			
			gVar.assertequalsIgnoreCase(gVar.assertEqual("OrderHistory_SelectByDate_DropdownBox", OrderHistoryProperties,"title"), 
					exptdOption,"Verify the Selected option");
			
			rowNum++;
		}
		
		sa.assertTrue(false,"Verify Custom Date Range");
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="DMED-209 DRM-757")
	public void TC05_sortBy_TotalPrice_Dropdown_Functionality() throws Exception
	{
		log.info("Verify the Select by price dropdown");
		sa.assertTrue(gVar.assertVisible("OrderHistory_SelectByPrice_DropdownBox", OrderHistoryProperties),"Select by total price dropdownbox");
		sa.assertTrue(gVar.assertVisible("OrderHistory_SelectByPrice", OrderHistoryProperties),"Verify the select by total price select box");
		double actTotPriceVal = 0;
		//Selecting the different sortBy options of price
		for (int i = 1; i <= 3; i++) 
		{
			log.info("LOOP I:- "+ i);
			WebElement totalPriceSelectBox = l1.getWebElement("OrderHistory_SelectByPrice", OrderHistoryProperties);
			Select selectBox = new Select(totalPriceSelectBox);
			
			log.info("Select the total price options");
			selectBox.selectByIndex(i);
			Thread.sleep(3000);
			int pageNumber = 1;
			log.info("Verify for all pages in pagination");
			boolean val = true;
			while (val) 
			{
				if (gVar.assertVisible("OrderHistory_OrdersListTable", OrderHistoryProperties)) 
				{
					log.info("Collect the price");
					List<WebElement> allPrices = l1.getWebElements("OrderHistory_AllTotalPrice", OrderHistoryProperties);
					for (int j = 0; j < allPrices.size(); j++) 
					{
						log.info("LOOP J:- "+j);
						String totPrice = allPrices.get(j).getText().replace("$", "").replace(",", "");
						log.info("totPrice:- "+ totPrice);
						actTotPriceVal = Double.parseDouble(totPrice);
						log.info("actTotPriceVal:- "+ actTotPriceVal);
						
						if (i==1) //for under $100 
						{
							sa.assertTrue(actTotPriceVal<=100.00,"less than 100"+ "Page:- "+pageNumber +" Loop:- "+ j);
						} 
						else if (i==2) //for $100-$1000
						{
							sa.assertTrue(100.00 < actTotPriceVal && actTotPriceVal<=1000.00, "grater than 100 and less than 1000 "+"Page:- "+ pageNumber + " Loop:- "+ j);
						}	
						else //for $1000 and above
						{
							sa.assertTrue(actTotPriceVal>= 1000.00,"grater than 1000 "+"Page:- " + pageNumber + " Loop:- "+ j);
						}
					}
					
				} 
				else 
				{
					log.info("No Prices are displaying");
				}
				
				if (gVar.assertVisible("OrderHistory_PaginationNext_Link", OrderHistoryProperties)) 
				{
					log.info("Click on nextlink in pagination");
					l1.getWebElement("OrderHistory_PaginationNext_Link", OrderHistoryProperties).click();
				}
				else
				{
					log.info("User is at last page, no need to click on NExt link in pagination");
					val = false;
				}
				
				pageNumber++;
			}	//End of while loop
				
		} 
		
		sa.assertAll();
	}
	
 	@Test(groups={"reg"}, description="OOTB-061 DRM-1195")
	public void TC06_sortByOrderNumber() throws Exception
	{
		
		sa.assertFalse(true,"SERVER ERROR is displaying on selecting SORT BY ORDER NUMBER, Uncomment the belo code once after fixing the issue");
	
//		log.info("Navigate to Order history page");
//		s.navigateToOrderHistoryPage();
//		
//		log.info("Sort Order Id");
//		int rowNum = 4;	
//		//Verifying for both Ascending and Descending orderIds
//		for (int i = 0; i < 2; i++) 
//		{
//			List<WebElement> orderIds = l1.getWebElements("OrderHistory_AllOrderId", OrderHistoryProperties);
//			
//			log.info("LOOP:- "+i);
//			String orderIdSortOption = GetData.getDataFromExcel("//data//GenericData.xls", "OrderHistory", rowNum, 1);
//			
//			log.info("Select the option "+orderIdSortOption);
//			WebElement sortByDropDownele = l1.getWebElement("OrderHistory_SortBy_SelectBox_Top", OrderHistoryProperties);
//			
//			Select sel = new Select(sortByDropDownele);
//			sel.selectByVisibleText(orderIdSortOption);
//			Thread.sleep(2000);
//			
//			//Check for three pages(Pagination)
//			for (int k = 0; k < 3; k++)
//			{
//				//Create a array list to store all ids
//				ArrayList<String> allOrderIds = new ArrayList<String>();
//				ArrayList<String> allOrderIdsCopy = new ArrayList<String>();
//				
//				//Collect the order ids and stor it in one array
//				for (int j = 0; j < orderIds.size(); j++)
//				{
//					String actualId = orderIds.get(j).getText();
//					allOrderIds.add(actualId);
//				}
//				
//				//Make a copy of stored array list
//				allOrderIdsCopy = (ArrayList<String>) allOrderIds.clone();
//
//				//Sort the copy of saved ordered arraylist
//				Collections.sort(allOrderIdsCopy);
//				
//				log.info("Verify the Sorted ids");
//				sa.assertEquals(allOrderIds, allOrderIdsCopy,"Verify the sorted Ids");
//				
//				//Verify the next link in pagination. If its present click on next link
//				if (gVar.assertVisible("OrderHistory_PaginationNext_Link", OrderHistoryProperties)) 
//				{
//					log.info("Click on next button in pagination");
//					l1.getWebElement("OrderHistory_PaginationNext_Link", OrderHistoryProperties).click();
//					Thread.sleep(2000);
//				} 
//				else 
//				{
//					log.info("There is no next page");
//					break;	//if there is no next button in pagination, exit from the loop
//				}
//			}
//		}
		
		sa.assertAll();
	}
	
	
	@Test(groups = {"reg"}, description="place Order")
	public void TC07_placeOrder() throws Exception
	{
		//############### NOT REQUIRED ###########################
//		log.info("Navigate to LOGIN page");
//		p.navigateToLoginPage();
//		p.logIn(xmlTest);
		//##########################################
		
		log.info("Navigate to CART page");
		searchKeyWord = GetData.getDataFromExcel("//data//GenericData.xls", "Products", 1, 1);
		quantity = 1;
		numberOfProducts = 2;	//Here 2 indicates "number of products added to cart"
		s.navigateToCart(searchKeyWord,numberOfProducts, quantity);	
		 Collections.sort(s.prodnames);
		Collections.sort(s.prodPrice);
		Collections.sort(s.materialId);
		log.info("Products Name which are added to Cart:- "+ s.prodnames);
		log.info("Products Price which are added to Cart:- "+s.prodPrice);
		log.info("Products Material ID's which are added to Cart:- "+s.materialId);
		
		proNameList = s.prodnames;
		materialIdsList = s.materialId;
		
		log.info("proNameList "+ proNameList);
		log.info("materialIdsList:- "+materialIdsList);
		
		log.info("Verify the CART page");
		sa.assertTrue(gVar.assertVisible("Cart_Heading", "Cart//Cart.properties"),"Verify the CART page");
		
		log.info("Collect the Cart ID");
		cartID = l1.getWebElement("Cart_ID", "Cart//Cart.properties").getText();
		log.info("cartID:- "+ cartID);
		
//		log.info("Collect the product Details");
//		productColor = l1.getWebElement("Cart_ProductColor", "Cart//Cart.properties").getText().split(":")[1].trim();
//		log.info("productColor:- "+ productColor);
//		productSize = l1.getWebElement("Cart_ProductSize", "Cart//Cart.properties").getText().split(":")[1].trim();
//		log.info("productSize:- "+ productSize);
//		productPrice = l1.getWebElement("Cart_ItemPrice", "Cart//Cart.properties").getText();
//		log.info("productPrice:_ "+ productPrice);
		productTotalPrice = l1.getWebElement("Cart_OrderTotal_Amount", "Cart//Cart.properties").getText();
		log.info("productTotalPrice:- "+ productTotalPrice);
		
		log.info("Click on PROCEED TO CHECKOUT button");
		l1.getWebElement("Cart_CheckoutButton", "Cart//Cart.properties").click();
		Thread.sleep(2000);
		log.info("Verify the Checkout page");
		sa.assertTrue(gVar.assertVisible("Checkout_Heading", "Checkout//Checkout.properties"),"Verify the CHECKOUT page");
		
		log.info("Click on Select Pick Up link");
		l1.getWebElement("ShipTo_SelectShipToLink", "Checkout//ShipTo.properties").click();
		
		log.info("Click on radio button");
		l1.getWebElement("ShipTo_ShipTo_Address_Radio", "Checkout//ShipTo.properties").click();
		
		log.info("Click on SELECT pickup address CONTINUE button");
		l1.getWebElement("ShipTo_Continue_button", "Checkout//ShipTo.properties").click();
		
		log.info("Collect the SHIPPTO Address");
		shippToAddress = l1.getWebElement("ShipTo_Shipping_Addr", "Checkout//ShipTo.properties").getText();
		log.info("shippToAddress:- "+ shippToAddress);
		
		if (gVar.assertVisible("ShippingMtd_Next_Btn", "Checkout//ShippingMethod.properties")) 
		{
			l1.getWebElement("ShippingMtd_Next_Btn", "Checkout//ShippingMethod.properties").click();
		}
		
		log.info("Collect the shipping method");
		selectedShippingMethod = l1.getWebElement("ShippingMtd_Selected_ShippingMethod", "Checkout//ShippingMethod.properties").getText();
		log.info("selectedShippingMethod:- " + selectedShippingMethod);
		
		log.info("Enter PO number");
		sa.assertTrue(gVar.assertVisible("PT_POBox_Textbox", "Checkout//PaymentType.properties"),"verify the PO number");
		
		log.info("Enter PO number");
		int randomNumber = gVar.generateRandomNum();
		poNumber = String.valueOf(randomNumber);
		log.info("poNumber:- "+ poNumber);
		l1.getWebElement("PT_POBox_Textbox", "Checkout//PaymentType.properties").clear();
		l1.getWebElement("PT_POBox_Textbox", "Checkout//PaymentType.properties").sendKeys(poNumber);
		
		log.info("Click on CONTINUE button in payment section");
		l1.getWebElement("Next_Button", "Checkout//PaymentType.properties").click();
		
		log.info("Click on PLACE ORDER button");
		l1.getWebElement("OrderReview_PlaceOrder_Btn", "Checkout//OrderReview.properties").click();
		//Fetch the system details and compare
		systemDate = gVar.getSystemDate("MMM dd, YYYY H aa");
		log.info("systemDate:- "+ systemDate);

		log.info("Verify the THANK YOU page");
		sa.assertTrue(gVar.assertVisible("OrderConfirmation_Heading", "Checkout//OrderConfirmation.properties"),"Verify the Order Confirmation page");
		
		log.info("Collect the Order ID");
		orderID = l1.getWebElement("OrderConfir_OrderNum", "Checkout//OrderConfirmation.properties").getText().split("#")[1].trim();
		log.info("orderID:- "+ orderID);
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="DMED-209 DRM-759")
	public void TC08_searchBy_PO_Number() throws Exception
	{
		log.info("Navigate to Order history page");
		s.navigateToOrderHistoryPage();
		log.info("Verify the Order History page");
		String heading = GetData.getDataFromExcel("//data//GenericData.xls", "OrderHistory", 1, 0);
		sa.assertEquals(gVar.assertEqual("OrderHistory_Section_Header", OrderHistoryProperties), 
				heading,"Order History Page Heading");
		
		log.info("verify the Search box in Order History page");
		sa.assertTrue(gVar.assertVisible("OrderHistory_SearchBox", OrderHistoryProperties),"Search box in Order history page");
		log.info("Enter PO number in search BOX");
		l1.getWebElement("OrderHistory_SearchBox", OrderHistoryProperties).sendKeys(poNumber);
		log.info("Click on Search button");
		l1.getWebElement("OrderHistory_Search_Button", OrderHistoryProperties).click();
		log.info("Count the Order rows");
		int numberOfRows = l1.getWebElements("OrderHistory_OrderItem_Rows", OrderHistoryProperties).size();
		log.info("Number of Rows:- "+numberOfRows);
		log.info("Only one row should display");
		sa.assertEquals(1, numberOfRows);
		log.info("Verify the result count");
		int searchCuont = Integer.parseInt(gVar.assertEqual("OrderHistory_OrdersCount_Section", OrderHistoryProperties).split(" ")[0].trim());
		sa.assertEquals(searchCuont, numberOfRows,"Verify the search count");
		
		log.info("Verify the PO number");
		gVar.assertEqual(gVar.assertEqual("OrderHistory_AllPONumber", OrderHistoryProperties), 
				poNumber,"Verify the PO number");
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="DMED-209 DRM-760")
	public void TC9_searchBy_OrderNumber() throws Exception
	{
		driver.navigate().refresh();
		log.info("Enter Order ID in search box");
		l1.getWebElement("OrderHistory_SearchBox", OrderHistoryProperties).clear();
		l1.getWebElement("OrderHistory_SearchBox", OrderHistoryProperties).sendKeys(orderID);
		log.info("Click on Search button");
		l1.getWebElement("OrderHistory_Search_Button", OrderHistoryProperties).click();
		
		log.info("Count the Order rows");
		int numberOfRows = l1.getWebElements("OrderHistory_OrderItem_Rows", OrderHistoryProperties).size();
		log.info("Number of Rows:- "+numberOfRows);
		log.info("Only one row should display");
		sa.assertEquals(1, numberOfRows);
		log.info("Verify the result count");
		int searchCuont = Integer.parseInt(gVar.assertEqual("OrderHistory_OrdersCount_Section", OrderHistoryProperties).split(" ")[0].trim());
		sa.assertEquals(searchCuont, numberOfRows,"Verify the search count");
		
		log.info("Verify the Order number");
		gVar.assertEqual(gVar.assertEqual("OrderHistory_AllOrderId", OrderHistoryProperties), 
				orderID,"Verify the Order ID number");
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="DMED-209 DRM-761")
	public void TC10_searchByMaterialNumber() throws Exception
	{
		log.info("Enter material number");
		l1.getWebElement("OrderHistory_SearchBox", OrderHistoryProperties).clear();
		l1.getWebElement("OrderHistory_SearchBox", OrderHistoryProperties).sendKeys(searchKeyWord);
		log.info("Click on Search button");
		l1.getWebElement("OrderHistory_Search_Button", OrderHistoryProperties).click();
		
		List<WebElement> orderIdsList = l1.getWebElements("OrderHistory_AllOrderId", OrderHistoryProperties);
		log.info("orderIdsList.size():- "+ orderIdsList.size());
		for (int i = 0; i < orderIdsList.size(); i++) 
		{
			log.info("LOOP:- "+i);
			orderIdsList = l1.getWebElements("OrderHistory_AllOrderId", OrderHistoryProperties);
			log.info("Click on Order ID link");
			String orderId = orderIdsList.get(i).getText();
			log.info("orderId:- "+ orderId);
			orderIdsList.get(i).click();
			
			log.info("Verify the Order Details page");
			String activeBreadCrumb = l1.getWebElement("Breadcrumb_ActiveElement", "Generic//Generic.properties").getText();
			log.info("activeBreadCrumb:- "+ activeBreadCrumb);
			sa.assertTrue(activeBreadCrumb.contains(orderId), "Verify the Order ID in breadcrumb");
			
			log.info("Verify the Material ID in Order Details page");
			String productDetail = l1.getWebElement("OrderDetails_OrderedProduct_Details", OrderHistoryProperties).getText();
			log.info("productDetail:- "+ productDetail);
			
			log.info("Verify the material number in product details section");
			sa.assertTrue(productDetail.contains(searchKeyWord),"Verify the Material ID in order details page");
			
			log.info("Navigate back to Order history page");
			l1.getWebElement("OrderDetails_BackToOrderHistory_Link", OrderHistoryProperties).click();
			
			log.info("Verify the Order History page");
			String heading = GetData.getDataFromExcel("//data//GenericData.xls", "OrderHistory", 1, 0);
			sa.assertEquals(gVar.assertEqual("OrderHistory_Section_Header", OrderHistoryProperties), 
					heading,"Order History Page Heading");
			
			log.info("Search with material ID");
			l1.getWebElement("OrderHistory_SearchBox", OrderHistoryProperties).sendKeys(searchKeyWord);
			log.info("Click on Search button");
			l1.getWebElement("OrderHistory_Search_Button", OrderHistoryProperties).click();
		
		}
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="DMED-209 DRM-750, OOTB-061 1196")
	public void TC11_reorderFunctionalityInOrderHistoryPage() throws Exception
	{
		log.info("Navigate to Order history page");
		s.navigateToOrderHistoryPage();
		
		log.info("Verify the REORDER button in OrderHistory page");
		int ordersIdCnt = l1.getWebElements("OrderHistory_AllOrderId", OrderHistoryProperties).size();
		List<WebElement> reOrderButtons = l1.getWebElements("OrderHistory_ReorderButton", OrderHistoryProperties);
		sa.assertEquals(ordersIdCnt, reOrderButtons.size(),"Verify the NUmber of REORDER buttons in Order History page");
		//Verify the REORDER button in OrderHistory page 
		for (int i = 0; i < reOrderButtons.size(); i++) 
		{
			log.info("LOOP:- "+i);
			sa.assertTrue(reOrderButtons.get(i).isDisplayed(),"REORDER BUTTON");
			String reOrderText = GetData.getDataFromExcel("//data//GenericData.xls", "OrderHistory", 10, 0);
			gVar.assertequalsIgnoreCase(reOrderButtons.get(i).getText(), reOrderText,"REORDER BUTTON text");
		}
		
		log.info("collect PO number");
		String poNumberInOrderHistoryPage = l1.getWebElement("OrderHistory_AllPONumber", OrderHistoryProperties).getText();
		log.info("poNumber:- "+ poNumberInOrderHistoryPage);
		
		log.info("Click on REORDER link in Order history page");
		l1.getWebElement("OrderHistory_ReorderButton", OrderHistoryProperties).click();
		
		log.info("Verify the Checkout page");
		sa.assertTrue(gVar.assertVisible("Checkout_Heading", "Checkout//Checkout.properties"),"Verify the CHECKOUT page");
		
		log.info("User Should be in ShipTo section");
		sa.assertTrue(gVar.assertVisible("ShipTo_Continue_button", "Checkout//ShipTo.properties"),"Verify continue button in ShipTo section");
		l1.getWebElement("ShipTo_Continue_button", "Checkout//ShipTo.properties").click();
		
		log.info("Verify the ShippTo Address");
		sa.assertEquals(gVar.assertEqual("ShipTo_Shipping_Addr", "Checkout//ShipTo.properties"), 
				shippToAddress,"Verify the SHipp To Address");
		
		log.info("Verify the Shipping method section");
		sa.assertTrue(gVar.assertVisible("ShippingMtd_Next_Btn", "Checkout//ShippingMethod.properties"),"Verify the Shipping method");
		l1.getWebElement("ShippingMtd_Next_Btn", "Checkout//ShippingMethod.properties").click();
		
		log.info("Verify the shipping method");
		sa.assertEquals(gVar.assertEqual("ShippingMtd_Selected_ShippingMethod", "Checkout//ShippingMethod.properties"), selectedShippingMethod,"Verify the selected shipping method");
		
		log.info("Verify the PO number in Checkout page");
		sa.assertEquals(gVar.assertEqual("PT_POBox_Textbox", "Checkout//PaymentType.properties","value"), 
				poNumberInOrderHistoryPage,"Verify the PoNumber");
		
		log.info("Enter PO number");
		int randomNumber = gVar.generateRandomNum();
		poNumber = String.valueOf(randomNumber);
		log.info("poNumber:- "+ poNumber);
		l1.getWebElement("PT_POBox_Textbox", "Checkout//PaymentType.properties").clear();
		l1.getWebElement("PT_POBox_Textbox", "Checkout//PaymentType.properties").sendKeys(poNumber);
		
		log.info("Click on CONTINUE button in payment section");
		l1.getWebElement("Next_Button", "Checkout//PaymentType.properties").click();
		
		log.info("Verify the products in ORDER REVIEW page");
		List<WebElement> proNamesLink = l1.getWebElements("OrderReview_ProductName", "Checkout//OrderReview.properties");
		List<WebElement> proIdsText = l1.getWebElements("OrderReview_ProductId", "Checkout//OrderReview.properties");
		ArrayList<String> proNameInOrderReview = new ArrayList<String>();
		ArrayList<String> proIdsInOrderReview = new ArrayList<String>();
		
		for (int i = 0; i < proNamesLink.size(); i++) 
		{
			log.info("LOOP:-"+ i);
			proNameInOrderReview.add(proNamesLink.get(i).getText());
			log.info("proNamesLink.get(i).getText():- "+proNamesLink.get(i).getText());
			proIdsInOrderReview.add(proIdsText.get(i).getText().replace("#", ""));
		}
		
		Collections.sort(proIdsInOrderReview);
		log.info("proNameInOrderReview:- "+proNameInOrderReview);
		log.info("proIdsInOrderReview:- "+proIdsInOrderReview);
		
		log.info("Verify the Products name and ID");
		sa.assertEquals(proNameList, proNameInOrderReview,"Verify the Product name");
		sa.assertEquals(materialIdsList, proIdsInOrderReview,"Verify the Product Id's");
		
		log.info("Click on PLACE ORDER button");
		l1.getWebElement("OrderReview_PlaceOrder_Btn", "Checkout//OrderReview.properties").click();
		
		log.info("Verify the THANK YOU page");
		sa.assertTrue(gVar.assertVisible("OrderConfirmation_Heading", "Checkout//OrderConfirmation.properties"),"Verify the Order Confirmation page");

		log.info("Navigate to Order history page");
		s.navigateToOrderHistoryPage();
		log.info("Enter PO number in search BOX");
		l1.getWebElement("OrderHistory_SearchBox", OrderHistoryProperties).sendKeys(poNumber);
		log.info("Click on Search button");
		l1.getWebElement("OrderHistory_Search_Button", OrderHistoryProperties).click();
		log.info("Count the Order rows");
		int numberOfRows = l1.getWebElements("OrderHistory_OrderItem_Rows", OrderHistoryProperties).size();
		log.info("Number of Rows:- "+numberOfRows);
		log.info("Only one row should display");
		sa.assertEquals(numberOfRows, 1, "Verify the number of matching PONumbers");
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="DMED-209 DRM-751 DRM-752 DRM-753, OOTB-061 DRM-1198 DRM-1199 DRM-1200")
	public void TC12_uiOfOrderDetailsPage() throws Exception
	{
		log.info("Navigate to Order history page");
		s.navigateToOrderHistoryPage();
		
		log.info("Click on ORDER ID in Order history page");
		String orderId = l1.getWebElement("OrderHistory_AllOrderId", OrderHistoryProperties).getText();
		log.info("orderId:- "+ orderId);
		l1.getWebElement("OrderHistory_AllOrderId", OrderHistoryProperties).click();
		
		log.info("Verify the Order Details page");
		String orderDetailText = GetData.getDataFromExcel("//data//GenericData.xls", "OrderHistory", 2, 0);
		gVar.assertEqual(gVar.assertEqual("OrderDetails_Heading", OrderHistoryProperties), 
				orderDetailText,"Verify the Order details page heading");

		log.info("Verify the Back To order history link");
		String backToOrderHistory = GetData.getDataFromExcel("//data//GenericData.xls", "OrderHistory", 19, 0);
		sa.assertTrue(gVar.assertVisible("OrderDetails_BackToOrderHistory_Link", OrderHistoryProperties), "Verify the Back To order history link");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("OrderDetails_BackToOrderHistory_Bottom", OrderHistoryProperties), 
				backToOrderHistory,"Back to Order History link bottom");
		
		log.info("Verify the Order ID");
		gVar.assertEqual(gVar.assertEqual("OrderDetails_OrderId", OrderHistoryProperties), 
				orderId,"Order ID");
		log.info("Verify the Order ID in breadcrumb");
		String activeBreadCrumb = l1.getWebElement("Breadcrumb_ActiveElement", "Generic//Generic.properties").getText().replaceAll("[^0-9]", "");
		sa.assertEquals(activeBreadCrumb, orderId,"Verify the orderId in breadcrumb");
		
		log.info("Billing Info Heading");
		String expectedText = GetData.getDataFromExcel("//data//GenericData.xls", "OrderHistory", 3, 0);
		String actualBillingHeading = gVar.assertEqual("OrderDetails_BillingInfo_Heading", OrderHistoryProperties);
		actualBillingHeading = actualBillingHeading.substring(0, expectedText.length());
		gVar.assertequalsIgnoreCase(actualBillingHeading, expectedText,"Billing Info Heading");
		
		log.info("Payment Details label");
		String paymentInfoLabel = GetData.getDataFromExcel("//data//GenericData.xls", "OrderHistory", 9, 0);
		gVar.assertequalsIgnoreCase(gVar.assertEqual("OrderDetails_PaymentDetails_Label", OrderHistoryProperties), 
				paymentInfoLabel,"Payment Info label");
		sa.assertTrue(false,"Verify the Billing and payment info section once the build is confirmed");
		
		log.info("Order placed Date");
		String dateLabel = GetData.getDataFromExcel("//data//GenericData.xls", "OrderHistory", 4, 0);
		gVar.assertequalsIgnoreCase(gVar.assertEqual("OrderDetails_OrderPlacedDate_Label", OrderHistoryProperties), 
				dateLabel,"Date label");
		
		log.info("Compair the Order placed date in Order Detail page");
		String actualDate = gVar.assertEqual("OrderDetails_OrderPlacedDate", OrderHistoryProperties);
		String replaceString = actualDate.substring(actualDate.indexOf(":"), actualDate.lastIndexOf(" "));
		actualDate = actualDate.replace(replaceString, "");
		log.info("actualDate:-"+ actualDate);
		gVar.assertequalsIgnoreCase(actualDate, systemDate,"Fetch the system Date and compair");
		
		log.info("Order Status");
		String statusLabel = GetData.getDataFromExcel("//data//GenericData.xls", "OrderHistory", 5, 0);
		gVar.assertequalsIgnoreCase(gVar.assertEqual("OrderDetails_Status_Label", OrderHistoryProperties), 
				statusLabel,"Order status label");
		String status = GetData.getDataFromExcel("//data//GenericData.xls", "OrderHistory", 6, 0);
		gVar.assertequalsIgnoreCase(gVar.assertEqual("OrderDetails_Status", OrderHistoryProperties), 
				status,"Order status Value");

		log.info("PO Number");
		String poNumLabel = GetData.getDataFromExcel("//data//GenericData.xls", "OrderHistory", 7, 0);
//		gVar.assertequalsIgnoreCase(gVar.assertEqual("OrderDetails_POnum_Label", OrderHistoryProperties), 
//				poNumLabel,"Order poNum Label");
		sa.assertEquals(gVar.assertEqual("OrderDetails_POnum_Label", OrderHistoryProperties), 
				poNumLabel, "Order poNum Label");
		
		gVar.assertequalsIgnoreCase(gVar.assertEqual("OrderDetails_POnum", OrderHistoryProperties), 
				poNumber,"PO number");
		log.info("Order Total");
		String totalLabel = GetData.getDataFromExcel("//data//GenericData.xls", "OrderHistory", 8, 0);
		gVar.assertequalsIgnoreCase(gVar.assertEqual("OrderDetails_Total_Label_InOrderInfo", OrderHistoryProperties), 
				totalLabel,"Total Label");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("OrderDetails_Total_InOrderInfo", OrderHistoryProperties), 
				productTotalPrice,"Order total");
		
		log.info("Verify the REORDER buttons");
		String reOrder = GetData.getDataFromExcel("//data//GenericData.xls", "OrderHistory", 10, 0);
		gVar.assertequalsIgnoreCase(gVar.assertEqual("OrderDetails_Reorder1", OrderHistoryProperties), 
				reOrder,"Verify the Reorder button-1");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("OrderDetails_Reorder2", OrderHistoryProperties), 
				reOrder,"Verify the Reorder button-2");
		
		log.info("Verify the Order amount section");
		sa.assertTrue(gVar.assertVisible("OrderDetails_Subtotal_Label", OrderHistoryProperties),"Subtotal Label");
		sa.assertTrue(gVar.assertVisible("OrderDetails_Freight_Label", OrderHistoryProperties),"Freight Label");
		sa.assertTrue(gVar.assertVisible("OrderDetails_Tax_Label", OrderHistoryProperties),"Tax Label");
		
		String orderTotalLabel = GetData.getDataFromExcel("//data//GenericData.xls", "OrderHistory", 11, 0);
		gVar.assertequalsIgnoreCase(gVar.assertEqual("OrderDetails_Total_Label", OrderHistoryProperties), 
				orderTotalLabel, "Order total label");
		
		ArrayList<String> proNamesList = new ArrayList<String>();
		log.info("Verify the Product Details");
		List<WebElement> proName = l1.getWebElements("OrderDetails_ProductName", OrderHistoryProperties);
		for (int i = 0; i < proName.size(); i++) 
		{
			String proNameInOrderDetailsPAge = proName.get(i).getText();
																			
//			log.info("proNameInOrderDetailsPAge_TITLE:- "+proName.get(i).getAttribute("title"));
//			log.info("proNameInOrderDetailsPAge_ALT:- "+proName.get(i).getAttribute("alt"));
			proNamesList.add(proNameInOrderDetailsPAge);
			
		}
		log.info("proNamesList:- "+ proNamesList);
		log.info("Compair the product names in ORDER DETAILS page");
		sa.assertEquals(s.prodnames, proNamesList,"Verify the Products name in ORDER DETAILS page");
		
		sa.assertTrue(false,"Compair the site with FSD");
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="OOTB-061 DRM-1197")
	public void TC13_reOrderFunctionalityInOrderDetailsPAge() throws Exception
	{
		log.info("collect PO number in Order Details page");
		String poNumber = l1.getWebElement("OrderDetails_POnum", OrderHistoryProperties).getText();
		log.info("poNumber:- "+ poNumber);
		
		log.info("Click on REORDER button in Order Details page");
		l1.getWebElement("OrderDetails_Reorder1", OrderHistoryProperties).click();
		
		log.info("Verify the Checkout page");
		sa.assertTrue(gVar.assertVisible("Checkout_Heading", "Checkout//Checkout.properties"),"Verify the CHECKOUT page");
		
		log.info("User Should be in ShipTo section");
		sa.assertTrue(gVar.assertVisible("ShipTo_Continue_button", "Checkout//ShipTo.properties"),"Verify continue button in ShipTo section");
		l1.getWebElement("ShipTo_Continue_button", "Checkout//ShipTo.properties").click();
		
		log.info("Verify the ShippTo Address");
		sa.assertEquals(gVar.assertEqual("ShipTo_Shipping_Addr", "Checkout//ShipTo.properties"), 
				shippToAddress,"Verify the SHipp To Address");
		
		log.info("Verify the Shipping method section");
		sa.assertTrue(gVar.assertVisible("ShippingMtd_Next_Btn", "Checkout//ShippingMethod.properties"),"Verify the Shipping method");
		l1.getWebElement("ShippingMtd_Next_Btn", "Checkout//ShippingMethod.properties").click();
		
		log.info("Verify the shipping method");
		sa.assertEquals(gVar.assertEqual("ShippingMtd_Selected_ShippingMethod", "Checkout//ShippingMethod.properties"), selectedShippingMethod,"Verify the selected shipping method");
		
		log.info("Verify the PO number in Checkout page");
		sa.assertEquals(gVar.assertEqual("PT_POBox_Textbox", "Checkout//PaymentType.properties","value"), 
				poNumber,"Verify the PoNumber");
		
		log.info("Enter PO number");
		int randomNumber = gVar.generateRandomNum();
		poNumber = String.valueOf(randomNumber);
		log.info("poNumber:- "+ poNumber);
		l1.getWebElement("PT_POBox_Textbox", "Checkout//PaymentType.properties").clear();
		l1.getWebElement("PT_POBox_Textbox", "Checkout//PaymentType.properties").sendKeys(poNumber);
		
		log.info("Click on CONTINUE button in payment section");
		l1.getWebElement("Next_Button", "Checkout//PaymentType.properties").click();
		
		
		log.info("Verify the products in ORDER REVIEW page");
		List<WebElement> proNamesLink = l1.getWebElements("OrderReview_ProductName", "Checkout//OrderReview.properties");
		List<WebElement> proIdsText = l1.getWebElements("OrderReview_ProductId", "Checkout//OrderReview.properties");
		ArrayList<String> proNameInOrderReview = new ArrayList<String>();
		ArrayList<String> proIdsInOrderReview = new ArrayList<String>();
		for (int i = 0; i < proNamesLink.size(); i++) 
		{
			log.info("LOOP:-"+ i);
			proNameInOrderReview.add(proNamesLink.get(i).getText());
			proIdsInOrderReview.add(proIdsText.get(i).getText().replace("#", "").trim());
		}
		log.info("proNameInOrderReview:- "+proNameInOrderReview);
		log.info("proIdsInOrderReview:- "+proIdsInOrderReview);
		
		log.info("Verify the Products name and ID");
		sa.assertEquals(proNameList, proNameInOrderReview,"Verify the Product name");
		sa.assertEquals(materialIdsList, proIdsInOrderReview,"Verify the Product Id's");
		
		log.info("Click on PLACE ORDER button");
		l1.getWebElement("OrderReview_PlaceOrder_Btn", "Checkout//OrderReview.properties").click();
		
		log.info("Verify the THANK YOU page");
		sa.assertTrue(gVar.assertVisible("OrderConfirmation_Heading", "Checkout//OrderConfirmation.properties"),"Verify the Order Confirmation page");
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="OOTB-061 DRM-1191")
	public void TC14_emptyOrderHistoryUi(XmlTest xmlTest) throws Exception
	{
		Thread.sleep(2000);
		log.info("Logout from the application");
		p.logout(xmlTest);
		
		log.info("Login to the application whith user which is not having orders");
		p.navigateToLoginPage();
		
		String userName = GetData.getDataFromExcel("//data//GenericData.xls", "Login", 1, 3);
		String password = GetData.getDataFromExcel("//data//GenericData.xls", "Login", 2, 3);
		p.logInWithCredentials(userName, password);
		
		log.info("NAvigate to Order History page");
		s.navigateToOrderHistoryPage();
		log.info("Verify the Heading");
		String expected = GetData.getDataFromExcel("//data//GenericData.xls", "OrderHistory", 18, 0);
		gVar.assertequalsIgnoreCase(gVar.assertEqual("OrderHistory_Empty_Content", OrderHistoryProperties), 
				expected);
		
		log.info("Orders Table should not display");
		sa.assertFalse(gVar.assertVisible("OrderHistory_OrdersListTable", OrderHistoryProperties),"Orders TAble should not display");
		
		log.info("Pagination should not display");
		sa.assertFalse(gVar.assertVisible("OrderHistory_Pagination_Top", OrderHistoryProperties),"Pagination Top should not display");
		sa.assertFalse(gVar.assertVisible("OrderHistory_Pagination_Bottom", OrderHistoryProperties),"Pagination Bottom should not display");
		
		sa.assertFalse(gVar.assertVisible("OrderHistory_SortBy_SelectBox_Top", OrderHistoryProperties),"SortBy Top should not display");
		sa.assertFalse(gVar.assertVisible("OrderHistory_SortBy_SelectBox_Bottom", OrderHistoryProperties),"SortBy Bottom should not display");
		
		log.info("Search Box");
		sa.assertTrue(gVar.assertVisible("OrderHistory_SearchBox", OrderHistoryProperties),"Verify search box");
		
		log.info("Verify the Sort by DAte and total Price dropdown");
		sa.assertTrue(gVar.assertVisible("OrderHistory_SelectByDate", OrderHistoryProperties),"SortBY Date dropdown");
		sa.assertTrue(gVar.assertVisible("OrderHistory_SelectByPrice", OrderHistoryProperties),"SortBY Total price dropdown");
		
		p.logout(xmlTest);
		sa.assertAll();
	}


	@Test(groups={"reg"}, description="OOTB-061 DRM-1239")
	public void TC15_orderHistoryPageWithAdminRole(XmlTest xmlTest) throws Exception
	{
		//Placed Order through "non-admin" user and login to the ADMIN account and search the Order ID
		//If no matching order find this testcase will fail
		
		log.info("Logout from the application");
		p.logout(xmlTest);
		
		log.info("Login to the application with admin credentials");
		p.navigateToLoginPage();
		String userName = GetData.getDataFromExcel("//data//GenericData.xls", "AdminUser", 1, 6);
		String password = GetData.getDataFromExcel("//data//GenericData.xls", "AdminUser", 2, 6);
		p.logInWithCredentials(userName, password);
		
		log.info("Navigate to Order History page");
		s.navigateToOrderHistoryPage();
		log.info("Verify the Order History page");
		String orderHistoryText = GetData.getDataFromExcel("//data//GenericData.xls", "OrderHistory", 1, 0);
		sa.assertEquals(gVar.assertEqual("OrderHistory_Section_Header", OrderHistoryProperties), 
				orderHistoryText,"Order History Page Heading");
		
		log.info("Search Order ID");
		l1.getWebElement("OrderHistory_SearchBox", OrderHistoryProperties).sendKeys(orderID);
		l1.getWebElement("OrderHistory_Search_Button", OrderHistoryProperties).click();
		
		log.info("Verify the number of Order Rows displayed");
		int numberOfRows = l1.getWebElements("OrderHistory_OrderItem_Rows", OrderHistoryProperties).size();
		log.info("Number of Rows:- "+numberOfRows);
		log.info("Only one row should display");
		sa.assertEquals(1, numberOfRows,"One matching Order row should be displayed on search");
		
		log.info("Verify the searchd order");
		gVar.assertEqual(gVar.assertEqual("OrderHistory_AllOrderId", OrderHistoryProperties), 
				orderID,"Verify the Order ID number");
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="OOTB-061 DRM-1240")
	public void TC16_orderDetailsyPageWithAdminRole(XmlTest xmlTest) throws Exception
	{
		//Placed Order through "non-admin" user and login to the ADMIN account and search the Order ID
		//If no matching order find this testcase will fail
		log.info("Navigate to Order Details page");
		l1.getWebElement("OrderHistory_AllOrderId", OrderHistoryProperties).click();
		
		log.info("Verify the Order Details page");
		String activeBreadCrumb = l1.getWebElement("Breadcrumb_ActiveElement", "Generic//Generic.properties").getText().split(" ")[1].trim();
		log.info("activeBreadCrumb:- "+ activeBreadCrumb);
		sa.assertEquals(activeBreadCrumb, orderID,"Verify the Order ID in breadcrumb");
		
		log.info("Verify the Material ID in Order Details page");
		String productDetail = l1.getWebElement("OrderDetails_OrderedProduct_Details", OrderHistoryProperties).getText();
		log.info("productDetail:- "+ productDetail);
		
		sa.assertEquals(gVar.assertEqual("OrderDetails_OrderId", OrderHistoryProperties).split("#")[1].trim(), 
				orderID,"Verify the Order ID in Order Details page");
		
		log.info("Verify the material number in product details section");
		sa.assertTrue(productDetail.contains(searchKeyWord),"Verify the Material ID in order details page");
		
		sa.assertAll();
	}
	
	
}
