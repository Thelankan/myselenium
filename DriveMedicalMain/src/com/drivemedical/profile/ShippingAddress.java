package com.drivemedical.profile;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import org.testng.xml.XmlTest;

import com.drivemedical.utilgeneric.BaseTest;
import com.drivemedical.utilgeneric.GetData;

public class ShippingAddress extends BaseTest{

	
	@Test(groups={"reg"},description="DMED-555 DRM-737,738,739,740")
	public void verifyShippingAddr_UI(XmlTest xmlTest) throws Exception
	{
		log.info("Log in to the application");
		p.navigateToLoginPage();
		p.logIn(xmlTest);
		
		if(xmlTest.getParameter("executingIn").equalsIgnoreCase("mobile") || xmlTest.getParameter("executingIn").equalsIgnoreCase("Tablet")) {
			log.info("click on site logo");
			l1.getWebElement("Header_Mob_Logo", "Shopnav//header.properties").click();
			log.info("click on hamburger menu");
			l1.getWebElement("Header_Hamburger_Menu", "Shopnav//header.properties").click();
			log.info("click on Shipping address");
			l1.getWebElement("Hamburger_ShipAddr_Link", "Shopnav//header.properties").click();
			
		} else {
		l1.getWebElement("Header_Logo", "Shopnav//header.properties").click();
		log.info("click on site logo");
		log.info("hover on my account link from header");
		act.moveToElement(l1.getWebElement("Header_Reg_MyAct_Link", "Shopnav//header.properties")).perform();
		log.info("click on shipping address");
		l1.getWebElement("Header_ShipAddr_Link", "Shopnav//header.properties").click();
		}
		
		log.info("Verify Heading");
		sa.assertTrue(gVar.assertVisible("ShippingAddr_Heading", "Profile//ShippingAddr.properties"),"Shipping Address Heading");
		List<WebElement> savedShippingAddr=l1.getWebElements("ShippingAddr_SavedAddr", "Profile//ShippingAddr.properties");
		int rowNum = 1;
		for(int i=0;i<savedShippingAddr.size();i++) {
			log.info("Verify saved address");
			sa.assertEquals(GetData.getDataFromExcel("//data//GenericData.xls", "Address", rowNum, 10), savedShippingAddr.get(i).getText());
			rowNum++;
		}
		
		sa.assertAll();
	}
	
}
