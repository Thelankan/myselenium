package com.drivemedical.profile;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import org.testng.xml.XmlTest;

import com.drivemedical.utilgeneric.BaseTest;
import com.drivemedical.utilgeneric.GetData;

public class DropShipAddress extends BaseTest {
	
	String savedAddr;

	@Test(groups={"reg"},description="OOB-060 DRM0-823,824,825,826,827,828 OOB-013 DRM-846,847 DMED-535 DRM-1102")
	public void TC00_dropShip_UI(XmlTest xmlTest) throws Exception
	{
		log.info("Log in to the application");
		p.navigateToLoginPage();
		p.logIn(xmlTest);
		log.info("navigate to ship to page");
		checkout.navigateToShipToPage();
		log.info("add address");
		for(int i=0;i<2;i++) {
			
		log.info("click on add address link");
		l1.getWebElement("ShipTo_AddAddress_Link","Checkout//ShipTo.properties").click();
		int k=i+1;
		checkout.addAddress(k);
		log.info("click on continue");
		l1.getWebElement("ShipTo_Continue_button", "Checkout//ShipTo.properties").click();
		if(i==0) {
		log.info("navigate back to ship to page");
		l1.getWebElements("ShipTo_Edit_Btn", "Checkout//ShipTo.properties").get(1).click();
		}
		}
		
		if(xmlTest.getParameter("executingIn").equalsIgnoreCase("mobile") || xmlTest.getParameter("executingIn").equalsIgnoreCase("Tablet")) {
			log.info("click on site logo");
			l1.getWebElement("Header_Mob_Logo", "Shopnav//header.properties").click();
			log.info("click on hamburger menu");
			l1.getWebElement("Header_Hamburger_Menu", "Shopnav\\header.properties").click();
			log.info("click on drop ship address");
			l1.getWebElement("Hamburger_DropShip_Link", "Shopnav//header.properties").click();
			
		} else {
		l1.getWebElement("Header_Logo", "Shopnav//header.properties").click();
		log.info("click on site logo");
		log.info("hover on my account link from header");
		act.moveToElement(l1.getWebElement("Header_Reg_MyAct_Link", "Shopnav//header.properties")).perform();
		log.info("click on drop ship address");
		l1.getWebElement("Header_DropShip_Link", "Shopnav//header.properties").click();
		}
		
		log.info("verify UI");
		sa.assertTrue(gVar.assertVisible("DropShip_Heading", "Profile//DropShip.properties"),"Heading");
		log.info("verify addresses");
		List<WebElement> totAddresses=l1.getWebElements("DropShip_Address_Containers", "Profile//DropShip.properties");
		int j=1;
		for(int i=0;i<totAddresses.size();i++) {
			
			log.info("verify addresses");
			sa.assertEquals(GetData.getDataFromExcel("//data//GenericData.xls", "Address", j, 8), gVar.assertEqual("DropShip_Addresses", "Profile//DropShip.properties", i).toString(),"Address Verification"+i);
			j++;
		}
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="DMED-535 DRM-1103")
	public void TC01_savedAddrUnCheck() throws Exception 
	{
		log.info("Navigate to cart");
		l1.getWebElement("Header_Cart_Link", "Shopnav//header.properties").click();
		log.info("click on proceed to check out button");
		l1.getWebElement("Cart_CheckoutButton","Cart//Cart.properties").click();
		log.info("click on add address link");
		l1.getWebElement("ShipTo_AddAddress_Link","Checkout//ShipTo.properties").click();
		checkout.addAddress(3);
		log.info("un check save this address check box");
		l1.getWebElement("ShipTo_SaveThisAddr_CheckBox", "Checkout//ShipTo.properties").click();
		log.info("click on continue");
		l1.getWebElement("ShipTo_Continue_button", "Checkout//ShipTo.properties").click();
		
		List<WebElement> totAddresses=l1.getWebElements("DropShip_Address_Containers", "Profile//DropShip.properties");
		int j=1;
		for(int i=0;i<totAddresses.size();i++) {
			log.info("verify addresses");
			sa.assertNotEquals(gVar.assertEqual("DropShip_Addresses", "Profile//DropShip.properties", i), GetData.getDataFromExcel("//data//GenericData.xls", "Address", j, 8));
			j++;
		}
		sa.assertAll();
	}
	
}
