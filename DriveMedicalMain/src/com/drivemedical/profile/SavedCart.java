package com.drivemedical.profile;

import static org.testng.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import junit.framework.Assert;

import org.testng.annotations.Test;
import org.testng.xml.XmlTest;

import com.drivemedical.utilgeneric.BaseTest;
import com.drivemedical.utilgeneric.GetData;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;
import org.testng.annotations.TestInstance;
import org.testng.xml.XmlTest;

import com.drivemedical.utilgeneric.BaseTest;
import com.gargoylesoftware.htmlunit.javascript.host.media.webkitMediaStream;

public class SavedCart extends BaseTest 
{
	
@Test(groups={"reg"},description="DMED_067_DRM_561  DMED_067_DRM_571")
public void TC00_SavedCart_Test(XmlTest xmlTest) throws Exception
{
	log.info("navigate to Signin page");
	p.navigateToLoginPage();
	p.logIn(xmlTest);
	
	log.info("delete saved carts if any");
	s.removeSavedcarts();

	log.info("Navigate to cart page");
	String searchDAta = GetData.getDataFromExcel("//data//GenericData.xls", "Products", 1, 1);
	s.navigateToCart(searchDAta,1, 1);
	log.info("cart Heading");
	sa.assertTrue(l1.getWebElement("Cart_Heading", "Cart\\Cart.properties").isDisplayed());
    log.info("Click on Save Cart Button");
    if (xmlTest.getParameter("executingIn").equalsIgnoreCase("mobile") || xmlTest.getParameter("executingIn").equalsIgnoreCase("Tablet"))
	{
    	l1.getWebElement("Cart_SaveCart_btn_Mobile", "Cart\\Cart.properties").click();
	}
	else
	{
		l1.getWebElement("Cart_SaveCart_btn", "Cart\\Cart.properties").click();
	}
	
	log.info("Entering the Details in saved Cart popup");
	s.savedCartDetails(1);
	log.info("Click on save button");
	l1.getWebElement("Cart_SaveCart_Save_btn", "Cart\\Cart.properties").click();
	
	log.info("Saved cart Link verification in cart");
	sa.assertTrue(gVar.assertVisible("Cart_SaveCart_SavedCart_txt", "Cart\\Cart.properties"),"Verify the SAVED CART LINK");
	
	log.info("Verify the EMPTY cart page");
	sa.assertTrue(gVar.assertVisible("Cart_EmptyCart_Msg", "Cart\\Cart.properties"),"Verify the EMPTY cart message");
	
	sa.assertAll();
}


@Test(groups={"reg"},description="DMED_067_DRM_563")
public void TC01_removesinglesavecart_Test() throws Exception
{
	log.info("navigate to Saved cart page");
     s.navigateToSavedCart();
    log.info("Saved cart heading verificateion");
    sa.assertTrue(gVar.assertVisible("SavedCart_Heading", "Profile//Savedcart.properties"),"Verify the SAVED CART heading");
    
    String savedCartName = l1.getWebElement("SavedCart_Savedcart_name", "Profile//Savedcart.properties").getText();
    log.info("savedCartName:-"+savedCartName);
    String savedCartID = l1.getWebElement("SavedCart_firstId", "Profile//Savedcart.properties").getText();
    log.info("savedCartID:-"+savedCartID);
    
    for (int j = 0; j < 3; j++) 
    {
    	log.info("LOOP:-"+j);
    	
    	log.info("click on remove(X) link in save cart page");
        l1.getWebElement("SavedCart_Remove_link", "Profile//Savedcart.properties").click();
        log.info("Verify the Delete Carts Popup");
        sa.assertTrue(gVar.assertVisible("SavedCart_DeleteCart_PopUp", "Profile//Savedcart.properties"), "POPUP");
        
        if (j==0) 
        {
        	log.info("Click on x icon in popup");
        	sa.assertTrue(gVar.assertVisible("SavedCart_PopUp_Close", "Profile//Savedcart.properties"),"Verify the close icon in popup");
            l1.getWebElement("SavedCart_PopUp_Close", "Profile//Savedcart.properties").click();	
		} 
        else if(j==1)
		{
        	log.info("Click on CANCEL button in popup");	
        	l1.getWebElement("SavedCart_Cancel_btn", "Profile//Savedcart.properties").click();
        	
		}
		else 
		{
			log.info("Click on DELETE button in popup");
			l1.getWebElement("SavedCart_Delete_btn", "Profile//Savedcart.properties").click();
			
		}
        Thread.sleep(3000);
        log.info("POPUP should not be displayed");
    	sa.assertFalse(gVar.assertVisible("SavedCart_DeleteCart_PopUp", "Profile//Savedcart.properties"), "POPUP should not display");
    	
        if (j==2) 
        {
        	sa.assertFalse(gVar.assertVisible("SavedCart_FirstRow", "Profile//Savedcart.properties"),"Product row should NOT display");
        	log.info("Verify the SAVED CART NAME and ID after deleting");
        	sa.assertNotEquals(gVar.assertEqual("SavedCart_Savedcart_name", "Profile//Savedcart.properties"), 
        			savedCartName, "CArt name should NOT display");
        	log.info("TEST1:-"+gVar.assertEqual("SavedCart_Savedcart_name", "Profile//Savedcart.properties"));
        	sa.assertNotEquals(gVar.assertEqual("SavedCart_firstId", "Profile//Savedcart.properties"), 
        			savedCartID,"Product ID should NOT display");
        	log.info("Test2:-"+gVar.assertEqual("SavedCart_firstId", "Profile//Savedcart.properties"));
        	
		} 
        else 
        {
        	log.info("saved cart should not be displayed");
        	sa.assertTrue(gVar.assertVisible("SavedCart_FirstRow", "Profile//Savedcart.properties"),"Verify the row:-"+j);
        	
        	log.info("Verify the SAVED CART NAME and ID");
        	sa.assertEquals(gVar.assertEqual("SavedCart_Savedcart_name", "Profile//Savedcart.properties"), 
        			savedCartName, "CArt name should display:-"+j);
        	sa.assertEquals(gVar.assertEqual("SavedCart_firstId", "Profile//Savedcart.properties"), 
        			savedCartID,"Product ID should display:-"+j);
        	
		}
        
	}
    sa.assertAll();
}


@Test(groups={"reg"},description="DMED_064_DRM_564")
public void TC02_SavedCartDeleteFromsavedCartDetailsPage_Test() throws Exception
{

	log.info("Navigate to cart page");
	String searchDAta = GetData.getDataFromExcel("//data//GenericData.xls", "Products", 1, 1);
	s.navigateToCart(searchDAta,1, 1);
	log.info("cart Heading");
	sa.assertTrue(l1.getWebElement("Cart_Heading", "Cart\\Cart.properties").isDisplayed());
    log.info("Click on Save Cart Button");
    if (xmlTest.getParameter("executingIn").equalsIgnoreCase("mobile") || xmlTest.getParameter("executingIn").equalsIgnoreCase("Tablet"))
	{
    	l1.getWebElement("Cart_SaveCart_btn_Mobile", "Cart\\Cart.properties").click();
	}
	else
	{
		l1.getWebElement("Cart_SaveCart_btn", "Cart\\Cart.properties").click();
	}
	
	log.info("Entering the Details in saved Cart popup");
	s.savedCartDetails(1);
	log.info("Click on save button");
	l1.getWebElement("Cart_SaveCart_Save_btn", "Cart\\Cart.properties").click();
	
	log.info("Verify the Success message");
	String successText1 = GetData.getDataFromExcel("//data//GenericData.xls", "SavedCartdetails", 3, 3);
	String successText2 = GetData.getDataFromExcel("//data//GenericData.xls", "SavedCartdetails", 4, 3);
	String successText = successText1+" "+s.savedCartNameFromExcel+" "+successText2;
	log.info("successText:-"+ successText);
	
	String successDivText = gVar.assertEqual("Cart_SaveCart_SuccessMsg", "Cart\\Cart.properties");
	log.info("Act success message:- "+gVar.assertEqual("Cart_SaveCart_SuccessMsg", "Cart\\Cart.properties"));
	String successMsgCloseIconText = gVar.assertEqual("Cart_SaveCart_SuccessMsg_CloseLink", "Cart\\Cart.properties");
	log.info("successMsgCloseIconText:- "+successMsgCloseIconText);

	String successDivTextAfter = successDivText.replace(successMsgCloseIconText, "").trim();
	log.info("successDivTextAfter:- "+ successDivTextAfter);
	
	sa.assertTrue(gVar.assertVisible("Cart_SaveCart_SuccessMsg", "Cart\\Cart.properties"),"Verify the Success message");	
	gVar.assertequalsIgnoreCase(successDivTextAfter, successText,"Verify SUCCESS text");
	
	log.info("Navigate to Saved cart page / CLICK ON SAVED CARTS LINK");
	l1.getWebElement("Cart_SaveCart_SavedCart_txt", "Cart\\Cart.properties").click();
	
	 log.info("Saved cart heading verificateion");
     sa.assertTrue(l1.getWebElement("SavedCart_Heading", "Profile//Savedcart.properties").isDisplayed());
	
	log.info("Click on Saved cart name");
	String savedCartName = l1.getWebElement("SavedCart_Savedcart_name", "Profile//Savedcart.properties").getText();
	log.info("savedCartName:-"+savedCartName);
	String savedCartId = l1.getWebElement("SavedCart_firstId", "Profile//Savedcart.properties").getText();
	log.info("savedCartId:-"+savedCartId);
	l1.getWebElement("SavedCart_Savedcart_name", "Profile//Savedcart.properties").click();
	
	log.info("Verify the Saved CArt Details page");
	sa.assertTrue(gVar.assertVisible("SavedCartDetails_Heading", "Profile//Savedcart.properties"),"Saved CArt details page");
	sa.assertTrue(gVar.assertVisible("SavedCartDetails_Delete_Button", "Profile//Savedcart.properties"),"Verify the Delete button");
	log.info("Click on DELETE button in Saved CArt details page");
	l1.getWebElement("SavedCartDetails_Delete_Button", "Profile//Savedcart.properties").click();
	
	log.info("Verify the Popup");
	String deleteText = GetData.getDataFromExcel("//data//GenericData.xls", "SavedCartdetails", 1, 3);
	log.info("deleteText:-"+deleteText);
	gVar.assertequalsIgnoreCase(gVar.assertEqual("SavedCart_DeleteSavedCartPopUp_Heading", "Profile//Savedcart.properties"), 
			deleteText, "Delete Saved Cart popup heading");
	sa.assertTrue(gVar.assertVisible("SavedCart_DeleteSavedCartPopUp_DeleteButton", "Profile//Savedcart.properties"),"Delete button in popup");
	
	log.info("Click on Delete button in popup");
	l1.getWebElement("SavedCart_DeleteSavedCartPopUp_DeleteButton", "Profile//Savedcart.properties").click();
	
	log.info("On click, User should be in SAVED CART page");
	String savedCartsText = GetData.getDataFromExcel("//data//GenericData.xls", "SavedCartdetails", 2, 3);
	log.info("savedCartsText:-"+savedCartsText);
	gVar.assertequalsIgnoreCase(gVar.assertEqual("SavedCart_Heading", "Profile//Savedcart.properties"), 
			savedCartsText, "Verify the SAVED CARTS page");
	
	
	if (gVar.assertVisible("SavedCart_FirstRow", "Profile//Savedcart.properties")) 
	{
		//IF SAVED CART page contains more than one saved carts
		log.info("Collect the saved cart name and ID");
		String savedCartNameAfter = l1.getWebElement("SavedCart_Savedcart_name", "Profile//Savedcart.properties").getText();
		log.info("savedCartNameAfter:-"+savedCartNameAfter);
		String savedCartIdAfter = l1.getWebElement("SavedCart_firstId", "Profile//Savedcart.properties").getText();
		log.info("savedCartIdAfter:-"+savedCartIdAfter);
		
		sa.assertNotEquals(savedCartNameAfter.toLowerCase(), savedCartName.toLowerCase(),"Saved cart name should not be same after deleting");
		sa.assertNotEquals(savedCartIdAfter, savedCartId, "Saved Cart ID should not be same after deleting");
	}
	else 
	{
		log.info("Saved CArt page should be empty");
		sa.assertFalse(gVar.assertVisible("SavedCart_FirstRow", "Profile//Savedcart.properties"),"Saved cart page is EMPTY");
	}
	
	sa.assertAll();
}


@Test(groups={"reg"},description="DMED_064_DRM_565")
public void TC03_SavedCartRestoreWithCopy_Test() throws Exception
{

	log.info("Navigate to cart page");
	String searchDAta = GetData.getDataFromExcel("//data//GenericData.xls", "Products", 1, 1);
	s.navigateToCart(searchDAta,1, 1);
	log.info("cart Heading");
	sa.assertTrue(l1.getWebElement("Cart_Heading", "Cart\\Cart.properties").isDisplayed());
    log.info("Click on Save Cart Button");
    if (xmlTest.getParameter("executingIn").equalsIgnoreCase("mobile") || xmlTest.getParameter("executingIn").equalsIgnoreCase("Tablet"))
	{
    	l1.getWebElement("Cart_SaveCart_btn_Mobile", "Cart\\Cart.properties").click();
	}
	else
	{
		l1.getWebElement("Cart_SaveCart_btn", "Cart\\Cart.properties").click();
	}
	
	log.info("Entering the Details in saved Cart popup");
	s.savedCartDetails(1);
	log.info("Click on save button in SAVE CART POPUP");
	l1.getWebElement("Cart_SaveCart_Save_btn", "Cart//Cart.properties").click();
	log.info("Navigate to SAVE CART PAGE");
	s.navigateToSavedCart();
	
	String savedCartName = l1.getWebElement("SavedCart_Savedcart_name", "Profile//Savedcart.properties").getText();		
	log.info("savedCartName:-"+savedCartName);
	
	log.info("Click on RestoreLink");
	l1.getWebElement("SavedCart_Restore_link", "Profile//Savedcart.properties").click();
	
	log.info("Restore popup");
	sa.assertTrue(gVar.assertVisible("SavedCart_Restorepopup", "Profile//Savedcart.properties"),"Verify the RESTORE POPUP");
	
	log.info("Restore Checkbox");
	WebElement RestoreCheckbox=l1.getWebElement("savedCart_Restore_chkbox", "Profile//Savedcart.properties");
	log.info("Status of Checkbox:- "+RestoreCheckbox.isSelected());
	sa.assertTrue(RestoreCheckbox.isSelected(),"Checkbox status");
	if (!RestoreCheckbox.isSelected()) 
	{
		log.info("Check the checkbox");
		RestoreCheckbox.click();
	}
	
	log.info("Click  on restore button");
	l1.getWebElement("SavedCart_Restore_btn", "Profile//Savedcart.properties").click();
	
	log.info("Verify the CART page");
	sa.assertTrue(gVar.assertVisible("Cart_Heading", "Cart\\Cart.properties"),"Verify the CArt page");
	
	log.info("Saved cart Link verification in cart");
	sa.assertTrue(gVar.assertVisible("Cart_SaveCart_SavedCart_txt", "Cart\\Cart.properties"),"saved cart link in cart page");
	
	log.info("navigate to Saved cart page");
	s.navigateToSavedCart();
	
	log.info("verifying the copy of saved cart text");
	String copyOfText = GetData.getDataFromExcel("//data//GenericData.xls", "SavedCartdetails", 5, 3);
	String exptdSavedCartName = copyOfText+" "+savedCartName;
	sa.assertTrue(gVar.assertVisible("SavedCart_CopyofSavedCart", "Profile//Savedcart.properties"),"Copy of SavedCart");
	sa.assertEquals(gVar.assertEqual("SavedCart_Savedcart_name", "Profile//Savedcart.properties"), 
			exptdSavedCartName, "Verify the updated Saved cart name");
	
	sa.assertAll();
}
	
@Test(groups={"reg"},description="DMED_064_DRM_566")
public void TC04_SavedCartRestoreWithoutCopy_Test() throws Exception
{
	log.info("delete saved carts if any");
	s.removeSavedcarts();
	
	log.info("Navigate to cart page");
	String searchDAta = GetData.getDataFromExcel("//data//GenericData.xls", "Products", 1, 1);
	s.navigateToCart(searchDAta,1, 1);
	log.info("cart Heading");
	sa.assertTrue(gVar.assertVisible("Cart_Heading", "Cart\\Cart.properties"),"Cart page");

	log.info("Collect the CART ID");
	String cartID = l1.getWebElement("Cart_ID", "Cart\\Cart.properties").getText();
	log.info("cartID:- "+ cartID);
	
    log.info("Click on Save Cart Button");
    if (xmlTest.getParameter("executingIn").equalsIgnoreCase("mobile") || xmlTest.getParameter("executingIn").equalsIgnoreCase("Tablet"))
	{
    	l1.getWebElement("Cart_SaveCart_btn_Mobile", "Cart\\Cart.properties").click();
	}
	else
	{
		l1.getWebElement("Cart_SaveCart_btn", "Cart\\Cart.properties").click();
	}
	
	log.info("Entering the Details in saved Cart popup");
	s.savedCartDetails(1);
	log.info("Click on save button");
	l1.getWebElement("Cart_SaveCart_Save_btn", "Cart\\Cart.properties").click();
	s.navigateToSavedCart();
	log.info("Verify the CART ID in save cart page");
	sa.assertEquals(gVar.assertEqual("SavedCart_firstId", "Profile//Savedcart.properties"), 
			cartID, "Cart ID in saved cart page");
	
	log.info("Click on RestoreLink");
	l1.getWebElement("SavedCart_Restore_link", "Profile//Savedcart.properties").click();
	
	log.info("Restore popup");
	sa.assertTrue(gVar.assertVisible("SavedCart_Restorepopup", "Profile//Savedcart.properties"),"Popup is Displaying");
	WebElement RestoreCheckbox=l1.getWebElement("savedCart_Restore_chkbox", "Profile//Savedcart.properties");
	
	log.info("Verify the CHECKBOX status AND Uncheck the checkbox");
	if (RestoreCheckbox.isSelected()) 
	{
		log.info("Keep a copy of this cart on saved list CHECKBOX is selected bydefault");
		log.info("Uncheck the checkbox");
		RestoreCheckbox.click();
	}
	else 
	{
		sa.assertTrue(false,"Keep a copy of this cart on saved list CHECKBOX is NOT selected");
	}
	

	log.info("Click  on restore button");
	l1.getWebElement("SavedCart_Restore_btn", "Profile//Savedcart.properties").click();
	log.info("Verify the cart page");
	l1.getWebElement("Cart_Heading", "Cart\\Cart.properties").isDisplayed();
	
	log.info("Verify the Cart id");
	sa.assertEquals(gVar.assertEqual("Cart_ID", "Cart\\Cart.properties"), cartID, "Verify the CART ID in cart page after restoring the saved cart");
	log.info("Navigate to Saved cart page");
	s.navigateToSavedCart();
	
	log.info("No saved cart text verification");
    sa.assertTrue(gVar.assertVisible("SavedCart_NoSavedCartFoundtxt", "Profile//Savedcart.properties"), "Saved CArt page should be empty");

    sa.assertAll();
    
}
	
	
@Test(groups={"reg"},description="DMED_064_DRM_570")
public void TC05_SavedCartToPDP_Test() throws Exception
{
	log.info("Navigate to cart page");
	String searchDAta = GetData.getDataFromExcel("//data//GenericData.xls", "Products", 1, 1);
	s.navigateToCart(searchDAta,1, 1);
	log.info("cart Heading");
	sa.assertTrue(l1.getWebElement("Cart_Heading", "Cart\\Cart.properties").isDisplayed());
    log.info("Click on Save Cart Button");
    if (xmlTest.getParameter("executingIn").equalsIgnoreCase("mobile") || xmlTest.getParameter("executingIn").equalsIgnoreCase("Tablet"))
	{
    	l1.getWebElement("Cart_SaveCart_btn_Mobile", "Cart\\Cart.properties").click();
	}
	else
	{
		l1.getWebElement("Cart_SaveCart_btn", "Cart\\Cart.properties").click();
	}
	
	log.info("Entering the Details in saved Cart popup");
	s.savedCartDetails(1);
	log.info("Click on save button");
	l1.getWebElement("Cart_SaveCart_Save_btn", "Cart\\Cart.properties").click();
	s.navigateToSavedCart();
	log.info("Click on Savedcart Name");
	l1.getWebElement("SavedCart_Savedcart_name", "Profile//Savedcart.properties").click();
	log.info("Saved product name in saved cart page");
	String ProductName=l1.getWebElement("SavedCart_ProductName", "Profile//Savedcart.properties").getText();
	log.info("Product name in Saved cart page:-  "+ProductName);
	log.info("Click on ProductName in saved cart list page");
	l1.getWebElement("SavedCart_ProductName", "Profile//Savedcart.properties").click();
	log.info("PDP page");
	sa.assertTrue(l1.getWebElement("PDP_Element", "Shopnav\\PDP.properties").isDisplayed());
	log.info("Product name verification");
	sa.assertEquals(gVar.assertEqual("PDP_ProductName", "Shopnav\\PDP.properties"), 
			ProductName,"Verify the product name in PDP");

	sa.assertAll();
}
	

@Test(groups={"reg"},description="DMED_064_DRM_567")
public void TC06_SavedCartRestoreWithCopyFromsavedCartDetailsPage_Test() throws Exception
{
	
	log.info("Navigate to saved Cart List page");
	s.navigateToSavedCart();
	String savedCartName = l1.getWebElement("SavedCart_Savedcart_name", "Profile//Savedcart.properties").getText();
	log.info("savedCartName:- "+ savedCartName);	
	String ID = l1.getWebElement("SavedCart_firstId", "Profile//Savedcart.properties").getText();
	log.info("ID:-"+ID);
	
	log.info("Click on Savedcart Name");
	l1.getWebElement("SavedCart_Savedcart_name", "Profile//Savedcart.properties").click();
	log.info("Saved cart details page text");
	sa.assertTrue(gVar.assertVisible("SavedCart_Deatislpage_text", "Profile//Savedcart.properties"),"Saved cart details page text");

	log.info("Click on restore button");
	l1.getWebElement("Savedcart_DetailsPage_Restorebutton", "Profile//Savedcart.properties").click();
	log.info("Restore popup");
	sa.assertTrue(l1.getWebElement("SavedCart_Restorepopup", "Profile//Savedcart.properties").isDisplayed());
    log.info("RestoreCheckbox");
	WebElement RestoreCheckbox=l1.getWebElement("savedCart_Restore_chkbox", "Profile//Savedcart.properties");
	
	if (RestoreCheckbox.isSelected()) 
	{
		log.info("Checkbox is selected bydefault");
	} 
	else 
	{
		sa.assertTrue(false,"Checkbox is not selected bydefault");
		log.info("Check the checkbox");
		RestoreCheckbox.click();
	}
	
	log.info("Click  on restore button in popup");
	l1.getWebElement("Savedcart_DetailsPage_Restorebutton_InPopup", "Profile//Savedcart.properties").click();
	
	log.info("User should navigate to CArt page");
	sa.assertTrue(gVar.assertVisible("Cart_Heading", "Cart\\Cart.properties"),"Verify the CArt page");
	
	log.info("Saved cart Link verification in cart");
	WebElement SavedcartTexlink=l1.getWebElement("Cart_SaveCart_SavedCart_txt", "Cart\\Cart.properties");
	sa.assertTrue(SavedcartTexlink.isDisplayed());
	log.info("Verify the CART ID");
	sa.assertEquals(gVar.assertEqual("Cart_ID", "Cart\\Cart.properties"), 
			ID, "Verify the CART ID");
	
	log.info("navigate to Saved cart page");
	s.navigateToSavedCart();
	
	log.info("Copy of Save Cart name verification");
	String copyOfText = GetData.getDataFromExcel("//data//GenericData.xls", "SavedCartdetails", 5, 3);
	String exptName = copyOfText+" "+savedCartName;
	log.info("exptName:-"+exptName);
	sa.assertEquals(gVar.assertEqual("SavedCart_Savedcart_name", "Profile//Savedcart.properties"), 
			exptName, "Verify the updated Saved cart name");
	
	
	sa.assertAll();
	
}
	

@Test(groups={"reg"},description="DMED_064_DRM_568 DRM_569")
public void TC07_SavedCartRestoreWithoutCopyFromsavedCartDetailsPage_Test() throws Exception
{

	log.info("delete saved carts if any");
	s.removeSavedcarts();
	
	log.info("Navigate to cart page");
	String searchDAta = GetData.getDataFromExcel("//data//GenericData.xls", "Products", 1, 1);
	s.navigateToCart(searchDAta,1, 1);
	
	log.info("cart Heading");
	sa.assertTrue(gVar.assertVisible("Cart_Heading", "Cart\\Cart.properties"),"Verify CART page");
	String cartID = l1.getWebElement("Cart_ID", "Cart\\Cart.properties").getText();
	log.info("cartID:- "+cartID);
	
    log.info("Click on Save Cart Button");
    if (xmlTest.getParameter("executingIn").equalsIgnoreCase("mobile") || xmlTest.getParameter("executingIn").equalsIgnoreCase("Tablet"))
	{
    	l1.getWebElement("Cart_SaveCart_btn_Mobile", "Cart\\Cart.properties").click();
	}
	else
	{
		l1.getWebElement("Cart_SaveCart_btn", "Cart\\Cart.properties").click();
	}
	
	log.info("Entering the Details in saved Cart popup");
	s.savedCartDetails(1);
	log.info("Click on save button");
	l1.getWebElement("Cart_SaveCart_Save_btn", "Cart\\Cart.properties").click();
	s.navigateToSavedCart();
	
	log.info("Click on Savedcart Name");
	l1.getWebElement("SavedCart_Savedcart_name", "Profile//Savedcart.properties").click();
	
	log.info("Saved cart details page text");
	sa.assertTrue(gVar.assertVisible("SavedCart_Deatislpage_text", "Profile//Savedcart.properties"),"Saved cart details page text");
	
	log.info("Click on restore button");
	l1.getWebElement("Savedcart_DetailsPage_Restorebutton", "Profile//Savedcart.properties").click();
     log.info("Restore popup");
	sa.assertTrue(gVar.assertVisible("SavedCart_Restorepopup", "Profile//Savedcart.properties"),"Restore popup");
	
    log.info("RestoreCheckbox");
	sa.assertTrue(gVar.assertVisible("SavedCart_Restorepopup", "Profile//Savedcart.properties"),"RestoreCheckbox");
	
	WebElement RestoreCheckbox=l1.getWebElement("savedCart_Restore_chkbox", "Profile//Savedcart.properties");

	if (RestoreCheckbox.isSelected()) 
	{
		log.info("Bydefault checkbox is selected");
		log.info("Uncheck the checkbox");
		RestoreCheckbox.click();
	} 
	else 
	{
		sa.assertFalse(false,"bydefault checkbox is not selected");
	}
	
	log.info("Click  on restore button");
	l1.getWebElement("Savedcart_DetailsPage_Restorebutton_InPopup", "Profile//Savedcart.properties").click();
	
	log.info("VErify the cart page");
	sa.assertTrue(gVar.assertVisible("Cart_Heading", "Cart\\Cart.properties"),"Verify the CART page");
	sa.assertEquals(gVar.assertEqual("Cart_ID", "Cart//Cart.properties"),
			cartID,"Verify the cart id in CART page");

	log.info("NAvigate to SAVED CART page");
	s.navigateToSavedCart();
	 log.info("No saved cart text verification");
     sa.assertTrue(l1.getWebElement("SavedCart_NoSavedCartFoundtxt", "Profile//Savedcart.properties").isDisplayed());

     sa.assertAll();
     
}

@Test(groups={"reg"},description="DMED_064_DRM_571 DMED_064_DRM_572 DMED_064_DRM_573 DMED_064_DRM_574")
public void TC08_savedCartDetailsPageMobilDesktopTablet_Test() throws Exception
{

	log.info("Navigate to cart page");
	String searchDAta = GetData.getDataFromExcel("//data//GenericData.xls", "Products", 1, 1);
	int quantity = 1;
	s.navigateToCart(searchDAta,1, quantity);
	String searchedProductName = s.prodnames.get(0);
	
	log.info("Product NAME:- "+searchedProductName);
	String searchedProductPrice = s.prodPrice.get(0);
	log.info("searchedProductPrice:- "+searchedProductPrice);
	
	String cartID = l1.getWebElement("Cart_ID", "Cart\\Cart.properties").getText();
	
	log.info("cart Heading");
	sa.assertTrue(gVar.assertVisible("Cart_Heading", "Cart\\Cart.properties"), "Cart page");
	
    log.info("Click on Save Cart Button");
    if (xmlTest.getParameter("executingIn").equalsIgnoreCase("mobile") || xmlTest.getParameter("executingIn").equalsIgnoreCase("Tablet"))
	{
    	l1.getWebElement("Cart_SaveCart_btn_Mobile", "Cart\\Cart.properties").click();
	}
	else
	{
		l1.getWebElement("Cart_SaveCart_btn", "Cart\\Cart.properties").click();
	}
	
	log.info("Entering the Details in saved Cart popup");
	s.savedCartDetails(1);
	log.info("Click on save button");
	l1.getWebElement("Cart_SaveCart_Save_btn", "Cart\\Cart.properties").click();
	
	log.info("Navigate to Saved CArt page");
	s.navigateToSavedCart();
	log.info("Click on Savedcart Name");
	l1.getWebElement("SavedCart_Savedcart_name", "Profile//Savedcart.properties").click();
	
	log.info("Saved cart details page text");
    sa.assertTrue(gVar.assertVisible("SavedCart_Deatislpage_text", "Profile//Savedcart.properties"), "Saved cart details page text");
    
    log.info("Name Label in Saved Cart Details Page");
    sa.assertTrue(gVar.assertVisible("SavedCart_nameLabel", "Profile//Savedcart.properties"), "Name Label in Saved Cart Details Page");
    
    log.info("ID label");
    sa.assertTrue(gVar.assertVisible("SavedCart_IDlabel", "Profile//Savedcart.properties"),"ID label");
    
    log.info("Date saved label");
    sa.assertTrue(gVar.assertVisible("SavedCart_DateSavedLabel", "Profile//Savedcart.properties"),"Date saved label");
    
    log.info("QTY label");
    sa.assertTrue(gVar.assertVisible("SavedCart_QTYLabel", "Profile//Savedcart.properties"),"QTY label");
    
    log.info("Description  label");
    sa.assertTrue(gVar.assertVisible("SavedCart_DescriptionLabel", "Profile//Savedcart.properties"), "Description  label");
    
    log.info("Restore button");
    sa.assertTrue(gVar.assertVisible("Savedcart_DetailsPage_Restorebutton", "Profile//Savedcart.properties"),"Restore button");
    
    log.info("Edit button");
    sa.assertTrue(gVar.assertVisible("SavedCart_Edit_btn", "Profile//Savedcart.properties"),"Edit button");
    
    log.info("Saved Cart Details Name");
    String exptdNAme = GetData.getDataFromExcel("//data//GenericData.xls", "savedCartDetails", 1,0);
    gVar.assertequalsIgnoreCase(gVar.assertEqual("SavedCart_SavedDetails_Name", "Profile//Savedcart.properties"), 
    		exptdNAme, "Saved Cart Details Name");
    
    log.info("Saved Cart Details Discription");
    String exptdText = GetData.getDataFromExcel("//data//GenericData.xls", "savedCartDetails", 1,1);
    gVar.assertequalsIgnoreCase(gVar.assertEqual("SavedCart_SavedDetals_Discription", "Profile//Savedcart.properties"), 
    		exptdText, "Saved Cart Details Discription");
    
    log.info("Item label");
    sa.assertTrue(gVar.assertVisible("SavedCart_ProductDetails_Item_Label", "Profile//Savedcart.properties"),"Item label");
    
    log.info("Price label");
    sa.assertTrue(gVar.assertVisible("SavedCart_ProductDetails_Price_Label", "Profile//Savedcart.properties"), "Price label");
    
    log.info("Qty label");
    sa.assertTrue(gVar.assertVisible("SavedCart_ProductDetails_Qty_Label", "Profile//Savedcart.properties"), "Qty label");
    
    log.info("Total label");
    sa.assertTrue(gVar.assertVisible("SavedCart_ProductDetails_Total_Label", "Profile//Savedcart.properties"), "Total label");
    
    log.info("Verify the PRODUCT name");
    sa.assertEquals(gVar.assertEqual("SavedCart_ProductName", "Profile//Savedcart.properties"), 
    		searchedProductName, "Verify the PRODUCT NAME");
    
    log.info("Product image");
    sa.assertTrue(gVar.assertVisible("Savedcart_Productdetails_Image", "Profile//Savedcart.properties"), "Product image");
    
    log.info("Product Itemcode");
    sa.assertTrue(gVar.assertVisible("SavedCart_Productdetails_ItemCode", "Profile//Savedcart.properties"), "Product Itemcode");
    
    log.info("Product ItemPrice");
    sa.assertTrue(gVar.assertVisible("SavedCart_Productdetails_ItemPrice", "Profile//Savedcart.properties"),"Product ItemPrice");
    String actualPrice = gVar.assertEqual("SavedCart_Productdetails_ItemPrice", "Profile//Savedcart.properties");
    sa.assertEquals(actualPrice.substring(actualPrice.indexOf("$"), actualPrice.length()), 
    		searchedProductPrice,"Verify the PRODUCT PRICE");    

    log.info("Product ItemQty");
    sa.assertTrue(gVar.assertVisible("SavedCart_Productdetails_Itemqty", "Profile//Savedcart.properties"),"Product ItemQty");
    sa.assertEquals(gVar.assertEqual("SavedCart_Productdetails_Itemqty", "Profile//Savedcart.properties"),
    		quantity, "Verify QUANTITY");
    
    log.info("Product TotalPrice");
    sa.assertTrue(gVar.assertVisible("Savedcart_Productdetails_totalprice", "Profile//Savedcart.properties"),"Product TotalPrice");
    
    log.info("Product information");
    sa.assertTrue(gVar.assertVisible("SavedCart_Productdetails_Information", "Profile//Savedcart.properties"), "Product information");

    log.info("Product Back to savedcart");
    sa.assertTrue(gVar.assertVisible("SavedCart_BacktoSavedCart_btn", "Profile//Savedcart.properties"),"Product Back to savedcart");
    
    log.info("Product Restore button");
    sa.assertTrue(gVar.assertVisible("SavedCart_Productdetails_Bottom_Restorebutton", "Profile//Savedcart.properties"),"Product Restore button");
    
    log.info("Click on product details toggle icoon");
    sa.assertTrue(gVar.assertVisible("SavedCart_ProductDetials_Toggle", "Profile//Savedcart.properties"), "Click on product details toggle icoon");
    
    log.info("Grid image");
    sa.assertTrue(gVar.assertVisible("Savedcart_Productdetials_GridImage", "Profile//Savedcart.properties"),"Grid image");
    
    log.info("Grid details color");
    sa.assertTrue(gVar.assertVisible("SavedCart_Productvariant_Color", "Profile//Savedcart.properties"),"Grid details color");

    log.info("Grid details Fit");
    sa.assertTrue(gVar.assertVisible("SavedCart_Productvariant_Fit", "Profile//Savedcart.properties"),"Grid details Fit");

    log.info("Grid details Size");
    sa.assertTrue(gVar.assertVisible("SavedCart_Productvariant_Size", "Profile//Savedcart.properties"), "Grid details Size");
    
    log.info("Grid details Qty");
    sa.assertTrue(gVar.assertVisible("SavedCart_Productvariant_Qty", "Profile//Savedcart.properties"),"Grid details Qty");
    
    log.info("Grid details Price");
    sa.assertTrue(gVar.assertVisible("SavedCart_Productvariant_Price", "Profile//Savedcart.properties"),"Grid details Price");
    
    log.info("Verify the BREADCRUMB");
    String expected = GetData.getDataFromExcel("//data//GenericData.xls", "SavedCartdetails", 2, 3);
    String expectedBreadcrumb = expected+" "+ cartID;
    log.info("expected active breadcrumb:-  "+ expectedBreadcrumb);
    gVar.assertequalsIgnoreCase(gVar.assertEqual("Breadcrumb_ActiveElement", "Generic//Generic.properties"), 
    		expectedBreadcrumb,"verify the active breadcrumb");
    
    
}
	
@Test(groups={"reg"},description="DMED_064_DRM_575")
public void TC09_SavedCartUpdateNameDescription_Test() throws Exception
{
	
    log.info("Actual Saved cartName");
    String Name=l1.getWebElement("SavedCart_SavedDetails_Name",  "Profile//Savedcart.properties").getText();
    log.info("Actual description");
    String Description=l1.getWebElement("SavedCart_SavedDetals_Discription", "Profile//Savedcart.properties").getText();
    log.info("click on Edit button");
    l1.getWebElement("SavedCart_Edit_btn", "Profile//Savedcart.properties").click();
    log.info("Edit saved cart text");
    sa.assertTrue(gVar.assertVisible("EditSavedCart_PopUp_txt", "Profile//Savedcart.properties"),"Edit saved cart text");
    
    log.info("Enter name field");
    String updatedName = GetData.getDataFromExcel("//data//GenericData.xls", "savedCartDetails", 2,0);
    log.info("updatedName:- "+updatedName);
    l1.getWebElement("Editcart_Name_txtbox", "Profile//Savedcart.properties").clear();
    l1.getWebElement("Editcart_Name_txtbox", "Profile//Savedcart.properties").sendKeys(updatedName);
    log.info("Enter Description field");
    String updatedDisc = GetData.getDataFromExcel("//data//GenericData.xls", "savedCartDetails", 2,1);
    log.info("updatedDisc:- "+ updatedDisc);
    l1.getWebElement("Editcart_Discription_txtbox", "Profile//Savedcart.properties").clear();
    l1.getWebElement("Editcart_Discription_txtbox", "Profile//Savedcart.properties").sendKeys(updatedDisc);
    log.info("Click on Save button");
    sa.assertTrue(gVar.assertVisible("Editcart_Savecart_btn", "Profile//Savedcart.properties"),"Verify the SAVE button in EDIT saved cart popup");
    l1.getWebElement("Editcart_Savecart_btn", "Profile//Savedcart.properties").click();
    Thread.sleep(3000);
    log.info("Verify the UPDATED details");
    gVar.assertequalsIgnoreCase(gVar.assertEqual("SavedCart_SavedDetails_Name",  "Profile//Savedcart.properties"), 
    		updatedName,"Verify updated NAME");
    gVar.assertequalsIgnoreCase(gVar.assertEqual("SavedCart_SavedDetals_Discription", "Profile//Savedcart.properties"), 
    		updatedDisc,"Verify Updated discription");
	
    
}
	

@Test(groups="reg", description="DMED-067 DRM-562")
public void TC10_Sorting_Options_ForSavedCart() throws Exception
{
	int qty = 2;
	int rowDetail = 2;

	for (int i = 0; i < 2; i++) 
	{
		log.info("LOOP:-"+i);
		String product = GetData.getDataFromExcel("//data//GenericData.xls", "Products", rowDetail, 1);
		log.info("product:- "+ product);
		log.info("Navigate to cart page");
		s.navigateToCart(product,1, qty);
		log.info("cart Heading");
		sa.assertTrue(gVar.assertVisible("Cart_Heading", "Cart\\Cart.properties"),"Verify the cart page");
		
	    log.info("Click on Save Cart Button");
	    if (xmlTest.getParameter("executingIn").equalsIgnoreCase("mobile") || xmlTest.getParameter("executingIn").equalsIgnoreCase("Tablet"))
		{
	    	l1.getWebElement("Cart_SaveCart_btn_Mobile", "Cart\\Cart.properties").click();
		}
		else
		{
			l1.getWebElement("Cart_SaveCart_btn", "Cart\\Cart.properties").click();
		}
		
		log.info("Entering the Details in saved Cart popup");
		s.savedCartDetails(rowDetail);

		log.info("Click on save button");
		l1.getWebElement("Cart_SaveCart_Save_btn", "Cart\\Cart.properties").click();
		
		qty++;
		rowDetail++;
		Thread.sleep(3000);
	}
	
	log.info("Navigate to save cart page");
	s.navigateToSavedCart();
	
	log.info("Verify the SortBy dropdown box");
	sa.assertTrue(gVar.assertVisible("SavedCart_SortByDropdownBox_Top", "Profile//Savedcart.properties"),"SortBy dropdown TOP");
	sa.assertTrue(gVar.assertVisible("SavedCart_SortByDropdownBox_Bottom", "Profile//Savedcart.properties"),"SortBy dropdown Bottom");
	
	WebElement sortByDropdownTop = l1.getWebElement("SavedCart_SortByDropdown_Top", "Profile//Savedcart.properties");

	List<WebElement> optionsList1 = l1.getWebElements("SavedCart_DropdownOptions", "Profile//Savedcart.properties");
	for (int j = 1; j < optionsList1.size(); j++) 
	{
		log.info("LOOP:-  "+j);
		List<WebElement> optionsList = l1.getWebElements("SavedCart_DropdownOptions", "Profile//Savedcart.properties");
		log.info("Click on Dropdown box");
		l1.getWebElement("SavedCart_SortByDropdownBox_Top", "Profile//Savedcart.properties").click();
		
		log.info("Verify whether the dropdown is expanded or not");
		sa.assertEquals(l1.getWebElement("SavedCart_SortByDropdownBox_Top", "Profile//Savedcart.properties").getAttribute("aria-expanded"),
				"true", "Verify the expanded dropdown");
		
		log.info("Click on the options");
		log.info("xpath:- "+optionsList.get(j));
		optionsList.get(j).click();
		Thread.sleep(3000);
		
		log.info("Verify the Selected dropdown option");
		String exptedVal = GetData.getDataFromExcel("//data//GenericData.xls", "SavedCartdetails", j, 4);
		sa.assertEquals(l1.getWebElement("SavedCart_SortByDropdownBox_Top", "Profile//Savedcart.properties").getAttribute("title"),
				exptedVal, "Verify the Selected value in dropdown-TOP");
		
		sa.assertEquals(l1.getWebElement("SavedCart_SortByDropdownBox_Bottom", "Profile//Savedcart.properties").getAttribute("title"),
				exptedVal, "Verify the Selected value in dropdown-BOTTOM");
		
	}
	
	sa.assertAll();
}

@Test(groups={"reg"}, description="DMED-067 DRM-562")
public void TC11_Sorting_Functionality_ForSavedCart() throws Exception
{

	log.info("Verify the sort functionality for below mentioned options only");
	String sortOptions[] = {"byName","byCode","byTotal"};	//VALUE of the sort by options
	
	log.info("Collect the NAME,ID and PRICE");
	for (int j = 0; j < sortOptions.length; j++) 
	{
		log.info("LOOP:-"+j);
		log.info("Select the dropdown option");
		WebElement sortByDropdownTop = l1.getWebElement("SavedCart_SortByDropdown_Top", "Profile//Savedcart.properties");
		Select selTop = new Select(sortByDropdownTop);
		selTop.selectByValue(sortOptions[j]);
		
		log.info("Collect the Saved cart name, id, Quantity and price");
		List<WebElement> savedCartsNameList = l1.getWebElements("SavedCart_Savedcart_name", "Profile//Savedcart.properties");
		List<WebElement> savedCartsIdList = l1.getWebElements("SavedCart_All_Ids", "Profile//Savedcart.properties");
		List<WebElement> savedCartsPriceList = l1.getWebElements("SavedCart_All_Price", "Profile//Savedcart.properties");
		
		ArrayList<String> nameList = new ArrayList<String>();
		ArrayList<String> nameListBefor = new ArrayList<String>();
		ArrayList<Integer> idList = new ArrayList<Integer>();
		ArrayList<Integer> idListBefore = new ArrayList<Integer>();
		ArrayList<Double> priceList = new ArrayList<>();
		ArrayList<Double> priceListBefore = new ArrayList<>();
		
		log.info("nested for loop started:- "+j);
		for (int num = 0; num < savedCartsNameList.size(); num++) 
		{
			log.info("NESTED LOOP:- " + num);
			nameList.add(savedCartsNameList.get(num).getText());
			
			idList.add(Integer.parseInt(savedCartsIdList.get(num).getText()));
			
			priceList.add(Double.parseDouble(savedCartsPriceList.get(num).getText().replace("$", "")));
		}
		log.info("List of Saved carts name, ID and product price");
		log.info("nameList:- "+ nameList);
		log.info("idList:- "+ idList);
		log.info("priceList:- "+ priceList);
		
		log.info("Copy all the array list to another arraylist");
		nameListBefor = (ArrayList<String>) nameList.clone();
		idListBefore = (ArrayList<Integer>) idList.clone();
		priceListBefore = (ArrayList<Double>) priceList.clone();
		
		log.info("Sort the NAME, ID and PRICES");
		Collections.sort(nameList);
		Collections.sort(idList);
		Collections.sort(priceList);

		log.info("nameList After SORT:- "+ nameList);
		log.info("idList After SORT:- "+ idList);
		log.info("priceList After SORT:- "+ priceList);
		
		if (j==0) 
		{
			log.info("Verify SORT by name");
			sa.assertEquals(nameList, nameListBefor, "Verify the sorted NAmes");
		} 
		else if (j==1) 
		{
			log.info("Verify SORT by ID");
			sa.assertEquals(idList, idListBefore, "Verify the sorted ID");
		} 
		else 
		{
			log.info("Verify SORT by Price");
			sa.assertEquals(priceList, priceListBefore, "Verify the sorted Price");
		}
				
	}

	sa.assertAll();
}


@Test(groups={"reg"}, description="extra TC-1")
public void TC12_backToSavedCartFunctionality() throws Exception
{
	log.info("Navigate to Saved Cart Details page");
	l1.getWebElement("SavedCart_Savedcart_name", "Profile//Savedcart.properties").click();
	log.info("Verify the Saved Cart Details page");
	sa.assertTrue(gVar.assertVisible("SavedCartDetails_Heading", "Profile//Savedcart.properties"),"Saved CArt details page");
	
	log.info("Verify and Click on BACK to saved cart button");
	gVar.assertequalsIgnoreCase(gVar.assertEqual("SavedCart_BacktoSavedCart_btn", "Profile//Savedcart.properties"), 
			GetData.getDataFromExcel("//data//GenericData.xls", "SavedCartdetails", 6, 3),"Verify BACK to saved cart button ");
	
	l1.getWebElement("SavedCart_BacktoSavedCart_btn", "Profile//Savedcart.properties").click();

	log.info("Verify the SAVED cart page");
	sa.assertTrue(gVar.assertVisible("SavedCart_Heading", "Profile//Savedcart.properties"),"Verify the SAVED CART heading");
	
	
	log.info("Clear saved cart");
	s.removeSavedcarts();
	
	log.info("Clear cart");
	cart.clearCart();
	
	
	sa.assertAll();
}

}





