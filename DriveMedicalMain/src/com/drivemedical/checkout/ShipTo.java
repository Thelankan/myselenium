package com.drivemedical.checkout;

import org.testng.annotations.Test;

import com.drivemedical.utilgeneric.BaseTest;

public class ShipTo extends BaseTest{

	@Test(groups={"reg"},description="OOTB-013 DRM-848,849")
	public void verifyRequiredFields() throws Exception
	{
		log.info("navigate to shipTo page");
		checkout.navigateToShipToPage();
		
		log.info("expand add address section");
		l1.getWebElement("ShipTo_AddAddress_Link", "Checkout//ShipTo.properties").click();
		log.info("click on continue");
		l1.getWebElement("ShipTo_Continue_button", "Checkout//ShipTo.properties").click();
		
		log.info("verify displayed error messages");
		log.info("Name");
		sa.assertTrue(gVar.assertVisible("ShipTo_Name_Error_Msg", "Checkout//ShipTo.properties"),"First Name");
		log.info("Address 1");
		sa.assertTrue(gVar.assertVisible("ShipTo_Addr1_Erro_Msg", "Checkout//ShipTo.properties"),"Address 1");
		log.info("City");
		sa.assertTrue(gVar.assertVisible("ShipTo_City_Erro_Msg", "Checkout//ShipTo.properties"),"City");
		log.info("State");
		sa.assertTrue(gVar.assertVisible("ShipTo_State_Erro_Msg", "Checkout//ShipTo.properties"),"State");
		log.info("Zip");
		sa.assertTrue(gVar.assertVisible("ShipTo_Zip_Erro_Msg", "Checkout//ShipTo.properties"),"Zip");
		log.info("phone");
		sa.assertTrue(gVar.assertVisible("ShipTo_Phone_Erro_Msg", "Checkout//ShipTo.properties"),"Phone");
		
		sa.assertAll();
	}
	
}
