package com.drivemedical.checkout;

import org.openqa.selenium.By;
import org.testng.annotations.Test;
import org.testng.xml.XmlTest;

import com.drivemedical.projectspec.CheckoutFunctions;
import com.drivemedical.utilgeneric.BaseTest;

public class PONumber extends BaseTest{
	
	String orderNum;

	@Test(groups={"reg","sanity_reg"},description="OOTB-012 DRM-804")
	public void TC00_VerifyPONumberInOrderReview(XmlTest xmlTest) throws Exception
	{
		log.info("log out from application");
		p.logout(xmlTest);
		log.info("Log in to the application");
		p.navigateToLoginPage();
		p.logIn(xmlTest);
		
		log.info("navigate to Order Review page");
		checkout.navigateToOrderReviewPage();
		log.info("verify PO number");
		sa.assertTrue(gVar.assertVisible("PONumber", "Checkout//PaymentType.properties"),"PO Number");
		sa.assertEquals(gVar.assertEqual("PONumber", "Checkout//PaymentType.properties"),"PO: "+CheckoutFunctions.PONumberVal);
		sa.assertAll();
	}
	
	@Test(groups={"reg","sanity_reg"},description="OOTB-012 DRM-803,805 OOTB-023 DRM-1251")
	public void TC01_VerifyPONumberInOrderConfirmation() throws Exception
	{
		log.info("click on place order");
		l1.getWebElements("OrderReview_PlaceOrder_Btn", "Checkout//OrderReview.properties").get(0).click();
		log.info("verify order placed or not");
		sa.assertTrue(gVar.assertVisible("OrderConfirmation_Heading", "Checkout//OrderConfirmation.properties"),"Order Confirmation heading");
		log.info("verify PO Number in Order Confirmation page");
		sa.assertTrue(gVar.assertVisible("OrderConfirmation_PONumber_Text", "Checkout//OrderConfirmation.properties"),"Po Number text");
		sa.assertTrue(gVar.assertVisible("OrderConfirmation_PONumber_Value", "Checkout//OrderConfirmation.properties"),"Po Number value");
		sa.assertEquals(gVar.assertEqual("OrderConfirmation_PONumber_Value", "Checkout//OrderConfirmation.properties"), CheckoutFunctions.PONumberVal);
	
		log.info("Collect the Order ID");
		String orderID = l1.getWebElement("OrderConfir_OrderNum", "Checkout//OrderConfirmation.properties").getText();
		log.info("orderID:- "+ orderID);
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="OOTB-012 DRM-806")
	public void TC02_VerifyPONumberInOrderDetails() throws Exception
	{
		log.info("fetch order number");
		orderNum=l1.getWebElement("OrderConfir_OrderNum", "Checkout//OrderConfirmation.properties").getText().split("#")[1];
		log.info("click on order status from header");
		if(xmlTest.getParameter("executingIn").equalsIgnoreCase("mobile") || xmlTest.getParameter("executingIn").equalsIgnoreCase("Tablet")) {
			l1.getWebElement("Header_Hamburger_Menu", "Shopnav//header.properties").click();
			log.info("click on order history link");
			l1.getWebElement("Hamburger_OrderHistory_Link", "Shopnav//header.properties").click();
			log.info("click on order number link");
			driver.findElements(By.xpath("//td[contains(@class,'order-number')]//a")).get(0).click();
			
		} else {
		l1.getWebElement("Header_OrderStatus_Link", "Shopnav//header.properties").click();
		log.info("click on order number link");
		driver.findElement(By.xpath("//a[contains(text(),'"+orderNum+"')]")).click();
		}
		
		log.info("verify PO Number in Order Details page");
		sa.assertTrue(gVar.assertVisible("OrderConfirmation_PONumber_Text", "Checkout//OrderConfirmation.properties"),"Po Number text");
		sa.assertTrue(gVar.assertVisible("OrderConfirmation_PONumber_Value", "Checkout//OrderConfirmation.properties"),"Po Number value");
		sa.assertEquals(gVar.assertEqual("OrderConfirmation_PONumber_Value", "Checkout//OrderConfirmation.properties"), CheckoutFunctions.PONumberVal);
	}
	
	@Test(groups={"reg"},description="DMED-066 DRM-1183")
	public void TC03_verifyReorder(XmlTest xmlTest) throws Exception {
		
		log.info("verify re order button in order details page");
		sa.assertTrue(gVar.assertVisible("OrderDetails_Reorder_Btn", "Profile//OrderDetails.properties",0),"Re order On top");
		sa.assertTrue(gVar.assertVisible("OrderDetails_Reorder_Btn", "Profile//OrderDetails.properties",1),"Re order On bottom");
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="DMED-066 DRM-1184,1186")
	public void TC04_reorderFun(XmlTest xmlTest) throws Exception {
		
		log.info("click on re order button");
		l1.getWebElements("OrderDetails_Reorder_Btn", "Profile//OrderDetails.properties").get(0).click();
		log.info("verify navigation");
		sa.assertTrue(gVar.assertVisible("OrderReview_Heading", "Checkout//OrderReview.properties"),"Order Review Heading");
		log.info("place order buttton");
		sa.assertTrue(gVar.assertVisible("OrderReview_PlaceOrder_Btn", "Checkout//OrderReview.properties"),"Order Review Heading");
		log.info("verify item quantity");
		String itemCountText=l1.getWebElement("OrderReview_Items_Count", "Checkout//OrderReview.properties").getText();
		String[] qtyText=itemCountText.split(" ");
		String qty=qtyText[0].replace("(", "");
		sa.assertEquals(qty,"1");
		log.info("place order");
		l1.getWebElement("OrderReview_PlaceOrder_Btn", "Checkout//OrderReview.properties").click();
		log.info("verify order placed or not");
		sa.assertTrue(gVar.assertVisible("OrderConfirmation_Heading", "Checkout//OrderConfirmation.properties"),"Order Confirmation heading");
		sa.assertAll();
	}
	
	
}
