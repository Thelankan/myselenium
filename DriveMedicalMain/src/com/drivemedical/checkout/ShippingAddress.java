package com.drivemedical.checkout;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import org.testng.xml.XmlTest;

import com.drivemedical.utilgeneric.BaseTest;
import com.drivemedical.utilgeneric.GetData;

//DRM-850,851,852

public class ShippingAddress extends BaseTest{
	
	String savedAddr;

	@Test(groups={"reg"},description="OOB-013 DRM-846,847,844")
	public void TC00_dropShip_UI(XmlTest xmlTest) throws Exception
	{
		log.info("Log in to the application");
		p.navigateToLoginPage();
		p.logIn(xmlTest);
		log.info("navigate to ship to page");
		checkout.navigateToShipToPage();
		log.info("add address");
		for(int i=0;i<2;i++) {
			
		log.info("click on add address link");
		l1.getWebElement("ShipTo_AddAddress_Link","Checkout//ShipTo.properties").click();
		int k=i+1;
		checkout.addAddress(k);
		log.info("click on continue");
		l1.getWebElement("ShipTo_Continue_button", "Checkout//ShipTo.properties").click();
		log.info("click on Edit Button to come back to Ship To Page");
		l1.getWebElements("ShipTo_Edit_Btn", "Checkout//ShipTo.properties").get(0).click();
		}
		List<WebElement> allSavedAddr=l1.getWebElements("ShipTo_SavedAddr_Text", "Checkout//ShipTo.properties");
		int j=1;
		for(int i=0;i<allSavedAddr.size();i++) {
		log.info("verify saved addresses");
		if(i==0) {
		log.info("verify Shipping addresses");
		sa.assertEquals(allSavedAddr.get(i).getText(), GetData.getDataFromExcel("//data//GenericData.xls", "Address", 1, 11));
		
		} else { 
		log.info("verify Drop Ship addresses");
		sa.assertEquals(allSavedAddr.get(i).getText(), GetData.getDataFromExcel("//data//GenericData.xls", "Address", j, 9));
		j++;
		}
		}
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="OOB-013 DRM-842")
	public void TC01_SaveShippedAddress() throws Exception
	{
		
		log.info("fetch selecting saved address");
		savedAddr=l1.getWebElements("ShipTo_SavedAddr_Text", "Checkout//ShipTo.properties").get(1).getText();
		log.info("select saved address");
		l1.getWebElements("ShipTo_SavedAddr_Radio","Checkout//ShipTo.properties").get(1).click();
		log.info("click on continue button");
		l1.getWebElement("ShipTo_Continue_button","Checkout//ShipTo.properties").click();
		log.info("verify navigation to shipping method section");
		sa.assertTrue(gVar.assertVisible("ShippingMtd_ShipAddress_Heading", "Checkout//ShippingMethod.properties"),"shipping address heading");
		sa.assertEquals(GetData.getDataFromExcel("//data//GenericData.xls", "Address", 1, 9),gVar.assertEqual("ShipTo_Shipping_Addr", "Checkout//ShipTo.properties"));
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="OOB-013 DRM0-843,845")
	public void TC02_checkout_verifySavedAddrDisplay() throws Exception
	{
		log.info("Navigate back to ship to page");
		l1.getWebElements("ShipTo_Edit_Btn", "Checkout//ShipTo.properties").get(0).click();
		log.info("verify the display of addresses");
		List<WebElement> savedAddresses=l1.getWebElements("ShipTo_SavedAddr_Text", "Checkout//ShipTo.properties");
		int j=2;
		int i=0;
		for(WebElement ele:savedAddresses) {
			log.info("verify ship to should be a radio button");
			sa.assertTrue(gVar.assertVisible("ShipTo_SavedAddr_Radio", "Checkout//ShipTo.properties",i),"saved addresses radio buttons");
/*			log.info("verify saved addresses");
			sa.assertEquals(gVar.assertEqual("ShipTo_SavedAddr_Text", "Checkout//ShipTo.properties", i), GetData.getDataFromExcel("//data//GenericData.xls", "Address", j, 9));
			j++;*/
			
			if(i==0) {
				log.info("verify Shipping addresses");
				sa.assertEquals(ele.getText(), GetData.getDataFromExcel("//data//GenericData.xls", "Address", 1, 11));
				
				} else { 
				log.info("verify Drop Ship addresses");
				sa.assertEquals(ele.getText(), GetData.getDataFromExcel("//data//GenericData.xls", "Address", j, 9));
				j++;
				}
			
			i++;
		}
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="OOB-013 DRM0-848,849")
	public void TC03_checkout_verifyReqFields() throws Exception
	{
		log.info("click on add address link");
		l1.getWebElement("ShipTo_AddAddress_Link","Checkout//ShipTo.properties").click();
		log.info("click on continue");
		l1.getWebElement("ShipTo_Continue_button", "Checkout//ShipTo.properties").click();
		log.info("verify error messages");
		log.info("first name error messages");
		sa.assertTrue(gVar.assertVisible("ShipTo_Name_Error_Msg", "Checkout//ShipTo.properties"),"First name");
		sa.assertEquals(gVar.getCssValue("ShipTo_Name_Error_Msg", "Checkout//ShipTo.properties", "color"), gVar.emTextColor);
		log.info("Address 1 name error messages");
		sa.assertTrue(gVar.assertVisible("ShipTo_Addr1_Erro_Msg", "Checkout//ShipTo.properties"),"Address1 name");
		sa.assertEquals(gVar.getCssValue("ShipTo_Addr1_Erro_Msg", "Checkout//ShipTo.properties", "color"), gVar.emTextColor);
		log.info("city name error messages");
		sa.assertTrue(gVar.assertVisible("ShipTo_City_Erro_Msg", "Checkout//ShipTo.properties"),"city name");
		sa.assertEquals(gVar.getCssValue("ShipTo_City_Erro_Msg", "Checkout//ShipTo.properties", "color"), gVar.emTextColor);
		log.info("state error messages");
		sa.assertTrue(gVar.assertVisible("ShipTo_State_Erro_Msg", "Checkout//ShipTo.properties"),"state name");
		sa.assertEquals(gVar.getCssValue("ShipTo_State_Erro_Msg", "Checkout//ShipTo.properties", "color"), gVar.emTextColor);
		log.info("zip error messages");
		sa.assertTrue(gVar.assertVisible("ShipTo_Zip_Erro_Msg", "Checkout//ShipTo.properties"),"zip name");
		sa.assertEquals(gVar.getCssValue("ShipTo_Zip_Erro_Msg", "Checkout//ShipTo.properties", "color"), gVar.emTextColor);
		log.info("Phone error messages");
		sa.assertTrue(gVar.assertVisible("ShipTo_Phone_Erro_Msg", "Checkout//ShipTo.properties"),"Phone name");
		sa.assertEquals(gVar.getCssValue("ShipTo_Phone_Erro_Msg", "Checkout//ShipTo.properties", "color"), gVar.emTextColor);
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="OOB-013 DRM0-854")
	public void TC04_checkout_verifyCancelLink() throws Exception
	{
		log.info("click on cancel link");
		l1.getWebElement("ShipTo_Cancel_Link","Checkout//ShipTo.properties").click();
		log.info("verify collapsed or not");
		sa.assertTrue(gVar.assertNotVisible("ShipTo_Name_Textbox", "Checkout//ShipTo.properties"),"Name Should not visible");
		sa.assertTrue(gVar.assertNotVisible("ShipTo_Cancel_Link", "Checkout//ShipTo.properties"),"Name Should not visible");
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="OOB-013 DRM0-853")
	public void TC05_checkout_verifyOrderReviewDetails() throws Exception
	{
		log.info("fetch selecting saved address");
		savedAddr=l1.getWebElements("ShipTo_SavedAddr_Text", "Checkout//ShipTo.properties").get(1).getText();
		log.info("select saved address");
		l1.getWebElements("ShipTo_SavedAddr_Radio","Checkout//ShipTo.properties").get(1).click();
		log.info("click on continue button");
		l1.getWebElement("ShipTo_Continue_button","Checkout//ShipTo.properties").click();
		List<WebElement> allShipMtd=l1.getWebElements("ShippingMtd_DeliveryMtd", "Checkout//ShippingMethod.properties");
		String selShipMtdVal=null;
		for(WebElement ele:allShipMtd) {
			if(ele.isSelected())
			{
				selShipMtdVal=ele.findElement(By.xpath("following-sibling::label")).getText();
			}
		}
	/*	String selShipMtdVal=gVar.selectObj(l1.getWebElement("ShippingMtd_DropDown","Checkout//ShipTo.properties")).getFirstSelectedOption().getText();
		String shipMtdVal=selShipMtdVal.split("$")[1];*/
		log.info("click on continue");
		l1.getWebElement("ShippingMtd_Next_Btn", "Checkout//ShippingMethod.properties").click();
		log.info("Enter Account payment number");
		String PONumberVal=GetData.getDataFromProperties("//POM//Checkout//PaymentType.properties", "POBoxNumber");
		BaseTest.l1.getWebElement("PT_POBox_Textbox","Checkout//PaymentType.properties").clear();
		BaseTest.l1.getWebElement("PT_POBox_Textbox","Checkout//PaymentType.properties").sendKeys(PONumberVal);
		BaseTest.log.info("click on next button");
		BaseTest.l1.getWebElement("Next_Button","Checkout//PaymentType.properties").click();
		log.info("verify payment type and shipping method value");
		sa.assertTrue(false,"Issue in application");
		sa.assertAll();
	}
	
}
