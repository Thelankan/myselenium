package com.drivemedical.utilgeneric;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import org.testng.annotations.Listeners;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class Screenshot implements ITestListener{
	
	WebDriver driver;
	Method m;

	public void onTestStart(ITestResult result) {
		
	}

	public void onTestSuccess(ITestResult result) {
		 driver=BaseTest.driver;
		if(driver.getClass().getSimpleName().equalsIgnoreCase("InternetExplorerDriver")) {
			BaseTest.extentTestChildIE.log(LogStatus.PASS,"");
		}
		else if(driver.getClass().getSimpleName().equalsIgnoreCase("FirefoxDriver")) {
			BaseTest.extentTestChildFF.log(LogStatus.PASS, "");
			
		} else if (driver.getClass().getSimpleName().equalsIgnoreCase("ChromeDriver")) {
			try{
			BaseTest.extentTestChildAndroidPhone.log(LogStatus.PASS, "");
			} catch(Exception e) {
			BaseTest.extentTestChildAndroidTab.log(LogStatus.PASS, "");
			}
		} else if (driver.getClass().getSimpleName().equalsIgnoreCase("IOSDriver")) {
			try{
			BaseTest.extentTestChildiPhone.log(LogStatus.PASS, "");
			} catch(Exception e) {
			BaseTest.extentTestChildiPad.log(LogStatus.PASS, "");
			}
		}

	}

	public void onTestFailure(ITestResult result) {
		
		try{
			driver=GetDriver.driver;
			String imgName=result.getInstanceName()+" "+result.getMethod().getMethodName()+" "+driver.getClass().getSimpleName();
			//write take screen shot code
			 
			System.out.println(driver.getClass().getName());
			System.out.println("on failure");
			//((JavascriptExecutor) driver).executeScript("window.focus();");
			
			TakesScreenshot scrShot =((TakesScreenshot)driver);
			FileUtils.copyFile(scrShot.getScreenshotAs(OutputType.FILE), new File(BaseTest.filePath+"\\"+imgName+".png"));
			if(driver.getClass().getSimpleName().equalsIgnoreCase("InternetExplorerDriver")) {
				BaseTest.extentTestChildIE.log(LogStatus.FAIL,"",BaseTest.extentTestChildIE.addScreenCapture(BaseTest.filePath+"\\"+imgName+".png"));
			}
			else if(driver.getClass().getSimpleName().equalsIgnoreCase("FirefoxDriver")) {
				BaseTest.extentTestChildFF.log(LogStatus.FAIL, "",BaseTest.extentTestChildFF.addScreenCapture(BaseTest.filePath+"\\"+imgName+".png"));
				
			} else if (driver.getClass().getSimpleName().equalsIgnoreCase("ChromeDriver")) {
				try{
				BaseTest.extentTestChildAndroidPhone.log(LogStatus.FAIL, "",BaseTest.extentTestChildAndroidPhone.addScreenCapture(BaseTest.filePath+"\\"+imgName+".png"));
				} catch (Exception e) {
					BaseTest.extentTestChildAndroidTab.log(LogStatus.FAIL, "",BaseTest.extentTestChildAndroidTab.addScreenCapture(BaseTest.filePath+"\\"+imgName+".png"));
				}
				
			} else if (driver.getClass().getSimpleName().equalsIgnoreCase("IOSDriver")) {
				try{
				BaseTest.extentTestChildiPhone.log(LogStatus.FAIL, "",BaseTest.extentTestChildiPhone.addScreenCapture(BaseTest.filePath+"\\"+imgName+".png"));
				} catch (Exception e) {
					BaseTest.extentTestChildiPad.log(LogStatus.FAIL, "",BaseTest.extentTestChildiPad.addScreenCapture(BaseTest.filePath+"\\"+imgName+".png"));
				}
				
			}
			
			}
			catch(Exception e)
			{
				e.printStackTrace();
				System.out.println("error bro here");
			}
	}

	public void onTestSkipped(ITestResult result) {
		
	//	extentTest.log(LogStatus.SKIP, "Its skipped bro please check the reason");
		
	}

	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		
		
	}

	public void onStart(ITestContext context) {
		
		
	}

	public void onFinish(ITestContext context) {

	//	extentReport.flush();
		
	}
}
