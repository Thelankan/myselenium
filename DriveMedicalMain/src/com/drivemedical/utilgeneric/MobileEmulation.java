package com.drivemedical.utilgeneric;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

public class MobileEmulation {

	
	void mobEmu()
	{
/*		//set mobile emulation for existing devices
		HashMap<String,String> mobEmul=new HashMap<String, String>();
		mobEmul.put("deviceName", "Google Nexus 5");
		
		HashMap<String, Object> chromeOptions=new HashMap<String, Object>();
		chromeOptions.put("mobileEmulation", mobEmul);
		
		 or
		
		ChromeOptions c=new ChromeOptions();
		c.setExperimentalOption("mobileEmulation", mobEmul);
		
		DesiredCapabilities des=new DesiredCapabilities();
		des.setCapability(ChromeOptions.CAPABILITY, chromeOptions);
		des.setCapability(CapabilityType.BROWSER_NAME, "chrome");
		des.setCapability(CapabilityType.PLATFORM, "WINDOWS");
		
		System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+"\\drivers\\chromedriver.exe");
		ChromeDriver driver=new ChromeDriver(des);
		driver.get("http://www.google.com");*/
		
		
		//set custom mobile emulation
		
		Map<String, Object> deviceMetrics = new HashMap<String, Object>();
		deviceMetrics.put("width", 768);
		deviceMetrics.put("height", 1024);
		deviceMetrics.put("pixelRatio", 3.0);
		
		
		Map<String, Object> mobileEmulation = new HashMap<String, Object>();
		mobileEmulation.put("deviceMetrics", deviceMetrics);
		mobileEmulation.put("userAgent", "Mozilla/5.0 (Linux; Android 6.0.1; SM-T550 Build/MMB29M) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 YaBrowser/16.10.2.1487.01 Safari/537.36");
		

		Map<String, Object> chromeOption = new HashMap<String, Object>();
		chromeOption.put("mobileEmulation", mobileEmulation);
		DesiredCapabilities capabilities = DesiredCapabilities.chrome();
		capabilities.setCapability(ChromeOptions.CAPABILITY, chromeOption);
		System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+"\\drivers\\chromedriver.exe");
		WebDriver driver1 = new ChromeDriver(capabilities);
		driver1.get("https://development.elizabetharden.pfsweb.demandware.net/on/demandware.store/Sites-ElizabethArden-Site/default");
	}
	
}
