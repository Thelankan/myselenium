package com.drivemedical.utilgeneric;

import static org.testng.Assert.assertFalse;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.MobileCapabilityType;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.Platform;
import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.html5.Location;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.internal.ElementScrollBehavior;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import org.testng.xml.XmlTest;

import com.drivemedical.projectspec.BackOffice;
import com.relevantcodes.extentreports.LogStatus;


public class Test1 extends BaseTest{
	
	/*WebDriver driver;
		SoftAssert sa=new SoftAssert();
	String str;
*//*	int i;
	WebDriver driver;
	com.relevantcodes.extentreports.ExtentReports extReport;
	com.relevantcodes.extentreports.ExtentTest extTest;
	com.relevantcodes.extentreports.ExtentTest extTest1;*/
	
/*	@Test
	public void test() throws FindFailed, IOException, InterruptedException
	{
	
	//	Screen s=new Screen();
		//s.wheel("C:\\Users\\areddy2\\Desktop\\image.PNG",25,0);
//		Pattern p=new Pattern("C:\\Sel\\desktop.PNG");
		//C:\Sel
		//s.doubleClick(p);
	//	ScreenImage s1=s.capture();
		//s1.save("c:\\sel");
//		new File("C:\\Test").mkdir();
//		//Files.copy(new FileInputStream(),new File("C:\\Test").toPath());
//	
		DesiredCapabilities des=DesiredCapabilities.firefox();
		des.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		des.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS, true);
		System.setProperty("webdriver.gecko.driver",System.getProperty("user.dir")+"\\drivers\\geckodriver.exe");	
		WebDriver driver=new FirefoxDriver(des);
		
		driver.get("https://driveus.local:9002/drivestorefront/driveUS/en/USD/search/?text=strecher");
		
			try
			{
				Thread.sleep(1000);
//				driver.findElement(By.id("overridelink")).click();
				driver.navigate ().to ("javascript:document.getElementById('overridelink').click()");
				System.out.println("overrided");
				Thread.sleep(5000);
			}
			catch(Exception e)
			{
				e.printStackTrace();
				System.out.println("not overrided");
			}
			
			Thread.sleep(5000);
			//driver.findElement(By.xpath("//a[contains(text(),'Sign In')]")).click();
			//driver.findElement(By.linkText("SIGN IN")).click();
			System.out.println(driver.findElement(By.xpath("//div[@class='headline']")).getText());
			//driver.findElement(By.xpath("//div[@class='forgotten-password']/a")).click();
		//driver.findElement(By.xpath("//div[@class='yCmsContentSlot componentContainer']/descendant::a[@class='mini-cart-link js-mini-cart-link']")).click();
		//Thread.sleep(4000);
		//driver.findElement(By.linkText("Forgot your password?")).click();
			
			
	}*/
	
/*	@BeforeClass
	public void extReport()
	{
		extReport = new com.relevantcodes.extentreports.ExtentReports("/extTest/test.html", false);
		extTest=extReport.startTest("LogIn","testing hi");
	}
	
	@BeforeMethod
	public void exTest(Method m)
	{
		extTest1=extReport.startTest(m.getName());
		extTest.appendChild(extTest1);
	}
	
	@AfterTest
	public void close()
	{
		extReport.endTest(extTest);
		extReport.flush();
	}
	
	@Test(description="hey man")
	public void signInTest_1()
	{
		ExtentHtmlReporter ehr=new ExtentHtmlReporter("\\extent\\test1.html");
		//ehr.loadConfig("C:\\Sel\\extentreports-java-3.0.7\\extentreports-java-3.0.7\\extent-config.xml");
		ehr.setAppendExisting(true);
		ExtentReports ext=new ExtentReports();
		ext.attachReporter(ehr);
		ExtentTest eTest=ext.createTest("hello");
		eTest.log(Status.PASS, "hi");
		ext.flush();
		
		extTest1.log(LogStatus.INFO,"no details"+extTest.addScreenCapture("/DriveMedical/com.drivemedical.shopnav.Header header_Signin_CA_Test.png"));
	}
	
	@Test
	public void signIn_UI_Test_2()
	{
		extTest1.log(LogStatus.FAIL, "test");
	}*/
	
/*	@Test
	public void tc01_createuser() throws Exception
	{
//		Location loc=new Location(41.8781, 87.6298, 181);
		FirefoxProfile fp=new FirefoxProfile();
		//fp.setPreference("geo.prompt.testing", true);
		//fp.setPreference("geo.prompt.testing.allow", false);
		fp.setPreference("geo.enabled", true);
		fp.setPreference("geo.wifi.uri", System.getProperty("user.dir")+"\\loc.json");
		fp.setPreference("geo.provider.ms-windows-location", false);
		fp.setAcceptUntrustedCertificates(true);
		fp.setAssumeUntrustedCertificateIssuer(true);
		System.setProperty("webdriver.gecko.driver",System.getProperty("user.dir")+"\\drivers\\geckodriver.exe");	
		FirefoxDriver driver=new FirefoxDriver(fp);
		driver.get("https://med-d-www.ms.ycs.io/drivestorefront/driveUS/en/USD/store-finder");
		
	}
	@org.testng.annotations.Test(priority=1)
	public void UITesting() throws Exception
	{
		DesiredCapabilities des=DesiredCapabilities.chrome();
		des.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		des.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.DISMISS);
		//des.setCapability(CapabilityType.ELEMENT_SCROLL_BEHAVIOR,ElementScrollBehavior.BOTTOM);
		des.setPlatform(Platform.VISTA);
		
		System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+"\\drivers\\chromedriver.exe");
		driver=new ChromeDriver(des);
		
		driver.get("https://stage.medicaldepot.com:9002/drivestorefront/driveUS/en/USD/Products/Mobility/Canes/c/Canes");
		Thread.sleep(5000);
		int k=driver.findElements(By.xpath("//div[@class='product-item' or @class='yCmsContentSlot product-grid-section1-slot' or @class='yCmsContentSlot product-grid-section1-slot ']")).size();
		System.out.println(k);
	}*/
	
/*	@org.testng.annotations.Test()
	public void UITesting() throws Exception
	{
		driver.navigate().to("https://www.upwork.com/i/freelancer-categories/agencies/");
		Thread.sleep(5000);
		List<WebElement> subCat=driver.findElements(By.xpath("//div[contains(@class,'letter text-left m-t')]"));
		
		int j=2;
		for(int i=0;i<subCat.size();i++)
		{
			int l=1;
			System.out.println("sub cat"+subCat.get(i).getText());
			
			SetData.writeDataToExcel(System.getProperty("user.dir")+"//Data.xlsx","Browse Agencies", 0, i, subCat.get(i).getText());
			
			List<WebElement> subsubCat=driver.findElements(By.xpath(".//*[@id='body-content-wrapper']/main/div/section[3]/div/div["+j+"]/div/descendant::a"));
			
			for(int k=0;k<subsubCat.size();k++){
				System.out.println("sub sub cat"+subsubCat.get(k).getText());
				SetData.writeDataToExcel(System.getProperty("user.dir")+"//Data.xlsx","Browse Agencies", l, i, subsubCat.get(k).getText());
				l++;
				System.out.println("l values is "+l);
			}
			j=j+2;
			if(i==1) {
			break;
			}
		}
		//SetData.writeDataToExcel(System.getProperty("user.dir")+"//Data.xlsx","Test",2, 2, "hello");
	}*/
	
	
	
	/*@Test
	public void test2() throws Exception
	{
	driver.get("https://www.naukri.com/nlogin/login?msg=0&URL=http%3A%2F%2Fmy.naukri.com");
	Thread.sleep(3000);
	try{
		driver.findElement(By.id("emailTxt")).sendKeys("adithyareddy1209@gmail.com");
		driver.findElement(By.id("pwd1")).sendKeys("08fh1a1209");
		driver.findElement(By.id("sbtLog")).click();
	
	} catch (Exception e){
		driver.findElement(By.id("usernameField")).sendKeys("adithyareddy1209@gmail.com");
		driver.findElement(By.id("passwordField")).sendKeys("08fh1a1209");
		driver.findElement(By.xpath("//button[contains(text(),'Login')]")).click();
	}
	
	Thread.sleep(5000);
	log.info("click on snap");
	driver.findElement(By.xpath("//div[@id='leftNav_updateProfile']/ul/li/a[1]")).click();
	Thread.sleep(5000);
	String industryName=new Select(driver.findElement(By.id("industryTypeId"))).getFirstSelectedOption().getText();
	System.out.println("industry name"+industryName);
	Select funAreaSel=new Select(driver.findElement(By.id("funcAreaId")));
	log.info("funcAreaId");
	List<WebElement> funArea=new ArrayList<WebElement>();
	funArea=funAreaSel.getOptions();
	int size=funArea.size();
	System.out.println(size);
	for(int i=0;i<size;i++) {
		System.out.println("iteration");
		int l=1;
		int k=i+1;
		Select funAreaSel1=new Select(driver.findElement(By.id("funcAreaId")));
		List<WebElement> funArea1=new ArrayList<WebElement>();
		funArea1=funAreaSel1.getOptions();
		System.out.println("Functional area"+funArea1.get(k).getText());
		funAreaSel1.selectByVisibleText(funArea1.get(k).getText());
		SetData.writeDataToExcel(System.getProperty("user.dir")+"//Data.xlsx","IT-SoftwareSoftware Services", 0, i, funArea1.get(k).getText());
		List<WebElement> roles=new ArrayList<WebElement>();
		roles=new Select(driver.findElement(By.id("roleId"))).getOptions();
		 for(int j=0;j<roles.size();j++) {
			 System.out.println("roles "+ roles.get(j).getText());
			 SetData.writeDataToExcel(System.getProperty("user.dir")+"//Data.xlsx","IT-SoftwareSoftware Services", l, i, roles.get(j).getText());
			 l++;
		 }
		 System.out.println("i value is"+i);
		WebElement opt=driver.findElement(By.xpath("//select[@id='roleId']"));
		List<WebElement> optChilds=opt.findElements(By.xpath(".//*"));
		for(WebElement wb:optChilds){
			try{
				System.out.println(wb.getAttribute("label"));
				 SetData.writeDataToExcel(System.getProperty("user.dir")+"//Data.xlsx","IT-SoftwareSoftware Services", l, i,wb.getAttribute("label"));
				 l++;
			} catch(Exception e) {
				System.out.println("exception");
			System.out.println(wb.getText());
			 SetData.writeDataToExcel(System.getProperty("user.dir")+"//Data.xlsx","IT-SoftwareSoftware Services", l, i,wb.getText());
			 l++;
			}
		}
	}
	
	}*/
	
	/*@Test(groups="reg")
	public void liTest() throws Exception
	{
		driver.get("https://www.linkedin.com/in/adithyareddy-gajjala-b744428a/");
		Thread.sleep(4000);
		try{
		driver.findElement(By.xpath("//a[contains(text(),'Sign in')]")).click();
		driver.findElement(By.id("login-email")).sendKeys("adithyareddy1209@gmail.com");
		driver.findElement(By.id("login-password")).sendKeys("08fh1a1209");
		driver.findElement(By.id("login-submit")).click();
		//Thread.sleep(39000);
		} catch(Exception e) {
			
		}
		driver.findElement(By.xpath("//a[@data-control-name='edit_skills_add']")).click();
		Thread.sleep(2000);
		
		
		int l=69;
		
		for(char alphabet2='E';alphabet2<='E';alphabet2++) {
			int m=65;
			
			for(char alphabet1='A';alphabet1<='Z';alphabet1++) {
				
				int j=0;
			for(char alphabet='A';alphabet<='Z';alphabet++) {
				
			String construct=alphabet1+""+alphabet+" ";
		driver.findElement(By.xpath("//input[contains(@placeholder,'Skill')]")).clear();
		driver.findElement(By.xpath("//input[contains(@placeholder,'Skill')]")).sendKeys(construct);
		Thread.sleep(3000);
		try {
		List<WebElement> results=driver.findElements(By.xpath("//div[@class='type-ahead-result-info']/h3"));
		int i=0;
		for(WebElement ele:results) {
			System.out.println(ele.getText());
			SetData.writeDataToExcel(System.getProperty("user.dir")+"//Data.xlsx", alphabet1+"", i, j, ele.getText());
			i++;
		}
		} catch (Exception e){
			
		}
		j++;
		System.out.println(j);
		
		}
		System.out.println("just check");

			}

		//System.out.println(i);
		System.out.println(j);
		for(int k=0;k<10;k++) {

			Thread.sleep(2000);
			driver.findElement(By.xpath("//input[contains(@placeholder,'Skill')]")).clear();
			Thread.sleep(2000);
			driver.findElement(By.xpath("//input[contains(@placeholder,'Skill')]")).sendKeys(k+"");
			Thread.sleep(3000);
			List<WebElement> results=driver.findElements(By.xpath("//div[@class='type-ahead-result-info']/h3"));
			int i=0;
			for(WebElement ele:results) {
				System.out.println(ele.getText());
				SetData.writeDataToExcel(System.getProperty("user.dir")+"//Data.xlsx","LinkedInSkill", i, j, ele.getText());
				i++;
			}
			j++;
			System.out.println(j);
			
		}
		}
	}*/
	
	/*@Test(groups={"reg"})
	public void paysa() throws Exception
	{
		DesiredCapabilities des=DesiredCapabilities.firefox();
		des.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		des.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.DISMISS);
		des.setCapability("acceptInsecureCerts", true);
		des.setCapability("requireWindowFocus", true);
		
		int j=0;
		int l=1;
		for(int i=0;i<200;i++) {
			int k=0;
			
			System.out.println("came here FF");
			System.setProperty("webdriver.gecko.driver",System.getProperty("user.dir")+"\\drivers\\geckodriver.exe");
			driver=new FirefoxDriver(des);
			
			if(i==0) {
			driver.get("https://www.paysa.com/jobs/directory/company");
			} else{
				l=l+1;
				//driver.findElement(By.xpath("//a[@aria-label='Next']")).click();
				driver.get("https://www.paysa.com/jobs/directory/title?page="+l);
				if(i==1) {
					Thread.sleep(15000);
				}
			}
			Thread.sleep(10000);
			List<WebElement> skills=driver.findElements(By.xpath("//div[@class='torso-listing-entry']/a"));
			for(WebElement element:skills) {
				String str=element.getText();
				System.out.println(str);
				SetData.writeDataToExcel(System.getProperty("user.dir")+"//Data.xlsx", "company", k, j, str);
				k++;
			}
			j++;
			driver.quit();
		}
	}*/
	
/*	@Test(groups={"reg"})
	public void dice() throws Exception
	{
		driver.get("https://www.dice.com/skills/browse");
		Thread.sleep(3000);
		List<WebElement> allSkillLinks=driver.findElements(By.xpath("//div[@class='col-md-8 mTB10']/a"));
		int j=0;
		for(int k=0;k<allSkillLinks.size();k++) {
			int i=0;
			allSkillLinks=driver.findElements(By.xpath("//div[@class='col-md-8 mTB10']/a"));
			allSkillLinks.get(k).click();
			List<WebElement> indSkills=driver.findElements(By.xpath("//div[@class='row']/div[@class='col-md-3']/a"));
			for(WebElement ele1:indSkills) {
				System.out.println(ele1.getText());
				SetData.writeDataToExcel(System.getProperty("user.dir")+"//Data.xlsx", "DiceSkills", i, j, ele1.getText());
				i++;
			}
			j++;
		}
		
	}*/
	
	@Test(groups="reg")
	public void liTest() throws Exception
	{
		driver.get("https://www.linkedin.com/in/adithyareddy-gajjala-b744428a/");
		Thread.sleep(4000);
		try{
		driver.findElement(By.xpath("//a[contains(text(),'Sign in')]")).click();
		driver.findElement(By.id("login-email")).sendKeys("adithyareddy1209@gmail.com");
		driver.findElement(By.id("login-password")).sendKeys("08fh1a1209");
		driver.findElement(By.id("login-submit")).click();
		//Thread.sleep(31000);
		} catch(Exception e) {
			
		}
		driver.findElement(By.xpath("//a[@data-control-name='edit_skills_add']")).click();
		Thread.sleep(2000);
		
		
			
			for(int i=0;i<1;i++) {
				String data=GetData.getDataFromExcel("//Data.xlsx", "GivenData", i, 0);	
				int j=0;
		for(char alphabet='G';alphabet<='Z';alphabet++) {
			
			for(char alphabet1='A';alphabet1<='Z';alphabet1++) {
			int k=0;
		System.out.println(data);
		String construct=data+" "+alphabet+""+alphabet1;
		driver.findElement(By.xpath("//input[contains(@placeholder,'Skill')]")).clear();
		driver.findElement(By.xpath("//input[contains(@placeholder,'Skill')]")).sendKeys(construct);
		Thread.sleep(3000);
		try {
		List<WebElement> results=driver.findElements(By.xpath("//div[@class='type-ahead-result-info']/h3"));
		for(WebElement ele:results) {
			System.out.println(ele.getText());
			SetData.writeDataToExcel(System.getProperty("user.dir")+"//Data.xlsx", data+"cont", k, j, ele.getText());
			k++;
		}
		} catch (Exception e){
			
		}
		j++;
		System.out.println(j);
		
		}
		}
		}
	}
	
	/*@Test(groups={"reg"})
	public void lanuage_Code() throws Exception {
		driver.get("https://www.loc.gov/standards/iso639-2/php/code_list.php");
		Thread.sleep(3000);
		List<WebElement> reqISO=driver.findElements(By.xpath("//tr[@valign='top']/td[2]"));
		int j=0;
		int k=0;
		for(int i=0;i<reqISO.size();i++) {
			List<WebElement> reqISO1=driver.findElements(By.xpath("//tr[@valign='top']/td[2]"));
			String str2=reqISO1.get(i).getText();
			str2=str2+"hi";
			str2=str2.trim();
			//System.out.println(str2);
			if(!str2.equalsIgnoreCase("hi")) {
				List<WebElement> reqLang=driver.findElements(By.xpath("//tr[@valign='top']/td[3]"));
				String str=reqLang.get(i).getText();
				str2=str2.replace("hi","");
				System.out.println(str2);
				System.out.println(str);
				SetData.writeDataToExcel(System.getProperty("user.dir")+"//Data.xlsx", "ISO639-1codes", k, j, str2);
				k++;
			}
		}
	}*/
}
