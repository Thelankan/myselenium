package com.drivemedical.utilgeneric;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeGroups;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import org.testng.xml.XmlTest;

import com.drivemedical.projectspec.BackOffice;
import com.drivemedical.projectspec.CartFunctions;
import com.drivemedical.projectspec.CheckoutFunctions;
import com.drivemedical.projectspec.GlobalFunctions;
import com.drivemedical.projectspec.ProfileFunctions;
import com.drivemedical.projectspec.ShopnavFunctions;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

public class BaseTest{

	public static WebDriver driver;
	public static Locators l1;
	public static String c;
	public static ExtentReports extentReportIE;
	public static ExtentReports extentReportFF;
	public static ExtentReports extentReportAndroidPhone;
	public static ExtentReports extentReportAndroidTab;
	public static ExtentReports extentReportiPhone;
	public static ExtentReports extentReportiPad;
	public static ExtentTest extentTestIE;
	public static ExtentTest extentTestFF;
	public static ExtentTest extentTestAndroidPhone;
	public static ExtentTest extentTestAndroidTab;
	public static ExtentTest extentTestiPhone;
	public static ExtentTest extentTestiPad;
	public static ExtentTest extentTestChildIE;
	public static ExtentTest extentTestChildFF;
	public static ExtentTest extentTestChildAndroidPhone;
	public static ExtentTest extentTestChildAndroidTab;
	public static ExtentTest extentTestChildiPhone;
	public static ExtentTest extentTestChildiPad;
	public static Logger log;
	public static SoftAssert sa;
	public static ProfileFunctions p;
	public static Actions act;
	public int i;
	public static ShopnavFunctions s;
	public static CartFunctions cart;
	public static GlobalFunctions gVar;
	public static BackOffice backOfc;
	public static String pName;
	public static CheckoutFunctions checkout;
	public static String logFileName=new Date().getDate()+" "+new Date().getMonth()+" "+new Date().getYear()+" "+new Date().getHours()+" "+new Date().getMinutes()+" "+new Date().getSeconds();
	public static String filePath="\\DriveMedical\\Results "+logFileName;
	File source;
	File dest;
	public static XmlTest xmlTest;
	static int counter;
	
	@BeforeSuite(groups={"reg","sanity_guest","sanity_reg"})
	public void cleanUp(XmlTest xmlTest)
	{
		BaseTest.xmlTest=xmlTest;
		//delete log directory
		try
		{
		FileUtils.deleteDirectory(new File(System.getProperty("user.dir")+"//log"));
		}
		catch(Exception e)
		{
			e.printStackTrace();
			System.out.println("Leave it");
		}
		
		//create log derectories
		new File(filePath).mkdirs();
		source = new File(System.getProperty("user.dir")+"\\log");
		dest = new File(filePath);
	}
	
	@BeforeClass(groups={"reg","sanity_guest","sanity_reg"})
	public void initialize(XmlTest xmlTest) throws Exception
	{
		counter++;
		if(xmlTest.getParameter("broName").equalsIgnoreCase("ie")) {
		//Report declaration code 
		extentReportIE = new ExtentReports("\\SelFrameWork\\MyReportIE.html", false);
		extentReportIE.loadConfig(new File(System.getProperty("user.dir")+"\\ExtentReports\\extent-config.xml"));
		extentTestIE=extentReportIE.startTest(this.getClass().getSimpleName());
		
		} else if(xmlTest.getParameter("broName").equalsIgnoreCase("firefox")) {
		extentReportFF = new ExtentReports("\\SelFrameWork\\MyReportFF.html", true);
		extentReportFF.loadConfig(new File(System.getProperty("user.dir")+"\\ExtentReports\\extent-config.xml"));
		extentTestFF=extentReportFF.startTest(this.getClass().getSimpleName());
		
		} else if(xmlTest.getParameter("broName").equalsIgnoreCase("Android Phone")) {
			extentReportAndroidPhone = new ExtentReports("\\SelFrameWork\\MyReportAP.html", false);
			extentReportAndroidPhone.loadConfig(new File(System.getProperty("user.dir")+"\\ExtentReports\\extent-config.xml"));
			extentTestAndroidPhone=extentReportAndroidPhone.startTest(this.getClass().getSimpleName());
			
		} else if(xmlTest.getParameter("broName").equalsIgnoreCase("Android Tab")) {
			extentReportAndroidTab = new ExtentReports("\\SelFrameWork\\MyReportAT.html", false);
			extentReportAndroidTab.loadConfig(new File(System.getProperty("user.dir")+"\\ExtentReports\\extent-config.xml"));
			extentTestAndroidTab=extentReportAndroidTab.startTest(this.getClass().getSimpleName());
			
			
		} else if(xmlTest.getParameter("broName").equalsIgnoreCase("iPhone")) {
			extentReportiPhone = new ExtentReports("\\SelFrameWork\\MyReportiPhone.html", false);
			extentReportiPhone.loadConfig(new File(System.getProperty("user.dir")+"\\ExtentReports\\extent-config.xml"));
			extentTestiPhone=extentReportiPhone.startTest(this.getClass().getSimpleName());
			
		} else if(xmlTest.getParameter("broName").equalsIgnoreCase("iPad")) {
			extentReportiPad = new ExtentReports("\\SelFrameWork\\MyReportiPad.html", false);
			extentReportiPad.loadConfig(new File(System.getProperty("user.dir")+"\\ExtentReports\\extent-config.xml"));
			extentTestiPad=extentReportiPad.startTest(this.getClass().getSimpleName());
		}
		
		System.out.println(driver);
		driver=new GetDriver().getDriver(xmlTest);
		gVar=new GlobalFunctions(driver);
		
		gVar.navigateToSite(xmlTest);
		Dimension screenSize = new Dimension(900, 900);
		driver.manage().window().setSize(screenSize);
		driver.manage().window().maximize();
		
		log=Logger.getLogger(this.getClass().getName());
		act=new Actions(driver);
	}
	
	@BeforeMethod(groups={"reg","sanity_guest","sanity_reg"})
	public void extReport(Method m,XmlTest xmlTest)
	{
		if(xmlTest.getParameter("broName").equalsIgnoreCase("ie")) {
			
			extentTestChildIE=extentReportIE.startTest(m.getName()+" "+driver.getClass().getSimpleName());
			extentTestIE.appendChild(extentTestChildIE);
			l1=new Locators(driver,extentTestChildIE, xmlTest);
			
		} else if(xmlTest.getParameter("broName").equalsIgnoreCase("firefox")) {
			
			extentTestChildFF=extentReportFF.startTest(m.getName()+" "+driver.getClass().getSimpleName());
			extentTestFF.appendChild(extentTestChildFF);
			l1=new Locators(driver,extentTestChildFF,xmlTest);
			
		} else if(xmlTest.getParameter("broName").equalsIgnoreCase("Android Phone")) {
			extentTestChildAndroidPhone=extentReportAndroidPhone.startTest(m.getName()+" "+driver.getClass().getSimpleName());
			extentTestAndroidPhone.appendChild(extentTestChildAndroidPhone);
			l1=new Locators(driver, extentTestChildAndroidPhone, xmlTest);
			
		} else if(xmlTest.getParameter("broName").equalsIgnoreCase("Android Tab")) {
			extentTestChildAndroidTab=extentReportAndroidTab.startTest(m.getName()+" "+driver.getClass().getSimpleName());
			extentTestAndroidTab.appendChild(extentTestChildAndroidTab);
			l1=new Locators(driver, extentTestChildAndroidTab, xmlTest);
			
		} else if(xmlTest.getParameter("broName").equalsIgnoreCase("iPhone")) {
			extentTestChildiPhone=extentReportiPhone.startTest(m.getName()+" "+driver.getClass().getSimpleName());
			extentTestiPhone.appendChild(extentTestChildiPhone);
			l1=new Locators(driver, extentTestChildiPhone, xmlTest);
			
		} else if(xmlTest.getParameter("broName").equalsIgnoreCase("iPad")) {
			extentTestChildiPad=extentReportiPad.startTest(m.getName()+" "+driver.getClass().getSimpleName());
			extentTestiPad.appendChild(extentTestChildiPad);
			l1=new Locators(driver, extentTestChildiPad, xmlTest);
			
		}
		sa=new SoftAssert();
		log.info("Test Case "+m.getName()+"Started in "+driver.getClass().getSimpleName()+"driver");
		log.info("-------------------------------------------");
		p=new ProfileFunctions();
		s=new ShopnavFunctions();
		cart = new CartFunctions();
		checkout=new CheckoutFunctions();
		backOfc=new BackOffice();
	}
	
	@AfterMethod(groups={"reg","sanity_guest","sanity_reg"})
	public void endTest(Method m,XmlTest xmlTest)
	{
		log.info("Test Case "+m.getName()+"ended in "+driver.getClass().getSimpleName()+"driver");
		log.info("-------------------------------------------");
/*		if(xmlTest.getParameter("broName").equalsIgnoreCase("ie")) {
			
			extentReportIE.endTest(extentTestIE);
			
		} else if(xmlTest.getParameter("broName").equalsIgnoreCase("firefox")) {
			
			extentReportFF.endTest(extentTestFF);
			
		} else if(xmlTest.getParameter("broName").equalsIgnoreCase("Android Phone")) {
			
			extentReportAndroidPhone.endTest(extentTestAndroidPhone);
		}*/
	}
	
	@AfterTest(groups={"reg","sanity_guest","sanity_reg"})
	public void endTest(XmlTest xmlTest)
	{
		if(xmlTest.getParameter("broName").equalsIgnoreCase("ie")) {
			//extentTestIE.appendChild(extentTestChildIE);
			extentReportIE.endTest(extentTestIE);
			extentReportIE.flush();
			
		} else if(xmlTest.getParameter("broName").equalsIgnoreCase("firefox")) {
			//extentTestFF.appendChild(extentTestChildFF);
			extentReportFF.endTest(extentTestFF);
			extentReportFF.flush();
			
		} else if(xmlTest.getParameter("broName").equalsIgnoreCase("Android Phone")) {
			//extentTestAndroidPhone.appendChild(extentTestChildAndroidPhone);
			extentReportAndroidPhone.endTest(extentTestAndroidPhone);
			extentReportAndroidPhone.flush();
			
		} else if(xmlTest.getParameter("broName").equalsIgnoreCase("Android Tab")) {
			//extentTestAndroidPhone.appendChild(extentTestChildAndroidPhone);
			extentReportAndroidTab.endTest(extentTestAndroidTab);
			extentReportAndroidTab.flush();
		} else if(xmlTest.getParameter("broName").equalsIgnoreCase("iPhone")) {
			//extentTestAndroidPhone.appendChild(extentTestChildAndroidPhone);
			extentReportiPhone.endTest(extentTestiPhone);
			extentReportiPhone.flush();
		} else if(xmlTest.getParameter("broName").equalsIgnoreCase("iPad")) {
			//extentTestAndroidPhone.appendChild(extentTestChildAndroidPhone);
			extentReportiPad.endTest(extentTestiPad);
			extentReportiPad.flush();
		}
	}
	
	@AfterSuite(groups={"reg","sanity_guest","sanity_reg"})
	public void tearDown(XmlTest xmlTest) throws IOException
	{
		//new GetDriver().closeDriver();
		
		try {
		    FileUtils.copyDirectory(source, dest);
		} catch (IOException e) {
		    e.printStackTrace();
		}
/*
		try {
			Process p=Runtime.getRuntime().exec("cmd /c start "+System.getProperty("user.dir")+"\\Sel_Report.bat");
			p.waitFor();
		} catch (IOException e) {
			System.out.println("catch block");
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}*/

	}
	
}
