package com.drivemedical.utilgeneric;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Properties;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.openqa.selenium.WebDriver;

public class SetData {

	static String data;
	static File f;
	static Sheet sh;
	static Row r;
	
	public static void writeDataToExcel(String filePath,String sheetName,int rowNum,int colNum,String val) throws Exception {
		
		f=new File(filePath);
		FileInputStream fis=new FileInputStream(f);
		Workbook wb=WorkbookFactory.create(fis);
		try{
			sh=wb.createSheet(sheetName);
		} catch (Exception e){
		sh=wb.getSheet(sheetName);
		}
		
		try {
			r=sh.getRow(rowNum);
			r.getCell(colNum, Row.CREATE_NULL_AS_BLANK);
			r.getCell(colNum).setCellValue(val);
		} catch (Exception e)
		{
			r=sh.createRow(rowNum);
			r=sh.getRow(rowNum);
			r.createCell(colNum).setCellValue(val);
		}
		
		FileOutputStream fos=new FileOutputStream(f);
		wb.write(fos);
		
	}
	
	public static void writeDataToProperties(String filePath,String propName,String propVal) throws Exception {
		
		f=new File(filePath);
		FileInputStream fis=new FileInputStream(f);
		Properties p=new Properties();
		p.load(fis);
		p.put(propName, propVal);
		
		FileOutputStream fos=new FileOutputStream(f);
		p.store(fos, "");
	}
/*	
	public static void writeDataToProperties(String filePath,WebDriver propName,WebDriver propVal) throws Exception {
		
		f=new File(filePath);
		FileInputStream fis=new FileInputStream(f);
		Properties p=new Properties();
		p.load(fis);
		p.put(propName, propVal);
		
		FileOutputStream fos=new FileOutputStream(f);
		p.store(fos, "");
	}*/
	
}
