package com.drivemedical.shopnav;

import static org.testng.Assert.assertTrue;

import org.testng.annotations.Test;

import com.drivemedical.utilgeneric.BaseTest;
import com.drivemedical.utilgeneric.GetData;

public class Footer extends BaseTest{
	
	@Test(groups={"reg"},description="DMED_039_1")
	public void social_Test_1() throws Exception
	{
		log.info("verify social icons in footer");
		sa.assertTrue(gVar.assertVisible("Footer_Social_Share", "Shopnav\\footer.properties"),"Verify social sharing links");

		log.info("verify FaceBook Icon");
		sa.assertTrue(gVar.assertVisible("Footer_FB_Icon", "Shopnav\\footer.properties"),"Verify the FB icon");

		log.info("verify Twitter Icon");
		sa.assertTrue(gVar.assertVisible("Footer_Twitter_Icon", "Shopnav\\footer.properties"),"verify the twiter icon");

		log.info("verify Instagram Icon");
		sa.assertTrue(gVar.assertVisible("Footer_Instagram_Icon", "Shopnav\\footer.properties"),"Verify the Instagram icon");
		
		log.info("verify Linked In Icon");
		sa.assertTrue(gVar.assertVisible("Footer_LinkedIn_Icon", "Shopnav\\footer.properties"),"Verify the Linked icon");

		sa.assertAll();
	}
	

}
