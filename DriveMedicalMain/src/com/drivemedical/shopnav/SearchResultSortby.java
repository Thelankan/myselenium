package com.drivemedical.shopnav;



import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import org.testng.xml.XmlTest;

import com.drivemedical.utilgeneric.BaseTest;
import com.drivemedical.utilgeneric.GetData;

public class SearchResultSortby extends BaseTest
{
	
	ArrayList<String> prodNamesString;
	ArrayList<String> prodNamesAfterSortBy;
	ArrayList<String> price;
	ArrayList<String> pricesAfterSort;
	
	@Test(groups={"reg"},description="DMED_103 DRM-863")
	public void  sortOptionDisplay_Reg(XmlTest xmlTest) throws Exception
	{
		
		log.info("Mouse hover on sign in link");
		act.moveToElement(l1.getWebElement("Header_SignIn_LINK", "shopnav\\header.properties")).perform();
	    log.info("click on signin link");
	    l1.getWebElement("Header_SignIn_LINK","shopnav\\header.properties").click();
		p.logIn(xmlTest);
		
		log.info("Navigate to search result page");
		s.searchProduct(GetData.getDataFromProperties("//POM//Shopnav//SearchResultPage.properties", "Search_Sort_KeyWrod"));
		log.info("Search result page heading");
		sa.assertTrue(l1.getWebElement("SearchResult_PageHeading", "Shopnav\\SearchResultPage.properties").isDisplayed());
	    log.info("Sort by dropdown");
		sa.assertTrue(l1.getWebElement("SearchResult_SortBy_Dropdown", "Shopnav\\SearchResultPage.properties").isDisplayed());
		log.info("clcik on sort by dropdown");
		l1.getWebElement("SearchResult_SortBy_Dropdown", "Shopnav\\SearchResultPage.properties").click();
		log.info("verify sort by options");
		sa.assertTrue(gVar.assertVisible("Seacrh_SortByRelevance_Link", "Shopnav\\SearchResultPage.properties"),"relevance");
		log.info("verify sort by option name ascending order");
		sa.assertTrue(gVar.assertVisible("Seacrh_SortByNameAsc_Link", "Shopnav\\SearchResultPage.properties"),"name ascending");
		log.info("verify sort by option name descending order");
		sa.assertTrue(gVar.assertVisible("Seacrh_SortByNameDsc_Link", "Shopnav\\SearchResultPage.properties"),"name descending");
		log.info("verify sort by option price low to high");
		sa.assertTrue(gVar.assertVisible("Seacrh_SortByPriceLowFirst_Link", "Shopnav\\SearchResultPage.properties"),"price low to high");
		log.info("verify sort by option high to low");
		sa.assertTrue(gVar.assertVisible("Seacrh_SortByPriceHigestFirst_Link", "Shopnav\\SearchResultPage.properties"),"price high to low");
		sa.assertAll();
		
	}
	
	@Test(groups={"reg"},description="DMED_103 DRM-866")
	public void  sortByNameAsc() throws Exception
	{
		
		log.info("Navigate to search result page");
		s.searchProduct(GetData.getDataFromProperties("//POM//Shopnav//SearchResultPage.properties", "Search_Sort_KeyWrod"));
		
		log.info("fetch product names before select sort by name ascending");
		List<WebElement> prodNames= l1.getWebElements("Search_ProductNames", "Shopnav\\SearchResultPage.properties");
		for(WebElement ele:prodNames) {
			prodNamesString.add(ele.getText());
		}
		log.info("sort in ascending order");
		Collections.sort(prodNamesString);
		
		log.info("clcik on sort by dropdown");
		l1.getWebElement("SearchResult_SortBy_Dropdown", "Shopnav\\SearchResultPage.properties").click();
		log.info("click on sort by ascending link");
		l1.getWebElement("Seacrh_SortByNameAsc_Link", "Shopnav\\SearchResultPage.properties").click();
		
		Thread.sleep(3000);
		log.info("After selecting on sort by ascending fetch names");
		List<WebElement> prodNamesAsc= l1.getWebElements("Search_ProductNames", "Shopnav\\SearchResultPage.properties");
		
		for(WebElement ele:prodNamesAsc) {
			prodNamesAfterSortBy.add(ele.getText());
		}
		
		sa.assertEquals(prodNamesString, prodNamesAfterSortBy);
		sa.assertAll();
		
	}
	
	@Test(groups={"reg"},description="DMED_103 DRM-867")
	public void  sortByNameDsc() throws Exception
	{
		
		log.info("Navigate to search result page");
		s.searchProduct(GetData.getDataFromProperties("//POM//Shopnav//SearchResultPage.properties", "Search_Sort_KeyWrod"));
		
		log.info("fetch product names before select sort by name descending");
		List<WebElement> prodNames= l1.getWebElements("Search_ProductNames", "Shopnav\\SearchResultPage.properties");
		
		for(WebElement ele:prodNames) {
			prodNamesString.add(ele.getText());
		}
		log.info("sort in ascending order");
		Collections.sort(prodNamesString);
		
		log.info("clcik on sort by dropdown");
		l1.getWebElement("SearchResult_SortBy_Dropdown", "Shopnav\\SearchResultPage.properties").click();
		log.info("click on sort by ascending link");
		l1.getWebElement("Seacrh_SortByNameDsc_Link", "Shopnav\\SearchResultPage.properties").click();
		Thread.sleep(3000);
		
		log.info("After selecting on sort by ascending fetch names");
		List<WebElement> prodNamesDsc= l1.getWebElements("Search_ProductNames", "Shopnav\\SearchResultPage.properties");
		
		for(WebElement ele:prodNamesDsc) {
			prodNamesAfterSortBy.add(ele.getText());
		}
		log.info("reverse the ascending list");
		Collections.reverse(prodNamesAfterSortBy);
		
		sa.assertEquals(prodNamesString, prodNamesAfterSortBy);
		sa.assertAll();
		
	}
	
	@Test(groups={"reg"},description="DMED_103 DRM-867")
	public void  sortByPriceAsc() throws Exception
	{
		log.info("Navigate to search result page");
		s.searchProduct(GetData.getDataFromProperties("//POM//Shopnav//SearchResultPage.properties", "Search_Sort_KeyWrod"));
		
		log.info("fetch product names before select sort by Price Ascending");
		List<WebElement> prices=l1.getWebElements("Search_Prices", "//Shopnav//SearchResultPage.properties");
		
		for(WebElement ele:prices) {
			price.add(ele.getText());	
		}
		
		log.info("sort in ascending order");
		Collections.sort(price);
		
		log.info("clcik on sort by dropdown");
		l1.getWebElement("SearchResult_SortBy_Dropdown", "Shopnav\\SearchResultPage.properties").click();
		log.info("click on sort by ascending link");
		l1.getWebElement("Seacrh_SortByPriceLowFirst_Link", "Shopnav\\SearchResultPage.properties").click();
		Thread.sleep(3000);
		
		log.info("fetch product names before select sort by Price Ascending");
		List<WebElement> pricesAsc=l1.getWebElements("Search_Prices", "//Shopnav//SearchResultPage.properties");
		
		for(WebElement ele:pricesAsc) {
			pricesAfterSort.add(ele.getText());
		}
		
		sa.assertEquals(price, pricesAfterSort);
		sa.assertAll();
		
	}
	
	@Test(groups={"reg"},description="DMED_103 DRM-867")
	public void  sortByPriceDsc() throws Exception
	{
		log.info("Navigate to search result page");
		s.searchProduct(GetData.getDataFromProperties("//POM//Shopnav//SearchResultPage.properties", "Search_Sort_KeyWrod"));
		
		log.info("fetch product names before select sort by Price Ascending");
		List<WebElement> prices=l1.getWebElements("Search_Prices", "//Shopnav//SearchResultPage.properties");
		
		for(WebElement ele:prices) {
			price.add(ele.getText());	
		}
		
		log.info("sort in ascending order");
		Collections.sort(price);
		
		log.info("clcik on sort by dropdown");
		l1.getWebElement("SearchResult_SortBy_Dropdown", "Shopnav\\SearchResultPage.properties").click();
		log.info("click on sort by ascending link");
		l1.getWebElement("Seacrh_SortByPriceHigestFirst_Link", "Shopnav\\SearchResultPage.properties").click();
		Thread.sleep(3000);
		
		log.info("fetch product names before select sort by Price Ascending");
		List<WebElement> pricesAsc=l1.getWebElements("Search_Prices", "//Shopnav//SearchResultPage.properties");
		
		for(WebElement ele:pricesAsc) {
			pricesAfterSort.add(ele.getText());
		}
		log.info("reverse the ascending list");
		Collections.reverse(pricesAfterSort);
		
		sa.assertEquals(price, pricesAfterSort);
		sa.assertAll();
		
	}

	@Test(groups={"reg"},description="DMED_103 DRM-864")	
	public void  sortOptionDisplay_Guest(XmlTest xmlTest) throws Exception
	{
		
	   log.info("Log out from application");
	   p.logout(xmlTest);
	   sa.assertTrue(l1.getWebElement("Header_SignIn_LINK", "shopnav\\header.properties").isDisplayed());
	   s.searchProduct("Wheel");
		log.info("Search result page heading");
		sa.assertTrue(l1.getWebElement("SearchResult_PageHeading", "Shopnav\\SearchResultPage.properties").isDisplayed());
	    log.info("Sort by dropdown");
		sa.assertTrue(l1.getWebElement("SearchResult_SortBy_Dropdown", "Shopnav\\SearchResultPage.properties").isDisplayed());
		log.info("clcik on sort by dropdown");
		l1.getWebElement("SearchResult_SortBy_Dropdown", "Shopnav\\SearchResultPage.properties").click();
		
		log.info("verify sort by options");
		sa.assertTrue(gVar.assertVisible("Seacrh_SortByRelevance_Link", "Shopnav\\SearchResultPage.properties"),"relevance");
		log.info("verify sort by option name ascending order");
		sa.assertTrue(gVar.assertVisible("Seacrh_SortByNameAsc_Link", "Shopnav\\SearchResultPage.properties"),"name ascending");
		log.info("verify sort by option name descending order");
		sa.assertTrue(gVar.assertVisible("Seacrh_SortByNameDsc_Link", "Shopnav\\SearchResultPage.properties"),"name descending");
		log.info("verify sort by option price low to high");
		sa.assertFalse(gVar.assertVisible("Seacrh_SortByPriceLowFirst_Link", "Shopnav\\SearchResultPage.properties"),"price low to high");
		log.info("verify sort by option high to low");
		sa.assertFalse(gVar.assertVisible("Seacrh_SortByPriceHigestFirst_Link", "Shopnav\\SearchResultPage.properties"),"price high to low");
		
		sa.assertAll();
		 
	}
	
}







