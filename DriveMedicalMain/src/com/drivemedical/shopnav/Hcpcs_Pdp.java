package com.drivemedical.shopnav;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.drivemedical.utilgeneric.BaseTest;
import com.drivemedical.utilgeneric.GetData;

public class Hcpcs_Pdp extends BaseTest
{
	String pdpProperties = "Shopnav//PDP.properties";
	
	@Test (groups={"reg"}, description="DMED-050 DRM-645")
	public void TC00_verifyHCPCsCodeForBaseProduct() throws Exception
	{
		log.info("Navigate to PDP for which is configured for HCPCs code");
		String productId = GetData.getDataFromExcel("//data//GenericData.xls", "Products", 1, 7);
		s.navigateToPDP_Search(productId);
		
		String exptHcpcsCode = GetData.getDataFromExcel("//data//GenericData.xls", "Products", 1, 8);
		System.out.println("exptHcpcsCode:- "+exptHcpcsCode);
		
//		log.info("Expand HCPCs drawer");
//		l1.getWebElement("PDP_HCPCS_Link", "Shopnav//PDP.properties").click();
		log.info("HCPCS drawer should be opened bydefault");
		sa.assertFalse(l1.getWebElement("PDP_HCPCS_Link", pdpProperties).getAttribute("class").toLowerCase().contains("collapsed"),"HCPCS tab should be expanded");
		
		log.info("Verify the HCpcs code in HCPCS drawer");
		sa.assertEquals(gVar.assertEqual("PDP_HCPCS_Drawer_CODE", "Shopnav//PDP.properties"), 
				exptHcpcsCode,"Verify HCPCs code in PDP");
		Thread.sleep(3000);
		log.info("Click on Product Options link");
		l1.getWebElement("PDP_ProductOptions_link", "Shopnav//PDP.properties").click();
		sa.assertTrue(gVar.assertVisible("PDP_Expanded_ProductOptions_link", "Shopnav//PDP.properties"),"verify the Expanded section");
		
		log.info("Count the HCPCs rows");
		 List<WebElement> hcpcsElements = l1.getWebElements("PDP_ProductOptionTable_HcpcsRows", "Shopnav//PDP.properties");
		 System.out.println("Number of HCPCs rows in table:- " + hcpcsElements.size());
		 
		 log.info("Verify the HCPCs code for all variants");
		 for (int i = 0; i < hcpcsElements.size(); i++) 
		 {
			System.out.println("LOOP:- " +i);
			String actHCPCsCode = hcpcsElements.get(i).getText();
			System.out.println("HCPCS code in product details table:- " + actHCPCsCode);
			gVar.assertequalsIgnoreCase(actHCPCsCode, exptHcpcsCode, "Verify the HCPCS code in product details table");
		 }
		 sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="DMED-050 DRM-646")
	public void TC01_hcpcsCodeNotConfiguredForBaseProduct() throws Exception
	{
		log.info("Navigate to DP for which HCPCs code is configured");
		String productId = GetData.getDataFromExcel("//data//GenericData.xls", "Products", 4, 7);
		s.navigateToPDP_Search(productId);
		
//		log.info("Expand HCPCs drawer");
//		l1.getWebElement("PDP_HCPCS_Link", "Shopnav//PDP.properties").click();
		log.info("HCPCS drawer should be opened bydefault");
		sa.assertFalse(l1.getWebElement("PDP_HCPCS_Link", pdpProperties).getAttribute("class").toLowerCase().contains("collapsed"),"HCPCS tab should be expanded");
		
		log.info("Hcpcs code should not be displayed in HCPCS drawer");
		sa.assertTrue(gVar.assertNotVisible("PDP_HCPCS_Drawer_CODE", "Shopnav//PDP.properties"), "HCPCS code should not be displayed drawer");
		
		sa.assertAll();
	}
	@Test(groups={"reg"}, description="DMED-050 DRM-647 DRM-648")
	public void TC02_hcpcsCodeForVariantProduct() throws Exception
	{
		log.info("Navigate to variant product PDP for which HCPCs code is configured");
		String productId = GetData.getDataFromExcel("//data//GenericData.xls", "Products", 2, 7);
		s.navigateToPDP_Search(productId);
		
		String exptHcpcsCode = GetData.getDataFromExcel("//data//GenericData.xls", "Products", 2, 8);
		System.out.println("exptHcpcsCode:- "+exptHcpcsCode);
		
//		log.info("Expand HCPCs drawer");
//		l1.getWebElement("PDP_HCPCS_Link", "Shopnav//PDP.properties").click();
		log.info("HCPCS drawer should be opened bydefault");
		sa.assertFalse(l1.getWebElement("PDP_HCPCS_Link", pdpProperties).getAttribute("class").toLowerCase().contains("collapsed"),"HCPCS tab should be expanded");
		
		log.info("For Base product PDP, HCPCS code should not be displayed");
		sa.assertTrue(gVar.assertNotVisible("PDP_HCPCS_Drawer_CODE", "Shopnav//PDP.properties"),"HCPCS code should not be displayed when user in Base product PDP");
		
		
		log.info("Collect the current PDP Url");
		String currentUrl= driver.getCurrentUrl();
		System.out.println("currentUrl:- " +currentUrl);
		
		log.info("Convert current url to expected url");
		String variantProductId = GetData.getDataFromExcel("//data//GenericData.xls", "Products", 2, 9);
		String exptUrl = currentUrl.replace(productId, variantProductId);
		System.out.println("exptUrl:-"+ exptUrl);
		log.info("Navigate to variant product pdp");
		driver.get(exptUrl);
		Thread.sleep(1000);
		
//		log.info("Expand HCPCs drawer");
//		l1.getWebElement("PDP_HCPCS_Link", "Shopnav//PDP.properties").click();
		log.info("HCPCS drawer should be opened bydefault");
		sa.assertFalse(l1.getWebElement("PDP_HCPCS_Link", pdpProperties).getAttribute("class").toLowerCase().contains("collapsed"),"HCPCS tab should be expanded");
		
		log.info("Verify the HCPCs code in HCPCS drawer when user in variant product PDP");
		sa.assertEquals(gVar.assertEqual("PDP_HCPCS_Drawer_CODE", "Shopnav//PDP.properties"), 
				exptHcpcsCode,"Verify HCPCs code in PDP");
		
		log.info("Click on Product Options link");
		l1.getWebElement("PDP_ProductOptions_link", "Shopnav//PDP.properties").click();
		sa.assertTrue(gVar.assertVisible("PDP_Expanded_ProductOptions_link", "Shopnav//PDP.properties"),"verify the Expanded section");
		
		log.info("Verify the Hcpcs code for varient product");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("PDP_ProductOptionTable_HcpcsRowOne", "Shopnav//PDP.properties"), 
				exptHcpcsCode,"Verify the HCPCs code in product details table for variant");
		
		//******************************
		log.info("Count the HCPCs rows");
		 List<WebElement> hcpcsElements = l1.getWebElements("PDP_ProductOptionTable_HcpcsRows", "Shopnav//PDP.properties");
		 System.out.println("Number of HCPCs rows in table:- " + hcpcsElements.size());
		 
		 log.info("Verify the HCPCs code for all variants");
		 int cnt = 0;
		 for (int i = 0; i < hcpcsElements.size(); i++) 
		 {
			System.out.println("LOOP:- " +i);
			String hcpcsValue = hcpcsElements.get(i).getText();
			System.out.println("hcpcsValue:- " + hcpcsValue);
			
			if (hcpcsValue!= null) 
			{
				gVar.assertequalsIgnoreCase(hcpcsValue, exptHcpcsCode, "Verify the HCPCS code in product details table");
				cnt++;
			}
		 }
		 
		 System.out.println("HCPCs Count"+cnt);
		 if (cnt==1) 
		 {
			sa.assertTrue(true, "Only one HCPCS code is displaying");
		 }
		 else 
		 {
			sa.assertTrue(false,"More than one HCPCs values are displaying in Product discription table");
		 }
		 
		 sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="DMED-050 DRM-657")
	public void TC03_verifySearchFunctionalityForHcpcsCode() throws Exception
	{
		log.info("Search the unique HCPCs code");
		String uniqueHcpcsCode = GetData.getDataFromExcel("//data//GenericData.xls", "Products", 2, 8);
		System.out.println("exptHcpcsCode:- "+uniqueHcpcsCode);
		s.searchProduct(uniqueHcpcsCode);
		
		log.info("User should navigate to PDP");
		sa.assertTrue(gVar.assertVisible("PDP_Element", "Shopnav//PDP.properties"),"Verify the PDP");
		
		String variantProductName = GetData.getDataFromExcel("//data//GenericData.xls", "Products", 2, 10);
		System.out.println("variantProductName:-" + variantProductName);
		gVar.assertequalsIgnoreCase(gVar.assertEqual("PDP_ProductName", "Shopnav//PDP.properties"), 
				variantProductName, "Verify the Product name in PDP");
	
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description ="DMED-050 DRM-650")
	public void TC04_searchHcpcsCodeWhichIsAssociatedFordifferentVariantOfSameProduct() throws Exception
	{
		log.info("Search HCPCs code which is configured for different variant of Same product");
		String uniqueHcpcsCode = GetData.getDataFromExcel("//data//GenericData.xls", "Products", 3, 8);
		System.out.println("exptHcpcsCode:- "+uniqueHcpcsCode);
		s.searchProduct(uniqueHcpcsCode);
		
		log.info("User should navigate to PDP");
		sa.assertTrue(gVar.assertVisible("PDP_Element", "Shopnav//PDP.properties"),"Verify the PDP");
		
		String variantProductName = GetData.getDataFromExcel("//data//GenericData.xls", "Products", 3, 10);
		System.out.println("variantProductName:-" + variantProductName);
		gVar.assertequalsIgnoreCase(gVar.assertEqual("PDP_ProductName", "Shopnav//PDP.properties"), 
				variantProductName, "Verify the Product name in PDP");
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="DMED-050 DRM-654")
	public void TC05_verifyTheNavigationOfPdpBySearchingHcpcsCode() throws Exception
	{
		log.info("Search a base product which is configured for HCPCs code");
		String uniqueHcpcsCode = GetData.getDataFromExcel("//data//GenericData.xls", "Products", 1, 8);
		System.out.println("exptHcpcsCode:- "+uniqueHcpcsCode);
		s.searchProduct(uniqueHcpcsCode);
		
		log.info("User should navigate to PDP");
		sa.assertTrue(gVar.assertVisible("PDP_Element", "Shopnav//PDP.properties"),"Verify the PDP");
		
		String variantProductName = GetData.getDataFromExcel("//data//GenericData.xls", "Products", 1, 10);
		System.out.println("variantProductName:-" + variantProductName);
		gVar.assertequalsIgnoreCase(gVar.assertEqual("PDP_ProductName", "Shopnav//PDP.properties"), 
				variantProductName, "Verify the Product name in PDP");
		
		sa.assertAll();
	}
	
	
	@Test(groups={"reg"}, description="DMED-050 DRM-649 DRM-651 DRM-652 DRM-653 DRM-655")
	public void TC06_verifySearchResultPageForHcpcsCode() throws Exception
	{
		log.info("Search HCPCs code which is assigned for two different product");
		String uniqueHcpcsCode = GetData.getDataFromExcel("//data//GenericData.xls", "Products", 5, 8);
		System.out.println("exptHcpcsCode:- "+uniqueHcpcsCode);
		s.searchProduct(uniqueHcpcsCode);
		
		log.info("Verify the search result page");
		sa.assertTrue(gVar.assertVisible("PLP_Grid_Div", "Shopnav//PDP.properties"),"Verify the searched products section");

		sa.assertTrue(gVar.assertVisible("Search_ProductsPerPage", "Shopnav//SearchResultPage.properties"),"Verify the products per page count section");
		sa.assertTrue(gVar.assertVisible("SearchResult_SortBy_Dropdown", "Shopnav//SearchResultPage.properties"),"Verify Sort by dropdown");
		
		log.info("Search heading");
		String exptHeading = "Search Results For: \""+ uniqueHcpcsCode +"\"";
		gVar.assertequalsIgnoreCase(gVar.assertEqual("SearchResult_PageHeading", "Shopnav//SearchResultPage.properties"), 
				exptHeading,"Verify the search heading");
		
		log.info("Verify the total product count");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("Search_Tot_Prods", "Shopnav//SearchResultPage.properties").replaceAll("[^-?0-9]+", ""), 
				"3", "Verify the searched product count in search result page");
		
		log.info("Verify the number of product in search result page");
		List<WebElement> numberOfProducts = l1.getWebElements("PLP_Total_Grid_Products", "Shopnav//PLP.properties");
		System.out.println("Number of products in search result page:-  "+ numberOfProducts.size());
		sa.assertEquals(numberOfProducts.size(), 3);
		
		sa.assertAll();
	}

	
}
