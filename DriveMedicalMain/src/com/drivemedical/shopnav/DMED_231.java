package com.drivemedical.shopnav;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.xml.XmlTest;

import com.drivemedical.utilgeneric.BaseTest;
import com.drivemedical.utilgeneric.GetData;

public class DMED_231 extends BaseTest
{
	String pdpProperties = "Shopnav//PDP.properties";
	String productIdInPDP;
	String uomInPDP;
	String qty;
	
	
	@Test(groups={"reg"}, description="DMED-231 DRM-1478")
	public void TC00_uiOfPdpGuestUser() throws Exception
	{
		log.info("Navigate to PDP");
//		String searchKeyWord = GetData.getDataFromExcel("//data//GenericData.xls", "Products", 1, 3);
//		s.navigateToPDP_Search(searchKeyWord);
		String searchKeyWord = GetData.getDataFromExcel("//data//GenericData.xls", "Products", 4, 3);
		s.pdpNavigationThroughURL(searchKeyWord);
		
		uiOfPDPage();
		log.info("Product price should not display for Guest user");
		sa.assertFalse(gVar.assertVisible("PDP_Price", "Shopnav//PDP.properties"),"Verify the product price");
		sa.assertTrue(gVar.assertNotVisible("PDP_MSRP_Value", "Shopnav//PDP.properties"),"MSRP value should not display");

		log.info("Verify the SIGN IN TO VIEW PRICE button");
		sa.assertEquals(l1.getWebElement("PDP_SignInTo_View_Price", "Shopnav//PDP.properties").getText(), "SIGN IN TO VIEW PRICE","Verify The signin text");
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="DMED-231 DRM-1497")
	public void TC01_verifyTheProductVariant_GuestUser() throws Exception
	{
		
		variantOptions("guest");
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="DMED-231 DRM-1480")
	public void TC02_pdpHCPCSDrawer_GuestUser() throws Exception
	{
		pdpHCPCSDrawer_State();
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="DMED-231 DRM-1483")
	public void TC03_HCPCSCode_GuestUSer()
	{
		log.info("Verify the HCPCS code in HCPCS drawer");
		sa.assertTrue(gVar.assertVisible("PDP_HCPCS_Drawer_CODE", pdpProperties), "Verify the HCPCS code");
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="DMED-231 DRM-1487")
	public void TC04_pdpHCPCSCodeForVariantProduct_GuestUser() throws Exception
	{
		log.info("Select swatch");
		s.selectSwatch();
		log.info("Verify the HCPCS code in HCPCS drawer");
		sa.assertTrue(gVar.assertVisible("PDP_HCPCS_Drawer_CODE", pdpProperties), "Verify the HCPCS code");
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="DMED-231 DRM-1494")
	public void TC05_pdpSignInFunctionality() throws Exception
	{
		log.info("click on SignIn to View Price button");
		l1.getWebElement("PDP_SignInTo_View_Price", pdpProperties).click();
		
		log.info("Verify the SignIn page");
		sa.assertTrue(gVar.assertVisible("Login_Heading", "Profile//login.properties"),"Verify the SignIn heading");
		sa.assertTrue(gVar.assertVisible("Login_Form", "Profile//login.properties"),"Verify the signin page");
		
		sa.assertAll();
	}
	
	
	@Test(groups={"reg"}, description="DMED-231 DRM-1477")
	public void TC06_pdpContentMatrix() throws Exception
	{
		log.info("Login to the application");
		p.logIn(xmlTest);
		
		log.info("Navigate to PDP");
//		String searchKeyWord = GetData.getDataFromExcel("//data//GenericData.xls", "Products", 1, 3);
//		s.navigateToPDP_Search(searchKeyWord);
		String searchKeyWord = GetData.getDataFromExcel("//data//GenericData.xls", "Products", 4, 3);
		s.pdpNavigationThroughURL(searchKeyWord);
		
		log.info("Verify the PDP");
		sa.assertTrue(gVar.assertVisible("PDP_Element", pdpProperties),"Verify the PDP page");
		
		log.info("Collect the UOM and MAterial Number in PDP");
		String temp = l1.getWebElement("PDP_SKU", pdpProperties).getText();
		productIdInPDP = temp.substring(temp.indexOf("#"), temp.length()).trim();
		log.info("productIdInPDP:- "+ productIdInPDP);
		uomInPDP = l1.getWebElement("PDP_UOM", pdpProperties).getAttribute("title");
		log.info("uomInPDP:- "+ uomInPDP);
		
		log.info("Verify the details in PDP");
//		sa.assertTrue(false,"Update script based on Testlink");
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="DMED-231 DRM-1479 DRM-1488")
	public void TC07_uiOfPdpRegisteredUser() throws Exception
	{
		uiOfPDPage();
		log.info("For registered user Price and ADD TO CART button should display");
		sa.assertEquals(l1.getWebElement("PDP_AddToCart_button", "Shopnav//PDP.properties").getText(), "ADD TO CART", "Verify the Add To Cart button");
		sa.assertTrue(gVar.assertVisible("PDP_Price", "Shopnav//PDP.properties"),"Verify the product price");
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="DMED-231 DRM-1481")
	public void TC08_pdpHCPCSDrawer_RegisterUser() throws Exception
	{
		pdpHCPCSDrawer_State();
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="DMED-231 DRM-1482")
	public void TC09_HCPCSCode()
	{
		log.info("Verify the HCPCS code in HCPCS drawer");
		sa.assertTrue(gVar.assertVisible("PDP_HCPCS_Drawer_CODE", pdpProperties), "Verify the HCPCS code");
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="DMED-231 DRM-1498")
	public void TC10_verifyTheProductVariant() throws Exception
	{
		variantOptions("register");
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="DMED-231 DRM-1486")
	public void TC11_pdpHCPCSCodeForVariantProduct() throws Exception
	{
		log.info("Select swatch");
		s.selectSwatch();
		log.info("Verify the HCPCS code in HCPCS drawer");
		sa.assertTrue(gVar.assertVisible("PDP_HCPCS_Drawer_CODE", pdpProperties), "Verify the HCPCS code");
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="DMED-231 DRM-1476")
	public void TC12_pdpAddToCartNotification() throws Exception
	{
		log.info("Update the quantity");
		qty= "5";
		l1.getWebElement("PDP_Qty_Textbox", pdpProperties).clear();
		l1.getWebElement("PDP_Qty_Textbox", pdpProperties).sendKeys(qty);
		log.info("Add Product to CArt");
		l1.getWebElement("PDP_AddToCart_button", pdpProperties).click();
		
		log.info("Verify the minicart popup");
		if (BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("mobile") || BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("Tablet")) 
		{
			sa.assertTrue(gVar.assertNotVisible("MiniCart_Popup", "Shopnav//Minicart.properties"),"Minicart popup should not display");
		} 
		else 
		{
			sa.assertTrue(gVar.assertVisible("MiniCart_Popup", "Shopnav//Minicart.properties"),"Verify the mini cart popup");
		}
		sa.assertEquals(gVar.assertEqual("Header_Cart_Qty", "Shopnav//header.properties"), qty,"Verify the minicart qty in header");
		
		sa.assertAll();
	}
	
	@Test(groups="reg", description="DMED-231 DRM-1475")
	public void TC13_cartUomFunctionality(XmlTest xmlTest) throws Exception
	{
		
		log.info("Navigate to CART page");
		s.navigateToCartPage();
		
		log.info("Verify the cart page");
		sa.assertTrue(gVar.assertVisible("Cart_Heading", "Cart//Cart.properties"),"Verify the cart page");
		
		log.info("Verify the Product ID and Uom in cart page");
		String productIdInCart = gVar.assertEqual("Cart_Prod_ID", "Cart//Cart.properties");
		log.info("productIdInCart:- " + productIdInCart);
		
		sa.assertEquals(productIdInCart, productIdInPDP.replace("#", "").trim(),"Verify the product id in CArt page");
		sa.assertEquals(gVar.assertEqual("Cart_UOM", "Cart//Cart.properties","title"), uomInPDP, "Verify the UOM in cart page");
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="DMED-231 DRM-1490")
	public void TC14_verifyUpdatedQuantityInCartPAge()
	{
		log.info("Berify the updated quantity in cart page");
		sa.assertEquals(gVar.assertEqual("Cart_QtyBox", "Cart//Cart.properties", "value"), qty, "Verify the updated quantity in CART page");
	
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="DMED-231 DRM-1491")
	public void TC15_addToCartFunWithoutUpdatingQty() throws Exception
	{
		log.info("Clear cart");
		cart.clearCart();
		
		log.info("Navigate to PDP");
		String searchKeyWord = GetData.getDataFromExcel("//data//GenericData.xls", "Products", 1, 3);
		s.navigateToPDP_Search(searchKeyWord);
		String productNAmeInPDP = l1.getWebElement("PDP_ProductName", pdpProperties).getText();
		log.info("productNAmeInPDP:- "+ productNAmeInPDP);
		
		log.info("Product quantity should be 1");
		sa.assertEquals(gVar.assertEqual("PDP_Qty_Textbox", pdpProperties,"value"), "1", "verify the quantity in PDP");
		
		log.info("Click on ADD TO CART button");
		l1.getWebElement("PDP_AddToCart_button", pdpProperties).click();
		
		log.info("Navigate to CART page");
		if(xmlTest.getParameter("executingIn").equalsIgnoreCase("Tablet") || xmlTest.getParameter("executingIn").equalsIgnoreCase("mobile")) 
		{
			l1.getWebElement("Header_Mob_Cart_Link", "Shopnav//header.properties").click();
		}
		else
		{
			l1.getWebElement("Header_Cart_Link", "Shopnav//header.properties").click();
		}
		
		log.info("Verify the cart page");
		sa.assertTrue(gVar.assertVisible("Cart_Heading", "Cart//Cart.properties"),"Verify the cart page");
		
		log.info("Verify the product");
		sa.assertEquals(gVar.assertEqual("Cart_Prod_Name", "Cart//Cart.properties"), productNAmeInPDP, "verify the product name in cart page");
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="DMED-231 DRM-1495 DRM-1496")
	public void TC16_verifyUomInPDP() throws Exception
	{
		log.info("Navigate to PDP");
//		String searchKeyWord = GetData.getDataFromExcel("//data//GenericData.xls", "Products", 8, 1);
//		s.navigateToPDP_Search(searchKeyWord);
		String searchKeyWord = GetData.getDataFromExcel("//data//GenericData.xls", "Products", 4, 3);
		s.pdpNavigationThroughURL(searchKeyWord);
		
		String proPrice = l1.getWebElement("PDP_Price", pdpProperties).getText();
		log.info("proPrice:- "+ proPrice);
		
		log.info("Verify the UOM");
		sa.assertTrue(gVar.assertVisible("PDP_UOM_SelectBox", pdpProperties),"Verify the UOM dropdown box");
		WebElement uomDD = l1.getWebElement("PDP_UOM_SelectBox", pdpProperties);
		Select sel = new Select(uomDD);
		int numberOfOptions = sel.getOptions().size();
		log.info("numberOfOptions:- " + numberOfOptions);
		if (numberOfOptions<=1) 
		{
			sa.assertTrue(false,"Only on UOM option is present. Change the product which is having more UOM options");
		} 
		
		for (int i = 0; i < numberOfOptions; i++) 
		{
			log.info("LOOP:- "+ i);
			String uomOption = sel.getOptions().get(i).getText();
			log.info("uomOption:- "+ uomOption);
			sel.selectByIndex(i);
			Thread.sleep(3000);
			
			log.info("Verify the SELECTED option");
			sa.assertEquals(gVar.assertEqual("PDP_UOM", pdpProperties,"title"), uomOption,"Verify the selected UOM option");
			
			log.info("Price should be changed");
			if (i!=0)//No need to compare for first option 
			{
				sa.assertNotEquals(l1.getWebElement("PDP_Price", pdpProperties).getText(), proPrice,"Price should be changed");
			}
		}
		
		sa.assertAll();
	}
	
	
	@Test(groups={"reg"}, description="DMED-231 DRM-1493")
	public void TC17_quantityMoreThanInventory() throws Exception
	{
		log.info("Enter the quantity more than the inventory");
		String qyantity="9999";
		l1.getWebElement("PDP_Qty_Textbox", pdpProperties).clear();
		l1.getWebElement("PDP_Qty_Textbox", pdpProperties).sendKeys(qyantity);
		
		sa.assertTrue(false,"Ask Arjun, Update the script, HOW to check quantity is more than available");
		
		sa.assertAll();
		
	}
	

	@AfterClass(groups={"reg","sanity_guest","sanity_reg"})
	public void cleanUpApplication(XmlTest xmlTest) throws Exception
	{
		log.info("Start After Class");
		cart.clearCart();
		p.logout(xmlTest);
	}

//############################################################	
//############################################################	
	public void uiOfPDPage() throws Exception
	{
		log.info("Verify the PDP");
		sa.assertTrue(gVar.assertVisible("PDP_Element", "Shopnav//PDP.properties"),"PDP element");
		sa.assertTrue(gVar.assertVisible("PDP_ProductName", "Shopnav//PDP.properties"),"Product name");
		
		log.info("Verify the breadcrumb");
		sa.assertTrue(gVar.assertVisible("PDP_BreadcrumbElement", "Shopnav//PDP.properties"),"Breadcrumb");
		String ProNameInBreadCrumb = l1.getWebElement("PDP_ActiveElement_InBreadcrumb", "Shopnav//PDP.properties").getText();
		log.info("ProNameInBreadCrumb:-  " + ProNameInBreadCrumb);
		log.info("Verify the product name in breadcrumb");
		sa.assertTrue(l1.getWebElement("PDP_ProductName", "Shopnav//PDP.properties").getText().equalsIgnoreCase(ProNameInBreadCrumb),"Product name in breadcrumb");
		log.info("Verify the quantity section");
		sa.assertTrue(gVar.assertVisible("PDP_Qty_label", "Shopnav//PDP.properties"),"Quantity label");
		sa.assertTrue(gVar.assertVisible("PDP_Qty_Textbox", "Shopnav//PDP.properties"),"Quantity box");
		sa.assertEquals(gVar.assertEqual("PDP_Qty_Textbox", "Shopnav//PDP.properties", "value"), "1", "Verify the quantity");
		
		sa.assertTrue(gVar.assertVisible("PDP_StockStatus", "Shopnav//PDP.properties"),"Verify the Stock status");
		sa.assertTrue(gVar.assertVisible("PDP_StockStatusImage", "Shopnav//PDP.properties"),"Verify the Stock status Image");
		sa.assertTrue(gVar.assertVisible("PDP_SKU", "Shopnav//PDP.properties"),"Verify SKU");
		
		sa.assertTrue(gVar.assertVisible("PDP_ProductOptions_link", "Shopnav//PDP.properties"),"Verify the product options link");
		
		sa.assertEquals(l1.getWebElement("PDP_StockStatus", "Shopnav//PDP.properties").getText(), "IN STOCK"); 
		
		log.info("Verify the tabs in PDP");
		sa.assertTrue(gVar.assertVisible("PDP_tabs_ProductDetails", "Shopnav//PDP.properties"),"Verify the tabs");
		sa.assertEquals(l1.getWebElement("PDP_tabs_ACCESSORIES", "Shopnav//PDP.properties").getText(), "ACCESSORIES");
		sa.assertEquals(l1.getWebElement("PDP_tabs_PARTS", "Shopnav//PDP.properties").getText(), "PARTS");
		sa.assertTrue(l1.getWebElement("PDP_tabs_CustomerReviews", "Shopnav//PDP.properties").getText().equalsIgnoreCase("Customer Reviews"));
		
		sa.assertTrue(gVar.assertVisible("PDP_Ratings_Section", "Shopnav//PDP.properties"),"Verify the Rating section");
		
		List<WebElement> pdpDrawers = l1.getWebElements("PDP_Drawer_Section", "Shopnav//PDP.properties");
		log.info("PDP drawers count:-  " + pdpDrawers.size());
		for (int i = 0; i < pdpDrawers.size(); i++) 
		{
			log.info("LOOP:- "+ i);
			String actDrawerName = pdpDrawers.get(i).getText().trim();
			System.err.println("actDrawerName:- " + actDrawerName);
			log.info("Verify the drawer name in pdp");
			gVar.assertequalsIgnoreCase(actDrawerName, GetData.getDataFromExcel("//data//GenericData.xls", "PDP", i+1, 0), "PDP Drawer");
		}
		
		log.info("Verify the PDF link in PDP");
		sa.assertTrue(gVar.assertVisible("PDP_pdf_Link", "Shopnav//PDP.properties"),"Verify PDF link");
		log.info("Verify the product image in tabs section");
		sa.assertTrue(gVar.assertVisible("PDP_PdImageInTabs", "Shopnav//PDP.properties"),"Product image in tab section");
	}
	
	public void pdpHCPCSDrawer_State() throws Exception
	{
		log.info("HCPCS drawer should be opened bydefault");
		sa.assertFalse(l1.getWebElement("PDP_HCPCS_Link", pdpProperties).getAttribute("class").toLowerCase().contains("collapsed"),"HCPCS tab should be expanded");

		int totalTabs = l1.getWebElements("PDP_Drawer_Section", pdpProperties).size();
		int collapsedTabs = l1.getWebElements("PDP_Collapsed_Drawer", pdpProperties).size();
		log.info("totalTabs:- "+ totalTabs);
		log.info("collapsedTabs:- "+ collapsedTabs);
		sa.assertEquals(1, totalTabs-collapsedTabs, "Only one tab should be opened");
	}
	
	public void variantOptions(String user) throws Exception
	{
		int column;
		int expectedTabelHeadings;
		
		log.info("Verify the color variants");
		sa.assertTrue(gVar.assertVisible("PDP_ColorSwatchesList", pdpProperties),"Verify the color variants");
		sa.assertTrue(gVar.assertVisible("PDP_Size_Dropdown", pdpProperties),"Verify the SIZE dropdown box");
		
		if (user.equalsIgnoreCase("register")) //For Register User 
		{
			sa.assertTrue(gVar.assertVisible("PDP_UOM_SelectBox", pdpProperties),"Verify the UOM dropdown box");
			column=3;
			expectedTabelHeadings=8;
		}
		else 	//FOr Guest User
		{
			sa.assertFalse(gVar.assertVisible("PDP_UOM_SelectBox", pdpProperties),"For Guest user UOM should not display");
			column=4;
			expectedTabelHeadings=6;
		}
		
		
		log.info("Click on PRODUCT OPTIONS link");
		l1.getWebElement("PDP_ProductOptions_link", pdpProperties).click();
		
		log.info("Verify the Expanded section");
		sa.assertTrue(gVar.assertVisible("PDP_Expanded_ProductOptions_link", pdpProperties), "Verify the Expanded section");
		sa.assertTrue(gVar.assertVisible("PDP_ProductOptionTable", pdpProperties),"Verify the table");

		log.info("Collect the TAble headings");
		List<WebElement> productionOptionTableHeadings = l1.getWebElements("PDP_ProductOptionTable_Headings", pdpProperties);
		int tableHeadingCount= productionOptionTableHeadings.size();
		log.info("tableHeadingCount:- "+ tableHeadingCount);
		
		sa.assertEquals(tableHeadingCount, expectedTabelHeadings,"Verify the count of table heading");
		for (int i = 0; i < expectedTabelHeadings; i++) 
		{
			int row = i+1;
			String excelData = GetData.getDataFromExcel("//data//GenericData.xls", "PDP", row, column);
			
			String tableHeaderInApp = productionOptionTableHeadings.get(i).getText();
			log.info("tableHeaderInApp:-  "+ tableHeaderInApp);
			
			gVar.assertequalsIgnoreCase(tableHeaderInApp, excelData,"Verify the Table heading");
			
		}
		
	}
	
}
