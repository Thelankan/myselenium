package com.drivemedical.shopnav;

import static org.testng.Assert.assertTrue;

import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;
import org.testng.xml.XmlTest;

import com.drivemedical.utilgeneric.BaseTest;
import com.drivemedical.utilgeneric.GetData;
import org.openqa.selenium.JavascriptExecutor;
import com.gargoylesoftware.htmlunit.javascript.host.Window;

public class MiniCart extends BaseTest{
	
	@Test(groups={"reg"},description="OOTB-093,DRM-328")
	public void TC00_miniCart_Nav_2() throws Exception
	{
		log.info("click on mini cart link");
		l1.getWebElement("Header_Cart_Link_GuestUser", "Shopnav//header.properties").click();
		log.info("verify navigation to sign in page");
		sa.assertTrue(l1.getWebElement("Login_Heading", "Profile//login.properties").isDisplayed());
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="OOTB-093,DRM-322,324")
	public void TC01_miniCart_UI_1(XmlTest xmlTest) throws Exception
	{
		log.info("Login with valid credentials");
		p.logIn(xmlTest);
		Thread.sleep(5000);
		log.info("hover on mini cart");
		act.moveToElement(l1.getWebElement("Header_Cart_Link", "Shopnav//header.properties")).perform();
		log.info("verify UI of mini cart");
		String heading = GetData.getDataFromExcel("//data//GenericData.xls", "cart", 1, 0);
		gVar.assertequalsIgnoreCase(gVar.assertEqual("EmptyMiniCart_Msg", "Shopnav//Minicart.properties"),heading,"empty mincart heading");
	//	sa.assertTrue(gVar.assertVisible("EmptyMinicart_Close_Icon", "Shopnav//Minicart.properties"),"Minicart overlay close icon");
	//	log.info("Close the minicart overlay");
		//l1.getWebElement("EmptyMinicart_Close_Icon", "Shopnav//Minicart.properties").click();
//		log.info("Mini cart overlay should not display");
//		sa.assertFalse(gVar.assertVisible("EmptyMinicart_Close_Icon", "Shopnav//Minicart.properties"),"Minicart overlay should not display");
		
		sa.assertAll();
	}

	@Test(groups={"reg"},description="OOTB-093 DRM-325,326,327,490,477,480,481")
	public void TC02_miniCart_UI_With_Products_3(XmlTest xmlTest) throws Exception
	{
		//log.info("Login with valid credentials");
		//p.logIn(xmlTest);
		log.info("Add item to cart");
		String product = GetData.getDataFromExcel("//data//GenericData.xls", "Products", 1, 1);
		s.addItemToCart(product,1,1);
		Thread.sleep(5000);
		//((JavascriptExecutor)driver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
		act.moveToElement(l1.getWebElement("Header_Cart_Link_GuestUser", "Shopnav//header.properties")).perform();
		log.info("verify Mini Cart UI With products");
		sa.assertTrue(gVar.assertVisible("MiniCart_Heading", "Shopnav//Minicart.properties"),"MINI cart heading");
		
		log.info("product image");
		sa.assertTrue(gVar.assertVisible("MiniCart_Img", "Shopnav//Minicart.properties"),"Product image");
		log.info("product name");
		sa.assertTrue(gVar.assertVisible("MiniCart_Peoduct_Name", "Shopnav//Minicart.properties"),"minicart product name");
		String productName = s.prodnames.get(0);
		System.out.println("prod name:- "+productName);
		String proNameInMinicart = l1.getWebElement("MiniCart_Peoduct_Name", "Shopnav//Minicart.properties").getText();
		System.out.println("proNameInMinicart:- "+ proNameInMinicart);
		sa.assertTrue(productName.contains(proNameInMinicart), "verify the product name");
//		sa.assertEquals(s.prodnames.get(0), l1.getWebElement("MiniCart_Peoduct_Name", "Shopnav//Minicart.properties").getText());
		
		log.info("product price");
		sa.assertTrue(gVar.assertVisible("MiniCart_Price", "Shopnav//Minicart.properties"),"product price");
		String proPrice = l1.getWebElement("MiniCart_Price", "Shopnav//Minicart.properties").getText().split(":")[1].trim();
		System.out.println("proPrice:- "+ proPrice);
		sa.assertEquals(s.prodPrice.get(0),proPrice, "Verify the Product price");
		
		log.info("product qty");
		sa.assertTrue(gVar.assertVisible("MiniCart_Qty", "Shopnav//Minicart.properties"),"product quantity");
		String proQuantity = l1.getWebElement("MiniCart_Qty", "Shopnav//Minicart.properties").getText().split(":")[1].trim();
		System.out.println("proQuantity:- "+proQuantity);
		sa.assertEquals("1", ""+proQuantity,"Verify the Quantity");
		
		log.info("product product id");
		sa.assertTrue(gVar.assertVisible("MiniCart_ProductId", "Shopnav//Minicart.properties"),"Product ID");
		
		log.info("product Total");
		sa.assertTrue(gVar.assertVisible("MiniCart_Total_Text", "Shopnav//Minicart.properties"),"Total text");
		sa.assertTrue(gVar.assertVisible("MiniCart_Total", "Shopnav//Minicart.properties"),"Total");
		
		sa.assertEquals(s.prodPrice.get(0), l1.getWebElement("MiniCart_Total", "Shopnav//Minicart.properties").getText());
		
		log.info("checkout button");
		sa.assertTrue(gVar.assertVisible("MiniCart_Checkout_Btn", "Shopnav//Minicart.properties"),"Checkout button");
	
		log.info("view cart button");
		sa.assertTrue(gVar.assertVisible("MiniCart_ViewCart_Btn", "Shopnav//Minicart.properties"),"Cart button");
		
		log.info("banner image");
		sa.assertTrue(gVar.assertVisible("MiniCart_Banner_Img", "Shopnav//Minicart.properties"),"Minicart banner");
		
		sa.assertEquals(gVar.assertEqual("MiniCart_ItemCount", "Shopnav//Minicart.properties"),"1", "Verify the minicart count");
		
		sa.assertAll();
	}

	@Test(groups={"reg"},description="OOTB-095 DRM-490")
	public void TC03_miniCart_ViewCart_Btn_4() throws Exception
	{
		log.info("click on view cart button");
		l1.getWebElement("MiniCart_ViewCart_Btn", "Shopnav//Minicart.properties").click();
		log.info("verify navigation to cart page");
		sa.assertTrue(l1.getWebElement("Cart_Heading", "Cart//Cart.properties").isDisplayed());
		log.info("verify meterial number");
		sa.assertTrue(gVar.assertVisible("Cart_Prod_ID", "Cart//Cart.properties"),"meterial number");
		log.info("verify UOM");
		sa.assertTrue(gVar.assertVisible("Cart_UOM", "Cart//Cart.properties"),"UOM");
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="OOTB-093 DRM-475")
	public void TC04_miniCart_Checkout_Btn_5(XmlTest xmlTest) throws Exception
	{
		log.info("Hover on minicart link in header");
		act.moveToElement(l1.getWebElement("Header_Cart_Link", "Shopnav//header.properties")).perform();
		log.info("verify and click on check out button in minicart overlay");
		String checkoutBtn = GetData.getDataFromExcel("//data//GenericData.xls", "cart", 2, 0);
		gVar.assertequalsIgnoreCase(gVar.assertEqual("MiniCart_Checkout_Btn", "Shopnav//Minicart.properties"),checkoutBtn,"Verify the Proceed to CheckOut button");
		l1.getWebElement("MiniCart_Checkout_Btn", "Shopnav//Minicart.properties").click();
		log.info("verify navigation");
		sa.assertTrue(gVar.assertVisible("ShipTo_Continue_button", "Checkout//ShipTo.properties"),"veriyfing continue button in checkout first step");
		
		log.info("Clear the cart item");
		l1.getWebElement("Checkout_ReturnToCart_Link", "Checkout//Checkout.properties").click();
		cart.clearCart();
		sa.assertAll();
	}
	
}
