package com.drivemedical.shopnav;

import org.openqa.selenium.Keys;
import org.testng.annotations.Test;

import com.drivemedical.utilgeneric.BaseTest;
import com.drivemedical.utilgeneric.GetData;

public class NoSearch extends BaseTest{
	String NoSearchKeyWord;

	@Test(groups={"reg"},description="DMED-049 DRM-663")
	public void TC00_NoSearch_Navigation_Test_1() throws Exception
	{
		log.info("Enter invalid key word in search box and hit enter");
		NoSearchKeyWord=GetData.getDataFromExcel("GenericData.xls","NoSearch",1,0);
		l1.getWebElement("Header_Serach_Box", "Shopnav\\header.properties").sendKeys(NoSearchKeyWord);
		act.sendKeys(Keys.ENTER).perform();
		log.info("verify navigation to No Search results page");
		sa.assertTrue(gVar.assertVisible("NoSearch_Content_Heading", "Shopnav\\NoSearch.properties"),"Verify the noSearch page heading");
	}
	
	@Test(groups={"reg"},description="DMED-049-UIDRM-673,674,675")
	public void TC01_NoSearch_UI_Test_2() throws Exception
	{
		log.info("verify UI of no search results page");
		sa.assertTrue(gVar.assertVisible("NoSearch_Breadcrumb", "Shopnav\\NoSearch.properties"),"verify the breadcrumb");
		String expBreadCrumb=GetData.getDataFromExcel("GenericData.xls","NoSearch",1,1)+" "+NoSearchKeyWord;
		sa.assertEquals(gVar.assertEqual("NoSearch_Breadcrumb", "Shopnav\\NoSearch.properties"), 
				expBreadCrumb,"Verify the breadcrumb text");
		log.info("No Search text");
		String exptext=GetData.getDataFromExcel("GenericData.xls","NoSearch",1,2)+" "+NoSearchKeyWord;
		sa.assertTrue(gVar.assertVisible("NoSearch_Headline", "Shopnav\\NoSearch.properties"),"Verify the heading");
		
		sa.assertEquals(gVar.assertEqual("NoSearch_Headline", "Shopnav\\NoSearch.properties"), 
				expBreadCrumb,"Heading text");
		
		log.info("Continue Shopping button");
		sa.assertTrue(gVar.assertVisible("NoSearch_ContinueShop_Btn", "Shopnav\\NoSearch.properties"),"Verify CONTINUE SHIPPING method");
		log.info("Sorry we dont have any products heading");
		sa.assertTrue(gVar.assertVisible("NoSearch_Content_Heading", "Shopnav\\NoSearch.properties"),"Content heading");
		log.info("No Search content");
		sa.assertTrue(gVar.assertVisible("NoSearch_Suggestions", "Shopnav\\NoSearch.properties"),"No Search content");
		
	}
}
