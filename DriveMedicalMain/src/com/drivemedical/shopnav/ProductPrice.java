package com.drivemedical.shopnav;

import java.util.List;

import net.sourceforge.htmlunit.corejs.javascript.regexp.SubString;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import org.testng.xml.XmlTest;

import com.drivemedical.utilgeneric.BaseTest;
import com.drivemedical.utilgeneric.GetData;

public class ProductPrice extends BaseTest
{
	String contractPriceProduct;
	String expectedContractPriceProName;
	
//	 #### CONTRACT PRICE NEED TO CONFIGURE ##### 
	@Test(groups={"reg"}, description="DMED-107 DRM-280 DRM-282")
	public void TC00_contractPriceInPLP(XmlTest xmlTest) throws Exception
	{
		boolean booleanCondition = false;
		log.info("Navigate to login page");
		l1.getWebElement("Header_SignIn_LINK", "Shopnav//header.properties").click();
		
		log.info("Login with valid credential");
		p.logIn(xmlTest);
		
		log.info("Navigate to PLP where product is configured with contract price");
		contractPriceProduct = GetData.getDataFromExcel("//data//GenericData.xls", "Products", 4, 1);
		s.searchProduct(contractPriceProduct);
		
		log.info("Verify the Contract price in PLP");
		gVar.assertEqual(gVar.assertEqual("PLP_Contract_Price", "Shopnav//PLP.properties"), "C", "Verify the Contract price label");
		sa.assertTrue(gVar.assertVisible("PLP_Contract_Price", "Shopnav//PLP.properties"), "Verify the price tool tip");
		
		List<WebElement> productsInPLP = l1.getWebElements("PLP_Total_Grid_Products", "Shopnav//PLP.properties");
		List<WebElement> proNamesInPLP = l1.getWebElements("ProductNames", "Shopnav//PLP.properties");
		System.out.println("productsInPLP:- "+ productsInPLP);
		System.out.println("Number of products in PLP:- "+ productsInPLP.size());
		for (int i = 1; i <= productsInPLP.size(); i++) 
		{
			System.out.println("LOOP:-  " +i);
			String valString = l1.getWebElement("PLP_ContractPrice", "Shopnav//PLP.properties").toString().split(":")[2];
			System.out.println("valString:- "+ valString);
			
			String xpath = valString.replace("]]", "]");
			System.out.println("xpath:-" + xpath);
			
			try 
			{
				System.out.println("Try block Started");
				sa.assertTrue(productsInPLP.get(i).findElement(By.xpath(xpath)).isDisplayed(),"Verifying the CONTRACT price");
				booleanCondition = true;
				System.out.println("Try block Ended");
				break;
			} 
			catch (Exception e) 
			{
				System.err.println("Catch block executing");
			}
			 
		}	//End of FOR loop
		
		if (booleanCondition) 
		{
			log.info("Verify the product name in PLP");
			String proNameOfContactPrice = proNamesInPLP.get(i).getText();
			System.out.println("proNameOfContactPrice:- " + proNameOfContactPrice);
			expectedContractPriceProName = GetData.getDataFromExcel("//data//GenericData.xls", "Products", 4, 2);
			
			sa.assertTrue(proNameOfContactPrice.toLowerCase().contentEquals(expectedContractPriceProName.toLowerCase()),"Verify the product name in PLP");
			
//			gVar.assertequalsIgnoreCase(proNameOfContactPrice, expectedContractPriceProName, "Verify the product name in PLP");
			
			log.info("Navigate to PDP");
			proNamesInPLP.get(i).click();
			
			log.info("Verify the PDP");
			sa.assertTrue(gVar.assertVisible("PDP_Element", "Shopnav//PDP.properties"),"Verify the PDP");
			sa.assertTrue(gVar.assertVisible("PDP_ContractPrice", "Shopnav//PDP.properties"),"Verify the Contract price in PDP");
			gVar.assertequalsIgnoreCase(gVar.assertEqual("PDP_ProductName", "Shopnav//PDP.properties"),
					expectedContractPriceProName,"Verify the Product name in PDP"); 
		}
		
		sa.assertAll();
	}
	/*
	@Test(groups={"reg"}, description="DMED-107 DRM-286 DRM-288 DRM-289")
	public void TC01_contractPriceInCartPage() throws Exception
	{
		log.info("price in PDP");
		String proPrice = l1.getWebElement("PDP_Price", "Shopnav//PDP.properties").getText();
		System.out.println("proPrice:- "+ proPrice);
		
		log.info("Add product to cart page");
		l1.getWebElement("PDP_AddToCart_button", "Shopnav//PDP.properties").click();
		
		log.info("Navigate to CArt page");
		l1.getWebElement("Header_Cart_Link", "Shopnav//header.properties").click();
	
		log.info("Verify the Product in cart page");
		sa.assertEquals(gVar.assertEqual("Cart_Prod_Name", "Cart//Cart.properties"), 
				expectedContractPriceProName,"verify the product name in cart page");
		
		sa.assertEquals(gVar.assertEqual("Cart_ItemPrice", "Cart//Cart.properties"), proPrice, "Verify the product price in cart page");
		
		log.info("Navigate to Checkout page");
		l1.getWebElement("Cart_CheckoutButton", "Cart//Cart.properties").click();
		
		log.info("Verify the product price in checkout page");
		sa.assertEquals(gVar.assertEqual("OrderSummary_ItemPrice", "Checkout//Checkout.properties"), proPrice, "Verify the product price in Checkout page");
		
		sa.assertEquals(gVar.assertEqual("Checkout_SubTotalPrice", "Checkout//Checkout.properties"), proPrice,"Verify the subtotal price in cart page");
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="DMED-107 DRM-281")
	public void TC02_contractPriceForNotSpecifiedUser() throws Exception
	{
		log.info("Logout from the application");
		act.moveToElement(l1.getWebElement("Header_Reg_MyAct_Link", "Shopnav//header.properties")).perform();
		l1.getWebElement("Header_SignOut_Link", "Shopnav//header.properties").click();
		
		log.info("Login to the Application with different credentials");
		l1.getWebElement("Header_SignIn_LINK", "Shopnav//header.properties").click();
		
		log.info("Login to the application which is not assigned for contract price");
		String un = GetData.getDataFromExcel("//data//GenericData.xls", "Login", 4, 0);
		String pwd = GetData.getDataFromExcel("//data//GenericData.xls", "Login", 5, 0);
		l1.getWebElement("Login_Email_TextBox", "Profile//login.properties").sendKeys(un);
		l1.getWebElement("Login_Password_Box", "Profile//login.properties").sendKeys(pwd);
		l1.getWebElement("Login_Login_Btn", "Profile//login.properties").click();
		
		log.info("search a product");
		s.searchProduct(contractPriceProduct);
		
		log.info("Verify the Contract price in PLP");
		sa.assertTrue(gVar.assertNotVisible("PLP_Contract_Price", "Shopnav//PLP.properties"),"Contract price should not be displayed in PLP");
		sa.assertTrue(gVar.assertNotVisible("PLP_Contract_Price", "Shopnav//PLP.properties"), "Price tool tip should not display");
		
		List<WebElement> proNamesInPLP = l1.getWebElements("ProductNames", "Shopnav//PLP.properties");
		for (int i = 0; i < proNamesInPLP.size(); i++) 
		{
			System.out.println("LOOP:- " +i);
			String actPro = proNamesInPLP.get(i).getText().toLowerCase();
			String exptPro = expectedContractPriceProName.toLowerCase();
			System.out.println("actPro:- " + actPro);
			System.out.println("exptPro:- " + exptPro);
			
			if (actPro.contains(exptPro))
			{
				log.info("Navigate to PDP");
				proNamesInPLP.get(i).click();
				
				log.info("Verify the PDP");
				sa.assertTrue(gVar.assertVisible("PDP_Element", "Shopnav//PLP.properties"),"Verify the PDP");
				gVar.assertequalsIgnoreCase(gVar.assertEqual("PDP_ProductName", "Shopnav//PDP.properties"),
						expectedContractPriceProName,"Verify the Product name in PDP"); 
				
				log.info("Verify the Contract price in PDP");
				sa.assertTrue(gVar.assertNotVisible("PDP_ContractPrice", "Shopnav//PDP.properties"),"Contract price should not be displayed in PDP");
			
			}
			else 
			{
				sa.assertTrue(false,"expected product is not present in PLP");
			}
		}
		
		log.info("Logout from the application");
		p.logout();
		sa.assertAll();
		
	}
	
	//#### LEVEL PRICE NEED TO CONFIGURE ####
	@Test(groups={"reg"}, description="DMED-107 DRM-285 DRM-284 DRM-287 DRM-290")
	public void verifyLevelPrice(XmlTest xmlTest) throws Exception
	{
		log.info("Login to the application with user which is assigned to LEVEL price");
		p.logIn(xmlTest);
		
		log.info("Search a product which is associated with LEVEL PRICE");
		String levelPriceProd = GetData.getDataFromExcel("//data//GenericData.xls", "Products", 5, 1);
		s.searchProduct(levelPriceProd);
		
		String proPriceInPLP = l1.getWebElement("PLP_Price", "Shopnav//PLP.properties").getText();
		System.out.println("Product price in PLP:- " +proPriceInPLP);
		
		log.info("Navigate to PDP");
		l1.getWebElement("ProductNames", "Shopnav//PLP.properties").click();
		
		log.info("collect the LEVEL price");
		String proPriceInPDP = l1.getWebElement("PDP_Price", "Shopnav//PDP.properties").getText();
		System.out.println("Level price in PDP:- " + proPriceInPDP);
		
		log.info("Add product to CART");
		l1.getWebElement("PDP_AddToCart_button", "Shopnav//PLP.properties");
		
		log.info("Navigate to CART page");
		l1.getWebElement("Header_Cart_Link", "Shopnav//header.properties").click();
		log.info("collect the price in CART page");
		String proPriceInCart = l1.getWebElement("Cart_ItemPrice", "Cart//Cart.properties").getText();
		System.out.println("Product price in Cart:- " + proPriceInCart);
		sa.assertEquals(proPriceInPDP, proPriceInCart, "Verify the product price in PDP and Cart page");
		
		log.info("Navigate to CHECKOUT page");
		l1.getWebElement("Cart_CheckoutButton", "Cart//Cart.properties").click();
		log.info("Collect the price in checkout price");
		String proPriceInCheckout = l1.getWebElement("Checkout_SubTotalPrice", "Checkout//Checkout.properties").getText();
		System.out.println("Product price in Checkout page:- " + proPriceInCheckout);
		sa.assertEquals(proPriceInPDP, proPriceInCheckout, "Verify the product price in PDP and checkout page");
		
		log.info("Logout from the application");
		p.logout();
		
		//****************************************
		log.info("Login to the application with user which is NOT ASSOCIATED with LEVEL PRICE");
		String un = GetData.getDataFromExcel("//data//GenericData.xls", "Login", 4, 0);
		String pwd = GetData.getDataFromExcel("//data//GenericData.xls", "Login", 5, 0);
		l1.getWebElement("Login_Email_TextBox", "Profile//login.properties").sendKeys(un);
		l1.getWebElement("Login_Password_Box", "Profile//login.properties").sendKeys(pwd);
		l1.getWebElement("Login_Login_Btn", "Profile//login.properties").click();
		
		log.info("Search the same product");
		s.searchProduct(levelPriceProd);
		
		String proPriceInPLPNew = l1.getWebElement("PLP_Price", "Shopnav//PLP.properties").getText();
		System.out.println("Product price in PLP:- " +proPriceInPLPNew);
		sa.assertNotEquals(proPriceInPLPNew, proPriceInPLP, "Level/product price should be differen for both the user");
		
		log.info("Navigate to PDP");
		l1.getWebElement("ProductNames", "Shopnav//PLP.properties").click();
		
		log.info("collect the LEVEL price");
		String proPriceInPDPNew = l1.getWebElement("PDP_Price", "Shopnav//PDP.properties").getText();
		System.out.println("Level price in PDP:- " + proPriceInPDPNew);
		
		sa.assertNotEquals(proPriceInPDPNew, proPriceInPDP,"product price(level price) in PDP should be different for both users");
		
		log.info("Add product to CART");
		l1.getWebElement("PDP_AddToCart_button", "Shopnav//PLP.properties");
		
		log.info("Navigate to CART page");
		l1.getWebElement("Header_Cart_Link", "Shopnav//header.properties").click();
		log.info("collect the price in CART page");
		String proPriceInCartNew = l1.getWebElement("Cart_ItemPrice", "Cart//Cart.properties").getText();
		System.out.println("Product price in Cart:- " + proPriceInCartNew);
		sa.assertEquals(proPriceInPDPNew, proPriceInCartNew, "Verify the product price in PDP and cart page");
		
		sa.assertNotEquals(proPriceInCartNew, proPriceInCart,"Verify the price in cart page");
		
		log.info("Navigate to CHECKOUT page");
		l1.getWebElement("Cart_CheckoutButton", "Cart//Cart.properties").click();
		log.info("Collect the price in checkout price");
		String proPriceInCheckoutNew = l1.getWebElement("Checkout_SubTotalPrice", "Checkout//Checkout.properties").getText();
		System.out.println("Product price in Checkout page:- " + proPriceInCheckoutNew);
		sa.assertNotEquals(proPriceInCheckoutNew, proPriceInCheckout,"Verify the price in checkout page");
		sa.assertEquals(proPriceInPDPNew, proPriceInCheckoutNew, "Verify the product price in PDP and Checkout page");
		
		sa.assertAll();
	}
	*/
	
}
