package com.drivemedical.shopnav;

import org.testng.annotations.Test;
import org.testng.xml.XmlTest;

import com.drivemedical.projectspec.BackOffice;
import com.drivemedical.projectspec.GlobalFunctions;
import com.drivemedical.utilgeneric.BaseTest;
import com.drivemedical.utilgeneric.GetData;

public class PLPFeatureProds extends BaseTest{

	
	@Test
	public void tc00_setFeatureproduct(XmlTest xmlTest) throws Exception
	{
		backOfc.featuredProduct();
		log.info("navigate to PLP");
		driver.get(GetData.getDataFromProperties("\\data\\sitedata.properties","PLPFeatureURL"));
	}
	
	@Test
	public void tc01_verifyFeaturedPorudtct() throws Exception
	{
		log.info("verify first product");
		s.searchProduct("Bed Rails");
		sa.assertEquals(BackOffice.lastProdName,l1.getWebElements("ProductNames", "\\Shopnav\\PLP.properties").get(0).getText());
		sa.assertAll();
	}
	
	
}
