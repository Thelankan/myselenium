package com.drivemedical.shopnav;

import java.util.List;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import org.testng.xml.XmlTest;

import com.drivemedical.utilgeneric.BaseTest;
import com.drivemedical.utilgeneric.GetData;

public class pdpPom extends BaseTest
{
	String pdpProperties= "Shopnav//PDP.properties";
	
	@Test(groups={"reg"}, description="DMED-069 DRM-909")
	public void TC00_pomContentInPDP_ForProductNotAssociatedWithPom_Guest() throws Exception
	{
		pdpNotAssociatedWithPOM();
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="DMED-069 DRM-908")
	public void TC01_addPOMtoCart(XmlTest xmlTest) throws Exception
	{
		String productID = GetData.getDataFromExcel("//data//GenericData.xls", "Products", 9, 1);
		s.pdpNavigationThroughURL(productID);
		
		log.info("Verify the PDP");
		sa.assertTrue(gVar.assertVisible("PDP_Element", pdpProperties),"Verify the PDP");
		
		log.info("Click on SIGNIN TO VIEW price button");
		l1.getWebElement("PDP_SignInTo_View_Price", pdpProperties).click();
		
		log.info("Login to the application");
		p.logIn(xmlTest);
		
		log.info("Collect the Product name");
		String proNAmeInPdp = l1.getWebElement("PDP_ProductName", pdpProperties).getText();
		System.out.println("proNAmeInPdp:- "+ proNAmeInPdp);
		log.info("Collect the Qty in pdp");
		String productQty = l1.getWebElement("PDP_Qty_Textbox", pdpProperties).getAttribute("value");
		System.out.println("productQty:- "+ productQty);
		
		if (xmlTest.getParameter("broName").equalsIgnoreCase("ie"))
		{
			System.out.println("Executing in IE");
			Thread.sleep(4000);
		}
		
		log.info("Click on ADD TO CART button");
		l1.getWebElement("PDP_AddToCart_button", pdpProperties).click();
		Thread.sleep(3000);
		log.info("Verify the minicart qty in heading");
		String miniCArtQty = l1.getWebElement("Header_Cart_Qty", "Shopnav//header.properties").getText();
		System.out.println("mini cart qty in header:- "+miniCArtQty);
		
		log.info("Mini cart quantity should be doubled");
		int pdpQtyInInt = Integer.parseInt(productQty);
		int expectedQty = pdpQtyInInt+pdpQtyInInt;
		sa.assertEquals(expectedQty+"", miniCArtQty, "verify the minicart quantity in header");
		
		log.info("NAvigate to CArt page");
		s.navigateToCartPage();
		
		log.info("Verify the cart page");
		sa.assertTrue(gVar.assertVisible("Cart_Heading", "Cart//Cart.properties"),"Verify the cart page");
		
		log.info("verify the BOGO product");
		List<WebElement> productNamesInCart = l1.getWebElements("Cart_Prod_Name", "Cart//Cart.properties");
		sa.assertEquals(productNamesInCart.size(), expectedQty,"Verify the number of products in cart");
		for (int i = 0; i < productNamesInCart.size(); i++) 
		{
			System.out.println("LOOP:- "+ i);
			sa.assertEquals(productNamesInCart.get(i).getText(), proNAmeInPdp,"Verify the product name in cart page");
		}
		
		sa.assertTrue(gVar.assertVisible("Cart_Promo_Description", "Cart//Cart.properties"),"Verify the promo description");
		
		String bogoQty = l1.getWebElement("Cart_QtyBox_SecondRowItem", "Cart//Cart.properties").getAttribute("value");
		System.out.println("bogoQty:- "+ bogoQty);
		String bogoQtyStatus = l1.getWebElement("Cart_QtyBox_SecondRowItem", "Cart//Cart.properties").getAttribute("disabled");
		System.out.println("bogoQtyStatus:- "+ bogoQtyStatus);
		
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="DMED-069 DRM-911")
	public void TC02_removePomProductInCartPage() throws Exception
	{
		
		log.info("Only one remove button should be displayed");
		sa.assertEquals(l1.getWebElements("Cart_DotButtons", "Cart//Cart.properties").size(), 1,"Verify the number of remove icons");
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="DMED-069 DRM-914")
	public void TC03_updatePomProductQuantity() throws Exception
	{
		log.info("Update the product in CArt page");
		l1.getWebElement("Cart_QtyBox", "Cart//Cart.properties").clear();
		String qty = "4";
		l1.getWebElement("Cart_QtyBox", "Cart//Cart.properties").sendKeys(qty);
		l1.getWebElement("Cart_QtyBox", "Cart//Cart.properties").sendKeys(Keys.TAB);
		Thread.sleep(3000);
		
		log.info("Verify the updated quantity");
		sa.assertEquals(gVar.assertEqual("Cart_QtyBox", "Cart//Cart.properties", "value"), qty, "verify the updated quantity");
		sa.assertEquals(gVar.assertEqual("Cart_QtyBox_SecondRowItem", "Cart//Cart.properties","value"), "1", "verify the quantity of the BOGO product");
		
		sa.assertAll();
		
	}
	
	@Test(groups={"reg"}, description="DMED-069 DRM-913")
	public void TC04_removePomProduct() throws Exception
	{
		log.info("Click on Remove link in cart page");
		l1.getWebElement("Cart_DotButtons", "Cart//Cart.properties").click();
		
		log.info("Product should be deleted, Cart should get empty");
		sa.assertTrue(gVar.assertVisible("Cart_EmptyCart_Msg", "Cart//Cart.properties"),"Verify the empty cart");
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="DMED-069 DRM-910")
	public void TC05_pomContentInPDP_ForProductNotAssociatedWithPom_Reg() throws Exception
	{
		pdpNotAssociatedWithPOM();
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="DMED-069 Extra TC")
	public void TC06_updateQuantityOfProductInPDP_AddToCart(XmlTest xmlTest) throws Exception
	{
		log.info("Clear the cart");
		cart.clearCart();
		String productID = GetData.getDataFromExcel("//data//GenericData.xls", "Products", 9, 1);
		s.pdpNavigationThroughURL(productID);
		
		log.info("Update the quantity in PDP");
		String qtyInPDP ="3";
		l1.getWebElement("PDP_Qty_Textbox", pdpProperties).clear();
		l1.getWebElement("PDP_Qty_Textbox", pdpProperties).sendKeys(qtyInPDP);
		
		log.info("Click on ADD TO CART button");
		if (xmlTest.getParameter("broName").equalsIgnoreCase("ie"))
		{
			System.out.println("Executing in IE");
			Thread.sleep(4000);
		}
		l1.getWebElement("PDP_AddToCart_button", pdpProperties).click();
		
		log.info("Navigate to cart page");
		s.navigateToCartPage();
		
		log.info("verify the product quantity in cart");
		sa.assertEquals(gVar.assertEqual("Cart_QtyBox", "Cart//Cart.properties", "value"), 
				qtyInPDP, "Verify the product quantity in cart page");
		
		log.info("Verify the quantity of BOGO product");
		sa.assertEquals(gVar.assertEqual("Cart_QtyBox_SecondRowItem", "Cart//Cart.properties","value"), 
				"1", "verify the quantity of the BOGO product");
		sa.assertEquals(gVar.assertEqual("Cart_QtyBox_SecondRowItem", "Cart//Cart.properties","disabled"), 
				"disabled", "BOGO product quantity box should be disabled");
		//sa.assertTrue(l1.getWebElement("Cart_QtyBox_SecondRowItem", "Cart//Cart.properties").isEnabled(),"Verify the Disabled condition of BOGO product quantity box");
	
		
		cart.clearCart();
		sa.assertAll();
	}
	
	
//####################################################################################
	void pdpNotAssociatedWithPOM() throws Exception
	{
		log.info("Navigate to PDP, product which is not associated to POM");
		String searchKeyWord = GetData.getDataFromExcel("//data//GenericData.xls", "Products", 1, 1);
		s.navigateToPDP_Search(searchKeyWord);
		
		log.info("Verify the PDP");
		sa.assertTrue(gVar.assertVisible("PDP_Element", pdpProperties),"Verify the PDP");
		
		log.info("POM content should not display in PDP");
		sa.assertFalse(gVar.assertVisible("PDP_PomContent1", pdpProperties), "POM content1 should not display");
		sa.assertFalse(gVar.assertVisible("PDP_PomContent2", pdpProperties),"POM content2 should not display");
	}
}
