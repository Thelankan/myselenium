package com.drivemedical.shopnav;

import org.testng.annotations.Test;
import org.testng.xml.XmlTest;

import com.drivemedical.utilgeneric.BaseTest;
import com.drivemedical.utilgeneric.GetData;

public class ProductWithMultipleCat extends BaseTest {

	@Test(groups={"reg"},description="DMED-055 DRM-462,467")
	public void TC00_verifyProductDisplay() throws Exception
	{
		log.info("navigate to one category");
		driver.get(GetData.getDataFromProperties("//data//MultipleCategoryProduct.properties", "Category1_URL"));
		log.info("click on another category name from facets");
		l1.getWebElement("category1_Facet_Link", "//Shopnav//PLP.properties").click();
		log.info("verify product displayed or not");
		sa.assertEquals(l1.getWebElements("ProductNames", "//Shopnav//PLP.properties").get(0).getText(),GetData.getDataFromProperties("//data//MultipleCategoryProduct.properties", "ProductName"));
		log.info("navigate to another category");
		driver.get(GetData.getDataFromProperties("//data//MultipleCategoryProduct.properties", "Category2_URL"));
		log.info("click on another category name from facets");
		l1.getWebElement("category2_Facet_Link", "//Shopnav//PLP.properties").click();
		log.info("verify product displayed or not");
		sa.assertEquals(l1.getWebElements("ProductNames", "//Shopnav//PLP.properties").get(0).getText(),GetData.getDataFromProperties("//data//MultipleCategoryProduct.properties", "ProductName"));
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="DMED-055 DRM-465,466")
	public void TC01_verifyBreadCrumb() throws Exception
	{
		log.info("click on product name");
		l1.getWebElements("ProductNames", "//Shopnav//PLP.properties").get(0).click();
		log.info("verify bread crumb");
		sa.assertEquals(GetData.getDataFromProperties("//data//MultipleCategoryProduct.properties", "ExpBreadCrumb").toLowerCase(), l1.getWebElement("PDP_BreadcrumbElement", "//Shopnav//PDP.properties").getText().toLowerCase());
		log.info("navigate to another category");
		driver.get(GetData.getDataFromProperties("//data//MultipleCategoryProduct.properties", "Category1_URL"));
		log.info("click on another category name from facets");
		l1.getWebElement("category1_Facet_Link", "//Shopnav//PLP.properties").click();
		Thread.sleep(5000);
		log.info("click on product name");
		l1.getWebElements("ProductNames", "//Shopnav//PLP.properties").get(0).click();
		log.info("verify bread crumb");
		sa.assertEquals(GetData.getDataFromProperties("//data//MultipleCategoryProduct.properties", "ExpBreadCrumb").toLowerCase(), l1.getWebElement("PDP_BreadcrumbElement", "//Shopnav//PDP.properties").getText().toLowerCase());
		sa.assertAll();
	}
	
	/*@Test(groups={"reg"},description="DMED-055 DRM-463,468")
	public void TC_verifyProductDisplay_Reg(XmlTest xmlTest) throws Exception
	{
		log.info("log in to the application");
		l1.getWebElement("Header_SignIn_LINK", "Shopnav\\header.properties").click();
		log.info("Log in with valid credentials");
		p.logIn(xmlTest);
		Thread.sleep(3000);
		TC00_verifyProductDisplay();
	}*/
	
}
