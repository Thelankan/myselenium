package com.drivemedical.shopnav;

import org.testng.annotations.Test;

import com.drivemedical.utilgeneric.BaseTest;
import com.drivemedical.utilgeneric.GetData;

public class PDPRecommendation extends BaseTest
{
	
	String pdpProperties = "Shopnav//PDP.properties";
	@Test(groups={"reg"}, description="DMED-056 DRM-1590")
	public void navigationOfProductNameAndImage_InRecommendationSection() throws Exception
	{
		log.info("Navigate to PDP which is configured with Recommendation");
		String searchData = GetData.getDataFromExcel("//data//GenericData.xls", "Products", 11, 1);
		s.navigateToPDP_Search(searchData);
		
		log.info("Collect the product name in reccomendation section");
		String proNameInRec = l1.getWebElement("PDP_Recommendation_ProductName", pdpProperties).getText();
		log.info("proNameInRec:- "+ proNameInRec);
		
		log.info("Click on Product name in recommendation section");
		l1.getWebElement("PDP_Recommendation_ProductName", pdpProperties).click();
		
		log.info("Verify the PDP");
		sa.assertTrue(gVar.assertVisible("PDP_Element", pdpProperties),"Verify the PDP");
		log.info("Verify the Product name in PDP");
		sa.assertEquals(gVar.assertEqual("PDP_ProductName", pdpProperties), proNameInRec,"Verify the product name");
		
		log.info("NAvigate to same PDP which is configured with Recommendation");
		s.navigateToPDP_Search(searchData);
		
		log.info("Click on Product image");
		l1.getWebElement("PDP_Recommendation_ProductImage", pdpProperties).click();
		
		log.info("Verify the PDP");
		sa.assertTrue(gVar.assertVisible("PDP_Element", pdpProperties),"Verify the PDP");
		log.info("Verify the Product name in PDP");
		sa.assertEquals(gVar.assertEqual("PDP_ProductName", pdpProperties), proNameInRec,"Verify the product name");
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="DMED-056 DRM-1594")
	public void numberOfProducts_InRecommendaionSection() throws Exception
	{
		log.info("Navigate to PDP which is configured with Recommendation");
		String searchData = GetData.getDataFromExcel("//data//GenericData.xls", "Products", 11, 1);
		s.navigateToPDP_Search(searchData);
		
		log.info("Count the number of products in Recommendation section");
		int proCountsInRecommandationSection = l1.getWebElements("PDP_Recommendation_ProductsCount", pdpProperties).size();
		sa.assertEquals(5, proCountsInRecommandationSection,"Verify the products count in recommandation section");
		
		
		sa.assertAll();
	}
}
