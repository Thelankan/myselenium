package com.drivemedical.shopnav;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import org.testng.xml.XmlTest;

import com.drivemedical.utilgeneric.BaseTest;
import com.drivemedical.utilgeneric.GetData;

public class SearchResults extends BaseTest{

	static String searchKeyWord;
	String totProds;
	String facetName;
	
	String plpProperties = "Shopnav//PLP.properties";
	
	
	@Test(groups={"reg"},description="DMED-049 DRM-682")
	public void TC00_searchFunctionality() throws Exception
	{
		searchKeyWord=GetData.getDataFromProperties("//POM//Shopnav//SearchResultPage.properties", "SearchKeyWord");
		log.info("search for any product");
		
		s.searchProduct(searchKeyWord);
		log.info("verify display of search key word");
		sa.assertTrue(l1.getWebElement("Search_Heading", "Shopnav//SearchResultPage.properties").getText().contains(searchKeyWord),"Verify the search keyword");

//		log.info("verify search key word in search box in header");
//		sa.assertEquals(gVar.assertEqual("Header_Serach_Box", "Shopnav//header.properties"), searchKeyWord);
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="DMED-049 DRM-683,684")
	public void TC01_prodNameLink() throws Exception
	{
		log.info("fetch product name");
		pName=l1.getWebElements("Search_ProductNames", "Shopnav//SearchResultPage.properties").get(0).getText();
		
		log.info("click on product name link");
		l1.getWebElements("Search_Prod_Names", "Shopnav//SearchResultPage.properties").get(0).click();
		
		log.info("verify navigation to PDP");
		s.verifyNavToPDP(pName);
		
		log.info("navigate back to search results page");
		s.searchProduct(searchKeyWord);
		log.info("click on product image");
		l1.getWebElement("Search_Product_Img", "Shopnav//SearchResultPage.properties").click();
		log.info("verify navigation to PDP");
		s.verifyNavToPDP(pName);
		sa.assertAll();
	}
	
	
	@Test(groups={"reg"},description="DMED-049 DRM-686,687,688")
	public void TC02_facets() throws Exception
	{
		log.info("navigate back search results page");
		s.searchProduct(searchKeyWord);
		
		log.info("click on first facet");
		l1.getWebElements("Search_Facets", "Shopnav//SearchResultPage.properties").get(0).click();
		
		log.info("fetch total products in that facet");
		totProds=l1.getWebElements("Search_facetCount", "Shopnav//SearchResultPage.properties").get(0).getText().replace("(", "").replace(")", "");
		
		log.info("fetch facet name");
		facetName=l1.getWebElements("Search_Facets_Links", "Shopnav//SearchResultPage.properties").get(0).getText().toLowerCase();
		
		log.info("click on facet links");
		l1.getWebElements("Search_Facets_Links", "Shopnav//SearchResultPage.properties").get(0).click();
		Thread.sleep(3000);
		
		log.info("verify total number of products displayed");
		sa.assertEquals(l1.getWebElements("Search_Prod_Names", "Shopnav//SearchResultPage.properties").size()+"", totProds,"Verify the count");
		sa.assertTrue(facetName.contains(gVar.assertEqual("Search_Selected_Facet_List", "Shopnav//SearchResultPage.properties").toLowerCase()),"verify the name");
		
		log.info("remove facet");
		l1.getWebElement("Search_Facet_Remove_Link", "Shopnav//SearchResultPage.properties").click();
		
		log.info("verify select facet should be removed");
		sa.assertTrue(gVar.assertNotVisible("Search_Selected_Facet_List", "Shopnav//SearchResultPage.properties"),"selected facet");
		sa.assertTrue(gVar.assertNotVisible("Search_Facet_Remove_Link", "Shopnav//SearchResultPage.properties"),"selected facet remove link");
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="DMED-049 DRM-689,690,666")
	public void TC03_multipleFacets() throws Exception
	{
		String facetName = "";
		log.info("select multiple filetrs");
		
		for(int i=0;i<2;i++)
		{
		log.info("expand facet");
		l1.getWebElements("Search_Facets", "Shopnav//SearchResultPage.properties").get(i).click();
		String cnt = l1.getWebElements("Search_facetCount", "Shopnav//SearchResultPage.properties").get(i).getText();
		facetName=facetName+l1.getWebElements("Search_Facets_Links", "Shopnav//SearchResultPage.properties").get(i).getText().replace(cnt, "");
		log.info("select facet");
		l1.getWebElements("Search_Facets_Links", "Shopnav//SearchResultPage.properties").get(i).click();
		}
		
		log.info("verify both facets are selected or not"+facetName);
		gVar.assertequalsIgnoreCase(l1.getWebElement("Search_Selected_Facet_List", "Shopnav//SearchResultPage.properties").getText(), facetName,"Verify the name");
		log.info("verify remove links");
		for(int i=0;i<2;i++) {
			sa.assertTrue(gVar.assertVisible("Search_Facet_Remove_Link", "Shopnav//SearchResultPage.properties",i),i+"");
		}
			
		log.info("de select selected filters");
		for(int i=0;i<2;i++){
			log.info("expand facet");
			l1.getWebElements("Search_Facets", "Shopnav//SearchResultPage.properties").get(i).click();
			log.info("de select facet");
			l1.getWebElements("Search_Facets_Links", "Shopnav//SearchResultPage.properties").get(i).click();
		}
		
		sa.assertTrue(gVar.assertNotVisible("Search_Selected_Facet_List", "Shopnav//SearchResultPage.properties"),"selected facet");
		sa.assertTrue(gVar.assertNotVisible("Search_Facet_Remove_Link", "Shopnav//SearchResultPage.properties"),"selected facet remove link");

		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="DMED-049 DRM-692,694,696,697")
	public void TC04_SearchUI_Guest() throws Exception
	{
		verifyPlpUI("guest");
		sa.assertAll(); 
	}
	
	@Test(groups={"reg"},description="DMED-049 DRM-691,693,695")
	public void TC05_SearchUI_Reg(XmlTest xmlTest) throws Exception
	{
		verifyPlpUI("reg");
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="DMED-049 DRM-658")
	public void TC06_Search_Fun() throws Exception
	{
		log.info("search for single product");
		s.searchProduct(searchKeyWord);
		log.info("verify search results");
		sa.assertTrue(gVar.assertVisible("Search_Grid_Div","Shopnav//SearchResultPage.properties"),"search results");
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="DMED-049 DRM-659,662")
	public void TC07_PartialSearch_Fun() throws Exception
	{
		log.info("search with partial word");
		searchKeyWord=GetData.getDataFromProperties("//POM//Shopnav//SearchResultPage.properties", "Search_Partial_Word");
		s.searchProduct(searchKeyWord);
		
		log.info("verify search results");
		List<WebElement> prodNames=l1.getWebElements("Search_Prod_Names", "Shopnav//SearchResultPage.properties");
		for(int i=0;i<prodNames.size();i++)
		{
			sa.assertTrue(prodNames.get(i).getText().toLowerCase().contains(searchKeyWord.toLowerCase()),"product names "+i);
		}
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="DMED-049 DRM-660")
	public void TC08_CapitalSearch_Fun() throws Exception
	{
		log.info("search with partial word");
		searchKeyWord=GetData.getDataFromProperties("//POM//Shopnav//SearchResultPage.properties", "Search_capital_Word");
		s.searchProduct(searchKeyWord);
		
		log.info("verify search results");
		List<WebElement> prodNames=l1.getWebElements("Search_Prod_Names", "Shopnav//SearchResultPage.properties");
		for(int i=0;i<prodNames.size();i++)
		{
			sa.assertTrue(prodNames.get(i).getText().toLowerCase().contains(searchKeyWord.toLowerCase()),"product names"+i);
		}
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="DMED-049 DRM-662")
	public void TC09_CombinationCapandSmall_Fun() throws Exception
	{
		log.info("search with partial word");
		searchKeyWord=GetData.getDataFromProperties("//POM//Shopnav//SearchResultPage.properties", "Search_combination_capsmall");
		s.searchProduct(searchKeyWord);
		log.info("verify search results");
		List<WebElement> prodNames=l1.getWebElements("Search_Prod_Names", "Shopnav//SearchResultPage.properties");
		for(int i=0;i<prodNames.size();i++)
		{
			sa.assertTrue(prodNames.get(i).getText().toLowerCase().contains(searchKeyWord.toLowerCase()),"product names"+i);
		}
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="DMED-049 DRM-661")
	public void TC10_searchSmallLetters() throws Exception
	{
		log.info("Navigate to Home page");
		l1.getWebElement("Header_Logo", "Shopnav//header.properties").click();
		
		log.info("Enter search small letter search keyword");
		String searchKeyword = "airgo";
		searchKeyword_VerifyItInPlpProductName(searchKeyword);
		
		sa.assertAll();
	}
	
	
	@Test(groups={"reg"}, description="DMED-049 DRM-665")
	public void TC11_searchResultSearchModification() throws Exception
	{
		log.info("Select the facets");
		PLP plp = new PLP();	//Create the object of PLP
		int numOfIteration = 3;
		plp.selectFilters(numOfIteration);
		
		log.info("Verify whether facets are selected or not");
		if (BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("mobile") || BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("Tablet"))
		{
			l1.getWebElement("PLP_RefineDropdownBox", "Shopnav//PLP.properties").click();
		}
		sa.assertTrue(gVar.assertVisible("PLP_Selected_Facet_List", "Shopnav//PLP.properties"),"verify the selected facets");
		sa.assertEquals(l1.getWebElements("PLP_Facet_Remove_Link", "Shopnav//PLP.properties").size(), numOfIteration,"Verifying remove link counts");
		
		log.info("Search the product and verify the searched keyword in product name in PLP");
		String keyWord="flex";
		searchKeyword_VerifyItInPlpProductName(keyWord);

		
		log.info("facets should not display");
		if (BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("mobile") || BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("Tablet"))
		{
			l1.getWebElement("PLP_RefineDropdownBox", "Shopnav//PLP.properties").click();
		}
		sa.assertFalse(gVar.assertVisible("PLP_Selected_Facet_List", "Shopnav//PLP.properties"),"facets should not display");
		sa.assertFalse(gVar.assertVisible("PLP_Facet_Remove_Link", "Shopnav//PLP.properties"),"facets remove link  should not display");
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="DMED-049 DRM-685")
	public void TC12_searchResultPagePagination() throws Exception
	{
		String str1=l1.getWebElement("PLP_ItemPerPage_DD", "Shopnav//PLP.properties").getText().split(" ")[0];
		log.info(str1);
		int itemsDisplayedInPage=new Integer(str1);
		log.info(itemsDisplayedInPage);
		log.info("verify pagination");
		s.paginationAndItemPerPage(itemsDisplayedInPage);
		
		sa.assertAll();
	}
	
	
	@Test(groups={"reg"}, description="DMED-049 DRM-677")
	public void TC13_searchSuggestionOverlay() throws Exception
	{
		log.info("Navigate to Home page");
		l1.getWebElement("Header_Logo", "Shopnav//header.properties").click();
		
		log.info("Enter three letters");
		l1.getWebElement("Header_Serach_Box", "Shopnav//header.properties").sendKeys("air");
		
		log.info("Verify the search suggestion popup");
		String popUpStyle = gVar.assertEqual("Header_SearchSuggestion_Popup1", "Shopnav//header.properties", "style");
		System.out.println("popUpStyle:- "+ popUpStyle);
		sa.assertTrue(popUpStyle.contains("block"),"Verify the popup");
		sa.assertTrue(gVar.assertVisible("Header_SearchSuggestion_Popup", "Shopnav//header.properties"),"Verify suggestion popup");
		
		
	}
	
	@Test(groups={"reg"}, description="DMED-049 DRM-678 DRM-679 DRM-680")
	public void TC14_uiOfSearchSuggestionOverlay() throws Exception
	{
		
		List<WebElement> numberOfSearchSuggestionProductsName = l1.getWebElements("Header_SearchSuggestion_ItemsName", "Shopnav//header.properties");
		System.out.println("numberOfSearchSuggestionProductsName:- " + numberOfSearchSuggestionProductsName.size());
		
		sa.assertEquals(numberOfSearchSuggestionProductsName.size(), 4, "Verify the NUmber of search suggestion product name");
		log.info("verif the searched keyword in products name");
		for (int i = 0; i < numberOfSearchSuggestionProductsName.size(); i++) 
		{
			System.out.println("LOOP:- "+ i);
			String proName = numberOfSearchSuggestionProductsName.get(i).getText();
			System.out.println("proName:- " + proName);
			
			sa.assertTrue(proName.contains("air"),"Verify the searched keyword in product name");
			
		}
		
		log.info("Verify the Close icon");
		sa.assertTrue(gVar.assertVisible("Header_SearchSuggestion_Close_Link", "Shopnav//header.properties"),"Verify the Close icon");
		
		
		sa.assertFalse(true, "Update the Scripts based on the wireframe and application");
		
		
	}
	
//	##############################################################################
//	##############################################################################
	
	void searchKeyword_VerifyItInPlpProductName(String searchKeyword) throws Exception
	{
		l1.getWebElement("Header_Serach_Box", "Shopnav//header.properties").sendKeys(searchKeyword);
		l1.getWebElement("Header_Search_Icon", "Shopnav//header.properties").click();
		
		log.info("Verify the Search result page");
		sa.assertTrue(gVar.assertVisible("Search_Grid_Div", "Shopnav//SearchResultPage.properties"),"Verify the PLP");
		
		log.info("Verify the search heading");
		String expectedHeading = GetData.getDataFromExcel("//data//GenericData.xls", "PLP", 1, 0) +" " + "\""+searchKeyword+"\"";
		System.out.println("expectedHeading:- "+ expectedHeading);
		gVar.assertequalsIgnoreCase(gVar.assertEqual("PLP_Heading", plpProperties), expectedHeading,"Verify the search heading");
		
		List<WebElement> productsInPlp = l1.getWebElements("ProductNames", plpProperties);
		for (int i = 0; i < productsInPlp.size(); i++) 
		{
			String proNameInPlp = productsInPlp.get(i).getText().toLowerCase();
			sa.assertTrue(proNameInPlp.contains(searchKeyword),"Verify searchKeyword in productname in PLP");
			
		}
	}
	
	void verifyPlpUI(String user) throws Exception
	{
		log.info("verify search heading");
		sa.assertTrue(gVar.assertVisible("Search_Heading", "Shopnav//SearchResultPage.properties"),"Search heading");
		String expectedHeading = GetData.getDataFromExcel("//data//GenericData.xls", "PLP", 1, 0)+" "+ "\""+searchKeyWord+"\"";
		sa.assertEquals(gVar.assertEqual("Search_Heading", "Shopnav//SearchResultPage.properties"), 
				expectedHeading ,"search heading");
		
		log.info("verify bread crumb");
		sa.assertTrue(gVar.assertVisible("Search_BreadCrumb", "Shopnav//SearchResultPage.properties"),"Search bread crumb");
		sa.assertEquals(gVar.assertEqual("Breadcrumb_ActiveElement", "Generic//Generic.properties"), searchKeyWord, "Verify the active element in breadcrumb");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("BreadcrumbElement", "Generic//Generic.properties"), "Home"+ " "+ searchKeyWord, "Verify the full breadcrumb");
		log.info("verify sort by drop down");
		sa.assertTrue(gVar.assertVisible("Search_SortBY_DD", "Shopnav//SearchResultPage.properties"),"sort by");
		
		log.info("verify items per page drop down");
		sa.assertTrue(gVar.assertVisible("Search_ItemPerPage_DD", "Shopnav//SearchResultPage.properties"),"Items per page");
		
		log.info("verify category section");
		sa.assertTrue(gVar.assertVisible("Search_Leftnav_CatHeading", "Shopnav//SearchResultPage.properties"),"category section heading");
		sa.assertEquals(gVar.assertEqual("Search_Leftnav_CatHeading", "Shopnav//SearchResultPage.properties"), "CATEGORY", "verify category text");
		
		log.info("displayed categories");
		for(WebElement ele:l1.getWebElements("Search_Leftnav_CatNames","Shopnav//SearchResultPage.properties"))
		{
			sa.assertTrue(ele.isDisplayed());
		}
		log.info("Refine By heading");
		sa.assertTrue(gVar.assertVisible("Search_RefineBY_Heading", "Shopnav//SearchResultPage.properties"),"Refine By");
		log.info("verify display of refine section");
		for(WebElement ele:l1.getWebElements("Search_Facets","Shopnav//SearchResultPage.properties")){
			sa.assertTrue(ele.isDisplayed());
		}
		log.info("products text");
		sa.assertTrue(gVar.assertVisible("Search_Tot_Prods", "Shopnav//SearchResultPage.properties"),"Total Products text");
//		log.info("pagination");
//		sa.assertTrue(gVar.assertVisible("Search_Pagination", "Shopnav//SearchResultPage.properties"),"pagination");
		
		List<WebElement> totProds=l1.getWebElements("Search_Prod_Names", "Shopnav//SearchResultPage.properties");
		
		for(int i=0;i<totProds.size();i++) 
		{
			log.info("products image");
			sa.assertTrue(gVar.assertVisible("Search_Product_Img", "Shopnav//SearchResultPage.properties",i),"product image");
			log.info("products name");
			sa.assertTrue(gVar.assertVisible("Search_Prod_Names", "Shopnav//SearchResultPage.properties",i),"product name");
//			log.info("models text");
//			sa.assertTrue(gVar.assertVisible("Search_Model_Text", "Shopnav//SearchResultPage.properties",i),"product model text");
			if (user.equalsIgnoreCase("reg"))
			{
				log.info("product price");
				sa.assertTrue(gVar.assertVisible("Search_Price", "Shopnav//SearchResultPage.properties",i),"product price");
			}
		}
		log.info("product price should not display");
		sa.assertTrue(gVar.assertNotVisible("Search_Price", "Shopnav//SearchResultPage.properties"),"product price");
	}
}
