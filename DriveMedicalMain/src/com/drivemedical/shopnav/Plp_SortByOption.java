package com.drivemedical.shopnav;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;
import org.testng.xml.XmlTest;

import com.drivemedical.utilgeneric.BaseTest;
import com.drivemedical.utilgeneric.GetData;

public class Plp_SortByOption extends BaseTest
{
	String plpProperties = "Shopnav//PLP.properties";
	
	@Test(groups={"reg"},description="OOB-103 for 1 &2")
	public void TC00_sortByPriceOption_GuestANdRegisterUser() throws Exception
	{
		log.info("logout from the application");
		p.logout(xmlTest);
		Select sel;
		
		for (int k = 0; k < 2; k++) 
		{
			System.out.println("LOOP K:-"+k);
			if (k==1) 
			{
				log.info("Login to the application");
				p.navigateToLoginPage();
				p.logIn(xmlTest);
			}
			
			log.info("Navigate to PLP");
			String searchData = GetData.getDataFromExcel("//data//GenericData.xls", "Category", 1, 0);
			s.searchProduct(searchData);
			
			WebElement sortByDD = l1.getWebElement("PLP_SortBY_DD", plpProperties);
			sel = new Select(sortByDD);
			int selectOptions = sel.getOptions().size();
			System.out.println("selectOptions:- "+ selectOptions);
			
			if (k==0) 
			{
				//For Guest User
				sa.assertEquals(selectOptions, 4);
			} 
			else 
			{
				//For Registered USer
				sa.assertEquals(selectOptions, 6);
			}
			
			for (int i = 1; i < selectOptions; i++) 
			{
				sortByDD = l1.getWebElement("PLP_SortBY_DD", plpProperties);
				sel = new Select(sortByDD);
				sel.selectByIndex(i);
				Thread.sleep(2000);
				
				log.info("Verify the selected sortby option");
				String sortOptions = GetData.getDataFromExcel("//data//GenericData.xls", "Category", i, 1);
				gVar.assertequalsIgnoreCase(gVar.assertEqual("PLP_SortBY_DD_SelectedOptions", plpProperties), 
						sortOptions,"Verify the Sort by options");
				gVar.assertequalsIgnoreCase(gVar.assertEqual("PLP_SortBY_DD_SelectedOption1", plpProperties), 
						sortOptions, "Verify the Sort by options1");
				
			}
		}
		
		
	}

	@Test(groups={"reg"}, description="OOB-103 Sort by name ascending/descending order  3 & 4 ")
	public void TC01_sortByNameFunctionality() throws Exception
	{
		log.info("Select the sort by name options");
		WebElement sortByDD = l1.getWebElement("PLP_SortBY_DD", plpProperties);
		int rowNum = 2;		
		for (int i = 0; i < 2; i++)
		{
			rowNum=rowNum+i;
			log.info("LOOP:- " +i);
			Select sel = new Select(sortByDD);
			String sortOptions = GetData.getDataFromExcel("//data//GenericData.xls", "Category", rowNum, 1);
			log.info("Select the option:- " + sortOptions);
			sel.selectByVisibleText(sortOptions);
			
			Thread.sleep(4000);
			log.info("Collect the product name from PLP");
			List<WebElement> allNames = l1.getWebElements("ProductNames", plpProperties);
			
			ArrayList<String> prodoctNames = new ArrayList<String>();
			log.info("Number of products:- " + prodoctNames.size());
			for (int j = 0; j < allNames.size(); j++)
			{
				prodoctNames.add(allNames.get(j).getText());
			}
			
			log.info("collected product names:- " + prodoctNames);
			ArrayList<String> copyOfProdoctNames = (ArrayList<String>) prodoctNames.clone();
			
			log.info("Sort the copyOfProdoctNames");
			if (i==0)
			{
				//For the product name in ascending order
				Collections.sort(copyOfProdoctNames);
			}
			else
			{
				//For the products name in descending order
				Collections.reverse(copyOfProdoctNames);
			}
			log.info("Product NAmes after sort:- "+ copyOfProdoctNames);
			
			log.info("Compair the names");
			sa.assertEquals(prodoctNames, copyOfProdoctNames, "Compair the product names list");
		}
		
		
	}
	
	
// #***** CANT AUTOMATE BECAUSE PRICE-RANGE is displaying in PLP *****# 	
//				@Test(groups={"reg"}, description="OOB-103 sort by price 5 & 6")
//				public void sortByPriceFunctionality() throws Exception
//				{
//					log.info("Select the sort by Price options");
//					WebElement sortByDD = l1.getWebElement("PLP_SortBY_DD", plpProperties);
//					int rowNum = 4;		
//					for (int i = 0; i < 2; i++)
//					{
//						rowNum=rowNum+i;
//						log.info("LOOP:- " +i);
//						Select sel = new Select(sortByDD);
//						String sortOptions = GetData.getDataFromExcel("//data//GenericData.xls", "Category", rowNum, 1);
//						log.info("Select the option:- " + sortOptions);
//						sel.selectByVisibleText(sortOptions);
//						
//						Thread.sleep(4000);
//						log.info("Collect the product Price from PLP");
//						List<WebElement> allPrice = l1.getWebElements("PLP_Price", plpProperties);
//						
//						ArrayList<String> prodoctPrice = new ArrayList<String>();
//						log.info("Number of products:- " + prodoctPrice.size());
//						for (int j = 0; j < allPrice.size(); j++)
//						{
//							prodoctPrice.add(allPrice.get(j).getText());
//						}
//						
//						log.info("collected product prices:- " + prodoctPrice);
//						ArrayList<String> copyOfProdoctPrices = (ArrayList<String>) prodoctPrice.clone();
//						
//						log.info("Sort the copyOfProdoctPrices");
//						if (i==0)
//						{
//							//For the product Price in High to Low order
//							Collections.sort(copyOfProdoctPrices);
//						}
//						else
//						{
//							//For the products name in Low to High order
//							Collections.reverse(copyOfProdoctPrices);
//						}
//						log.info("Product Price after sort:- "+ copyOfProdoctPrices);
//						
//						log.info("Compair the Prices");
//						sa.assertEquals(prodoctPrice, copyOfProdoctPrices, "Compair the product prices list");
//					}
//					
//					sa.assertAll();
//				}
	

}
