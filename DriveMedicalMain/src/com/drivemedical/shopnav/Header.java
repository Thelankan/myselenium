package com.drivemedical.shopnav;

import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.BeforeGroups;
import org.testng.annotations.Test;
import org.testng.xml.XmlTest;

import com.drivemedical.utilgeneric.BaseTest;
import com.drivemedical.utilgeneric.GetData;

public class Header extends BaseTest {
	

	@Test(groups={"reg","sanity_guest"},description="DMED-525_1_2_3 and DMED_091_DRM-349 and DMED_024_1_2_3 and OOTB-093 DRM-316,318,320")
	public void TC00_heder_UI_Test(XmlTest xmlTest) throws Exception
	{
		if(xmlTest.getParameter("executingIn").equalsIgnoreCase("mobile") || xmlTest.getParameter("executingIn").equalsIgnoreCase("Tablet")) {
			log.info("Verifying UI of Header as Guest user in mobile");
			log.info("Verify Logo");
			sa.assertTrue(gVar.assertVisible("Header_Mob_Logo", "Shopnav\\header.properties"),"Header Logo Mobile");
			log.info("Verify Hamburger menu");
			sa.assertTrue(gVar.assertVisible("Header_Hamburger_Menu", "Shopnav\\header.properties"),"Hambureger menu");
			log.info("Verify Search");
			sa.assertTrue(gVar.assertVisible("Header_Mob_Search_Text", "Shopnav\\header.properties"),"Mobile Search icon");
			log.info("Verify Cart link");
			sa.assertTrue(gVar.assertVisible("Header_Mob_Cart_Link", "Shopnav\\header.properties"),"Cart Link");
			log.info("Verify Cart Qty");
			sa.assertTrue(gVar.assertVisible("Header_Cart_Qty_Mobile", "Shopnav\\header.properties"),"Cart Quanityt Mobile");
		}
		else {
		log.info("Verifying UI of Header as Guest user");
		sa.assertTrue(gVar.assertVisible("Header_SignIn_LINK", "Shopnav\\header.properties"),"Sign In Link");
		log.info("Verifying Language selector");
		sa.assertTrue(gVar.assertVisible("Header_Lang_Selector", "Shopnav\\header.properties"),"Language selector");
		log.info("Verifying store locator link");
		sa.assertTrue(gVar.assertVisible("Header_StroeLoc_Link", "Shopnav\\header.properties"),"Store Locator Link");
		log.info("Verifying chat link");
		sa.assertTrue(gVar.assertVisible("Header_Chat_Link", "Shopnav\\header.properties"),"Chat Link");
		log.info("Verifying Order status link");
		sa.assertTrue(gVar.assertVisible("Header_OrderStatus_Link", "Shopnav\\header.properties"),"Order Status Link");
		log.info("Verify Cart link");
		sa.assertTrue(gVar.assertVisible("Header_Cart_Link", "Shopnav\\header.properties"),"Cart Link");
		log.info("Verify cart quantity");
		sa.assertTrue(gVar.assertVisible("Header_Cart_Qty", "Shopnav\\header.properties"),"Cart Qty");
		log.info("Verify Logo");
		sa.assertTrue(gVar.assertVisible("Header_Logo", "Shopnav\\header.properties"),"Header Logo");
		log.info("Verify Search Box");
		sa.assertTrue(gVar.assertVisible("Header_Serach_Box", "Shopnav\\header.properties"),"Search Box");
		log.info("Verify Search Icon");
		sa.assertTrue(gVar.assertVisible("Header_Search_Icon", "Shopnav\\header.properties"),"Search icon");
		}
		sa.assertAll();
	}
	
	@Test(groups={"reg","sanity_reg"},description="OOTB_055_DRM-591")
	public void TC01_header_Signin_Test(XmlTest xmlTest) throws Exception
	{
		log.info("Log in to the application");
		p.navigateToLoginPage();
		log.info("verify navigation");
		sa.assertTrue(gVar.assertVisible("Login_Heading", "Profile\\login.properties"),"Login Heading");
		log.info("Log in with valid credentials");
		p.logIn(xmlTest);
		log.info("Verify navigation back to home page");
		sa.assertTrue(gVar.assertVisible("Home_Identifier", "Shopnav\\header.properties"),"Home page identifier");
		sa.assertAll();
	}
	
	@Test(groups={"reg","sanity_reg"},description="DMED-526_1_2_3 and OOTB-093 DRM-317,319,321 and DMED091 DRM-350")
	public void TC02_header_Reg_UI_Test(XmlTest xmlTest) throws Exception
	{
		
		if(xmlTest.getParameter("executingIn").equalsIgnoreCase("mobile") || xmlTest.getParameter("executingIn").equalsIgnoreCase("Tablet")) {
			log.info("Verify Logo");
			sa.assertTrue(gVar.assertVisible("Header_Mob_Logo", "Shopnav\\header.properties"),"Logo");
			log.info("Verify Hamburger menu");
			sa.assertTrue(gVar.assertVisible("Header_Hamburger_Menu", "Shopnav\\header.properties"),"Hambureger menu");
			log.info("Verify Search");
			sa.assertTrue(gVar.assertVisible("Header_Mob_Search_Text", "Shopnav\\header.properties"),"Mobile Search icon");
			log.info("Verify Cart link");
			sa.assertTrue(gVar.assertVisible("Header_Mob_Cart_Link", "Shopnav\\header.properties"),"Cart Link Mobile");
			log.info("Verify Cart Qty");
			sa.assertTrue(gVar.assertVisible("Header_Cart_Qty_Mobile", "Shopnav\\header.properties"),"Cart Quanityt Mobile");
		}
		else {
			
			log.info("Verifying Language selector");
			sa.assertTrue(gVar.assertVisible("Header_Lang_Selector", "Shopnav\\header.properties"),"Language Selector");
			log.info("Verifying store locator link");
			sa.assertTrue(gVar.assertVisible("Header_StroeLoc_Link", "Shopnav\\header.properties"),"Store Locator Link");
			log.info("Verifying chat link");
			sa.assertTrue(gVar.assertVisible("Header_Chat_Link", "Shopnav\\header.properties"),"Chat Link");
			log.info("Verifying Order status link");
			sa.assertTrue(gVar.assertVisible("Header_OrderStatus_Link", "Shopnav\\header.properties"),"Order Status Link");
			log.info("Verify Cart link");
			sa.assertTrue(gVar.assertVisible("Header_Cart_Link", "Shopnav\\header.properties"),"Cart Link");
			log.info("Verify cart quantity");
			sa.assertTrue(gVar.assertVisible("Header_Cart_Qty", "Shopnav\\header.properties"),"Cart Qty");
			log.info("Verify Logo");
			sa.assertTrue(gVar.assertVisible("Header_Logo", "Shopnav\\header.properties"),"Logo");
			log.info("Verify Search Box");
			sa.assertTrue(gVar.assertVisible("Header_Serach_Box", "Shopnav\\header.properties"),"Search box");
			log.info("Verify Search Icon");
			sa.assertTrue(gVar.assertVisible("Header_Search_Icon", "Shopnav\\header.properties"),"Sarch icon");
			log.info("Verify Quick Order Link");
			sa.assertTrue(gVar.assertVisible("Header_Reg_QuickOrder_Link", "Shopnav\\header.properties"),"Quick Order");
			log.info("Verify My Account Link");
			sa.assertTrue(gVar.assertVisible("Header_Reg_MyAct_Link", "Shopnav\\header.properties"),"My Account link");
			log.info("Verify sign out Link");
			sa.assertTrue(gVar.assertNotVisible("Header_UnExp_SignOut", "Shopnav\\header.properties"),"Sign Out link");
		}
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="DRM-487,488,489 DMED-530 DRM-292,293,294")
	public void TC03_header_Reg_UI_Cat(XmlTest xmlTest) throws Exception
	{
	log.info("click on category link");
	//l1.getWebElement("MegaMenu_Products_Cat", "Shopnav\\header.properties").click();
	driver.get("https://stage.medicaldepot.com:9002/drivestorefront/driveUS/en/USD/Products/Mobility/Canes/c/Canes");
	
	if(xmlTest.getParameter("executingIn").equalsIgnoreCase("mobile") || xmlTest.getParameter("executingIn").equalsIgnoreCase("Tablet")) {
		log.info("Verify Logo");
		sa.assertTrue(gVar.assertVisible("Header_Mob_Logo", "Shopnav\\header.properties"),"Logo");
		log.info("Verify Hamburger menu");
		sa.assertTrue(gVar.assertVisible("Header_Hamburger_Menu", "Shopnav\\header.properties"),"Hambureger menu");
		log.info("Verify Search");
		sa.assertTrue(gVar.assertVisible("Header_Mob_Search_Text", "Shopnav\\header.properties"),"Mobile Search icon");
		log.info("Verify Cart link");
		sa.assertTrue(gVar.assertVisible("Header_Mob_Cart_Link", "Shopnav\\header.properties"),"Cart Link Mobile");
		log.info("Verify Cart Qty");
		sa.assertTrue(gVar.assertVisible("Header_Cart_Qty_Mobile", "Shopnav\\header.properties"),"Cart Quanityt Mobile");
	}
	else {
		
		log.info("Verifying Language selector");
		sa.assertTrue(gVar.assertVisible("Header_Lang_Selector", "Shopnav\\header.properties"),"Language Selector");
		log.info("Verifying store locator link");
		sa.assertTrue(gVar.assertVisible("Header_StroeLoc_Link", "Shopnav\\header.properties"),"Store Locator Link");
		log.info("Verifying chat link");
		sa.assertTrue(gVar.assertVisible("Header_Chat_Link", "Shopnav\\header.properties"),"Chat Link");
		log.info("Verifying Order status link");
		sa.assertTrue(gVar.assertVisible("Header_OrderStatus_Link", "Shopnav\\header.properties"),"Order Status Link");
		log.info("Verify Cart link");
		sa.assertTrue(gVar.assertVisible("Header_Cart_Link", "Shopnav\\header.properties"),"Cart Link");
		log.info("Verify cart quantity");
		sa.assertTrue(gVar.assertVisible("Header_Cart_Qty", "Shopnav\\header.properties"),"Cart Qty");
		log.info("Verify Logo");
		sa.assertTrue(gVar.assertVisible("Header_Logo", "Shopnav\\header.properties"),"Logo");
		log.info("Verify Search Box");
		sa.assertTrue(gVar.assertVisible("Header_Serach_Box", "Shopnav\\header.properties"),"Search box");
		log.info("Verify Search Icon");
		sa.assertTrue(gVar.assertVisible("Header_Search_Icon", "Shopnav\\header.properties"),"Sarch icon");
		log.info("Verify Quick Order Link");
		sa.assertTrue(gVar.assertVisible("Header_Reg_QuickOrder_Link", "Shopnav\\header.properties"),"Quick Order");
		log.info("Verify My Account Link");
		sa.assertTrue(gVar.assertVisible("Header_Reg_MyAct_Link", "Shopnav\\header.properties"),"My Account link");
		log.info("Verify sign out Link");
		sa.assertTrue(gVar.assertNotVisible("Header_UnExp_SignOut", "Shopnav\\header.properties"),"Sign Out link");
	}
	sa.assertAll();
	}
	
	@Test(groups={"reg"},description="OOTB_056 DRM-736")
	public void TC04_header_Signout_Test(XmlTest xmlTest) throws Exception
	{
		log.info("hover on my account link");
		if(xmlTest.getParameter("executingIn").equalsIgnoreCase("mobile") || xmlTest.getParameter("executingIn").equalsIgnoreCase("Tablet")) {
			log.info("click on hamburger menu");
			l1.getWebElement("Header_Hamburger_Menu", "Shopnav\\header.properties").click();
			log.info("click on sign out link");
			l1.getWebElement("Hamburger_SignOut_Link", "Shopnav\\header.properties").click();
			log.info("click on hamburger menu");
			l1.getWebElement("Header_Hamburger_Menu", "Shopnav\\header.properties").click();
			log.info("verify logged out or not");
			sa.assertTrue(l1.getWebElement("Hamburger_SignIn_Link", "Shopnav\\header.properties").isDisplayed(),"Sign In Link hamburger");
			
		} 
		else 
		{
		log.info("Logout from the application");
		p.logout(xmlTest);
		log.info("verify logged out or not");
		sa.assertTrue(l1.getWebElement("Header_SignIn_LINK", "Shopnav\\header.properties").isDisplayed(),"Sign In Link");
		}
		sa.assertAll();
	}
	
	@Test(priority=6,groups={"reg"},description="OOTB_055_DRM-592")
	public void header_Signin_CA_Test(XmlTest xmlTest) throws Exception
	{
		
//		log.info("select canada country");
//		l1.getWebElement("Header_Lang_Selector", "Shopnav\\header.properties").click();
//		l1.getWebElement("Header_CandaEng_Link", "Shopnav\\header.properties").click();
//		log.info("Log in to the application");
//		l1.getWebElement("Header_SignIn_LINK", "Shopnav\\header.properties").click();
//		log.info("verify navigation");
//		sa.assertTrue(l1.getWebElement("Login_Heading", "Profile\\login.properties").isDisplayed());
//		log.info("Log in with valid credentials");
//		p.logIn(xmlTest);
//		log.info("Verify navigation back to home page");
//		sa.assertTrue(l1.getWebElement("Home_Identifier", "Shopnav\\header.properties").isDisplayed());
	}
	
	@Test(groups={"reg"},description="OOTB_055_DRM-595")
	public void TC05_header_Signout_secure_Test(XmlTest xmlTest) throws Exception
	{
		log.info("Navigate to login page");
		p.navigateToLoginPage();
		log.info("Log in with valid credentials");
		p.logIn(xmlTest);
		
		log.info("navigate to my account page");
		if(xmlTest.getParameter("executingIn").equalsIgnoreCase("mobile") || xmlTest.getParameter("executingIn").equalsIgnoreCase("Tablet")) {
			log.info("click on hamburger menu");
			l1.getWebElement("Header_Hamburger_Menu", "Shopnav\\header.properties").click();
			log.info("click in contact details");
			l1.getWebElement("Hamburger_ContactDetails_Link", "Shopnav\\header.properties").click();
			log.info("click on hamburger menu");
			l1.getWebElement("Header_Hamburger_Menu", "Shopnav\\header.properties").click();
			log.info("click on sign out link");
			l1.getWebElement("Hamburger_SignOut_Link", "Shopnav\\header.properties").click();
			log.info("verify logged out or not and navigation to home page");
			log.info("click on hamburger menu");
			l1.getWebElement("Header_Hamburger_Menu", "Shopnav\\header.properties").click();
			sa.assertTrue(l1.getWebElement("Hamburger_SignIn_Link", "Shopnav\\header.properties").isDisplayed(),"Sign In Link hamburger");
			
		} else {
//			act.moveToElement(l1.getWebElement("Header_Reg_MyAct_Link", "Shopnav\\header.properties")).perform();
//			log.info("click in contact details");
//			l1.getWebElement("Header_ContactDetails_Link", "Shopnav\\header.properties").click();
			s.navigateToPersonalDetailsPAge();
			log.info("log out from application");
			p.logout(xmlTest);
			log.info("verify logged out or not and navigation to home page");
			sa.assertTrue(gVar.assertVisible("Header_SignIn_LINK", "Shopnav\\header.properties"),"Sign In Link");
		}
		sa.assertTrue(l1.getWebElement("Home_Identifier", "Shopnav\\header.properties").isDisplayed());
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="OOTB_055_DRM-589-598")
	public void TC06_logout_Insecure_Test(XmlTest xmlTest) throws Exception
	{
		log.info("Navigate to PDP");
//		driver.get(GetData.getDataFromProperties("\\data\\sitedata.properties", "PDPURL"));
		String searchKeyWord = GetData.getDataFromExcel("//data//GenericData.xls", "Products", 1, 3);
		s.navigateToPDP_Search(searchKeyWord);

		log.info("log in to the application");
		p.navigateToLoginPage();
		p.logIn(xmlTest);

		log.info("Should be on pdp");
		sa.assertTrue(l1.getWebElement("PDP_Element", "\\Shopnav\\PDP.properties").isDisplayed());
		log.info("Price should display");
		sa.assertTrue(l1.getWebElement("PDP_Price", "\\Shopnav\\PDP.properties").isDisplayed(),"PDP Price");
		log.info("log out from application");
		p.logout(xmlTest);
		
		log.info("Should be on same page");
		sa.assertTrue(l1.getWebElement("PDP_Element", "\\Shopnav\\PDP.properties").isDisplayed());
		log.info("price should not display");
		sa.assertTrue(gVar.assertNotVisible("PDP_Price", "\\Shopnav\\PDP.properties"),"Pdp Price shoud not display");
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="OOTB-055_DRM-590,597")
	public void TC07_plp_Sign_In_Out(XmlTest xmlTest) throws Exception
 	{
		log.info("Navigate to PLP");
		s.navigateToPLP();
		log.info("log in to the application");
		p.navigateToLoginPage();
		p.logIn(xmlTest);
		log.info("Verify Navigation to PLP");
		sa.assertTrue(gVar.assertVisible("PLP_Grid_Div", "Shopnav//PLP.properties"),"PLP grid view");
		log.info("log out from application");
		p.logout(xmlTest);
		
		log.info("Should be on same page");
		sa.assertTrue(gVar.assertVisible("PLP_Grid_Div", "Shopnav//PLP.properties"),"PDP grid view");
		sa.assertAll();
	}
	
}
