package com.drivemedical.shopnav;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import org.testng.xml.XmlTest;

import com.drivemedical.utilgeneric.BaseTest;
import com.drivemedical.utilgeneric.GetData;

//DRM-503,440,441,442,443,444,445,446,504,429

public class PLP extends BaseTest {

	String totProds;
	String facetName;
	
	@Test(groups={"reg"},description="OOTB-029 DRM-430,431,501")
	public void TC00_ProductImgClick() throws Exception
	{
		log.info("Navigate to PLP");
		s.navigateToPLP();
		log.info("fetch product name");
		pName=l1.getWebElements("ProductNames", "Shopnav//PLP.properties").get(1).getText();
		log.info("Click on product image");
		l1.getWebElements("Product_Img", "Shopnav//PLP.properties").get(1).click();
		log.info("verify navigation to PDP");
		sa.assertTrue(gVar.assertEqual("PDP_ProductName", "Shopnav//PDP.properties").contains(pName),"Verify the Product name in PDP and plp");
		
	}
	
	@Test(groups={"reg"},description="OOTB-029 DRM-434,435,436")
	public void TC01_facets() throws Exception
	{
		log.info("navigate back to PLP");
		s.navigateToPLP();
		log.info("fetch total facets");
		l1.getWebElements("PLP_Facets", "Shopnav//PLP.properties").get(0).click();
		log.info("fetch total products in that facet");
		totProds=l1.getWebElements("PLP_facetCount", "Shopnav//PLP.properties").get(0).getText().replace("(", "").replace(")", "");
		log.info("fetch facet name");
		facetName=l1.getWebElements("PLP_Facets_Links", "Shopnav//PLP.properties").get(0).getText();
		log.info("click on facet links");
		l1.getWebElements("PLP_Facets_Links", "Shopnav//PLP.properties").get(0).click();
		Thread.sleep(3000);
		log.info("verify total number of products displayed");
		sa.assertEquals(totProds,""+l1.getWebElements("ProductNames", "Shopnav//PLP.properties").size(),"Verify the product count");
		sa.assertEquals(facetName,l1.getWebElement("PLP_Selected_Facet_List", "Shopnav//PLP.properties").getText(),"Verify the facets");
		log.info("remove facet");
		l1.getWebElement("PLP_Facet_Remove_Link", "Shopnav//PLP.properties").click();
		log.info("verify select facet should be removed");
		try
		{
		sa.assertFalse(l1.getWebElement("PLP_Selected_Facet_List", "Shopnav//PLP.properties").isDisplayed(),"selected facet");
		sa.assertFalse(l1.getWebElement("PLP_Facet_Remove_Link", "Shopnav//PLP.properties").isDisplayed(),"selected facet remove link");
		} catch(Exception e){
			
			sa.assertTrue(true,"selected facets are removed");
		}
		
		
	}
	
	@Test(groups={"reg"},description="OOTB-029 DRM-502")
	public void TC02_verifyElements() throws Exception
	{
		log.info("verify model text");
		List<WebElement> models=l1.getWebElements("PLP_Model_Text", "Shopnav//PLP.properties");
		for(WebElement ele:models)
		{
			sa.assertTrue(ele.isDisplayed(),"Model text is displaying");
		}
		sa.assertTrue(models.get(0).getText().contains("Models Available"),"verifying model text");
		
	}
	
	@Test(groups={"reg","sanity_guest","sanity_reg"},description="OOTB-029 DRM-437,438,DMED-062 DRM-742,743")
	public void TC03_multipleFacets() throws Exception
	{
		log.info("navigate to PLP");
		s.navigateToPLP();
		facetName="";
		int numOfIteration = 3;
		log.info("select multiple filetrs");
		selectFilters(numOfIteration);
		
		if (BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("mobile") || BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("Tablet"))
		{
			l1.getWebElement("PLP_RefineDropdownBox", "Shopnav//PLP.properties").click();
		}
		
		log.info("verify facets are selected or not"+facetName);
		String selectedFecets = l1.getWebElement("PLP_Selected_Facet_List", "Shopnav//PLP.properties").getText().trim();
		System.out.println("selectedFecets:- "+ selectedFecets);
		gVar.assertequalsIgnoreCase(selectedFecets, facetName.trim(), "Verify the selected fecets in breadcrumb");
		log.info("verify remove links");
		sa.assertEquals(l1.getWebElements("PLP_Facet_Remove_Link", "Shopnav//PLP.properties").size(), numOfIteration,"Verifying remove link counts");
		
		log.info("deselect selected filters by clicking on selected checkbox");
		for(int i=0;i<numOfIteration;i++)
		{
			System.out.println("LOOP2:- "+i);
			log.info("expand facet");
			l1.getWebElements("PLP_Facets", "Shopnav//PLP.properties").get(i).click();
			
			if (i==0 || i==1)
			{
				log.info("deselect facet");
				l1.getWebElement("PLP_Facet_Child_Checkbox", "Shopnav//PLP.properties").click();
			}
			else
			{
				log.info("Close the selected facet by clicking on close link");
				l1.getWebElement("PLP_Facet_Remove_Link", "Shopnav//PLP.properties").click();
			}
			Thread.sleep(3000);
			if (BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("mobile") || BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("Tablet"))
			{
				l1.getWebElement("PLP_RefineDropdownBox", "Shopnav//PLP.properties").click();
			}
		}
		
		log.info("Selected Facets should not display");
		sa.assertFalse(gVar.assertVisible("PLP_Selected_Facet_List", "Shopnav//PLP.properties"),"selected facet");
		sa.assertFalse(gVar.assertVisible("PLP_Facet_Remove_Link", "Shopnav//PLP.properties"),"selected facet remove link");

		
		//***** BELOW CODE IS COMMENTED TO REDUCE THE TIME********
		//Removing the selected facets by clicking on CLOSE(x) link
//		log.info("Select facets");
//		selectFilters(numOfIteration);
//		
//		if (BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("mobile") || BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("Tablet"))
//		{
//			l1.getWebElement("PLP_RefineDropdownBox", "Shopnav//PLP.properties").click();
//		}
//		log.info("verify remove links");
//		sa.assertTrue(gVar.assertVisible("PLP_Facet_Remove_Link", "Shopnav//PLP.properties"),"Verify the REMOVE link");
//		int numberOfRemoveLinks = l1.getWebElements("PLP_Facet_Remove_Link", "Shopnav//PLP.properties").size();
//		sa.assertEquals(numberOfRemoveLinks, numOfIteration,"Verifying remove link counts");
//		//Remove selected facets by clicking on REMOVE link
//		for (int i = 0; i < numberOfRemoveLinks; i++) 
//		{
//			System.out.println("LOOP:- "+ i);
//			//click on Remove link
//			l1.getWebElement("PLP_Facet_Remove_Link", "Shopnav//PLP.properties").click();
//			Thread.sleep(3000);
//			if (BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("mobile") || BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("Tablet"))
//			{
//				l1.getWebElement("PLP_RefineDropdownBox", "Shopnav//PLP.properties").click();
//			}
//		
//		}
//		log.info("Selected Facets should not display");
//		sa.assertFalse(gVar.assertVisible("PLP_Selected_Facet_List", "Shopnav//PLP.properties"),"selected facet");
//		sa.assertFalse(gVar.assertVisible("PLP_Facet_Remove_Link", "Shopnav//PLP.properties"),"selected facet remove link");
//		sa.assertAll();
		
	}
	
	void selectFilters(int numOfIteration) throws Exception
	{
		Thread.sleep(2000);
		for(int i=0;i<numOfIteration;i++)
		{
			log.info("LOOP:- "+i);
			if (BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("mobile") || BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("Tablet"))
			{
				//Check whether the refinement TAB is expanded or not.
				String refinementClassName = gVar.assertEqual("PLP_RefineDropdownBox", "Shopnav//PLP.properties", "class");
				System.out.println("refinementClassName:- "+ refinementClassName);
				//If the REFINE dropdown box is expanded, then no need to click on dropdownbox
				if (!refinementClassName.contains("activetab")) 
				{
					log.info("Click on REFINE Dropdown box");
					l1.getWebElement("PLP_RefineDropdownBox", "Shopnav//PLP.properties").click();
				}
				
			}
			
			log.info("expand facet");
			l1.getWebElements("PLP_Facets", "Shopnav//PLP.properties").get(i).click();
			Thread.sleep(2000);
			String facetName1=l1.getWebElement("PLP_Facets_Links", "Shopnav//PLP.properties").getText();
			String childText=l1.getWebElement("PLP_Facet_Child_Links",  "Shopnav//PLP.properties").getText();
			System.out.println("facetName01:- "+ facetName1);
			System.out.println("childText:- "+childText);
			
			facetName1=facetName1.replace(childText, "");
			System.out.println("facetName1:- "+facetName1);
			facetName=facetName+facetName1;
			System.out.println("facetName:- "+facetName);
			log.info("select facet");
			l1.getWebElement("PLP_Facet_Child_Checkbox", "Shopnav//PLP.properties").click();
			
			Thread.sleep(5000);
		}
		
	}
	
	@Test(groups={"reg"},description="OOTB-029 DRM-439")
	public void TC04_priceDisplay_Guest() throws Exception
	{ 
		log.info("verify display of price for guest user");
		try
		{
			l1.getWebElements("PLP_Price", "Shopnav//PLP.properties").get(0).getText();
			sa.assertFalse(true,"price is displaying in PLP for guest user");
		}
		catch(Exception e)
		{
			sa.assertTrue(true,"price is not displaying for guest user");
			
		}
		
	}
	
	@Test(groups={"reg"},description="OOTB-029 DRM-456,458,460")
	public void TC05_PLPUI_Guest() throws Exception
	{
		log.info("verify category name");
		sa.assertTrue(gVar.assertVisible("PLP_Heading", "Shopnav//PLP.properties"),"PLP heading");
		sa.assertEquals(gVar.assertEqual("PLP_Heading", "Shopnav//PLP.properties"), GetData.getDataFromProperties("//POM//Shopnav//PLP.properties", "PLP_exp_Heading"));
		log.info("verify bread crumb");
		sa.assertTrue(gVar.assertVisible("PLP_BreadCrumb", "Shopnav//PLP.properties"),"PLP bread crumb");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("PLP_BreadCrumb", "Shopnav//PLP.properties"), GetData.getDataFromProperties("//POM//Shopnav//PLP.properties", "PLP_Exp_Breadcrumb"));
		log.info("verify sort by drop down");
		sa.assertTrue(gVar.assertVisible("PLP_SortBY_DD", "Shopnav//PLP.properties"),"sort by");
		log.info("verify items per page drop down");
		sa.assertTrue(gVar.assertVisible("PLP_ItemPerPage_DD", "Shopnav//PLP.properties"),"Items per page");
		log.info("verify category section");
		sa.assertTrue(gVar.assertVisible("PLP_Leftnav_CatHeading", "Shopnav//PLP.properties"),"category section heading");
		sa.assertEquals(gVar.assertEqual("PLP_Leftnav_CatHeading", "Shopnav//PLP.properties"), GetData.getDataFromProperties("//POM//Shopnav//PLP.properties", "PLP_Exp_CategorySecHeading"),"Leftnave category heading");
		log.info("displayed categories");
		for(WebElement ele:l1.getWebElements("PLP_Leftnav_CatNames","Shopnav//PLP.properties")){
			sa.assertTrue(ele.isDisplayed());
		}
		log.info("Refine By heading");
		sa.assertTrue(gVar.assertVisible("PLP_RefineBY_Heading", "Shopnav//PLP.properties"),"Refine By");
		log.info("verify display of refine section");
		for(WebElement ele:l1.getWebElements("PLP_Facets","Shopnav//PLP.properties")){
			sa.assertTrue(ele.isDisplayed());
		}
		log.info("products text");
		sa.assertTrue(gVar.assertVisible("PLP_Tot_Prods", "Shopnav//PLP.properties"),"Total Products text");
		log.info("pagination");
		sa.assertTrue(gVar.assertVisible("PLP_Pagination", "Shopnav//PLP.properties"),"pagination");
		
		List<WebElement> totProds=l1.getWebElements("ProductNames", "Shopnav//PLP.properties");
		
		for(int i=0;i<totProds.size();i++) {
			log.info("products image");
			sa.assertTrue(gVar.assertVisible("Product_Img", "Shopnav//PLP.properties",i),"product image");
			log.info("products name");
			sa.assertTrue(gVar.assertVisible("ProductNames", "Shopnav//PLP.properties",i),"product name");
			log.info("models text");
			sa.assertTrue(gVar.assertVisible("PLP_Model_Text", "Shopnav//PLP.properties",i),"product model text");
		}
		
		sa.assertAll();
		
	}
	
	@Test(groups={"reg"},description="OOTB-029 DRM-455,457,459,499,500")
	public void TC06_PLPUI_Reg(XmlTest xmlTest) throws Exception
	{
		log.info("Log in to the application");
		l1.getWebElement("Header_SignIn_LINK", "Shopnav\\header.properties").click();
		p.logIn(xmlTest);
		
		log.info("verify category name");
		sa.assertFalse(gVar.assertVisible("PLP_Heading", "Shopnav//PLP.properties"),"PLP heading");
		
		log.info("verify bread crumb");
		sa.assertTrue(gVar.assertVisible("PLP_BreadCrumb", "Shopnav//PLP.properties"),"PLP bread crumb");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("PLP_BreadCrumb", "Shopnav//PLP.properties"), GetData.getDataFromProperties("//POM//Shopnav//PLP.properties", "PLP_Exp_Breadcrumb"),"Verify the breadcrumb");
		log.info("verify sort by drop down");
		sa.assertTrue(gVar.assertVisible("PLP_SortBY_DD", "Shopnav//PLP.properties"),"sort by");
		log.info("verify items per page drop down");
		sa.assertTrue(gVar.assertVisible("PLP_ItemPerPage_DD", "Shopnav//PLP.properties"),"Items per page");
		log.info("verify category section");
		sa.assertTrue(gVar.assertVisible("PLP_Leftnav_CatHeading", "Shopnav//PLP.properties"),"category section heading");
		sa.assertEquals(gVar.assertEqual("PLP_Leftnav_CatHeading", "Shopnav//PLP.properties"), GetData.getDataFromProperties("//POM//Shopnav//PLP.properties", "PLP_Exp_CategorySecHeading"));
		log.info("displayed categories");
		for(WebElement ele:l1.getWebElements("PLP_Leftnav_CatNames","Shopnav//PLP.properties")){
			sa.assertTrue(ele.isDisplayed());
		}
		log.info("Refine By heading");
		sa.assertTrue(gVar.assertVisible("PLP_RefineBY_Heading", "Shopnav//PLP.properties"),"Refine By");
		log.info("verify display of refine section");
		for(WebElement ele:l1.getWebElements("PLP_Facets","Shopnav//PLP.properties")){
			sa.assertTrue(ele.isDisplayed());
		}
		log.info("products text");
		sa.assertTrue(gVar.assertVisible("PLP_Tot_Prods", "Shopnav//PLP.properties"),"Total Products text");
		log.info("pagination");
		sa.assertTrue(gVar.assertVisible("PLP_Pagination", "Shopnav//PLP.properties"),"pagination");
		
		List<WebElement> totProds=l1.getWebElements("ProductNames", "Shopnav//PLP.properties");
		
		for(int i=0;i<totProds.size();i++) {
			log.info("products image");
			sa.assertTrue(gVar.assertVisible("Product_Img", "Shopnav//PLP.properties",i),"product image");
			log.info("products name");
			sa.assertTrue(gVar.assertVisible("ProductNames", "Shopnav//PLP.properties",i),"product name");
			log.info("models text");
			sa.assertTrue(gVar.assertVisible("PLP_Model_Text", "Shopnav//PLP.properties",i),"product model text");
			log.info("product price");
			sa.assertTrue(gVar.assertVisible("PLP_Price", "Shopnav//PLP.properties",i),"product price");
		}
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="OOTB-029 DRM-497,498,432")
	public void TC07_verifyContractPrice() throws Exception
	{
		log.info("verify contract price");
		sa.assertTrue(gVar.assertVisible("PLP_Contract_Price", "Shopnav//PLP.properties"),"contract price");
		log.info("click on contract price link");
		l1.getWebElement("PLP_Contract_Price", "Shopnav//PLP.properties").click();
		log.info("verify contract price tool tip");
		sa.assertTrue(gVar.assertVisible("PLP_ContractPrice_ToolTip", "Shopnav//PLP.properties"),"contract price tool tip");
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="OOTB-029 DRM-447,448")
	public void TC08_verifyTotItemsDisplay() throws Exception
	{
		log.info("Navigate to category for which total products are more than 18");
		
		log.info("verify default display of dorp down list");
		String defaultValue=l1.getWebElement("PLP_ItemPerPage_DD", "Shopnav//PLP.properties").getText().split(" ")[0];
		int tot=new Integer(defaultValue);
		sa.assertEquals(tot, 18);
		sa.assertEquals(l1.getWebElements("PLP_Total_Grid_Products", "Shopnav//PLP.properties").size(), tot);
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="OOTB-029 DRM-433,449,450,451,452,453,454")
	public void TC09_verifyPagination() throws Exception
	{
		driver.get("https://stage.medicaldepot.com:9002/drivestorefront/driveUS/en/USD/Products/Mobility/Canes/c/Canes");
		log.info("select max products display in page");
		l1.getWebElement("PLP_ItemPerPage_DD", "Shopnav//PLP.properties").click();
		int optionsItemsPerPage=l1.getWebElements("PLP_Max_ItemsPerPage", "Shopnav//PLP.properties").size();
		log.info(optionsItemsPerPage);
		optionsItemsPerPage=optionsItemsPerPage/2;
		log.info(optionsItemsPerPage);
		
		for(int i=0;i<optionsItemsPerPage;i++)
		{
			if(i!=0)
			{
				log.info("navigate again");
				driver.get("https://stage.medicaldepot.com:9002/drivestorefront/driveUS/en/USD/Products/Mobility/Canes/c/Canes");
				log.info("select max products display in page");
				l1.getWebElement("PLP_ItemPerPage_DD", "Shopnav//PLP.properties").click();
				Thread.sleep(2000);
			}
			log.info("click on max products display in page");
			l1.getWebElement("PLP_ItemsperPage"+i, "Shopnav//PLP.properties").click();
			Thread.sleep(4000);
			log.info("fetch product count");
			String str1=l1.getWebElement("PLP_ItemPerPage_DD", "Shopnav//PLP.properties").getText().split(" ")[0];
			log.info(str1);
			int itemsDisplayedInPage=new Integer(str1);
			log.info(itemsDisplayedInPage);
			log.info("verify pagination");
			s.paginationAndItemPerPage(itemsDisplayedInPage);
		}
		
	}
	
}
