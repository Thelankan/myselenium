package com.drivemedical.shopnav;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;
import org.testng.xml.XmlTest;

import com.drivemedical.utilgeneric.BaseTest;
import com.drivemedical.utilgeneric.GetData;
import com.gargoylesoftware.htmlunit.javascript.background.JavaScriptExecutor;

public class AccessorialPDP extends BaseTest
{
	String PdpProperties = "Shopnav//PDP.properties"; 
	@Test(groups={"reg"}, description="DMED-221 DRM-360 DRM-1241 DRM-1242 DRM-1243")
	public void TC00_display_of_configured_Accessory_products_in_PDP(XmlTest xmlTest) throws Exception
	{
		log.info("Navigate to PDP for which Accessory is configured");
		String configuredProID = GetData.getDataFromExcel("//data//GenericData.xls", "Products", 7, 1);
		s.pdpNavigationThroughURL(configuredProID);
		Thread.sleep(5000);
		log.info("Click on ACCESSORIES tab");
		l1.getWebElement("PDP_tabs_ACCESSORIES", PdpProperties).click();
		
		log.info("Verify the UI of Accessory product section for Guest user");
		uiOfAccessoryProducts("guest");
		Thread.sleep(3000);
		log.info("Click on SignIn link in Accessories section");
		WebDriverWait wait=new WebDriverWait(driver, 5);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[button[@class='accessories_addto_cart_form']]")));
		JavascriptExecutor exe=(JavascriptExecutor)driver;
		exe.executeScript("arguments[0].click();", driver.findElement(By.xpath("//a[button[@class='accessories_addto_cart_form']]")));
		
		log.info("Login with valid credentials");
		p.logIn(xmlTest);
		
		log.info("Verify the UI of Accessory product section for registered user");
		uiOfAccessoryProducts("reg");
		
		sa.assertAll();
	}

	void uiOfAccessoryProducts(String user) throws Exception
	{
		log.info("Verify the PDP");
		sa.assertTrue(gVar.assertVisible("PDP_Element", PdpProperties),"Verify the PDP");
		log.info("Get Product Name");
		String productName = l1.getWebElement("PDP_ProductName", PdpProperties).getText();
		log.info("Verify the tab ACCESSORIES in PDP");
		String expected = GetData.getDataFromExcel("//data//GenericData.xls", "PDP", 2, 1);
		sa.assertTrue(gVar.assertEqual("PDP_tabs_ACCESSORIES", PdpProperties).contains(expected),"Verify the tab ACCESSORIES");
		log.info("Click on ACCESSORIES tab");
		l1.getWebElement("PDP_tabs_ACCESSORIES", PdpProperties).click();
		
		String expectedHeading = productName+" "+expected;
		System.out.println("expectedHeading:- "+ expectedHeading);
		gVar.assertequalsIgnoreCase(gVar.assertEqual("PDP_tabs_ACCESSORIES_Heading", PdpProperties), 
				expectedHeading,"Verify the Heading");
		
		if (user.equalsIgnoreCase("reg")) 
		{
			sa.assertFalse(gVar.assertVisible("PDP_tabs_ACCESSORIES_SignInLink", PdpProperties),"SignIn link should not display for registered user");
		} 
		else 
		{
			String signInLinkText = GetData.getDataFromExcel("//data//GenericData.xls", "PDP", 2, 2);
			gVar.assertequalsIgnoreCase(gVar.assertEqual("PDP_tabs_ACCESSORIES_SignInLink", PdpProperties), 
					signInLinkText,"Sign In Link");
		}
		
		log.info("Verify the ACCESSORIES items");
		sa.assertTrue(gVar.assertVisible("PDP_tabs_ACCESSORIES_Items", PdpProperties),"Accessories items");
		
		List<WebElement> accessoriesItems = l1.getWebElements("PDP_tabs_ACCESSORIES_Items", PdpProperties);
		System.out.println("Number of accessories items:- " + accessoriesItems.size());
		if (xmlTest.getParameter("executingIn").equalsIgnoreCase("mobile") || xmlTest.getParameter("executingIn").equalsIgnoreCase("Tablet"))
		{
			sa.assertTrue(gVar.assertVisible("PDP_ViewAllAccessories_Link", PdpProperties),"verify the VIEW all Accessories link");
			String expectedLink = GetData.getDataFromExcel("//data//GenericData.xls", "PDP", 1, 5);
			gVar.assertequalsIgnoreCase(gVar.assertEqual("PDP_ViewAllAccessories_Link", PdpProperties), 
					expectedLink,"Verify the ViewAllAccessories text");
			
			log.info("Click on ViewAllAccessories");
			l1.getWebElement("PDP_ViewAllAccessories_Link", PdpProperties).click();
			
			String expectedLink1 = GetData.getDataFromExcel("//data//GenericData.xls", "PDP", 2, 5);
			log.info("Verify the Opened link");
			gVar.assertequalsIgnoreCase(gVar.assertEqual("PDP_ViewLessAccessories_Link", PdpProperties), 
					expectedLink1,"Verify the ViewLessAccessories text");
			
		}
		for (int i = 0; i < accessoriesItems.size(); i++) 
		{
			System.out.println("LOOP:- "+ i);
			sa.assertTrue(gVar.assertVisible("PDP_tabs_ACCESSORIES_Items_Names", PdpProperties),"accessories item name");
			sa.assertTrue(gVar.assertVisible("PDP_tabs_ACCESSORIES_Items_Sku", PdpProperties),"accessories item SKU");
			sa.assertTrue(gVar.assertVisible("PDP_tabs_ACCESSORIES_Items_Stock", PdpProperties),"accessories item STOCK");
			String stockText = GetData.getDataFromExcel("//data//GenericData.xls", "PDP", 3, 2);
			gVar.assertequalsIgnoreCase(gVar.assertEqual("PDP_tabs_ACCESSORIES_Items_Stock", PdpProperties), 
					stockText,"Stock text");
			sa.assertTrue(gVar.assertVisible("PDP_tabs_ACCESSORIES_ItemsInStockImage", PdpProperties),"accessories item STOCK Image");
			sa.assertEquals(gVar.assertEqual("PDP_tabs_ACCESSORIES_Items_Quantity", PdpProperties, "value"), 
					"1", "Verify the Quantity");
			sa.assertTrue(gVar.assertVisible("PDP_tabs_ACCESSORIES_Items_Image", PdpProperties),"accessories item Image");
			
			if (user.equalsIgnoreCase("reg")) 
			{
				log.info("Price");
				sa.assertTrue(gVar.assertVisible("PDP_tabs_ACCESSORIES_Items_Price", PdpProperties),"accessories item Price");
				log.info("ADD TO CART button");
				String addToCartText = GetData.getDataFromExcel("//data//GenericData.xls", "PDP", 4, 2);
				gVar.assertequalsIgnoreCase(gVar.assertEqual("PDP_tabs_ACCESSORIES_Items_AddToCArt_Button", PdpProperties), 
						addToCartText,"Add To Cart button text");
				sa.assertTrue(gVar.assertVisible("PDP_tabs_ACCESSORIES_Items_UnitSelectBox", PdpProperties),"accessories item UNIT selectbox");
			} 
			else 
			{
				log.info("Price");
				sa.assertFalse(gVar.assertVisible("PDP_tabs_ACCESSORIES_Items_Price", PdpProperties),"accessories item Price should not display for guest user");
				log.info("ADD TO CART button");
				sa.assertTrue(gVar.assertNotVisible("PDP_tabs_ACCESSORIES_Items_AddToCArt_Button", PdpProperties),"Add To CArt button should not display");
				sa.assertTrue(gVar.assertNotVisible("PDP_tabs_ACCESSORIES_Items_UnitSelectBox", PdpProperties),"accessories item UNIT selectbox");
			}
			
		}
		
	}
}
