package com.drivemedical.projectspec;

import java.util.List;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import com.drivemedical.utilgeneric.BaseTest;
import com.drivemedical.utilgeneric.GetData;

public class BackOffice {
	
	public List<WebElement> prodNames;
	public List<WebElement> prodBtn;
	public static String lastProdName;
	
	public void backofcLogin() throws Exception
	{
		Thread.sleep(5000);
		BaseTest.l1.getWebElement("Backofc_UN", "\\backoffice\\backoffice.properties").sendKeys("admin");
		BaseTest.l1.getWebElement("Backofc_Pewd", "\\backoffice\\backoffice.properties").sendKeys("nimda");
		BaseTest.l1.getWebElement("Backofc_Login_Btn", "\\backoffice\\backoffice.properties").click();
		try{
			if(BaseTest.l1.getWebElement("Backofc_UN", "\\backoffice\\backoffice.properties").isDisplayed())
			{
				Thread.sleep(3000);
				BaseTest.l1.getWebElement("Backofc_UN", "\\backoffice\\backoffice.properties").clear();
				BaseTest.l1.getWebElement("Backofc_UN", "\\backoffice\\backoffice.properties").sendKeys("admin");
				BaseTest.l1.getWebElement("Backofc_Pewd", "\\backoffice\\backoffice.properties").clear();
				BaseTest.l1.getWebElement("Backofc_Pewd", "\\backoffice\\backoffice.properties").sendKeys("nimda");
				BaseTest.l1.getWebElement("Backofc_Login_Btn", "\\backoffice\\backoffice.properties").click();
			}
		} catch(Exception e){
			
		}
	}
	
	public void deleteUser() throws Exception
	{
		BaseTest.log.info("navigate to backoffice");
		BaseTest.driver.get(GetData.getDataFromProperties("\\data\\sitedata.properties", "BackOfcURL"));
		BaseTest.log.info("log in to back office");
		backofcLogin();
		BaseTest.log.info("expand user tab");
		BaseTest.l1.getWebElement("Backofc_user_link", "\\backoffice\\backoffice.properties").click();
		Thread.sleep(10000);
		BaseTest.log.info("click on customers link");
		BaseTest.l1.getWebElement("Backofc_Customre_Link", "\\backoffice\\backoffice.properties").click();
		try{
			BaseTest.log.info("click on email row");
			BaseTest.l1.getWebElement("Backofc_Email_row1", "\\backoffice\\backoffice.properties").click();
			BaseTest.log.info("click on delete icon");
			BaseTest.l1.getWebElements("Backofc_delete_Icon", "\\backoffice\\backoffice.properties").get(0).click();
		} catch(Exception e) {
			
		}
		
		try{
			BaseTest.log.info("click on email row");
			BaseTest.l1.getWebElement("Backofc_Email_row", "\\backoffice\\backoffice.properties").click();
			BaseTest.log.info("click on delete icon");
			BaseTest.l1.getWebElements("Backofc_delete_Icon", "\\backoffice\\backoffice.properties").get(0).click();
		} catch(Exception e) {
			
		}
	}
	
	public void featuredProduct() throws Exception
	{
		BaseTest.log.info("navigate to backoffice");
		BaseTest.driver.get(GetData.getDataFromProperties("\\data\\sitedata.properties", "BackOfcURL"));
		BaseTest.log.info("log in to back office");
		backofcLogin();
		Thread.sleep(15000);
		BaseTest.log.info("click on administration link from header");
		BaseTest.l1.getWebElement("BackOfc_Admin_List", "\\backoffice\\backoffice.properties").click();
		BaseTest.log.info("select adaptive search");
		BaseTest.l1.getWebElements("BackOfc_Options_List", "\\backoffice\\backoffice.properties").get(3).click();
		Thread.sleep(3000);
		BaseTest.log.info("Enter index configuration");
		BaseTest.l1.getWebElements("BackOfc_LeftNav_textboxes", "\\backoffice\\backoffice.properties").get(0).sendKeys("Drive Solr Index");
		Thread.sleep(3000);
		BaseTest.log.info("Select Search profile");
		BaseTest.l1.getWebElements("BackOfc_LeftNav_textboxes", "\\backoffice\\backoffice.properties").get(3).sendKeys("test");
		BaseTest.log.info("select profile");
		BaseTest.l1.getWebElement("BackOfc_Profile_Name", "\\backoffice\\backoffice.properties").click();
		Thread.sleep(3000);
		BaseTest.log.info("search for sub category");
		BaseTest.l1.getWebElement("BackOfc_Search", "\\backoffice\\backoffice.properties").sendKeys("Bed Rails");
		BaseTest.log.info("expand beds sub category");
		BaseTest.l1.getWebElement("BackOfc_Search_Btn", "\\backoffice\\backoffice.properties").click();
		Thread.sleep(3000);
		try{
			if(BaseTest.l1.getWebElement("BackOfc_Promoted_Sec", "\\backoffice\\backoffice.properties").isDisplayed())
			{
				prodBtn=BaseTest.l1.getWebElements("BackOfc_Promoted_Prods_Btn", "\\backoffice\\backoffice.properties");
				for(WebElement ele:prodBtn)
				{
					ele.click();
					BaseTest.l1.getWebElement("BackOfc_Remove_Btn", "\\backoffice\\backoffice.properties").click();
					Thread.sleep(2000);
				}
			}
			}catch(Exception e){
				BaseTest.log.info("No promoted elements");
			}
		BaseTest.log.info("fetch ast product name");
		prodNames=BaseTest.l1.getWebElements("BackOfc_Prod_names", "\\backoffice\\backoffice.properties");
		System.out.println(prodNames.size()-1);
		lastProdName=prodNames.get(prodNames.size()-1).getText();
		System.out.println(lastProdName);
		BaseTest.l1.getWebElements("BackOfc_Options", "\\backoffice\\backoffice.properties").get(prodNames.size()-1).click();
		BaseTest.log.info("click on promote");
		BaseTest.l1.getWebElement("BackOfc_Promote", "\\backoffice\\backoffice.properties").click();
		Thread.sleep(5000);
	}
	
}
