package com.drivemedical.projectspec;

import org.openqa.selenium.support.ui.Select;

import com.drivemedical.utilgeneric.BaseTest;
import com.drivemedical.utilgeneric.GetData;

public class CheckoutFunctions {
	
	public static String PONumberVal;

	
	public void navigateToPaymentTypePage() throws Exception
	{
		BaseTest.log.info("navigate to Payment type page");
		navigateToShippingMethodPage();
		Thread.sleep(4000);
		BaseTest.log.info("click on next button");
		BaseTest.l1.getWebElement("ShippingMtd_Next_Btn","Checkout//ShippingMethod.properties").click();
	}
	
	public void navigateToShipToPage() throws Exception
	{
		BaseTest.log.info("add item to cart");
		String excelDAta = GetData.getDataFromExcel("//data//GenericData.xls", "Category", 1, 0);;
		BaseTest.log.info("excelDAta:- "+ excelDAta);
		BaseTest.s.navigateToCart(excelDAta, 1, 1);
		
		BaseTest.log.info("click on proceed to check out button");
		BaseTest.l1.getWebElement("Cart_CheckoutButton","Cart//Cart.properties").click();
		Thread.sleep(2000);
	}
	
	public void navigateToShippingMethodPage() throws Exception
	{
		BaseTest.log.info("navigate to Ship To page");
		navigateToShipToPage();
		Thread.sleep(4000);
		BaseTest.log.info("Select any saved address and navigate to Shipping method page");
		//BaseTest.l1.getWebElements("ShipTo_SavedAddr_Radio","Checkout//ShipTo.properties").get(0).click();
		BaseTest.l1.getWebElement("ShipTo_Continue_button","Checkout//ShipTo.properties").click();
	}
	
	
	
	public void navigateToOrderReviewPage() throws Exception
	{
		BaseTest.log.info("navigate to Order Review page");
		navigateToPaymentTypePage();
		BaseTest.log.info("enter PO boc number");
		int randomNum = BaseTest.gVar.generateRandomNum();
		PONumberVal =Integer.toString(randomNum);
		System.out.println("Random PO NUMBER:-  "+ PONumberVal);
//		PONumberVal=GetData.getDataFromProperties("//POM//Checkout//PaymentType.properties", "POBoxNumber");
		BaseTest.l1.getWebElement("PT_POBox_Textbox","Checkout//PaymentType.properties").clear();
		BaseTest.l1.getWebElement("PT_POBox_Textbox","Checkout//PaymentType.properties").sendKeys(PONumberVal);
		BaseTest.log.info("click on next button");
		BaseTest.l1.getWebElement("Next_Button","Checkout//PaymentType.properties").click();
	}
	
	public void addAddress(int rowNum) throws Exception
	{
		BaseTest.log.info("enter name");
		BaseTest.l1.getWebElement("ShipTo_Name_Textbox","Checkout//ShipTo.properties").sendKeys(GetData.getDataFromExcel("//data//GenericData.xls", "Address", rowNum, 1));
		BaseTest.log.info("enter address 1");
		BaseTest.l1.getWebElement("ShipTo_Addr1_Textbox","Checkout//ShipTo.properties").sendKeys(GetData.getDataFromExcel("//data//GenericData.xls", "Address", rowNum, 2));
		BaseTest.log.info("enter city");
		BaseTest.l1.getWebElement("ShipTo_city_Textbox","Checkout//ShipTo.properties").sendKeys(GetData.getDataFromExcel("//data//GenericData.xls", "Address", rowNum, 4));
		BaseTest.log.info("enter state");
		new Select(BaseTest.l1.getWebElement("ShipTo_State_Textbox","Checkout//ShipTo.properties")).selectByValue(GetData.getDataFromExcel("//data//GenericData.xls", "Address", rowNum, 5));
		BaseTest.log.info("enter zip code");
		BaseTest.l1.getWebElement("ShipTo_Zip_Textbox","Checkout//ShipTo.properties").sendKeys(GetData.getDataFromExcel("//data//GenericData.xls", "Address", rowNum, 6));
		BaseTest.log.info("enter phone");
		BaseTest.l1.getWebElement("ShipTo_Phone_Textbox","Checkout//ShipTo.properties").sendKeys(GetData.getDataFromExcel("//data//GenericData.xls", "Address", rowNum,7));
	}
	
}
