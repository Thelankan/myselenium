package com.drivemedical.projectspec;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.jetty.util.log.Log;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.asserts.SoftAssert;
import org.testng.xml.XmlTest;

import com.drivemedical.utilgeneric.BaseTest;
import com.drivemedical.utilgeneric.GetData;
import com.drivemedical.utilgeneric.GlobalVariables;
import com.drivemedical.utilgeneric.Locators;



public class ShopnavFunctions {
	
   public List<String> prodnames=new ArrayList<String>();
   public List<String> prodPrice=new ArrayList<String>();
   public ArrayList<String> materialId = new ArrayList<String>();
   int RemoveIcon[];
   int leftOutProductsInLastPage;
   int totPages;
   public String savedCartNameFromExcel;

   public void navigateToPLP() throws Exception
   {
	   if (BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("mobile") || BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("Tablet")) 
	   {
		   BaseTest.log.info("Click on Hamburgar menu");
		   BaseTest.l1.getWebElement("Header_Hamburger_Menu","Shopnav\\header.properties").click();
		   BaseTest.log.info("Click on arrow icon");
		   BaseTest.l1.getWebElement("MegaMenu_Products_Cat_Arrow_Mobile", "Shopnav//header.properties").click();
		   BaseTest.log.info("click on sub category");
		   BaseTest.l1.getWebElement("MegaMenu_Canes_SubCat_Mobile", "Shopnav//header.properties").click();
		   
	   } 
	   else 
	   {
		   BaseTest.log.info("Hover on root category");
		   BaseTest.act.moveToElement(BaseTest.l1.getWebElement("MegaMenu_Products_Cat","Shopnav//header.properties")).perform();
		   BaseTest.log.info("click on sub category");
		   BaseTest.l1.getWebElement("MegaMenu_Canes_SubCat", "Shopnav//header.properties").click();
	   }
	 
   }
   
	public void searchProduct(String searchKeyWord) throws Exception
	{
		if(BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("mobile"))
		{
			System.out.println("Mobile View");
			BaseTest.l1.getWebElement("Header_Mob_Search_Text", "Shopnav//header.properties").click();
			BaseTest.l1.getWebElement("Header_Mob_Search_Box", "Shopnav//header.properties").clear();
			BaseTest.l1.getWebElement("Header_Mob_Search_Box", "Shopnav//header.properties").sendKeys(searchKeyWord);
			BaseTest.l1.getWebElement("Header_Mob_Search_Icon", "Shopnav//header.properties").click();
			
		}
		else if(BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("Tablet")) 
		{
			System.out.println("Tab View");
			BaseTest.l1.getWebElement("Header_Mob_Search_Text", "Shopnav//header.properties").click();
			BaseTest.l1.getWebElement("Header_SearchBox_Tab", "Shopnav//header.properties").clear();
			BaseTest.l1.getWebElement("Header_SearchBox_Tab", "Shopnav//header.properties").sendKeys(searchKeyWord);
			BaseTest.l1.getWebElement("Header_SearchBox_Tab_Icon", "Shopnav//header.properties").click();
			
		} 
		else 
		{
			System.out.println("Desktop View");
			Thread.sleep(3000);
			BaseTest.l1.getWebElement("Header_Serach_Box", "Shopnav//header.properties").clear();
			BaseTest.l1.getWebElement("Header_Serach_Box", "Shopnav//header.properties").sendKeys(searchKeyWord);
//			BaseTest.log.info("Cloce minicart if its vivible");
//			Thread.sleep(2000);
//			if (BaseTest.gVar.assertVisible("MiniCart_Close_Button", "Cart//MiniCart.properties")) 
//			{
//				BaseTest.l1.getWebElement("MiniCart_Close_Button", "Cart//MiniCart.properties").click();
//			}
			BaseTest.l1.getWebElement("Header_Search_Icon", "Shopnav//header.properties").click();
		
		}
	}
	
	public void navigateToPDP_Search(String searchKeyWord) throws Exception
	{
		searchProduct(searchKeyWord);
		BaseTest.log.info(BaseTest.l1.getWebElements("ProductNames", "Shopnav//PLP.properties").size());
		prodnames.add(BaseTest.l1.getWebElement("ProductNames_Text", "Shopnav//PLP.properties").getText());
		BaseTest.l1.getWebElement("ProductNames", "Shopnav//PLP.properties").click();
		if (BaseTest.gVar.assertVisible("PDP_Price", "Shopnav//PDP.properties")) 
		{
			prodPrice.add(BaseTest.l1.getWebElement("PDP_Price", "Shopnav//PDP.properties").getText());
		}
		
	}
	
	public void selectSwatch() throws Exception
    {

       WebElement ColorSwatchText=BaseTest.l1.getWebElement("PDP_ColorText", "Shopnav//PDP.properties");
       try{
       if(ColorSwatchText.isDisplayed())
       {
     	  
       try{
     	  WebElement SelectedColor=BaseTest.l1.getWebElement("PDP_SelectedColorSwatch", "Shopnav//PDP.properties");
        }
       catch (Exception e){
    	   BaseTest. l1.getWebElement("PDP_SelectedColorSwatch", "Shopnav//PDP.properties").click();
       }
       }
       }catch(Exception e)
       {
     	  System.out.println("color is not there");
       }
       try{
       if(BaseTest.l1.getWebElement("PDP_Fit_Swatch", "Shopnav//PDP.properties").isDisplayed())
       {
    	   BaseTest.gVar.selectObj(BaseTest.l1.getWebElement("PDP_Fit_DropDown", "Shopnav//PDP.properties")).selectByIndex(1);
       }
       }catch(Exception e){
     	  System.out.println("Fit is not there");
       }
       try{
       if(BaseTest.l1.getWebElement("PDP_Size_Swatch_Text", "Shopnav//PDP.properties").isDisplayed())
       {
    	   BaseTest.gVar.selectObj(BaseTest.l1.getWebElement("PDP_Size_Dropdown", "Shopnav//PDP.properties")).selectByIndex(1);
       }
       }catch(Exception e){
     	  System.out.println("Fit is not there");
       }
    }

// ################## Commenting below script is becouse we are adding multiple products from PLP. If the product is OUT OF STOCK then the script will fail. To avoid that will add the products which are InStock(these product ID fetched from excel sheet)
	
//		public void addItemToCart(String searchKeyWord,int numOfItems,int qty) throws Exception
//		{
//			for(int i=0; i< numOfItems; i++)
//			{
//				BaseTest.log.info("LOOP:- "+ i);
//				
//				if (BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("mobile") || BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("Tablet"))
//				{
//					//Mini cart popup is not displaying in mobile view. Hence Home page navigation is not required
//				}
//				else
//				{
//					BaseTest.log.info("Click on Home PAge for Desktop view");
//					//After clicking on ADDTOCArt, minicart popup is displaying. If Minicart popup is displaying unable to search.Hence Home page navigation(Refreshing the page) is required
//					BaseTest.l1.getWebElement("Header_Logo", "Shopnav//header.properties").click();
//					Thread.sleep(2000);
//				}
//				
//				BaseTest.log.info("Navigate to PLP");
//				searchProduct(searchKeyWord);
//				BaseTest.log.info("fetch product names");
//				String proNAmeInPLP = BaseTest.l1.getWebElements("ProductNames_Text", "Shopnav//PLP.properties").get(i).getText();
//				BaseTest.log.info("proNAmeInPLP:- "+ proNAmeInPLP);
//				prodnames.add(proNAmeInPLP);
//				System.out.println("prodnames:- "+ prodnames);
//				BaseTest.l1.getWebElements("ProductNames", "Shopnav//PLP.properties").get(i).click();
//				BaseTest.log.info("fetch product price from PDP");
//				prodPrice.add(BaseTest.l1.getWebElement("PDP_Price", "Shopnav//PDP.properties").getText());
//				materialId.add(BaseTest.l1.getWebElement("PDP_SKU", "Shopnav//PDP.properties").getText().split("#")[1].trim());
//				
//				BaseTest.log.info("Select Swath");
//				//selectSwatch();
//				BaseTest.log.info("Enter quantity");
//				BaseTest.l1.getWebElement("PDP_Qty_Textbox", "Shopnav//PDP.properties").clear();
//				BaseTest.l1.getWebElement("PDP_Qty_Textbox", "Shopnav//PDP.properties").sendKeys(qty+"");
//				BaseTest.log.info("Click on Add to cart");
//				BaseTest.l1.getWebElement("PDP_AddToCart_Btn", "Shopnav//PDP.properties").click();
//			}
//		}
	
	public void addItemToCart(String searchKeyWord,int numOfItems,int qty) throws Exception
	{
		for(int i=0; i< numOfItems; i++)
		{
			BaseTest.log.info("LOOP:- "+ i);
			
			if (BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("mobile") || BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("Tablet"))
			{
				//Mini cart popup is not displaying in mobile view. Hence Home page navigation is not required
			}
			else
			{
				BaseTest.log.info("Navigate to Home PAge for Desktop view");
				//After clicking on ADDTOCArt, minicart popup is displaying. If Minicart popup is displaying unable to search.Hence Home page navigation(Refreshing the page) is required
//				BaseTest.l1.getWebElement("Header_Logo", "Shopnav//header.properties").click();
				BaseTest.gVar.navigateToSite(BaseTest.xmlTest);
				Thread.sleep(2000);
			}
			
			BaseTest.log.info("Navigate to PLP");
			int rowNum = 1+i;
			String searchProduct = GetData.getDataFromExcel("//data//GenericData.xls", "Products", rowNum, 3);
			searchProduct(searchProduct);
			BaseTest.log.info("fetch product names");
			String proNAmeInPLP = BaseTest.l1.getWebElement("ProductNames_Text", "Shopnav//PLP.properties").getText();
			BaseTest.log.info("proNAmeInPLP:- "+ proNAmeInPLP);
			prodnames.add(proNAmeInPLP);
			System.out.println("prodnames:- "+ prodnames);
			BaseTest.l1.getWebElement("ProductNames", "Shopnav//PLP.properties").click();
			BaseTest.log.info("fetch product price from PDP");
			prodPrice.add(BaseTest.l1.getWebElement("PDP_Price", "Shopnav//PDP.properties").getText());
			materialId.add(BaseTest.l1.getWebElement("PDP_SKU", "Shopnav//PDP.properties").getText().split("#")[1].trim());
			
			BaseTest.log.info("Select Swath");
			//selectSwatch();
			BaseTest.log.info("Enter quantity");
			BaseTest.l1.getWebElement("PDP_Qty_Textbox", "Shopnav//PDP.properties").clear();
			BaseTest.l1.getWebElement("PDP_Qty_Textbox", "Shopnav//PDP.properties").sendKeys(qty+"");
			BaseTest.log.info("Click on Add to cart");
			BaseTest.l1.getWebElement("PDP_AddToCart_Btn", "Shopnav//PDP.properties").click();
		}
	}
	
	
	public void navigateToCartPage() throws Exception
	{
		BaseTest.log.info("Navigate to cart");
		Thread.sleep(4000);
		if (BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("mobile") || BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("Tablet"))
		{
			System.out.println("MOBILE VIEW");
//			BaseTest.l1.getWebElement("Header_Mobile_CartLink", "Shopnav//header.properties").click();
			BaseTest.driver.get("https://stage.medicaldepot.com:9002/drivestorefront/driveUS/en/USD/cart");
		} 
		else 
		{
			System.out.println("DESKTOP view");
			BaseTest.l1.getWebElement("Header_Cart_Link", "Shopnav//header.properties").click();
		}
		
		Thread.sleep(3000);
		if (BaseTest.gVar.assertNotVisible("Cart_Heading", "Cart//Cart.properties"))
		{
//			BaseTest.sa.assertFalse(true,"TestCAse is not failed, Click on CART link is not worked hence navigating through cart page URL(Once verify the cart link navigation in header)");
			BaseTest.driver.get("https://stage.medicaldepot.com:9002/drivestorefront/driveUS/en/USD/cart");
		} 
	}
	public void navigateToCart(String searchKeyWord,int numOfItems,int qty) throws Exception
	{
		BaseTest.log.info("Add Item to Cart");
		addItemToCart(searchKeyWord,numOfItems,qty);
		navigateToCartPage();
	}
	
	public void navigateToSavedCart() throws Exception
	{
		Thread.sleep(2000);
/*		
//		if (BaseTest.gVar.assertVisible("Header_Reg_MyAct_Collapsed", "Shopnav//header.properties")) 
		{
			BaseTest.log.info("Click on MY Account link in header");
			BaseTest.act.moveToElement(BaseTest.l1.getWebElement("Header_Reg_MyAct_Link", "Shopnav//header.properties")).perform();
		}
		
		BaseTest.log.info("click on saved carts link from myaccount flyout");
		Thread.sleep(2000);
		BaseTest.l1.getWebElement("Header_Saved_Carts_Link", "Shopnav//header.properties").click();*/
		BaseTest.driver.get("https://stage.medicaldepot.com:9002/drivestorefront/driveUS/en/USD/my-account/saved-carts");
		Thread.sleep(1000);
		BaseTest.log.info("Verify the Saved cart page");
		 BaseTest.sa.assertTrue(BaseTest.gVar.assertVisible("SavedCart_Heading", "Profile//Savedcart.properties"),"Verify the SAVED CART heading");
	}
	
	public void navigateToOrderHistoryPage() throws Exception
	{
		
		
		if (BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("mobile") || BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("Tablet"))
		{
			BaseTest.log.info("Click on Hamburgar menu");
			BaseTest.l1.getWebElement("Header_Hamburger_Menu","Shopnav\\header.properties").click();
			BaseTest.log.info("Click on Order History link");
			BaseTest.l1.getWebElement("Hamburger_OrderHistory_Link", "Shopnav\\header.properties").click();
			
		}
		else 
		{
			BaseTest.act.moveToElement(BaseTest.l1.getWebElement("Header_Reg_MyAct_Link", "Shopnav//header.properties")).perform();		
			Thread.sleep(2000);
//			BaseTest.l1.getWebElement("Header_Reg_MyAct_Link", "Shopnav//header.properties").click();
			if (BaseTest.gVar.assertVisible("Header_OrderHistory_Link", "Shopnav//header.properties")) 
			{
				BaseTest.l1.getWebElement("Header_OrderHistory_Link", "Shopnav//header.properties").click();
			} 
			else 
			{
//				BaseTest.sa.assertFalse(true,"Navigated using URL, not by clicking on ORDER HISTORY link");
				BaseTest.driver.get("https://stage.medicaldepot.com:9002/drivestorefront/driveUS/en/USD/my-account/orders");
			}
		}
		
		BaseTest.log.info("Verify the Order History page");
		String orderHistoryText = GetData.getDataFromExcel("//data//GenericData.xls", "OrderHistory", 1, 0);
		BaseTest.sa.assertEquals(BaseTest.gVar.assertEqual("OrderHistory_Section_Header", "Profile//OrderHistory.properties"), 
				orderHistoryText,"Order History Page Heading");
	}

	public void navigateToPersonalDetailsPAge() throws Exception
	{
		if (BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("mobile") || BaseTest.xmlTest.getParameter("executingIn").equalsIgnoreCase("Tablet"))
		{
			BaseTest.log.info("Click on hamburger menu and click on contact details button");
			BaseTest.l1.getWebElement("Header_Hamburger_Menu","Shopnav//header.properties").click();
			BaseTest.l1.getWebElement("Hamburger_ContactDetails_Link", "Shopnav//header.properties").click();
		}
		else 
		{
			BaseTest.log.info("hover on my account and click on contact details");
//			BaseTest.act.moveToElement(BaseTest.l1.getWebElement("Header_Reg_MyAct_Link", "Shopnav//header.properties")).perform();
			BaseTest.l1.getWebElement("Header_Reg_MyAct_Link", "Shopnav//header.properties").click();
			if (BaseTest.gVar.assertVisible("Header_ContactDetails_Link", "Shopnav//header.properties"))
			{
				//Click on link in my account overlay
				BaseTest.l1.getWebElement("Header_ContactDetails_Link", "Shopnav//header.properties").click();
			}
			else if(BaseTest.gVar.assertVisible("AccountOverview_LeftNavContactDetailsLink", "Profile//AccountOverview.properties")) 
			{
				//click on link in left navigation section
				BaseTest.l1.getWebElement("AccountOverview_LeftNavContactDetailsLink", "Profile//AccountOverview.properties").click();
			}
			
			else 
			{
				BaseTest.log.info("NAvigating to Contact Details page through URL"); 
				BaseTest.driver.get("https://stage.medicaldepot.com:9002/drivestorefront/driveUS/en/USD/my-account/update-profile");
			}
		}
	}
	
	public void removeSavedcarts() throws Exception
	{
		
		BaseTest.log.info("navigate to saved cart");
		navigateToSavedCart();
	   try
	   {
		   List<WebElement> removelinkcount=BaseTest.l1.getWebElements("SavedCart_Remove_link", "Profile//Savedcart.properties");
		   System.out.println("removelinkcount:- "+ removelinkcount.size());
		   for(int i=0; i<=removelinkcount.size(); i++)
		    { 
			   System.out.println("LOOP:- "+i);
//			   BaseTest.l1.getWebElements("SavedCart_Remove_link", "Profile//Savedcart.properties").get(i).click();
			   BaseTest.l1.getWebElement("SavedCart_Remove_link", "Profile//Savedcart.properties").click();
			   BaseTest.l1.getWebElement("SavedCart_Delete_btn", "Profile//Savedcart.properties").click();		
			   Thread.sleep(3000);
			}
	  }
	   
	   catch(Exception e)
	   {
		   BaseTest.log.info("No Saved Carts are present");
	   }
	}
	
   public void removeSingleSavedcart(int numberofremovesavedcart) throws Exception
   {
	   for(int i=0; i<numberofremovesavedcart;i++)
	   {
		   System.out.println("LOOP:-"+i);
		   try 
		   {
			   BaseTest.l1.getWebElements("SavedCart_Remove_link", "Profile//Savedcart.properties").get(i).click();
			   BaseTest.l1.getWebElement("SavedCart_Delete_btn", "Profile//Savedcart.properties").click();
		   } 
		   catch (Exception e) 
		   {
			   BaseTest.log.info("CATCH BLOCKING EXECUTING No Saved Carts are present");
		   }
		   
	   }
	   
   }

   
	public void savedCartDetails(int row) throws Exception
	{
		BaseTest.log.info("Entering Name field");
		savedCartNameFromExcel = GetData.getDataFromExcel("//data//GenericData.xls", "SavedCartdetails", row,0);
		System.out.println("savedCartNameFromExcel:-"+savedCartNameFromExcel);
		BaseTest.l1.getWebElement("Cart_SaveCart_Name_txtBox", "Cart//Cart.properties").sendKeys(savedCartNameFromExcel);
		BaseTest.log.info("Entering discriptions for saved Carts");
		BaseTest.l1.getWebElement("Cart_SaveCart_Discription_txtbox", "Cart//Cart.properties").sendKeys(GetData.getDataFromExcel("//data//GenericData.xls", "SavedCartdetails", row,1));
	}
	
	public void verifyNavToPDP(String pName) throws Exception
	{
		BaseTest.log.info("verify product name");
		BaseTest.sa.assertEquals(BaseTest.gVar.assertEqual("PDP_ProductName", "Shopnav//PDP.properties"), 
				pName, "Verify the product name");
		
		BaseTest.sa.assertAll();
	}
	
	
	public void paginationAndItemPerPage(int itemsPerPage) throws Exception
	{
		BaseTest.log.info("fetch total products");
		String totProducts=BaseTest.l1.getWebElement("PLP_Tot_Prods", "Shopnav//PLP.properties").getText();
		int start=totProducts.indexOf("(")+1;
		int end=totProducts.indexOf(")");
		totProducts=totProducts.substring(start,end);
		BaseTest.log.info(totProducts);
		int totProdsInInteger=new Integer(totProducts);
		BaseTest.log.info("calculate left out products at last page");
		leftOutProductsInLastPage=totProdsInInteger%itemsPerPage;
		totPages=(totProdsInInteger/itemsPerPage)+1;
		
		if(BaseTest.gVar.assertVisible("PLP_Pagination", "Shopnav//PLP.properties")) {
			
		BaseTest.log.info("verify next link");
		for(int j=0;j<totPages;j++)
		{
			if(j==0) {
			BaseTest.log.info("verify products in first page");
			BaseTest.sa.assertEquals(BaseTest.l1.getWebElements("PLP_Total_Grid_Products", "Shopnav//PLP.properties").size(), itemsPerPage);
			BaseTest.log.info("Previous link should be disabled");
			BaseTest.sa.assertTrue(BaseTest.gVar.assertVisible("PLP_Pagination_Prev_disable", "Shopnav//PLP.properties"));
			
			} else if(j==totPages-1){
			BaseTest.log.info("click on next link");
			BaseTest.l1.getWebElement("PLP_Pagination_Next", "Shopnav//PLP.properties").click();
			BaseTest.log.info("verify products in last page");
			BaseTest.sa.assertEquals(BaseTest.l1.getWebElements("PLP_Total_Grid_Products", "Shopnav//PLP.properties").size(), leftOutProductsInLastPage);
			BaseTest.log.info("next link should be disabled");
			BaseTest.sa.assertTrue(BaseTest.gVar.assertVisible("PLP_Pagination_Next_disable", "Shopnav//PLP.properties"));
			
			} else {
			BaseTest.log.info("click on next link");
			BaseTest.l1.getWebElement("PLP_Pagination_Next", "Shopnav//PLP.properties").click();
			BaseTest.log.info("verify products in page");
			BaseTest.sa.assertEquals(BaseTest.l1.getWebElements("PLP_Total_Grid_Products", "Shopnav//PLP.properties").size(), itemsPerPage);
			BaseTest.log.info("both next and previous links should display");
			BaseTest.sa.assertTrue(BaseTest.gVar.assertVisible("PLP_Pagination_Next", "Shopnav//PLP.properties"));
			BaseTest.sa.assertTrue(BaseTest.gVar.assertVisible("PLP_Pagination_Previous", "Shopnav//PLP.properties"));
			}
		}
		
		BaseTest.log.info("verify previous link");
		for(int j=0;j<totPages;j++)
		{
			if(j==0) {
			BaseTest.log.info("verify products in last page");
			BaseTest.sa.assertEquals(BaseTest.l1.getWebElements("PLP_Total_Grid_Products", "Shopnav//PLP.properties").size(), leftOutProductsInLastPage);	
			BaseTest.log.info("next link should be disabled");
			BaseTest.sa.assertTrue(BaseTest.gVar.assertVisible("PLP_Pagination_Next_disable", "Shopnav//PLP.properties"));
			
			} else if(j==totPages-1){
			BaseTest.log.info("click on next link");
			BaseTest.l1.getWebElement("PLP_Pagination_Previous", "Shopnav//PLP.properties").click();
			BaseTest.log.info("verify products in last page");
			BaseTest.sa.assertEquals(BaseTest.l1.getWebElements("PLP_Total_Grid_Products", "Shopnav//PLP.properties").size(), itemsPerPage);
			BaseTest.log.info("Previous link should be disabled");
			BaseTest.sa.assertTrue(BaseTest.gVar.assertVisible("PLP_Pagination_Prev_disable", "Shopnav//PLP.properties"));
			
			} else {
			BaseTest.log.info("click on previous link");
			BaseTest.l1.getWebElement("PLP_Pagination_Previous", "Shopnav//PLP.properties").click();
			BaseTest.log.info("verify products in page");
			BaseTest.sa.assertEquals(BaseTest.l1.getWebElements("PLP_Total_Grid_Products", "Shopnav//PLP.properties").size(), itemsPerPage);
			BaseTest.log.info("both next and previous links should display");
			BaseTest.sa.assertTrue(BaseTest.gVar.assertVisible("PLP_Pagination_Next", "Shopnav//PLP.properties"));
			BaseTest.sa.assertTrue(BaseTest.gVar.assertVisible("PLP_Pagination_Previous", "Shopnav//PLP.properties"));
			}
		}
		
		BaseTest.log.info("verify page number links");
		for(int j=1;j<=totPages;j++)
		{
			if(j==1) {
			BaseTest.log.info("verify products in last page");
			BaseTest.sa.assertEquals(BaseTest.l1.getWebElements("PLP_Total_Grid_Products", "Shopnav//PLP.properties").size(), itemsPerPage);	
			BaseTest.log.info("Previous link should be disabled");
			BaseTest.sa.assertTrue(BaseTest.gVar.assertVisible("PLP_Pagination_Prev_disable", "Shopnav//PLP.properties"));
			
			} else if(j==totPages){
			BaseTest.log.info("click on next link");
			BaseTest.driver.findElement(By.xpath("//ul[@class='pagination']/descendant::a[contains(text(),'"+j+"')]")).click();
			BaseTest.log.info("verify products in last page");
			BaseTest.sa.assertEquals(BaseTest.l1.getWebElements("PLP_Total_Grid_Products", "Shopnav//PLP.properties").size(), leftOutProductsInLastPage);
			BaseTest.log.info("next link should be disabled");
			BaseTest.sa.assertTrue(BaseTest.gVar.assertVisible("PLP_Pagination_Next_disable", "Shopnav//PLP.properties"));
			
			} else {
			BaseTest.log.info("click on previous link");
			BaseTest.driver.findElement(By.xpath("//ul[@class='pagination']/descendant::a[contains(text(),'"+j+"')]")).click();;
			BaseTest.log.info("verify products in page");
			BaseTest.sa.assertEquals(BaseTest.l1.getWebElements("PLP_Total_Grid_Products", "Shopnav//PLP.properties").size(), itemsPerPage);
			BaseTest.log.info("both next and previous links should display");
			BaseTest.sa.assertTrue(BaseTest.gVar.assertVisible("PLP_Pagination_Next", "Shopnav//PLP.properties"));
			BaseTest.sa.assertTrue(BaseTest.gVar.assertVisible("PLP_Pagination_Previous", "Shopnav//PLP.properties"));
			}
		}
		
	} else {
		
		BaseTest.log.info("verify products in one page");
		BaseTest.sa.assertEquals(BaseTest.l1.getWebElements("PLP_Total_Grid_Products", "Shopnav//PLP.properties").size(), totProducts);
	}
		
		BaseTest.log.info("validate all asserts");
		BaseTest.sa.assertAll();
		
	}
	
	public void pdpNavigationThroughURL(String productID) throws Exception
	{
		//First navigate to Any PDP
		String searchKeyWord = GetData.getDataFromExcel("//data//GenericData.xls", "Products", 1, 1);
		navigateToPDP_Search(searchKeyWord);
		
		//COllect the URL
		String currentUrl = BaseTest.driver.getCurrentUrl();
		System.out.println("currentUrl:- "+ currentUrl);
		String temp = currentUrl;
		String id = currentUrl.substring(currentUrl.lastIndexOf("/")+1, currentUrl.length());
		System.out.println("id:- "+ id);
		
		String updatedURL = temp.replace(id, productID);
		System.out.println("updatedURL:- "+ updatedURL);
		BaseTest.driver.get(updatedURL);
		
	}
	
	
	
	
}
