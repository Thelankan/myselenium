package com.drivemedical.projectspec;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.TimeZone;

import org.apache.bcel.generic.BASTORE;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.asserts.SoftAssert;
import org.testng.xml.XmlTest;

import com.drivemedical.utilgeneric.BaseTest;
import com.drivemedical.utilgeneric.GetData;

public class GlobalFunctions {
	
	WebDriver driver;
	String assertEqual;
	boolean isVisible;
	public String emTextColor = "rgb(208, 2, 27)";
	
	public GlobalFunctions(WebDriver driver)
	{
		this.driver=driver;
	}
	
	public void navigateToSite(XmlTest xmlTest) throws Exception
	{
		
		driver.get(new GetData().getDataFromProperties("//data//sitedata.properties", "URL"));
		//driver.getClass().getName().indexOf("ie")>0
		if(xmlTest.getParameter("broName").equalsIgnoreCase("ie")) {
			try
			{
				Thread.sleep(5000);
				driver.navigate ().to ("javascript:document.getElementById('overridelink').click()");
				//driver.findElement(By.id("overridelink")).click();
				System.out.println("overrided");
				Thread.sleep(5000);
			}
			catch(Exception e)
			{
				e.printStackTrace();
				System.out.println("not overrided");
			}
			
		} else if(driver.getClass().getSimpleName().equalsIgnoreCase("IOSDriver")) {
			Thread.sleep(15000);
			
		} else if(xmlTest.getParameter("broName").equalsIgnoreCase("Edge")) {
			try
			{
			Thread.sleep(5000);
			driver.navigate().to("javascript:document.getElementById('continueLink').click()");
			System.out.println("overrided");
			Thread.sleep(5000);
			
			} catch(Exception e)
			{
				e.printStackTrace();
				System.out.println("not overrided");
			}
		}
		
	}
	
	public Select selectObj(WebElement element)
	{
		return new Select(element);
	}
	
	public boolean assertNotVisible(String str1,String str2)
	{
		try{
			BaseTest.l1.getWebElement(str1, str2).isDisplayed();
			
		}catch(Exception e){
			isVisible=true;
		}
		return isVisible;
	}
	
	public boolean assertVisible(String str1,String str2)
	{
		try{
			BaseTest.l1.getWebElement(str1, str2).isDisplayed();
			isVisible=true;
			
		}catch(Exception e){
			isVisible=false;
		}
		return isVisible;
	}
	
	public boolean assertVisible(String str1,String str2,int i)
	{
		try{
			BaseTest.l1.getWebElements(str1, str2).get(i).isDisplayed();
			isVisible=true;
			
		}catch(Exception e){
			isVisible=false;
		}
		return isVisible;
	}
	
	public String assertEqual(String str1,String str2)
	{
		try{
			assertEqual=BaseTest.l1.getWebElement(str1, str2).getText();
		}catch(Exception e){
			assertEqual="No Such Element";	
		}
		return assertEqual;
	}
	
	public String assertEqual(String str1,String str2,int i)
	{
		try{
			assertEqual=BaseTest.l1.getWebElements(str1, str2).get(i).getText();
		}catch(Exception e){
			assertEqual="No Such Element";	
		}
		return assertEqual;
	}
	
	public String assertEqual(String str1,String str2,String str3)
	{
		try{
			assertEqual=BaseTest.l1.getWebElement(str1, str2).getAttribute(str3);
		}catch(Exception e){
			assertEqual="No Such Element";	
		}
		return assertEqual;
	}
	
	public String getCssValue(String str1,String str2,String str3)
	{
		try{
			assertEqual=BaseTest.l1.getWebElement(str1, str2).getCssValue(str3);
		}catch(Exception e){
			assertEqual="No Such Element";	
		}
		return assertEqual;
	}
	
	public String assertEqual(String str1,String str2,String str3,int i)
	{
		try{
			assertEqual=BaseTest.l1.getWebElements(str1, str2).get(i).getAttribute(str3);
		}catch(Exception e){
			assertEqual="No Such Element";	
		}
		return assertEqual;
	}
	
	public void assertequalsIgnoreCase(String actual, String expected, String message)
	{
		BaseTest.sa.assertEquals(actual.toLowerCase(), expected.toLowerCase(), message);
	}
	
	public void assertequalsIgnoreCase(String actual, String expected)
	{
		BaseTest.sa.assertEquals(actual.toLowerCase(), expected.toLowerCase());
	}
	
	public String getSystemDate(String dateFormate)
	{
		//Fetch the system details and compair
		SimpleDateFormat dFormate = new SimpleDateFormat(dateFormate);
		Date date = new Date();
		dFormate.setTimeZone(TimeZone.getTimeZone("GMT"));
		String systemDate = dFormate.format(date);
		
		return systemDate;
	}
	
	//Generate 6digit random number
	public int generateRandomNum()
	{
		Random rand = new Random();
		int randomNumber = rand.nextInt(900000) + 100000;
		return randomNumber;
	}
}
