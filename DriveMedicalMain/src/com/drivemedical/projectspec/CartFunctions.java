package com.drivemedical.projectspec;

import java.util.List;

import org.openqa.selenium.WebElement;

import com.drivemedical.utilgeneric.BaseTest;



public class CartFunctions 
{

	public void clearCart() throws Exception
	{
		System.out.println("Click on Cart button in header");
		try 
		{
			BaseTest.l1.getWebElement("Header_Cart_Link", "Shopnav//header.properties").click();
		} 
		catch (Exception e) 
		{
			BaseTest.log.info("Cart link is not found");
		}
		Thread.sleep(3000);
		
		BaseTest.log.info("Count the number of rows");
		try 
		{
			List<WebElement> dotButtons = BaseTest.l1.getWebElements("Cart_DotButtons", "Cart//Cart.properties");
			System.out.println("Number of rows/dot buttons count in cart page:-  " + dotButtons.size());	
			
			for (int i = 0; i < dotButtons.size(); i++) 
			{
				System.out.println("LOOP:- " + i);
				System.out.println("Click Close icon button in cart page");
				BaseTest.l1.getWebElement("Cart_DotButtons", "Cart//Cart.properties").click();
			//	BaseTest.l1.getWebElement("Cart_RemoveLink", "Cart//Cart.properties").click();
			}
		} 
		catch (Exception e) 
		{
			BaseTest.sa.assertTrue(false,"Clear cart function is not completed");
		}
	}
}
