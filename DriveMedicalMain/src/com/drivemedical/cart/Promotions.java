package com.drivemedical.cart;

import org.testng.annotations.Test;
import org.testng.xml.XmlTest;

import com.drivemedical.utilgeneric.BaseTest;

public class Promotions extends BaseTest{

	Float actTotInt;
	
	@Test(groups={"reg"},description="DMED-043 DRM-762,764")
	public void TC00_cartPromo(XmlTest xmlTest) throws Exception
	{
		log.info("Click on Sign In Link");
		p.navigateToLoginPage();
		log.info("Log in to the application");
		p.logIn(xmlTest);
		log.info("Add item to cart which is having promo");
		s.navigateToCart("330011", 1, 1);
		log.info("verify the promotion in cart page");
		sa.assertTrue(gVar.assertVisible("Cart_Promo_Msg", "Cart//Cart.properties"),"Promotion Message");
		String expTot=l1.getWebElement("Cart_Item_Tot","Cart//Cart.properties").getText().trim();
		expTot=expTot.replace("$", "");
		String actTot=s.prodPrice.get(0);
		actTot=actTot.replace("$", "");
		actTotInt=new Float(actTot);
		actTotInt=actTotInt-10;
		sa.assertEquals(actTotInt+"",expTot,"total Price");
		log.info("verify Order Suntotal");
		sa.assertEquals(actTotInt+"", gVar.assertEqual("Cart_Total_Sec_Divs", "Cart//Cart.properties",1).replace("$", ""));
		log.info("verify Order Discount");
		sa.assertEquals("10", gVar.assertEqual("Cart_Total_Sec_Divs", "Cart//Cart.properties",3).replace("-$", ""));
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="DMED-043 DRM-763")
	public void TC01_checkoutPromo() throws Exception
	{
		log.info("click on proceed to check out button in cart page");
		l1.getWebElement("Cart_CheckoutButton", "Cart//Cart.properties").click();
		log.info("verify order subtotal in Checkout");
		sa.assertEquals(actTotInt+"", gVar.assertEqual("ShipTo_OrderSummary_Values", "Checkout//ShipTo.properties",0));
		log.info("verify Order Discount");
		sa.assertEquals("10", gVar.assertEqual("ShipTo_OrderSummary_Values", "Checkout//ShipTo.properties",1).replace("-$", ""));
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="DMED-043 DRM-767")
	public void TC02_invalidCoupon() throws Exception
	{
		log.info("navigate back to cart page");
		l1.getWebElement("Checkout_ReturnToCart_Link", "Checkout//Checkout.properties").click();
		log.info("enter invalid coupon code");
		l1.getWebElement("Cart_Coupon_Textbox", "Cart//Cart.properties").sendKeys("hello");
		log.info("click on apply");
		l1.getWebElement("Cart_Coupon_Apply_Btn", "Cart//Cart.properties").click();
		log.info("invalid error message");
		sa.assertTrue(gVar.assertVisible("Cart_Invalid_Coupon_Error_Msg", "Cart//Cart.properties"),"Error message");
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="DMED-043 DRM-766")
	public void TC03_validCoupon() throws Exception
	{
		log.info("clear cart items");
		cart.clearCart();
		log.info("Add item to cart which is having promo");
		s.navigateToCart("220011", 1, 1);
		log.info("enter valid coupon code");
		l1.getWebElement("Cart_Coupon_Textbox", "Cart//Cart.properties").sendKeys("Auto_Coupon");
		log.info("click on apply");
		l1.getWebElement("Cart_Coupon_Apply_Btn", "Cart//Cart.properties").click();
		log.info("valid message");
		sa.assertTrue(gVar.assertVisible("Cart_valid_Coupon_Error_Msg", "Cart//Cart.properties"),"success message");
		log.info("verify Order Discount");
		sa.assertEquals("10", gVar.assertEqual("Cart_Total_Sec_Divs", "Cart//Cart.properties",3).replace("-$", ""));
		String actTot=s.prodPrice.get(0);
		actTot=actTot.replace("$", "");
		actTotInt=new Float(actTot);
		actTotInt=actTotInt-10;
		log.info("verify Order Suntotal");
		sa.assertEquals(actTotInt+"", gVar.assertEqual("Cart_Total_Sec_Divs", "Cart//Cart.properties",1).replace("$", ""));
		sa.assertAll();
	}
	
	@Test(groups={"reg"},description="DMED-043 DRM-765")
	public void TC04_cartOrder(XmlTest xmlTest) throws Exception
	{
		log.info("clear cart items");
		cart.clearCart();
		log.info("Add item to cart which is having promo");
		s.navigateToCart("110077", 1, 2);
		log.info("verify the promotion in cart page");
		sa.assertTrue(gVar.assertVisible("Cart_Promo_Msg", "Cart//Cart.properties"),"Promotion Message");
		String expTot=l1.getWebElement("Cart_Item_Tot","Cart//Cart.properties").getText().trim();
		expTot=expTot.replace("$", "");
		String actTot=s.prodPrice.get(0);
		actTot=actTot.replace("$", "");
		actTotInt=new Float(actTot);
		actTotInt=actTotInt-20;
		sa.assertEquals(actTotInt+"",expTot,"total Price");
		log.info("verify Order Suntotal");
		sa.assertEquals(actTotInt+"", gVar.assertEqual("Cart_Total_Sec_Divs", "Cart//Cart.properties",1).replace("$", ""));
		log.info("verify Order Discount");
		sa.assertEquals("20", gVar.assertEqual("Cart_Total_Sec_Divs", "Cart//Cart.properties",3).replace("-$", ""));
		sa.assertAll();
	}
	
}
