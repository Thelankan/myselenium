package com.drivemedical.cart;

import java.security.Key;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;
import org.testng.xml.XmlTest;

import com.drivemedical.projectspec.ShopnavFunctions;
import com.drivemedical.utilgeneric.BaseTest;
import com.drivemedical.utilgeneric.GetData;

public class CartRegister extends BaseTest {

	String proColorInPDP;
	String proNameINPDP;

	@Test(groups={"reg"}, description="OOTB-097_DRM-642,643,644")
	public void TC00_verify_Previous_Prods_Test(XmlTest xmlTest) throws Exception
	{
		log.info("Log in to the application");
		l1.getWebElement("Header_SignIn_LINK", "Shopnav//header.properties").click();
		p.logIn(xmlTest);
		log.info("add item to cart");
		s.navigateToCart("110077",1, 1);
		log.info("verify navigation to cart");
		sa.assertTrue(gVar.assertVisible("Cart_Heading", "Cart//Cart.properties"),"Cart Heading");
		log.info("log out from application");
		act.moveToElement(l1.getWebElement("Header_Reg_MyAct_Link", "Shopnav//header.properties")).perform();
		l1.getWebElement("Header_SignOut_Link", "Shopnav//header.properties").click();
		log.info("log in to the application again");
		l1.getWebElement("Header_SignIn_LINK", "Shopnav//header.properties").click();
		p.logIn(xmlTest);
		log.info("verify cart quantity");
		sa.assertEquals(gVar.assertEqual("Header_Cart_Qty", "Cart//Cart.properties"),"1");
		log.info("Navigate to cart page");
		l1.getWebElement("Header_Cart_Link", "Cart//Cart.properties").click();
		log.info("verify saved products");
		sa.assertEquals(l1.getWebElements("Cart_Line_Items", "Cart//Cart.properties").size(),"2");
		for(int i=0;i<2;i++)
		{
			sa.assertEquals(s.prodnames.get(i),l1.getWebElements("Cart_Prod_Name", "Cart//Cart.properties").get(i).getText());
		}
		sa.assertAll();
	}
	
	
	@Test(groups={"reg"}, description="OOTB-096 DRM-712 DRM-713 DRM-714")
	public void TC01_uiOfCartPage(XmlTest xmlTest) throws Exception
	{
		
		log.info("Navigate to PDP");
		String searchData = GetData.getDataFromExcel("//data//GenericData.xls", "NoSearch", 3, 4);
		s.navigateToPDP_Search(searchData);
		
		proNameINPDP = l1.getWebElement("PDP_ProductName", "Shopnav//PDP.properties").getText();
		System.out.println("proNameINPDP:-   "+ proNameINPDP);
		
		String proPriceInPDP = l1.getWebElement("PDP_Price", "Shopnav//PDP.properties").getText();
		System.out.println("proPriceInPDP:-  "+ proPriceInPDP);
		
		String proQty = l1.getWebElement("PDP_Qty_Textbox", "Shopnav//PDP.properties").getAttribute("value");
		System.out.println("Product quantity in PDP:- "+proQty);
		
//		String proColorInPDP;
		if (gVar.assertVisible("PDP_ColorSwatchesList", "Shopnav//PDP.properties")) 
		{
			proColorInPDP = l1.getWebElement("PDP_SelectedColorSwatchImg", "Shopnav//PDP.properties").getAttribute("title");
			System.out.println("Product color in PDP:-  "+proColorInPDP);
		}
		log.info("Cick on ADD TO CART button");
		l1.getWebElement("PDP_AddToCart_button", "Shopnav//PDP.properties").click();
		Thread.sleep(3000);
		log.info("Navigate to CART page");
		l1.getWebElement("Header_Cart_Link", "Shopnav//header.properties").click();
		
		log.info("Verify the CART page ");
		sa.assertTrue(gVar.assertVisible("Cart_Heading", "Cart//Cart.properties"), "Verify the Cart Heading");
		sa.assertEquals(gVar.assertEqual("Breadcrumb_ActiveElement", "Generic//Generic.properties"), "Cart");
		sa.assertEquals(gVar.assertEqual("BreadcrumbElement", "Generic//Generic.properties"), "Home Cart");
		sa.assertTrue(gVar.assertVisible("Cart_Banner", "Cart//Cart.properties"), "Cart page Banner");
		
		sa.assertEquals(gVar.assertEqual("Cart_ExportCSV_Button", "Cart//Cart.properties"), "Export CSV");
		sa.assertEquals(gVar.assertEqual("Cart_SaveCart_SavedCart_txt", "Cart//Cart.properties"), "Save Cart");
		
		log.info("Verify the CArt item headings");
		sa.assertTrue(gVar.assertVisible("Cart_ItemLIst_Header", "Cart//Cart.properties"), "Cart page item headings");
		sa.assertEquals(gVar.assertEqual("Cart_ItemLIst_HeaderItem", "Cart//Cart.properties"), "Item");
		sa.assertEquals(gVar.assertEqual("Cart_ItemLIst_HeaderPrice", "Cart//Cart.properties"), "Price");
		sa.assertEquals(gVar.assertEqual("Cart_ItemLIst_HeaderQuantity", "Cart//Cart.properties"), "Qty");
		sa.assertEquals(gVar.assertEqual("Cart_ItemLIst_HeaderTotal", "Cart//Cart.properties"), "Unit");
		
		log.info("Verify the product details in cart page");
		sa.assertEquals(gVar.assertEqual("Cart_Prod_Name", "Cart//Cart.properties"), proNameINPDP);
		sa.assertEquals(gVar.assertEqual("Cart_Prod_ID", "Cart//Cart.properties"), proColorInPDP);
		sa.assertEquals(gVar.assertEqual("Cart_ItemQuantity", "Cart//Cart.properties"), proQty);
		sa.assertEquals(gVar.assertEqual("Cart_ItemPrice", "Cart//Cart.properties"), proPriceInPDP);
		sa.assertEquals(gVar.assertEqual("Cart_ItemTotal", "Cart//Cart.properties"), proPriceInPDP);
		sa.assertTrue(gVar.assertVisible("Cart_DotButtons", "Cart//Cart.properties"), "remove item button");
		
		log.info("Verify the Promo section in cart page");
		sa.assertTrue(gVar.assertVisible("Cart_PromoSection", "Cart//Cart.properties"), "Promo section");
		sa.assertTrue(gVar.assertVisible("Cart_PromoSection", "Cart//Cart.properties"), "Promo section");
		sa.assertEquals(gVar.assertEqual("Cart_PromoCode_link", "Cart//Cart.properties", "aria-expanded"), "true");
		sa.assertEquals(gVar.assertEqual("Cart_PromoCode_Label", "Cart//Cart.properties"), "Promo code");
		sa.assertEquals(gVar.assertEqual("Cart_PromoCode_Textbox", "Cart//Cart.properties", "placeholder"), "Enter promo code");
		sa.assertEquals(gVar.assertEqual("Cart_PromoCode_ApplyButton", "Cart//Cart.properties"), "Apply");
		
		sa.assertEquals(gVar.assertEqual("Cart_Subtotal_label", "Cart//Cart.properties"), "Subtotal:");
		sa.assertEquals(gVar.assertEqual("Cart_OrderTotal_label", "Cart//Cart.properties"), "Order Total");
		sa.assertEquals(gVar.assertEqual("Cart_Subtotal_Amount", "Cart//Cart.properties"), proPriceInPDP);
		sa.assertEquals(gVar.assertEqual("Cart_OrderTotal_Amount", "Cart//Cart.properties"), proPriceInPDP);
		
		log.info("Verify the Checkout button in cart page");
		sa.assertEquals(gVar.assertEqual("Cart_CheckoutButton", "Cart//Cart.properties"), " Proceed to CheckOut");
		
		sa.assertAll();
	}
	
	@Test(groups={"reg"}, description="OOTB-096 DRM-719")
	public void cartBannerImage()
	{
		log.info("Verify the banner image in cart page");
		sa.assertTrue(gVar.assertVisible("Cart_Banner_Image", "Cart//Cart.properties"),"Verify the banner image in Cart page");
		
	}
	
	@Test(groups={"reg"}, description="OOTB-096 DRM-724")
	public void TC02_verify_Product_Availability_MessageIn_Cart()
	{
		log.info("Verify the in stock message in cart page");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("Cart_ProductAvailabilityMsg", "Cart//Cart.properties"),"In Stock", "Verify the in stock message in cart page");
		
		sa.assertTrue(false, "Verify the Out Of Stock message in CART for different product. First Configure the OOS product");
		
	}
	
	@Test(groups={"reg"}, description="OOTB-096 DRM-712")
	public void TC03_clickOnProductImageInCartPAge(XmlTest xmlTest) throws Exception
	{
		log.info("Click on Product image");
		l1.getWebElement("Cart_LineItem_ProdImg", "Cart//Cart.properties").click();
		log.info("Verify the PDP");
		sa.assertTrue(gVar.assertVisible("PDP_Element", "Shopnav//PDP.properties"));
		sa.assertEquals(gVar.assertEqual("PDP_ProductName", "Shopnav//PDP.properties"), proNameINPDP);
		
		//###############################
		log.info("Navigate to Cart page");
		l1.getWebElement("Header_Cart_Link", "Shopnav//header.properties").click();
		log.info("Verify the Cart page");
		sa.assertTrue(gVar.assertVisible("Cart_Heading", "Cart//Cart.properties"),"Verify the cart page");
		
		log.info("Click on Product name");
		l1.getWebElement("Cart_Prod_Name", "Cart//Cart.properties").click();
		log.info("Verify the PDP");
		sa.assertTrue(gVar.assertVisible("PDP_Element", "Shopnav//PDP.properties"));
		sa.assertEquals(gVar.assertEqual("PDP_ProductName", "Shopnav//PDP.properties"), proNameINPDP);

		sa.assertAll();
	}
	
	
	@Test(groups={"reg"}, description="OOTB-096 DRM-725 DRM-726")
	public void TC04_saveCartFunctionality(XmlTest xmlTest) throws Exception
	{
		
		String cartId = l1.getWebElement("Cart_ID", "Cart//Cart.properties").getText();
		System.out.println("Cart Id:-  " + cartId);
		log.info("Click on SAVE CART button");
		l1.getWebElement("Cart_SaveCart_SavedCart_txt", "Cart//Cart.properties").click();
		
		log.info("Verify the Save cart popup");
		sa.assertTrue(gVar.assertVisible("Cart_SavedCart_Popup_text", "Cart//Cart.properties"),"Verify the save cart pop headline");
		
		log.info("Enter valid details in popup");
		String saveCardName = "TestSaveCard";
		String saveCardDisc = "TestSaveCard Discription";
		l1.getWebElement("Cart_SaveCart_Name_txtBox", "Cart//Cart.properties").sendKeys(saveCardName);
		l1.getWebElement("Cart_SaveCart_Discription_txtbox", "Cart//Cart.properties").sendKeys(saveCardDisc);
		l1.getWebElement("Cart_SaveCart_Save_btn", "Cart//Cart.properties").click();
		
		log.info("Verify the Successfull message");
		sa.assertTrue(gVar.assertVisible("Cart_SaveCart_SuccessMsg", "Cart//Cart.properties"),"Verify the success message");
		String exptMsg = "Cart"+" "+saveCardName+" "+"was successfully saved";
		sa.assertEquals(gVar.assertEqual("Cart_SaveCart_SuccessMsg", "Cart//Cart.properties"), exptMsg);
		
		log.info("Verify the Empty cart page");
		sa.assertTrue(gVar.assertVisible("Cart_EmptyCart_Msg", "Cart//Cart.properties"),"Empty Cart");
		
		log.info("Verify the number of saved carts link in Cart page");
		sa.assertEquals(gVar.assertEqual("Cart_SaveCart_SavedCart_txt", "Cart//Cart.properties"), "1 Saved Carts");
		
		
		log.info("Navigate to SAVE CART page");
		act.moveToElement(l1.getWebElement("Header_Reg_MyAct_Link", "Shopnav\\header.properties")).perform();
		l1.getWebElement("Header_SignOut_Link","Shopnav\\header.properties").click();
		
		log.info("Verify the product in Saved cart page");
		sa.assertEquals(gVar.assertEqual("SavedCart_Savedcart_name", "Profile//Savedcart.properties"), saveCardName);
		sa.assertEquals(gVar.assertEqual("SavedCart_CartID", "Profile//Savedcart.properties"), cartId);
		sa.assertEquals(gVar.assertEqual("SavedCart_Product_Description", "Profile//Savedcart.properties"), saveCardDisc);

		
		sa.assertAll();
	}
	
	
	
	@Test(groups={"reg"}, description="OOTB-096 DRM-729")
	public void TC05_updateProductQuantity() throws Exception
	{
		log.info("Navigate to Cart page");
		String searchData = GetData.getDataFromExcel("//data//GenericData.xls", "NoSearch", 3, 4);
		s.navigateToCart(searchData, 1, 1);
		String proPriceInPDP = s.prodPrice.get(0);
		System.out.println("Product price in PDP:-  "+proPriceInPDP);

		Double proPrice = new Double(proPriceInPDP.replace("$", ""));
		System.out.println("Product price double type:-  "+ proPrice);
		
//		log.info("Navigate to PDP");
//		String searchData = GetData.getDataFromExcel("//data//GenericData.xls", "NoSearch", 3, 4);
//		s.navigateToPDP_Search(searchData);
//		
//		proNameINPDP = l1.getWebElement("PDP_ProductName", "Shopnav//PDP.properties").getText();
//		System.out.println("proNameINPDP:-   "+ proNameINPDP);
//		
//		String proPriceInPDP = l1.getWebElement("PDP_Price", "Shopnav//PDP.properties").getText();
//		System.out.println("proPriceInPDP:-  "+ proPriceInPDP);
//		
//		String proQty = l1.getWebElement("PDP_Qty_Textbox", "Shopnav//PDP.properties").getAttribute("value");
//		System.out.println("Product quantity in PDP:- "+proQty);
//		
//		log.info("Cick on ADD TO CART button");
//		l1.getWebElement("PDP_AddToCart_button", "Shopnav//PDP.properties").click();
//		Thread.sleep(3000);
//		log.info("Navigate to CART page");
//		l1.getWebElement("Header_Cart_Link", "Shopnav//header.properties").click();
		
		
		log.info("Update cart quantity");
		int qty = 10;
		l1.getWebElement("Cart_QtyBox", "Cart//Cart.properties").sendKeys(Integer.toString(qty));
		l1.getWebElement("Cart_QtyBox", "Cart//Cart.properties").sendKeys(Keys.TAB);	//Remove focus from the quantity box
		
		Thread.sleep(5000);
		
		log.info("Verify the updated quantity");
		sa.assertEquals(l1.getWebElement("Cart_QtyBox", "Cart//Cart.properties").getAttribute("value"), qty+"");
		
		double exptTotPrice = proPrice*qty;
		System.out.println("Total price :- "+exptTotPrice);
		
		log.info("Verify the updated price in cart page");
		String actualTotPrice = l1.getWebElement("Cart_ItemTotal", "Cart//Cart.properties").getText().replace("$", "").replaceAll(",", "");
		System.err.println("Actual Total price:-  " + actualTotPrice);
		sa.assertEquals(actualTotPrice, exptTotPrice, "Verify the total line item price");
		
		
		sa.assertAll();
	}
	
	@Test(groups ={"reg"}, description="OOTB-096 DRM-715")
	public void TC_06_continueShoppingFunctionality() throws Exception
	{
		log.info("NAvigate to home page");
		l1.getWebElement("Header_Logo", "Shopnav//header.properties").click();
		
		log.info("Navigate to CArt page");
		l1.getWebElement("Header_Cart_Link", "Shopnav//header.properties").click();
		log.info("Verify the CART page");
		sa.assertTrue(gVar.assertVisible("Cart_Heading", "Cart//Cart.properties"),"Verify the CART page");
		
		log.info("Click on CONTINUE SHOPPING link in cart page");
		l1.getWebElement("Cart_Continue_Shopping_Link", "Cart//Cart.properties").click();
		
		log.info("Verify the home page");
		sa.assertTrue(gVar.assertVisible("Home_Identifier", "Shopnav//header.properties"),"Verify the HOME page");
		
		//####################
		log.info("NAvigate to PLP");
		String normalProduct = GetData.getDataFromExcel("//data//GenericData.xls", "Products", 0, 1);
		s.searchProduct(normalProduct);
		
		String plpHeader = l1.getWebElement("PLP_Heading", "Shopnav//PLP.properties").getText();
		log.info("Navigate to CArt page");
		l1.getWebElement("Header_Cart_Link", "Shopnav//header.properties").click();
		log.info("Verify the CART page");
		sa.assertTrue(gVar.assertVisible("Cart_Heading", "Cart//Cart.properties"),"Verify the CART page");

		log.info("Click on CONTINUE SHOPPING link in cart page");
		l1.getWebElement("Cart_Continue_Shopping_Link", "Cart//Cart.properties").click();
		
		log.info("Verify the NAvigated page");
		sa.assertTrue(gVar.assertVisible("PLP_Grid_Div", "Shopnav//PLP.properties"),"Verify the PLP");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("PLP_Heading", "Shopnav//PLP.properties"), plpHeader,"PLP heading");
		
		//####################
		log.info("NAvigate to PDP");
		s.navigateToPDP_Search(normalProduct);
		
		log.info("Verify the PDP");
		sa.assertTrue(gVar.assertVisible("PDP_Element", "Shopnav//PDP.properties"),"Verify the PDP");
		String proNameINPDP = l1.getWebElement("PDP_ProductName", "Shopnav//PDP.properties").getText();
		
		log.info("Navigate to CArt page");
		l1.getWebElement("Header_Cart_Link", "Shopnav//header.properties").click();
		log.info("Verify the CART page");
		sa.assertTrue(gVar.assertVisible("Cart_Heading", "Cart//Cart.properties"),"Verify the CART page");

		log.info("Click on CONTINUE SHOPPING link in cart page");
		l1.getWebElement("Cart_Continue_Shopping_Link", "Cart//Cart.properties").click();
		
		log.info("Verify the Navigated PAGE");
		sa.assertTrue(gVar.assertVisible("PDP_Element", "Shopnav//PDP.properties"),"Verify the PDP");
		gVar.assertequalsIgnoreCase(gVar.assertEqual("PDP_ProductName", "Shopnav//PDP.properties"), proNameINPDP,"Verify the PDP name");
		
		sa.assertAll();
	}

	@Test(groups={"reg"}, description="OOTB-096 DRM-728")
	public void TC06_removeLineItem() throws Exception
	{
		
		log.info("Navigate to Cart page");
		String searchData = GetData.getDataFromExcel("//data//GenericData.xls", "NoSearch", 3, 4);
		s.navigateToCart(searchData, 1, 1);
		
		log.info("Clear cart item");
		cart.clearCart();
		
		log.info("Verify the empty cart page after removing the line item");
		sa.assertTrue(gVar.assertVisible("Cart_EmptyCart_Msg", "Cart//Cart.properties"),"Empty Cart");
	}
	
	
	@Test(groups={"reg"}, description="OOTB-096 DRM-730")
	public void TC07_verifyUIofEmptyCartPage()
	{
		log.info("Verify the Empty cart page heading");
		sa.assertTrue(gVar.assertVisible("Cart_EmptyCart_Msg", "Cart//Cart.properties"), "Verify the Empty cart page heading");
		
		log.info("Verify the START shopping button in empty cart page");
		sa.assertTrue(gVar.assertVisible("Cart_Start_Shopping_btn", "Cart//Cart.properties"),"Verify the START SHOPPING button");
		
		sa.assertTrue(gVar.assertVisible("Cart_Heading", "Cart//Cart.properties"),"Verify the cart page HEADING");
		sa.assertTrue(false, "Verify the CONTINUE SHOPPING link in below the cart header");
	}
	
	
	@Test(groups={"reg"}, description="OOTB-096 DRM-731")
	public void TC08_startShoppingInEmptyCart() throws Exception
	{
		
		log.info("Click on START SHOPPING button");
		l1.getWebElement("Cart_Start_Shopping_btn", "Cart//Cart.properties").click();
		
		log.info("Verify the navigated Home page");
		sa.assertTrue(gVar.assertVisible("Home_Identifier", "Shopnav//header.properties"),"Verify navigated HOME PAGE");
		
	}
	
	
	@Test(groups={"reg"}, description="OOTB-096 DRM-716")
	public void TC09_navigateToCheckout(XmlTest xmlTest) throws Exception
	{
		log.info("Navigate to Cart page");
		String searchData = GetData.getDataFromExcel("//data//GenericData.xls", "NoSearch", 3, 4);
		s.navigateToCart(searchData, 1, 1);
		String proPriceInPDP = s.prodPrice.get(0);
		System.out.println("Product price in PDP:-  "+proPriceInPDP);

		Double proPrice = new Double(proPriceInPDP.replace("$", ""));
		System.out.println("Product price double type:-  "+ proPrice);
		
		log.info("Click on Checkout button");
		l1.getWebElement("Cart_CheckoutButton", "Cart//Cart.properties").click();
		
		log.info("Verify the Checkout page");
		sa.assertTrue(gVar.assertVisible("Checkout_Heading", "Checkout//Checkout.properties"),"Verify the CHECKOUT page");
		
		log.info("Click on RETURN TO CART link");
		l1.getWebElement("Checkout_ReturnToCart_Link", "Checkout//Checkout.properties").click();
		
		log.info("Verify the CART page");
		sa.assertTrue(gVar.assertVisible("Cart_Heading", "Cart//Cart.properties"),"Verify the CART page");
		
		sa.assertAll();
	}
	
	//Precondition:- Product should contain multiple UOM option
	@Test(groups={"reg"}, description="OOTB-096 DRM-721")
	public void TC10_modifyLineItemUOMinCart() throws Exception
	{
		int uomOptionsCount=0;
		boolean uomTrue = false;
		List<String> proPrice = new ArrayList<String>();
		
		log.info("Navigate to PDP");
		String searchData = GetData.getDataFromExcel("//data//GenericData.xls", "NoSearch", 3, 4);
		s.navigateToPDP_Search(searchData);
		
		proNameINPDP = l1.getWebElement("PDP_ProductName", "Shopnav//PDP.properties").getText();
		System.out.println("proNameINPDP:-   "+ proNameINPDP);
		
//		String proPriceInPDP = l1.getWebElement("PDP_Price", "Shopnav//PDP.properties").getText();
//		System.out.println("proPriceInPDP:-  "+ proPriceInPDP);
		
		log.info("Verify the UOM in PDP");
		Thread.sleep(2000);
		if (gVar.assertVisible("PDP_UOM_SelectBox", "Shopnav//PDP.properties")) 
		{
			Select uomOptions = new Select(l1.getWebElement("PDP_UOM_SelectBox", "Shopnav//PDP.properties"));
			List<WebElement> uomOptionsList = uomOptions.getOptions();
			uomOptionsCount = uomOptionsList.size();
			System.out.println("UOM options count:- "+ uomOptionsCount);
			
			if (uomOptionsCount>1) 
			{
				uomTrue = true;
				String uomSelectedOption = l1.getWebElement("PDP_UOM", "Shopnav//PDP.properties").getAttribute("title");
				System.out.println("uomSelectedOption:-  " + uomSelectedOption);
				
				for (int i = 0; i < uomOptionsCount; i++) 
				{
					System.out.println("LOOP:- " + i);
					String Options = uomOptionsList.get(i).getText();
					System.out.println("Options:-  " + Options);

					uomOptions.selectByIndex(i);
					String uomSelectedOptionAfter = l1.getWebElement("PDP_UOM", "Shopnav//PDP.properties").getAttribute("title");
					System.out.println("uomSelectedOptionAfter:-  "+ uomSelectedOptionAfter);
					gVar.assertequalsIgnoreCase(uomSelectedOptionAfter, Options, "Verify the selected UOM options");
					
					log.info("Collect the Product price");
					proPrice.add(l1.getWebElement("PDP_Price", "Shopnav//PDP.properties").getText());
					
				}
			}
			else 
			{
				sa.assertTrue(false,"Only one option is there in UOM dropdown box, configure UOM with more option");
			}
		}
		else 
		{
			sa.assertTrue(false, "UOM is not displaying in PDP");
		}
		
		
		log.info("Cick on ADD TO CART button");
		l1.getWebElement("PDP_AddToCart_button", "Shopnav//PDP.properties").click();
		Thread.sleep(3000);
		log.info("Navigate to CART page");
		l1.getWebElement("Header_Cart_Link", "Shopnav//header.properties").click();
		
		log.info("Verify the UOM in cart page");
		if (uomTrue) 
		{
			sa.assertTrue(gVar.assertVisible("Cart_UOMbox", "Cart//Cart.properties"));
			Select uomInCart = new Select(l1.getWebElement("Cart_UOMbox", "Cart//Cart.properties"));
			List<WebElement> uomOptionsInCart = uomInCart.getOptions();
			for (int i = 0; i < uomOptionsCount; i++) 
			{
				String uomOption = uomOptionsInCart.get(i).getText();
				System.out.println("uomOption:- "+uomOption);
				uomInCart.selectByIndex(i);
				Thread.sleep(2000);
				String selectedUomInCart = l1.getWebElement("Cart_SelectedUOM", "Cart//Cart.properties").getAttribute("title");
				System.out.println("selectedUomInCart:-  "+ selectedUomInCart);
				
				log.info("Verify the Selected UOM option in cart page");
				gVar.assertequalsIgnoreCase(selectedUomInCart, uomOption,"Verify the selected UOM options");
				
				log.info("Verify the product price on selecting the UOM options");
				String proPriceInCart = l1.getWebElement("Cart_ItemPrice", "Cart//Cart.properties").getText();
				System.out.println("proPriceInCart:-  "+ proPriceInCart);
				
				sa.assertEquals(proPriceInCart, proPrice,"Verify the pro price in cart with PDP for different UOMs");
				
			}
		}
		else 
		{
			sa.assertTrue(false, "UOM in cart is not having dropdown values");
		}
		
		sa.assertAll();
	}
	
}
