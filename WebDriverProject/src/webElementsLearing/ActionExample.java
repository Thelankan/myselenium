package webElementsLearing;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;


public class ActionExample {

	public static void main(String[] args) {
		//Test Data
				String userName="admin";
				String password="manager";
				
				//Launch App and login to App
				WebDriver driver=new FirefoxDriver();
				driver.get("http://127.0.0.1:81/login.do");
				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
				driver.manage().window().maximize();
				
				driver.findElement(By.name("username")).sendKeys(userName);
				driver.findElement(By.name("pwd")).sendKeys(password);
				//driver.findElement(By.xpath("//input[@type='submit']")).click();
				Actions action=new Actions(driver);
				action.sendKeys(Keys.ENTER).perform();
				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
				driver.findElement(By.linkText("Users")).click();
				
				Actions act=new Actions(driver);
				act.moveToElement(driver.findElement(By.xpath("//td[a[text()='bc, ab (XYZ)']]/following-sibling::td[1]"))).perform();
			
			System.out.println(driver.findElement(By.xpath("//td[a[text()='bc, ab (XYZ)']]/following-sibling::td[1]")).getAttribute("title"));
				

	}

}
