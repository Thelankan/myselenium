package webElementsLearing;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;

public class Screnshot {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		WebDriver driver=new FirefoxDriver();
		driver.navigate().to("http://dilip:8080/login.do");
		
		EventFiringWebDriver edriver=new EventFiringWebDriver(driver);
		File srcImg=edriver.getScreenshotAs(OutputType.FILE);
		File dest=new File("E:\\selenium\\WebDriverProject\\Output");
		
		FileUtils.copyFileToDirectory(srcImg, dest);
		

	}

}
