package webElementsLearing;

//import java.awt.List;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.security.Credentials;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AlertHandling {

	public static void main(String[] args) {
		
		
		WebDriver driver = new FirefoxDriver();
		driver.get("http://www.businessenergyshop.com/");
		driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
		
		driver.findElement(By.xpath("//a[@href='/sub/display/formid-104/tmpl-component']")).click();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		
		
		//WebElement iframe = driver.findElement(By.xpath("//iframe[@src='/sub/display/formid-104/tmpl-component']"));
		//driver.switchTo().frame(iframe);
		
		driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@src='/sub/display/formid-104/tmpl-component']")));
		driver.findElement(By.name("user[name]")).sendKeys("dilip");
		driver.findElement(By.name("user[email]")).sendKeys("dil@gmail.com");
		driver.findElement(By.name("Submit")).click();
//		//String originalHandle = driver.getWindowHandle();
//		driver.findElement(By.xpath("//span[text()='Subscribe Newsletter']")).click();
//		
//		//driver.switchTo().frame(driver.findElement(By.xpath("//span[text()='Subscribe Newsletter']")));
//		//driver.findElements(By.tagName("iframe")).get(0);
//		driver.switchTo().frame(3);
//		driver.findElement(By.xpath("//input[@value='Name']")).sendKeys("dilip");
		
		
//		
//		List<WebElement> nofrm=driver.findElements(By.tagName("iframe")).get(0);
//		System.out.println(nofrm.size());
//		driver.findElement(By.xpath("//span[text()='Subscribe Newsletter']")).click();
//		System.out.println("================");
//		driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
//		driver.switchTo().frame(1);
		
	/* 1	WebDriver driver = new FirefoxDriver();
		driver.get("http://www.businessenergyshop.com/");
		driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
		driver.findElement(By.xpath("//span[text()='Subscribe Newsletter']")).click();
		Set<String> set = driver.getWindowHandles();
		System.out.println(set.size());
		Iterator<String> it = set.iterator();
		String pid = it.next();
		String cid = it.next();
		System.out.println(pid+"///"+cid);
		driver.switchTo().window(cid);
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		WebElement wb = driver.findElement(By.id("businessType"));
		Select sel = new Select(wb);
		List<WebElement> lst = sel.getOptions();
		for(int i=0; i<lst.size();i++)
		{
		System.out.println(lst.get(i).getText());
		}
		driver.close();
		driver.switchTo().window(pid);
		driver.findElement(By.xpath("//a[@href='/sub/display/formid-104/tmpl-component']")).click();
//		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
//		set = driver.getWindowHandles();
//		System.out.println(set.size());
		//WebElement wbEle=driver.findElement(By.xpath("//input[@value='Name']"));
		//driver.switchTo().alert();
		WebDriverWait wait =new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.xpath("//iframe[@src='/sub/display/formid-104/tmpl-component']")));
		//wait.until(ExpectedConditions.alertIsPresent());
		
		driver.switchTo().frame(driver.findElement(By.xpath("//input[@value='Name']")));
		
		driver.findElement(By.xpath("//input[@value='Name']")).sendKeys("dilip");  
		1*/
	/*	//Test Data
		String userName="admin";//iframe[@src='/sub/display/formid-104/tmpl-component']
		String password="manager";
		
		//Launch App and login to App
		WebDriver driver=new FirefoxDriver();
		driver.get("http://dilip:8080/login.do");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		
		driver.findElement(By.name("username")).sendKeys(userName);
		driver.findElement(By.name("pwd")).sendKeys(password);
		driver.findElement(By.xpath("//input[@type='submit']")).click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.findElement(By.linkText("Projects & Customers")).click();
		driver.findElement(By.linkText("HDFC")).click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.findElement(By.xpath("//input[@value='Delete This Customer']")).click();
		//confirmation pop-up
	Alert	alt =driver.switchTo().alert();
	System.out.println(alt.getText());
		//alt.accept();
		alt.dismiss();
		*/
		
	}

}
