package webElementsLearing;

import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class IrctcExample {

	public static void main(String[] args){
		
		//step 1:Launch browser and navigate to IRCTC application
		WebDriver driver = new FirefoxDriver();
		driver.get("https://www.irctc.co.in/");
		
		//step 2:click on Tourist Train link, which will open new window
		driver.findElement(By.linkText("Tourist Train")).click();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		
		//step 3:To get current existing windows id
		Set<String> set =driver.getWindowHandles();
		Iterator<String> itr = set.iterator();
		String parentId = itr.next();
		String childId = itr.next();
		
		//step 4:Pass control to child or new window opened
		driver.switchTo().window(childId);
		
		//step 5:Perform action on child window (means click on 'Know More' button correspond to 'Bharat Darshan' link)
		driver.findElement(By.xpath("//tbody[tr[td[h2[contains(text(),'Bharat Darshan')]]]]/descendant::input[1]")).click();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		
		//step 6:once u click on 'Know More' button one more new window will open so again u need to get windows id and pass control to newly opened window
		Set<String> set1 =driver.getWindowHandles();
		Iterator<String> itr1 = set1.iterator();
		String parentId1 = itr1.next();
		String childId1 = itr1.next();
		String childId2 = itr1.next();
		driver.close();
		
		//step 7:pass driver control to newly opened window n perform action(i,e. click on 'Close' button
		driver.switchTo().window(childId2);
		driver.findElement(By.xpath("//a[text()='Close']")).click();
		
		//step 8:To pass driver control to parent window
		driver.switchTo().window(parentId1);
		
		//step 9:Now perform action on parent window(means here i clicked on 'Tour Packages' link present in parent window 
		driver.findElement(By.linkText("Tour Packages")).click();
		

	}

}
