package webElementsLearing;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Sampletable {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		//step:-1 launch app 
		WebDriver driver=new FirefoxDriver();
		driver.get("http://www.nytimes.com/");
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		//step:-2 navigate to most emailed

		driver.findElement(By.id("mostPopTabMostEmailed")).click();

		// step:-3 write xpath to get 10 most emailed news

		//String wbxpath="//div[div[ul[li[a[text()='MOST EMAILED']]]]]/div[2]/table/tbody/tr/td[2]"; 
		String wbxpath="//tbody[tr[td[@class='mostPopularTitle']]]/tr";
		//get all top new from the webtable

		List<WebElement> wblistt=driver.findElements(By.xpath(wbxpath));
		//System.out.println(wblistt.size());
		for(int i=0; i<wblistt.size();i++)
		{
		System.out.println(wblistt.get(i).getText());
		}

	}

}
