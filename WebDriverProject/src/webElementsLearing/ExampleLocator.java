package webElementsLearing;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ExampleLocator {

	public static void main(String[] args) throws InterruptedException {
	
		//Test Data
				String userName="admin";
				String password="manager";
				
				//Launch App and login to App
				WebDriver driver=new FirefoxDriver();
				driver.get("http://dilip:8080/login.do");
				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
				driver.manage().window().maximize();
				driver.switchTo().window("gdh");//window handle
				driver.findElement(By.name("username")).sendKeys(userName);
				driver.findElement(By.name("pwd")).sendKeys(password);
				driver.findElement(By.xpath("//input[@type='submit']")).click();
				
				Thread.sleep(2000);
				//Explicit wait
//				WebDriverWait wait=new WebDriverWait(driver, 10);
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("q")));
//				
			List<WebElement>	list=driver.findElements(By.tagName("select"));
			System.out.println(list.size());
//			for(int i=0;i<list.size();i++)
//			{
//				System.out.println(list.get(i).getText());
//			}
			Select sel=new Select(driver.findElement(By.name("customerProject.shownCustomer")));
			
			
			System.out.println(sel.isMultiple());
		List<WebElement>	lst=sel.getOptions();
		
		System.out.println(lst.size());	
		for(int i=0;i<lst.size();i++)
		{
			System.out.println(lst.get(i).getText());
		}
		//System.out.println(sel.toString());
		sel.selectByIndex(0);//only multiple select box
		sel.selectByIndex(1);
		sel.selectByIndex(7);
		System.out.println("..........");
		System.out.println(sel.getFirstSelectedOption().getText());
		System.out.println("..........");
		List<WebElement> li=sel.getAllSelectedOptions();
		for(int i=0;i<li.size();i++)
		{
			System.out.println(li.get(i).getText());
		}
		System.out.println("..........");
	}

}
