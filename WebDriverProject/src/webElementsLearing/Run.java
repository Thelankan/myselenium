package webElementsLearing;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Run {

	public static void main(String[] args) {
		
		//WebDriver d=new FirefoxDriver();
		
		System.setProperty("webdriver.chrome.driver", "");
		WebDriver d=new ChromeDriver();
		
		d.get("http://dilip:8080/login.do");
		d.findElement(By.name("username")).sendKeys("admin");
		d.findElement(By.name("pwd")).sendKeys("manager");
		d.findElement(By.xpath("//input[@type='submit']")).click();
		
		d.findElement(By.xpath("//input[@value='Add New Tasks']")).click();
		d.findElement(By.xpath("//select[@name='customerId']")).sendKeys("ICICI");
		d.findElement(By.xpath("//img[@src='/img/default/top_nav/logout.gif']")).click();
	}

}


