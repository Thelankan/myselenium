package webElementsLearing;

import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class AssignmentSample {
	
	static WebDriver driver;
	static String parentId = null;
	static String childId =null;
	
	public static void main(String[] args) {
				
				//Test Data
				String userName="admin";
				String password="manager";
				String user="asdfghjk";
				String fname="ab";
				String lname="bc";
				String expOption=lname+","+" "+fname+" "+"("+user+")";
				
				//Launch Application and login to Application
				driver=new FirefoxDriver();
				driver.get("http://127.0.0.1:81/login.do");
				driver.findElement(By.name("username")).sendKeys(userName);
				driver.findElement(By.name("pwd")).sendKeys(password);
				driver.findElement(By.xpath("//input[@type='submit']")).click();
				
				driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
				
				//Navigate to user page
				driver.findElement(By.linkText("Users")).click();
				
				//Create user
				driver.findElement(By.xpath("//input[@value='Add New User']")).click();
				
				driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
				
				driver.findElement(By.name("username")).sendKeys(user);
				driver.findElement(By.name("passwordText")).sendKeys("12345");
				driver.findElement(By.name("passwordTextRetype")).sendKeys("12345");
				driver.findElement(By.name("firstName")).sendKeys(fname);
				driver.findElement(By.name("lastName")).sendKeys(lname);
				driver.findElement(By.xpath("//input[@type='submit']")).click();
				
				//Navigate to user detail page and capture the details
				driver.findElement(By.partialLinkText(user)).click();
				
				driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
				
				String userSelected= driver.findElement(By.xpath("//td[contains(text(),'You have sele')]/following-sibling::td/span")).getText();
				String actualUser =driver.findElement(By.name("username")).getAttribute("value");
				String actualfname=driver.findElement(By.name("firstName")).getAttribute("value");
				String actuallname=driver.findElement(By.name("lastName")).getAttribute("value");
				
				//verify user details
				if(user.equals(actualUser) && fname.equals(actualfname) && lname.equals(actuallname))
				{
					System.out.println("User created:  "+userSelected);
					System.out.println("Test case Passed(User detail verified)");
					System.out.println("name:  "+actualUser);
					System.out.println("firstname:  "+actualfname);
					System.out.println("lastname:  "+actuallname);
				}
				else
				{
					System.out.println("Test case Failed(User detail not match)");
				}
				
				//Navigate to time track page
				driver.findElement(By.linkText("Time-Track")).click();
				
				driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
				
				//add task to already created user
				Select sel = new Select(driver.findElement(By.xpath("//select[@name='selectedUser']")));
				List<WebElement> list=sel.getOptions();
				
				
				for(int i=0;i<list.size();i++)
				{
					String actOption=list.get(i).getText();
					if(expOption.equals(actOption))
					{
						sel.selectByVisibleText(actOption);
						break;
					}
				}
				
				//sel.selectByVisibleText(expOption);(instead of for loop we can use this single line statement also)
				driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
				driver.findElement(By.xpath("//a[contains(text(),'Add tasks to the list')]")).click();
				driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
				
				Set<String> set =driver.getWindowHandles();
				Iterator<String> itr = set.iterator();
				parentId = itr.next();
				childId = itr.next();
				
				//switch to new window
				driver.switchTo().window(childId);
				
				driver.findElement(By.xpath("//input[@value='Show Tasks']")).click();
				driver.findElement(By.xpath("//input[@name='selected_5']")).click();
				 String expTask=driver.findElement(By.xpath("//td[input[@name='selected_5']]/preceding-sibling::td[1]")).getText();
				driver.findElement(By.xpath("//input[@value='Add Selected Tasks to the List']")).click();
				driver.switchTo().window(parentId);
				driver.findElement(By.xpath("//input[@id='SubmitTTButton']")).click();
				driver.findElement(By.linkText(expTask)).click();
				driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
				
				Set<String> set1 =driver.getWindowHandles();
				Iterator<String> itr1 = set1.iterator();
				 parentId = itr1.next();
				 childId = itr1.next();
				
				driver.switchTo().window(childId);
				String actTask=	driver.findElement(By.xpath("//td[contains(text(),'You have sele')]/following-sibling::td/span")).getText();
				driver.findElement(By.xpath("//a[text()='Close Window']")).click();
				driver.switchTo().window(parentId);
				if(expTask.equals(actTask))
				{
				
					System.out.println("Test case Passed");
					
				}
				else
				{
					System.out.println("Test case Failed");
				}
				
				
				//Logout from application
				driver.findElement(By.xpath("//img[@class='logoutImg']")).click();
			
	}
	
}


