package webElementsLearing;

import java.io.File;



import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;



public class SampleSelect {

	public static void main(String[] args) throws InterruptedException, IOException {
		
		WebDriver driver=new FirefoxDriver();
		//driver.get("http://dilip/login.do");
		driver.navigate().to("http://dilip:8080/login.do");
		File screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		  FileUtils.copyFile(screenshot, new File("D:\\screenshot.jpg"));
		  System.out.print("Screenshot is captured and stored in your D: Drive");
		driver.findElement(By.name("username")).sendKeys("admin");
		driver.findElement(By.name("pwd")).sendKeys("manager");
		driver.findElement(By.xpath("//input[@type='submit']")).click();
		

		Thread.sleep(2000);
		
		//Create customer detail
		driver.findElement(By.linkText("Projects & Customers")).click();
		Thread.sleep(2000);
		driver.navigate().back();
		Thread.sleep(2000);
	
		driver.navigate().forward();
	
	System.out.println(driver.getCurrentUrl());
	System.out.println(driver.getPageSource());
	System.out.println(driver.getTitle());
	
	JavascriptExecutor javascript = (JavascriptExecutor) driver;
	  String DomainUsingJS=(String)javascript.executeScript("return document.domain");  
	  System.out.println("My Current URL Is  : "+DomainUsingJS);
	}
	
	
	
	
	
	
	

}
