package webElementsLearing;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class CreateCustomerAndVerifyDetails {

	public static void main(String[] args) throws InterruptedException {
		
		//Test Data
		String userName="admin";
		String password="manager";
		String expectedCustomerName="Vijaya";
		String expectedCustomerStatus="Active";
		
		//Launch App and login to App
		WebDriver driver=new FirefoxDriver();
		driver.get("http://127.0.0.1:81/login.do");
		driver.findElement(By.name("username")).sendKeys(userName);
		driver.findElement(By.name("pwd")).sendKeys(password);
		driver.findElement(By.xpath("//input[@type='submit']")).click();
		
		Thread.sleep(2000);
		
		//Create customer detail
		driver.findElement(By.linkText("Projects & Customers")).click();
		
		Thread.sleep(2000);
		
		driver.findElement(By.xpath("//input[@value='Add New Customer']")).click();
		
		Thread.sleep(2000);
		
		driver.findElement(By.name("name")).sendKeys(expectedCustomerName);
		driver.findElement(By.xpath("//input[@value='Create Customer']")).click();
		
		Thread.sleep(2000);
		
		//verify customer detail
		driver.findElement(By.linkText(expectedCustomerName)).click();
		
		Thread.sleep(2000);
		
		String actualCustomerName= driver.findElement(By.xpath("//td[contains(text(),'ve selected cus')]/following-sibling::td/span")).getText();
		String actualCustomerStatus= driver.findElement(By.xpath("//td[contains(text(),'er status')]/following-sibling::td")).getText();
		
		if(expectedCustomerName.equals(actualCustomerName) && expectedCustomerStatus.equals(actualCustomerStatus))
		{
			System.out.println("Test case Passed");
		}
		else
		{
			System.out.println("Test case Failed");
		}
		
		
		//Logout from App and close Browser
		driver.findElement(By.className("logoutImg")).click();
		driver.quit();
		
		
		
		
		
	}

}
