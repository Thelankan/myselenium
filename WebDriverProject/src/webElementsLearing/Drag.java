package webElementsLearing;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;

public class Drag {
public static void main(String args[]){
//Open website
WebDriver driver=new FirefoxDriver();
driver.get("http://www.w3schools.com/html/html5_draganddrop.asp");


//Create source webelement and destination webelement
WebElement srcwb=driver.findElement(By.xpath("//img[@id='drag1']"));
WebElement dstwb=driver.findElement(By.xpath("//div[@id='div2']"));

//pass these elements to dragAnddrop method of actions class
//Create object of actions class
Actions act=new Actions(driver);

act.dragAndDrop(srcwb,dstwb).perform();
//driver.quit();
}

}