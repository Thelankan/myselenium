package com.prevail.profile;


import java.util.ArrayList;
import java.util.List;






import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.prevail.projectspec.ProfileFunctions;
import com.prevail.utilgeneric.BaseTest;
import com.prevail.utilgeneric.GetData;

public class PaymentSettings extends BaseTest{

	@Test(priority=1)
	public void PaymentSettings_UI() throws Exception
	{
		p.loginToAppliction();
		l1.getWebElement("PaymentSettings_Link_MyaccountLandingPage","Profile\\PaymentSettings.properties").click();
		p.DeleteCreditCard();
		l1.getWebElement("PaymentSettings_AddCreditCard_Button","Profile\\PaymentSettings.properties").click();
		p.AddCreditCardOverlay_UI();
	}
	
	@Test(priority=2)
	public void PaymentSettings_AddCreditCard() throws Exception
	{
		p.CreateCreditCard("data\\ProfileData.xls","CreditCard",1);
		l1.getWebElement("PaymentSettings_ApplyButton","Profile\\PaymentSettings.properties").click();
		sa.assertTrue(l1.getWebElement("PaymentSettings_Payment_List", "Profile\\PaymentSettings.properties").isDisplayed());
	}

}
