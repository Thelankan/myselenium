package com.prevail.profile;


import java.util.ArrayList;
import java.util.List;







import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.prevail.projectspec.ProfileFunctions;
import com.prevail.utilgeneric.BaseTest;
import com.prevail.utilgeneric.GetData;

public class Address extends BaseTest{
	
	@Test(priority=1)
	public void AddressOverlayUI_TCID() throws Exception
	{
		p.loginToAppliction();
		System.out.println("app logged in");
		l1.getWebElement("Address_Link_AccountLandingPage","Profile\\Addresses.properties").click();
		p.DeleteAddress();
		l1.getWebElement("Address_CreateNew_Button","Profile\\Addresses.properties").click();
		p.AddressOverlay_UI();
	}
	
//	@Test(priority=2)
//	public void EnterAddress_TCID() throws Exception
//	{
//		p.EnterAddressValues("data\\ProfileData.xls","Address",1);
//		l1.getWebElement("Address_ApplyButton","Profile\\Addresses.properties").click();
//		sa.assertTrue(l1.getWebElement("Address_DefaultAddress_Heading", "Profile\\Addresses.properties").isDisplayed());
//		//It will return all the assertions
//		sa.assertAll();
//	}
	
//	@Test(priority=3)
//	public void Validations_TCID() throws Exception
//	{
//		
//		p.EnterAddressValues("data\\ProfileData.xls","Address",1);
//		l1.getWebElement("Address_ApplyButton","Profile\\Addresses.properties").click();
//		sa.assertTrue(l1.getWebElement("Address_DefaultAddress_Heading", "Profile\\Addresses.properties").isDisplayed());
//		//It will return all the assertions
//		sa.assertAll();
//	}
	
	
}