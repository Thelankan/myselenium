package com.prevail.profile;


import java.util.ArrayList;
import java.util.List;




import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.prevail.projectspec.ProfileFunctions;
import com.prevail.utilgeneric.BaseTest;
import com.prevail.utilgeneric.GetData;

public class Register extends BaseTest{
	
	
	@Test(priority=1)
	public void Register_UI_() throws Exception
	{
		
		l1.getWebElement("MyAccountIcon","Profile\\Register.properties").click();
		l1.getWebElements("Register_Myaccount_Elements","Profile\\Register.properties").get(1).click();
		p.CreateAccount_UI();
		
	}
	
	@Test(priority=2)
	public void Register_CreateAccount() throws Exception
	{
		
		p.CreateAccount("data\\ProfileData.xls","CreateAccount",1);
		l1.getWebElement("Register_ApplyButton","Profile\\Register.properties").click();
		//to check whether account is created or not
		sa.assertTrue(l1.getWebElement("Register_LoginConfirmation", "Profile\\Register.properties").isDisplayed());
		//It will return all the assertions
		sa.assertAll();
		
	}
	
//	@Test
//	public void Register_TestCase_004() throws Exception
//	{
//		
//	}
	
//	@Test
//	public void Register_TestCase_004() throws Exception
//	{
//		
//	}

	
}

