package com.prevail.projectspec;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.xml.XmlTest;

import com.prevail.utilgeneric.GetData;

public class GlobalFunctions {
	
	WebDriver driver;
	
	public GlobalFunctions(WebDriver driver)
	{
		this.driver=driver;
	}
	
	public void navigateToSite(XmlTest xmlTest) throws Exception
	{
		driver.get(new GetData().getDataFromProperties(System.getProperty("user.dir")+"\\data\\sitedata.properties", "URL"));
		//driver.getClass().getName().indexOf("ie")>0
		if(xmlTest.getParameter("broName").equalsIgnoreCase("ie"))
		{
			try
			{
				Thread.sleep(5000);
				driver.findElement(By.id("overridelink")).click();
				System.out.println("overrided");
				Thread.sleep(5000);
			}
			catch(Exception e)
			{
				e.printStackTrace();
				System.out.println("not overrided");
			}
		}
		
//		Thread.sleep(10000);
//		boolean b=driver.findElement(By.xpath("//button[@title='Close']")).isDisplayed();
//		System.out.println(b);
//		if(b)
//		{
//		driver.findElement(By.xpath("//button[@title='Close']")).click();
//		}
	}
	
}
