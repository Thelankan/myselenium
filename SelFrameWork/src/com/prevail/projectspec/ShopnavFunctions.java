package com.prevail.projectspec;

import org.testng.asserts.SoftAssert;

import com.prevail.utilgeneric.Locators;

public class ShopnavFunctions {

	Locators l1;
	SoftAssert sa;

	public ShopnavFunctions(Locators l1,SoftAssert sa){
		this.l1=l1;
		this.sa=sa;
	}
	
public void NavigateToCategoryPage() throws Exception{
		l1.getWebElements("RootCategory_Links","ShopNav\\CategoryLanding.properties").get(1).click();
		l1.getWebElements("Category_Links","ShopNav\\CategoryLanding.properties").get(1).click();
		//li[@class='expandable fa fa-angle-right']/a
		}
			
public void NavigateToSubcategory() throws Exception{
	l1.getWebElements("RootCategory_Links","ShopNav\\CategoryLanding.properties").get(1).click();
	l1.getWebElements("Category_Links","ShopNav\\CategoryLanding.properties").get(1).click();
	l1.getWebElements("Subcategory_Links","ShopNav\\CategoryLanding.properties").get(1).click();
	//li[@class='expandable fa fa-angle-right']/a
	}
	
}
