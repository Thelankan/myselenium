package com.prevail.projectspec;

import com.prevail.utilgeneric.GetData;
import com.prevail.utilgeneric.GetDriver;
import com.prevail.utilgeneric.Locators;

import org.apache.bcel.generic.Select;
import org.openqa.selenium.Alert;
import org.testng.asserts.SoftAssert;


public class ProfileFunctions {
	
	Locators l1;
	SoftAssert sa;
	
	
	public ProfileFunctions(Locators l1,SoftAssert sa){
		this.l1=l1;
		this.sa=sa;
	}

	

//p.loginToAppliction("Vinaylanka@gmail.com","Vin@74113");
//String UN=GetData.getDataFromExcel("data\\GenericData.xls","LoginCredentials",1,1);
//String PWD=GetData.getDataFromExcel("data\\GenericData.xls","LoginCredentials",1,2);

public void loginToAppliction(String userName,String password) throws Exception
{
	l1.getWebElement("MyAccountIcon","Profile\\Register.properties").click();
	l1.getWebElement("Login_Icon","Profile\\Register.properties").click();
	l1.getWebElement("Login_FirstName","Profile\\login.properties").sendKeys(userName);
	l1.getWebElement("Login_LastName","Profile\\login.properties").sendKeys(password);
	l1.getWebElement("Login_Login_Button","Profile\\login.properties").click();	
}

public void loginToAppliction() throws Exception
{
	l1.getWebElement("MyAccountIcon","Profile\\Register.properties").click();
	l1.getWebElement("Login_Icon","Profile\\Register.properties").click();
	l1.getWebElement("Login_FirstName","Profile\\login.properties").sendKeys("Vinaylanka@gmail.com");
	l1.getWebElement("Login_LastName","Profile\\login.properties").sendKeys("Vin@74113");
	l1.getWebElement("Login_Login_Button","Profile\\login.properties").click();	
}
	
public void CreateAccount(String excelSheetName,String SubSheetName,int row) throws Exception
{
	//Create Account
	l1.getWebElement("Register_FirstName", "Profile\\Register.properties").sendKeys(GetData.getDataFromExcel(excelSheetName,SubSheetName,row,0));
	l1.getWebElement("Register_LastName", "Profile\\Register.properties").sendKeys(GetData.getDataFromExcel(excelSheetName,SubSheetName,row,1));
	l1.getWebElement("Register_EmailID", "Profile\\Register.properties").sendKeys(GetData.getDataFromExcel(excelSheetName,SubSheetName,row,2));
	l1.getWebElement("Register_ConfirmEmail", "Profile\\Register.properties").sendKeys(GetData.getDataFromExcel(excelSheetName,SubSheetName,row,3));
	l1.getWebElements("Register_Password", "Profile\\Register.properties").get(0).sendKeys(GetData.getDataFromExcel(excelSheetName,SubSheetName,row,4));
	l1.getWebElement("Register_ConfirmPassword", "Profile\\Register.properties").sendKeys(GetData.getDataFromExcel(excelSheetName,SubSheetName,row,5));
}
	
public void CreateAccount_UI() throws Exception
{
	//To check elements are visible or not 
	 sa.assertTrue(l1.getWebElement("Register_FN_Label", "Profile\\Register.properties").isDisplayed());
	 sa.assertTrue(l1.getWebElement("Register_LN_Label", "Profile\\Register.properties").isDisplayed());
	 sa.assertTrue(l1.getWebElement("Register_Email_Label", "Profile\\Register.properties").isDisplayed());
	 sa.assertTrue(l1.getWebElement("Register_ConfirmEmail_Label", "Profile\\Register.properties").isDisplayed());
	 sa.assertTrue(l1.getWebElement("Register_Password_Label", "Profile\\Register.properties").isDisplayed());
	 sa.assertTrue(l1.getWebElement("Register_ConfirmPassword_Label", "Profile\\Register.properties").isDisplayed());
	 sa.assertTrue(l1.getWebElement("Register_FirstName", "Profile\\Register.properties").isDisplayed());
	 sa.assertTrue(l1.getWebElement("Register_LastName", "Profile\\Register.properties").isDisplayed());
	 sa.assertTrue(l1.getWebElement("Register_EmailID", "Profile\\Register.properties").isDisplayed());
	 sa.assertTrue(l1.getWebElement("Register_ConfirmEmail", "Profile\\Register.properties").isDisplayed());
	 sa.assertTrue(l1.getWebElement("Register_Password", "Profile\\Register.properties").isDisplayed());
	 sa.assertTrue(l1.getWebElement("Register_ConfirmPassword", "Profile\\Register.properties").isDisplayed());
	 sa.assertTrue(l1.getWebElement("Register_ApplyButton", "Profile\\Register.properties").isDisplayed());
	 //It will return all the assertions
	 sa.assertAll();
}
	
public void AddressOverlay_UI() throws Exception
{
	//To check elements are visible or not 
	 sa.assertTrue(l1.getWebElement("Address_AddressName", "Profile\\Addresses.properties").isDisplayed());
	 sa.assertTrue(l1.getWebElement("Address_FirstName", "Profile\\Addresses.properties").isDisplayed());
	 sa.assertTrue(l1.getWebElement("Address_LastName", "Profile\\Addresses.properties").isDisplayed());
	 sa.assertTrue(l1.getWebElement("Address_Address1", "Profile\\Addresses.properties").isDisplayed());
	 sa.assertTrue(l1.getWebElement("Address_Address2", "Profile\\Addresses.properties").isDisplayed());
	 sa.assertTrue(l1.getWebElement("Address_City", "Profile\\Addresses.properties").isDisplayed());
	 sa.assertTrue(l1.getWebElement("Address_State", "Profile\\Addresses.properties").isDisplayed());
	 sa.assertTrue(l1.getWebElement("Address_Postal", "Profile\\Addresses.properties").isDisplayed());
	 sa.assertTrue(l1.getWebElement("Address_Country", "Profile\\Addresses.properties").isDisplayed());
	 sa.assertTrue(l1.getWebElement("Address_phone", "Profile\\Addresses.properties").isDisplayed());
	 sa.assertTrue(l1.getWebElement("Address_ApplyButton", "Profile\\Addresses.properties").isDisplayed());
	 sa.assertTrue(l1.getWebElement("Address_CancelButton", "Profile\\Addresses.properties").isDisplayed());
	 //It will return all the assertions
	 sa.assertAll();
}

public void DeleteAddress() throws Exception
{
	
try {
	
	int AddressCount=l1.getWebElements("Address_Delete_Buttons","Profile\\Addresses.properties").size();
	System.out.println(l1.getWebElements("Address_Delete_Buttons","Profile\\Addresses.properties"));
	for (int i=0;i<AddressCount;i++)
	{
		l1.getWebElements("Address_Delete_Buttons","Profile\\Addresses.properties").get(i).click();
		//Handling alert popup
		Alert alert = GetDriver.driver.switchTo().alert();
		alert.accept();
	}
	
}
catch(Exception e){
}
	
}

public void DeleteCreditCard() throws Exception
{
	
try {
	
	int CardCount=l1.getWebElements("PaymentSettings_DeleteCard_Link","Profile\\PaymentSettings.properties").size();
	System.out.println("Card Count"+CardCount);
	System.out.println(l1.getWebElements("PaymentSettings_DeleteCard_Link","Profile\\PaymentSettings.properties"));
	for (int i=0;i<CardCount;i++)
	{
		l1.getWebElements("PaymentSettings_DeleteCard_Link","Profile\\PaymentSettings.properties").get(i).click();
		//Handling alert popup
		Alert alert = GetDriver.driver.switchTo().alert();
		alert.accept();
	}
	
}
catch(Exception e){
}
	
}

public void EnterAddressValues(String excelSheetName,String SubSheetName,int row) throws Exception
{
	l1.handingDropdown("Address_State", "Profile\\Addresses.properties",GetData.getDataFromExcel(excelSheetName,SubSheetName,row,8));
	Thread.sleep(5000); 
	l1.getWebElement("Address_AddressName", "Profile\\Addresses.properties").sendKeys(GetData.getDataFromExcel(excelSheetName,SubSheetName,row,0));
	Thread.sleep(5000); 
	l1.getWebElement("Address_FirstName", "Profile\\Addresses.properties").sendKeys(GetData.getDataFromExcel(excelSheetName,SubSheetName,row,1));
	Thread.sleep(5000); 
	l1.getWebElement("Address_LastName", "Profile\\Addresses.properties").sendKeys(GetData.getDataFromExcel(excelSheetName,SubSheetName,row,2));
	Thread.sleep(5000); 
	l1.getWebElement("Address_Address1", "Profile\\Addresses.properties").sendKeys(GetData.getDataFromExcel(excelSheetName,SubSheetName,row,3));
	Thread.sleep(5000); 
	l1.getWebElement("Address_Address2", "Profile\\Addresses.properties").sendKeys(GetData.getDataFromExcel(excelSheetName,SubSheetName,row,4));
	Thread.sleep(5000); 
	//l1.getWebElement("Address_Country", "Profile\\Addresses.properties").sendKeys(GetData.getDataFromExcel(excelSheetName,SubSheetName,row,8));
	//l1.getWebElement("Address_State", "Profile\\Addresses.properties").sendKeys(GetData.getDataFromExcel(excelSheetName,SubSheetName,row,6));
	l1.getWebElement("Address_City", "Profile\\Addresses.properties").sendKeys(GetData.getDataFromExcel(excelSheetName,SubSheetName,row,5));
	Thread.sleep(5000); 
	l1.getWebElement("Address_Postal", "Profile\\Addresses.properties").sendKeys(GetData.getDataFromExcel(excelSheetName,SubSheetName,row,7));
	Thread.sleep(5000); 
	l1.getWebElement("Address_phone", "Profile\\Addresses.properties").sendKeys(GetData.getDataFromExcel(excelSheetName,SubSheetName,row,9));
	Thread.sleep(5000); 
	l1.handingDropdown("Address_Country", "Profile\\Addresses.properties",GetData.getDataFromExcel(excelSheetName,SubSheetName,row,6));
	Thread.sleep(5000); 
	
}

public void AddCreditCardOverlay_UI() throws Exception
{
	
	sa.assertTrue(l1.getWebElement("PaymentSettings_AddCreditCardOverlay_Heading", "Profile\\PaymentSettings.properties").isDisplayed());
	sa.assertTrue(l1.getWebElement("PaymentSettings_NameOnCard", "Profile\\PaymentSettings.properties").isDisplayed());
	sa.assertTrue(l1.getWebElement("PaymentSettings_type", "Profile\\PaymentSettings.properties").isDisplayed());
	sa.assertTrue(l1.getWebElement("PaymentSettings_Number", "Profile\\PaymentSettings.properties").isDisplayed());
	sa.assertTrue(l1.getWebElement("PaymentSettings_ExpiresMonth", "Profile\\PaymentSettings.properties").isDisplayed());
	sa.assertTrue(l1.getWebElement("PaymentSettings_ExpiresYear", "Profile\\PaymentSettings.properties").isDisplayed());
	sa.assertTrue(l1.getWebElement("PaymentSettings_FirstName", "Profile\\PaymentSettings.properties").isDisplayed());
	sa.assertTrue(l1.getWebElement("PaymentSettings_Address1", "Profile\\PaymentSettings.properties").isDisplayed());
	sa.assertTrue(l1.getWebElement("PaymentSettings_Address2", "Profile\\PaymentSettings.properties").isDisplayed());
	sa.assertTrue(l1.getWebElement("PaymentSettings_Country", "Profile\\PaymentSettings.properties").isDisplayed());
	sa.assertTrue(l1.getWebElement("PaymentSettings_State", "Profile\\PaymentSettings.properties").isDisplayed());
	sa.assertTrue(l1.getWebElement("PaymentSettings_City", "Profile\\PaymentSettings.properties").isDisplayed());
	sa.assertTrue(l1.getWebElement("PaymentSettings_Zipcode", "Profile\\PaymentSettings.properties").isDisplayed());
	sa.assertTrue(l1.getWebElement("PaymentSettings_Phone", "Profile\\PaymentSettings.properties").isDisplayed());
	sa.assertTrue(l1.getWebElement("PaymentSettings_Email", "Profile\\PaymentSettings.properties").isDisplayed());
	sa.assertTrue(l1.getWebElement("PaymentSettings_ApplyButton", "Profile\\PaymentSettings.properties").isDisplayed());
	sa.assertTrue(l1.getWebElement("PaymentSettings_CancelButton", "Profile\\PaymentSettings.properties").isDisplayed());
	sa.assertTrue(l1.getWebElement("PaymentSettings_AddCreditCardOverlay_Heading", "Profile\\PaymentSettings.properties").isDisplayed());
	sa.assertTrue(l1.getWebElement("PaymentSettings_NameOnCard_Label", "Profile\\PaymentSettings.properties").isDisplayed());
	sa.assertTrue(l1.getWebElement("PaymentSettings_Type_Label", "Profile\\PaymentSettings.properties").isDisplayed());
	sa.assertTrue(l1.getWebElement("PaymentSettings_Number_Label", "Profile\\PaymentSettings.properties").isDisplayed());
	sa.assertTrue(l1.getWebElement("PaymentSettings_Expires_Label", "Profile\\PaymentSettings.properties").isDisplayed());
	sa.assertTrue(l1.getWebElement("PaymentSettings_FN_Label", "Profile\\PaymentSettings.properties").isDisplayed());
	sa.assertTrue(l1.getWebElement("PaymentSettings_LN_Label", "Profile\\PaymentSettings.properties").isDisplayed());
	sa.assertTrue(l1.getWebElement("PaymentSettings_Address1_Label", "Profile\\PaymentSettings.properties").isDisplayed());
	sa.assertTrue(l1.getWebElement("PaymentSettings_Address2_Label", "Profile\\PaymentSettings.properties").isDisplayed());
	sa.assertTrue(l1.getWebElement("PaymentSettings_Country_Label", "Profile\\PaymentSettings.properties").isDisplayed());
	sa.assertTrue(l1.getWebElement("PaymentSettings_State_Label", "Profile\\PaymentSettings.properties").isDisplayed());
	sa.assertTrue(l1.getWebElement("PaymentSettings_City_Label", "Profile\\PaymentSettings.properties").isDisplayed());
	sa.assertTrue(l1.getWebElement("PaymentSettings_Zipcode_Label", "Profile\\PaymentSettings.properties").isDisplayed());
	sa.assertTrue(l1.getWebElement("PaymentSettings_Phone_Label", "Profile\\PaymentSettings.properties").isDisplayed());
	sa.assertTrue(l1.getWebElement("PaymentSettings_Email_Label", "Profile\\PaymentSettings.properties").isDisplayed());
	sa.assertTrue(l1.getWebElement("PaymentSettings_Payment_List", "Profile\\PaymentSettings.properties").isDisplayed());
	sa.assertTrue(l1.getWebElement("PaymentSettings_DeleteCard_Link", "Profile\\PaymentSettings.properties").isDisplayed());
	
}

public void CreateCreditCard(String excelSheetName,String SubSheetName,int row) throws Exception
{
	
	l1.getWebElement("PaymentSettings_NameOnCard", "Profile\\PaymentSettings.properties").sendKeys(GetData.getDataFromExcel(excelSheetName,SubSheetName,row,0));
	l1.getWebElement("PaymentSettings_type", "Profile\\PaymentSettings.properties").sendKeys(GetData.getDataFromExcel(excelSheetName,SubSheetName,row,0));
	l1.getWebElement("PaymentSettings_Number", "Profile\\PaymentSettings.properties").sendKeys(GetData.getDataFromExcel(excelSheetName,SubSheetName,row,0));
	l1.getWebElement("PaymentSettings_ExpiresMonth", "Profile\\PaymentSettings.properties").sendKeys(GetData.getDataFromExcel(excelSheetName,SubSheetName,row,0));
	l1.getWebElement("PaymentSettings_ExpiresYear", "Profile\\PaymentSettings.properties").sendKeys(GetData.getDataFromExcel(excelSheetName,SubSheetName,row,0));
	l1.getWebElement("PaymentSettings_FirstName", "Profile\\PaymentSettings.properties").sendKeys(GetData.getDataFromExcel(excelSheetName,SubSheetName,row,0));
	l1.getWebElement("PaymentSettings_LastName", "Profile\\PaymentSettings.properties").sendKeys(GetData.getDataFromExcel(excelSheetName,SubSheetName,row,0));
	l1.getWebElement("PaymentSettings_Address1", "Profile\\PaymentSettings.properties").sendKeys(GetData.getDataFromExcel(excelSheetName,SubSheetName,row,0));
	l1.getWebElement("PaymentSettings_Address2", "Profile\\PaymentSettings.properties").sendKeys(GetData.getDataFromExcel(excelSheetName,SubSheetName,row,0));
	l1.getWebElement("PaymentSettings_Country", "Profile\\PaymentSettings.properties").sendKeys(GetData.getDataFromExcel(excelSheetName,SubSheetName,row,0));
	l1.getWebElement("PaymentSettings_State", "Profile\\PaymentSettings.properties").sendKeys(GetData.getDataFromExcel(excelSheetName,SubSheetName,row,0));
	l1.getWebElement("PaymentSettings_City", "Profile\\PaymentSettings.properties").sendKeys(GetData.getDataFromExcel(excelSheetName,SubSheetName,row,0));
	l1.getWebElement("PaymentSettings_Zipcode", "Profile\\PaymentSettings.properties").sendKeys(GetData.getDataFromExcel(excelSheetName,SubSheetName,row,0));
	l1.getWebElement("PaymentSettings_Phone", "Profile\\PaymentSettings.properties").sendKeys(GetData.getDataFromExcel(excelSheetName,SubSheetName,row,0));
	l1.getWebElement("PaymentSettings_Email", "Profile\\PaymentSettings.properties").sendKeys(GetData.getDataFromExcel(excelSheetName,SubSheetName,row,0));
}
	
}
