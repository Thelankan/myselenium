package com.prevail.shopnav;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.prevail.projectspec.ProfileFunctions;
import com.prevail.utilgeneric.BaseTest;
import com.prevail.utilgeneric.GetData;

public class CategoryLanding extends BaseTest {
	
@Test
public void NavigateToCatLanding() throws Exception
{
	l1.getWebElements("RootCategory_Links","ShopNav\\CategoryLanding.properties").get(1).click();
	l1.getWebElements("Category_Links","ShopNav\\CategoryLanding.properties").get(1).click();
	l1.getWebElements("Subcategory_Links","ShopNav\\CategoryLanding.properties").get(1).click();
	l1.getWebElements("Category_PaginationHeading","ShopNav\\CategoryLanding.properties").get(1).click();
	l1.getWebElements("Category_ListView","ShopNav\\CategoryLanding.properties").get(1).click();
	l1.getWebElements("Category_GridView","ShopNav\\CategoryLanding.properties").get(1).click();
	
}

}
