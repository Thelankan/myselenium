package com.prevail.utilgeneric;

import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.MobileCapabilityType;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Driver;
import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.ui.Select;

import org.testng.ISuite;
import org.testng.ISuiteListener;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;
import org.testng.xml.XmlTest;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import com.aventstack.extentreports.Status;


public class Test1{  
	
	WebDriver driver;
//	int i;
//	
//	@BeforeSuite
//	public void nav() throws Exception
//	{
//		DesiredCapabilities des=DesiredCapabilities.firefox();
//		des.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
//		des.setCapability("ELEMENT_SCROLL_BEHAVIOR", 1);
//		des.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.DISMISS);
//		des.setCapability("acceptInsecureCerts", true);
//		System.setProperty("webdriver.gecko.driver",System.getProperty("user.dir")+"\\drivers\\geckodriver.exe");
//		driver=new FirefoxDriver(des);
//		driver.get("http://development-elizabetharden-pfsweb.demandware.net/s/ElizabethArden/ceramide-lift-and-firm-3-piece-regimen-a-94-value-1001A0106028.html?cgid=daycreams");
//		Thread.sleep(10000);
//		boolean b=driver.findElement(By.xpath("//button[@title='Close']")).isDisplayed();
//		System.out.println(b);
//		if(b)
//		{
//		driver.findElement(By.xpath("//button[@title='Close']")).click();
//		}
//		driver.findElements(By.xpath("//div[@class='product-add-to-cart']/descendant::button[@id='add-to-cart']")).get(1).click();
//		Thread.sleep(3000);
//		driver.findElement(By.xpath("//span[@class='minicart-quantity']")).click();
//		Thread.sleep(5000);
//		driver.findElements(By.xpath("//button[@id='buttondisable']")).get(0).click();
//		Thread.sleep(5000);
//		driver.findElement(By.name("dwfrm_login_unregistered")).click();
//		Thread.sleep(5000);
//	}
///*	@Test(dataProvider="loginVal" , dataProviderClass=Data.class)
//	public void logInVal(String un,String pwd)
//	{
//	driver.findElement(By.xpath("//input[contains(@id,'dwfrm_login_username')]")).sendKeys(un);
//	driver.findElement(By.xpath("//input[contains(@id,'dwfrm_login_password')]")).sendKeys(pwd);
//	driver.findElement(By.name("dwfrm_login_login")).click();
//	}*/
//
//	@Test(dataProvider="shippingVal" , dataProviderClass=Data.class)
//	public void ShipVal(TestData data) throws Exception
//	{
//		if(i==1)
//		{
//		driver.findElement(By.id("dwfrm_singleshipping_shippingAddress_addressFields_firstName")).sendKeys(data.get(0));
//		driver.findElement(By.id("dwfrm_singleshipping_shippingAddress_addressFields_lastName")).sendKeys(data.get(1));
//		driver.findElement(By.id("dwfrm_singleshipping_shippingAddress_addressFields_address1")).sendKeys(data.get(2));
//		driver.findElement(By.id("dwfrm_singleshipping_shippingAddress_addressFields_city")).sendKeys(data.get(3));
//		driver.findElement(By.id("dwfrm_singleshipping_shippingAddress_addressFields_postal")).sendKeys(data.get(4));
//		Select sel=new Select(driver.findElement(By.id("dwfrm_singleshipping_shippingAddress_addressFields_states_state")));
//		sel.selectByValue(data.get(5));
//		driver.findElement(By.id("dwfrm_singleshipping_shippingAddress_addressFields_phone")).sendKeys(data.get(6));
//		}
//		i++;
//	}
	
	@Test
	public void Verification()
	{
		
//		String exePath="C:\\Selpract\\Project\\drivers\\geckodriver.exe";
//		System.setProperty("webdriver.gecko.driver",exePath);
//		WebDriver driver=new FirefoxDriver();
//		driver.get("https://dev32.na.pfsweb.demandware.net/s/PREVAIL/en_US/home");
//		driver.manage().window().maximize();
		driver.findElement(By.xpath("//i[@class='fa fa-user']")).click();
		driver.findElement(By.xpath("//a[contains(text(),'Register')]")).click();
		driver.findElement(By.xpath("//input[contains(@id,'dwfrm_login_username')]")).sendKeys("Vinaylanka@gmail.com");
		driver.findElement(By.xpath("//input[contains(@id,'dwfrm_login_password')]")).sendKeys("Vin@74113");
		
		driver.findElement(By.name("//h2[contains(text(),'Addresses')]")).click();
		driver.findElement(By.linkText("Create New Address")).click();
		//WebElement mySelectElm = driver.findElement(By.xpath("select[@id='dwfrm_profile_address_states_state']")); 
		Select mySelect= new Select(driver.findElement(By.xpath("select[@id='dwfrm_profile_address_states_state']")));
		mySelect.selectByVisibleText("Alaska");
	}

}
