package com.prevail.utilgeneric;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;


public class Locators{

	WebDriver driver;
	By by;
	WebElement wb;
	Logger log;
	ExtentTest extentTestChrome;
	ExtentTest extentTestFF;
	Select sel; 
	
	public Locators(WebDriver driver,ExtentTest extentTest)
	{
		this.driver=driver;
		//this.log=log;
		if(driver.getClass().getSimpleName().equalsIgnoreCase("ChromeDriver"))
		{
		this.extentTestChrome=extentTest;
		}
		else if(driver.getClass().getSimpleName().equalsIgnoreCase("FirefoxDriver"))
		{
		this.extentTestFF=extentTest;
		}
	}
	
	private By getLocators(String loc1,String filename) throws Exception
	{
		
		String str=GetData.getDataFromProperties(System.getProperty("user.dir")+"\\POM\\"+filename, loc1);
		String[] loc=str.split(";");
		
		if(loc[0].equalsIgnoreCase("id"))
		{
			by=By.id(loc[1]);
		}
		else if(loc[0].equalsIgnoreCase("csslocator"))
		{
			by=By.cssSelector(loc[1]);
		}
		else if(loc[0].equalsIgnoreCase("name"))
		{
			by=By.name(loc[1]);
		}
		else if(loc[0].equalsIgnoreCase("class"))
		{
			by=By.className(loc[1]);
		}
		else if(loc[0].equalsIgnoreCase("xpath"))
		{
			by=By.xpath(loc[1]);
		}
		else if(loc[0].equalsIgnoreCase("linktext"))
		{
			by=By.linkText(loc[1]);
		}
		else if(loc[0].equalsIgnoreCase("partiallinktext"))
		{
			by=By.partialLinkText(loc[1]);
		}
		else if(loc[0].equalsIgnoreCase("tagname"))
		{
			by=By.tagName(loc[1]);
		}
		
		System.out.println(by);
		
		WebDriverWait wait=new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.visibilityOfElementLocated(by));
		
		return by;
	}

	public WebElement getWebElement(String loc1,String filename) throws Exception 
	{
		
		wb=driver.findElement(getLocators(loc1, filename));
		//System.out.println(extentTest);
		if(driver.getClass().getSimpleName().equalsIgnoreCase("ChromeDriver"))
		{
		extentTestChrome.log(LogStatus.PASS, ""+wb);
		}else if(driver.getClass().getSimpleName().equalsIgnoreCase("FirefoxDriver"))
		{
			System.out.println("wb");
			extentTestFF.log(LogStatus.PASS, ""+wb);
		}
		if(driver.getClass().getSimpleName().equalsIgnoreCase("ChromeDriver") || driver.getClass().getSimpleName().equalsIgnoreCase("SafariDriver"))
		{
			try
			{
			if(!isElementInViewPort(wb))
			{
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", wb);
			Thread.sleep(500); 
			}
			else
			{
				System.out.println("not here");
			}
			}
			catch(Exception e)
			{
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", wb);
			Thread.sleep(500); 
			}
		}
		return wb;
	}
	
	public void handingDropdown(String loc1,String filename,String Value) throws Exception
	{
	    sel = new Select(driver.findElement(getLocators(loc1, filename)));
	    //sel.selectByVisibleText(Value);
	    sel.selectByIndex(2);
	    //return sel;
	}
	
	public List<WebElement> getWebElements(String loc1,String filename) throws Exception
	{
		return driver.findElements(getLocators(loc1, filename));
	}
	
	private boolean isElementInViewPort(WebElement ele)
	{
		Dimension weD =ele.getSize(); //to get the element Dimensions
		Point weP = ele.getLocation(); // getting the location of the element in the page.
		Dimension d = driver.manage().window().getSize(); // To get the browser dimensions
		int x = d.getWidth(); //browser width
		int y = d.getHeight(); //browser height
		int x2 = weD.getWidth() + weP.getX();
		int y2 = weD.getHeight() + weP.getY();
		System.out.println(x2 <= x && y2 <= y);
		return x2 <= x && y2 <= y;
	}
	
}
