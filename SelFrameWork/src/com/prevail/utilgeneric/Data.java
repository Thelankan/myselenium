package com.prevail.utilgeneric;

import java.util.Arrays;
import java.util.List;

import org.testng.annotations.DataProvider;
import org.testng.collections.Lists;

public class Data {
	
	@DataProvider()
	public Object[][] loginVal()
	{
		return new Object[][] {{"",""},{"test@revcom",""},{"testrev.com",""},{"testi@yaho.com","test"}};
	}
	
	@DataProvider
	public static Object[][] shippingVal() {
	    return new Object[][] {
	            { new TestData("", "", "","","","0","") },
	            { new TestData("test", "test", "1706 n nagle ave","chicago","60707","IL","3333333333") },
	    };
	}

}

class TestData {
    public String[] items;

    public TestData(String... items) {
        this.items = items; // should probably make a defensive copy
    }

    public String get(int x) {
        return items[x];
    }
}