package com.prevail.utilgeneric;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import org.testng.xml.XmlTest;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class Screenshot implements ITestListener {
	
	WebDriver driver;
	Method m;
	ExtentTest extentTestChrome;
	ExtentTest extentTestFF;
	
	public Screenshot(WebDriver driver,ExtentTest extentTest)
	{
		this.driver=driver;
		//this.log=log;
		if(driver.getClass().getSimpleName().equalsIgnoreCase("ChromeDriver"))
		{
		this.extentTestChrome=extentTest;
		}
		else if(driver.getClass().getSimpleName().equalsIgnoreCase("FirefoxDriver"))
		{
		this.extentTestFF=extentTest;
		}
	}
	

	public void onTestStart(ITestResult result) {
		
	}

	public void onTestSuccess(ITestResult result) {
		
	}

	public void onTestFailure(ITestResult result) {
		
		try{
		
		String imgName=result.getInstanceName()+" "+result.getMethod().getMethodName();
		//write take screen shot code
		driver=GetDriver.driver;
		System.out.println(driver.getClass().getName());
		TakesScreenshot scrShot =((TakesScreenshot)driver);
		FileUtils.copyFile(scrShot.getScreenshotAs(OutputType.FILE), new File("C:\\Screenshots\\"+imgName+".png"));
		if(driver.getClass().getSimpleName().equalsIgnoreCase("ChromeDriver")) {
			extentTestChrome.addScreenCapture("C:\\Screenshots\\"+imgName+".png");
			
		}
		else if(driver.getClass().getSimpleName().equalsIgnoreCase("FirefoxDriver")) {
			extentTestFF.addScreenCapture("C:\\Screenshots\\"+imgName+".png");
		}
		
		}
		catch(Exception e)
		{
			
		}
	}

	public void onTestSkipped(ITestResult result) {
		
	//	extentTest.log(LogStatus.SKIP, "Its skipped bro please check the reason");
		
	}

	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		
		
	}

	public void onStart(ITestContext context) {
		
		
	}

	public void onFinish(ITestContext context) {

	}
}
