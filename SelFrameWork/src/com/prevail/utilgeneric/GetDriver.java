package com.prevail.utilgeneric;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.internal.ElementScrollBehavior;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.annotations.Parameters;
import org.testng.xml.XmlTest;


public class GetDriver {
	
	public static WebDriver driver;
	String driverName;
	
	private static Map<String,WebDriver> drivers=new HashMap<String, WebDriver>();
	
	public WebDriver getDriver(XmlTest xmlTest) throws MalformedURLException, InterruptedException
	{
		System.out.println("coming here");
		try
		{
		if(drivers.get(xmlTest.getParameter("broName").toLowerCase())==null)
		{
			System.out.println("start");
			if(xmlTest.getParameter("broName").equalsIgnoreCase("Firefox"))
			{
				DesiredCapabilities des=DesiredCapabilities.firefox();
				des.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
				des.setCapability("ELEMENT_SCROLL_BEHAVIOR", 1);
				des.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.DISMISS);
				des.setCapability("acceptInsecureCerts", true);
				//des.setPlatform(Platform.WIN8_1);
				if(xmlTest.getParameter("GridExecution").equalsIgnoreCase("false"))
				{
					System.out.println("came here FF");
				System.setProperty("webdriver.gecko.driver",System.getProperty("user.dir")+"\\drivers\\geckodriver.exe");
				driver=new FirefoxDriver(des);
				}
				else
				{
					driver=new RemoteWebDriver(new URL(xmlTest.getParameter("GridURL")),des);
				}
				System.out.println("came here FF");
			}
			else if(xmlTest.getParameter("broName").equalsIgnoreCase("Chrome"))
			{
				DesiredCapabilities des=DesiredCapabilities.chrome();
				des.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
				des.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.DISMISS);
				//des.setCapability(CapabilityType.ELEMENT_SCROLL_BEHAVIOR,ElementScrollBehavior.BOTTOM);
				des.setPlatform(Platform.VISTA);
				
				if(xmlTest.getParameter("GridExecution").equalsIgnoreCase("false"))
				{
				System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+"\\drivers\\chromedriver.exe");
				driver=new ChromeDriver(des);
   			}
				else
				{
					driver=new RemoteWebDriver(new URL(xmlTest.getParameter("GridURL")),des);
				}
				System.out.println("came here chrome");
			}
			else if(xmlTest.getParameter("broName").equalsIgnoreCase("IE"))
			{
				DesiredCapabilities des=DesiredCapabilities.internetExplorer();
				des.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
				des.setCapability("ie.forceCreateProcessApi ",true);
				des.setCapability("ELEMENT_SCROLL_BEHAVIOR", ElementScrollBehavior.BOTTOM);
				des.setCapability("ENABLE_ELEMENT_CACHE_CLEANUP",true);
				des.setCapability("REQUIRE_WINDOW_FOCUS", true);
				des.setCapability("IE_ENSURE_CLEAN_SESSION",true);
				//des.setCapability("requireWindowFocus", true);
				des.setPlatform(Platform.VISTA);
				
				if(xmlTest.getParameter("GridExecution").equalsIgnoreCase("false"))
				{
				System.setProperty("webdriver.ie.driver",System.getProperty("user.dir")+"\\drivers\\IEDriverServer.exe");	
				driver=new InternetExplorerDriver(des);
				}
				else
				{
					driver=new RemoteWebDriver(new URL(xmlTest.getParameter("GridURL")),des);
				}
				System.out.println("came here ie");
			}
			else if(xmlTest.getParameter("broName").equalsIgnoreCase("Edge"))
			{
				DesiredCapabilities des=DesiredCapabilities.edge();
				des.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
				
				if(xmlTest.getParameter("GridExecution").equalsIgnoreCase("false"))
				{
				System.setProperty("webdriver.edge.driver",System.getProperty("user.dir")+"\\drivers\\MicrosoftWebDriver.exe");
				driver=new EdgeDriver(des);
				}
				else
				{
					driver=new RemoteWebDriver(new URL(xmlTest.getParameter("GridURL")),des);
				}
				System.out.println("came here edge");
			}
			else if(xmlTest.getParameter("broName").equalsIgnoreCase("Safari"))
			{
				driver=new SafariDriver();
			}
		}
		
		drivers.put(xmlTest.getParameter("broName").toLowerCase(), driver);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return driver;
	}
	
	public void closeDriver()
	{
		System.out.println(drivers.size());
		for(String str:drivers.keySet())
		{
			drivers.get(str).quit();
		}
	}

}
