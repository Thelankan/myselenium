package com.prevail.utilgeneric;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.junit.AfterClass;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.asserts.SoftAssert;
import org.testng.xml.XmlTest;










import com.prevail.projectspec.GlobalFunctions;
import com.prevail.projectspec.ProfileFunctions;
import com.prevail.projectspec.ShopnavFunctions;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;


public class BaseTest {

	public WebDriver driver;
	public Locators l1;
	public String c;
	public ExtentReports extentReportChrome;
	public ExtentReports extentReportFF;
	public ExtentTest extentTestChrome;
	public ExtentTest extentTestFF;
	public Logger log;
	int result;
	public SoftAssert sa;
	public ProfileFunctions p;
	public ShopnavFunctions s;
	
	@BeforeClass
	public void initialize(XmlTest xmlTest) throws Exception
	{
		if(xmlTest.getParameter("broName").equalsIgnoreCase("Chrome")) {
			
		//Report declaration code 
		extentReportChrome = new ExtentReports("C:\\SelFrameWork\\MyReportChrome.html", true);
		//report.addSystemInfo("Person Name", "Aditya");
		extentReportChrome.loadConfig(new File("C:\\Sel\\extentreports-java-3.0.7\\extentreports-java-3.0.7\\extent-config.xml"));
		System.out.println("Chrome report");
		}
		else if(xmlTest.getParameter("broName").equalsIgnoreCase("firefox")) {
			System.out.println("ff template");
		//Report declaration code 
		extentReportFF = new ExtentReports("C:\\SelFrameWork\\MyReportFF.html", true);
		//report.addSystemInfo("Person Name", "Aditya");
		extentReportFF.loadConfig(new File("C:\\Sel\\extentreports-java-3.0.7\\extentreports-java-3.0.7\\extent-config.xml"));
		System.out.println("ff report");
		}
		try
		{
			FileUtils.deleteDirectory(new File(System.getProperty("user.dir")+"\\log"));
		}
		catch(Exception e)
		{
			System.out.println("leave it");
		}
		//new SetLogData().setLogData("Results");
		driver=new GetDriver().getDriver(xmlTest);
		new GlobalFunctions(driver).navigateToSite(xmlTest);
		driver.manage().window().maximize();
		log=Logger.getLogger(this.getClass());
		sa=new SoftAssert();
	}
	
	@BeforeMethod
	public void extReport(Method m,XmlTest xmlTest)
	{
		if(xmlTest.getParameter("broName").equalsIgnoreCase("Chrome")) {
			System.out.println("after mtd chrome");
			extentTestChrome=extentReportChrome.startTest(this.getClass().getSimpleName()+" "+m.getName()+" "+driver.getClass().getSimpleName());
			l1=new Locators(driver,extentTestChrome);
			new Screenshot(driver, extentTestChrome);
		}
		else if(xmlTest.getParameter("broName").equalsIgnoreCase("firefox")) {
			System.out.println("after mtd ff");
			extentTestFF=extentReportFF.startTest(this.getClass().getSimpleName()+" "+m.getName()+" "+driver.getClass().getSimpleName());
			//extentTestFF.log(LogStatus.PASS, "hello");
			l1=new Locators(driver,extentTestFF);
			new Screenshot(driver, extentTestFF);
		}
		log.info(this.getClass().getSimpleName()+" "+m.getName()+" "+driver.getClass().getSimpleName());
		log.info("--------------------------------------");
		p=new ProfileFunctions(l1,sa);
		s=new ShopnavFunctions(l1, sa);
	}
	
	@AfterMethod
	public void afterMtd(Method m)
	{
		log.info(this.getClass().getSimpleName()+" "+m.getName()+" "+driver.getClass().getSimpleName()+" Test Case ended");
		log.info("--------------------------------------");
	}
	
	@AfterTest
	public void endTest(XmlTest xmlTest)
	{
		if(xmlTest.getParameter("broName").equalsIgnoreCase("Chrome")) {
			System.out.println("after cls chrome");
		extentReportChrome.endTest(extentTestChrome);
		extentReportChrome.flush();
		}
		else if(xmlTest.getParameter("broName").equalsIgnoreCase("firefox")) {
			System.out.println("after cls ff");
			extentReportFF.endTest(extentTestFF);
			extentReportFF.flush();
		}
	}
	
	@AfterSuite
	public void tearDown(XmlTest xmlTest)
	{
		System.out.println("next");
		new GetDriver().closeDriver();
		
		//moving log folder
		try{
			FileUtils.copyDirectory(new File(System.getProperty("user.dir")+"\\log"), new File("C:\\Prevail_Logs"));
		}
		catch(Exception e){
			System.out.println("leave it again");
		}
		
		try {
			Process p=Runtime.getRuntime().exec("cmd /c start "+System.getProperty("user.dir")+"\\Sel_Report.bat");
			p.waitFor();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("catch block");
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
