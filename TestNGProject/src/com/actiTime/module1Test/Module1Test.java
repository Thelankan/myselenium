package com.actiTime.module1Test;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class Module1Test {
	
	@BeforeClass
	public void configbefore()
	{
		System.out.println("configbefore");
	}
  
	@BeforeMethod
	public void setUp()
	{
		System.out.println("execute setup");
	}
	
	
	
	@Test(groups={"ST"})
  public void mtd1Test() {
		System.out.println("execute mtd1");
  }



	@Test(groups={"RT"})
  public void mtd2Test() {
		System.out.println("execute mtd2");
  }


@AfterMethod
public void tearDown()
{
	System.out.println("execute tearDown ");
}

@AfterClass
public void configafter()
{
	System.out.println("configafter");
}
}