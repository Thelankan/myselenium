package com.actiTime.module2Test;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class Module2Test {
	
	@BeforeClass
	public void configbefore()
	{
		System.out.println("lanch browser");
	}
  
	@BeforeMethod
	public void setUp()
	{
		System.out.println("login to app");
	}
	
  @Test(groups={"ST"})
  public void createCustomerAndVerifyTest() {
	  System.out.println("createCustomerAndVerifyTest");
  }
  
  @Test(groups={"RT"})
  public void modifyCustomerAndVerifyTest() {
	  System.out.println("modifyCustomerAndVerifyTest");
  }
  
  @AfterMethod
  public void tearDown()
  {
  	System.out.println("logout");
  }

  @AfterClass
  public void configafter()
  {
  	System.out.println("close browser");
  }
  
}
