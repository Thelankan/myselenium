package testngpackage;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class ActiTimeTest {
	
	public static WebDriver driver;
	String url="http://dilip:8080/login.do";
	String userName="admin";
	String password="manager";
	String expectedCustomerName="6";
	String expectedCustomerStatus="Active";
  @BeforeClass
  public void setup()
  {
	  driver=new FirefoxDriver();
  }
  
  @BeforeMethod
  public void loginToApp()
  {
	  driver.get(url);
	  driver.findElement(By.name("username")).sendKeys(userName);
		driver.findElement(By.name("pwd")).sendKeys(password);
		driver.findElement(By.xpath("//input[@type='submit']")).click();
		
		
  }
	
	
  @Test
  public void createCustomerAndVerify() throws InterruptedException {
	  Thread.sleep(2000);

		//Create customer detail
		driver.findElement(By.linkText("Projects & Customers")).click();
		
		Thread.sleep(2000);
		
		driver.findElement(By.xpath("//input[@value='Add New Customer']")).click();
		
		Thread.sleep(2000);
		
		driver.findElement(By.name("name")).sendKeys(expectedCustomerName);
		driver.findElement(By.xpath("//input[@value='Create Customer']")).click();
		
		Thread.sleep(2000);
		
		//verify customer detail
		driver.findElement(By.linkText(expectedCustomerName)).click();
		
		Thread.sleep(2000);
		
		String actualCustomerName= driver.findElement(By.xpath("//td[contains(text(),'ve selected cus')]/following-sibling::td/span")).getText();
		String actualCustomerStatus= driver.findElement(By.xpath("//td[contains(text(),'er status')]/following-sibling::td")).getText();
		
		if(expectedCustomerName.equals(actualCustomerName) && expectedCustomerStatus.equals(actualCustomerStatus))
		{
			System.out.println("Test case Passed");
		}
		else
		{
			System.out.println("Test case Failed");
		}
		
  }
  
  
  @Test
  public void sampleCheck() throws InterruptedException
  {
	  Thread.sleep(2000);
		
		//Create customer detail
		driver.findElement(By.linkText("Projects & Customers")).click();
		Thread.sleep(2000);
		Select sel=new Select(driver.findElement(By.xpath("//select[@name='recordsPerPage']")));
		List<WebElement> ls=sel.getOptions();
		System.out.println(ls.size());
		System.out.println(ls.get(0).getText());
		String cnt =ls.get(0).getText();
		int cntNew=Integer.parseInt(cnt);
		List<WebElement> lst=driver.findElements(By.xpath("//tbody[tr[th[contains(text(),'Customer / Proje')]]]/tr/td[6]/input"));
		//List<WebElement> lst=driver.findElements(By.xpath("//tbody[tr[th[contains(text(),'Customer / Proje')]]]/tr/td[2]/a"));
		System.out.println(lst.size());
		//String count=lst.size()+"";
		int count=lst.size();
		//if(cnt.equals(count))
		if(cntNew==count)	
		{
			System.out.println("pass");
			
		}
		else{
			System.out.println("Fail");
		}
		Thread.sleep(5000);
  }
		
	@AfterMethod
	public void logout()
	{
		driver.findElement(By.className("logoutImg")).click();
			
	}
	@AfterClass
	public void exitBrower()
	{
		//driver.quit();		
	}
  }

