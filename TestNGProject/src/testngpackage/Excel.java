package testngpackage;

import java.io.FileInputStream;
import java.io.IOException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

public class Excel {
	
String excelPath="E:\\FrameWorkData\\TestData.xlsx";
	
	//Get Test Data From Excel Sheet
	public String getExcelData(String sheetName, int rowNum, int columnNum) throws InvalidFormatException, IOException
	{
		FileInputStream fileIn =new FileInputStream(excelPath);
		Workbook wrkbk=WorkbookFactory.create(fileIn);
		Sheet sh=wrkbk.getSheet(sheetName);
		Row row=sh.getRow(rowNum);
		String data=row.getCell(columnNum).getStringCellValue();
		return data;

	}
}
