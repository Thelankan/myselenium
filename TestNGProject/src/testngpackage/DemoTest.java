package testngpackage;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class DemoTest {
 
	@BeforeMethod
	public void beforeConfigure(){
		System.out.println("before method");
	}
	@Test
  public void modifyCustomerTest() {
		System.out.println("modify customer");
  }
	
	@Test
  public void createCustomerTest() {
		System.out.println("create customer");
  }
	
	@AfterMethod
	public void afterConfigure(){
		System.out.println("after method");
	}
}
