package testngpackage;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;

public class ExampleTest  {
	
	@Test
  public void file() throws InvalidFormatException, IOException {
	  
		FileInputStream fi=new FileInputStream("E:\\FWdata\\TestData.xlsx");
		Workbook wb=WorkbookFactory.create(fi);
		Sheet sh=wb.getSheet("TestData");
		Row row=sh.getRow(1);
		String userName=row.getCell(2).getStringCellValue();
		String password=row.getCell(3).getStringCellValue();
	  
	  
	    WebDriver  driver=new FirefoxDriver();
		driver.get("http://dilip:8080/login.do");
		driver.findElement(By.name("username")).sendKeys(userName);
		driver.findElement(By.name("pwd")).sendKeys(password);
		driver.findElement(By.xpath("//input[@type='submit']")).click();
		
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		
		//Navigate to user page
		driver.findElement(By.linkText("Users")).click();
		 
		FileInputStream fio=new FileInputStream("E:\\FWdata\\TestData.xlsx");
		Workbook wbk=WorkbookFactory.create(fio);
		Sheet sht=wbk.getSheet("TestData");
		Row rw=sht.getRow(1);
		Cell cel=rw.createCell(7);
		cel.setCellType(cel.CELL_TYPE_STRING);
		cel.setCellValue("PASS");
		FileOutputStream fo=new FileOutputStream("E:\\FWdata\\TestData.xlsx");
		wbk.write(fo);
		
  }
}
