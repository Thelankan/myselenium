package testngpackage;

import java.util.concurrent.TimeUnit;

//import junit.framework.Assert;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

public class AssertionTest {
	@Test
	public void verifydefaultText() {
	String expectedpage="Open Tasks";
	WebDriver driver=new FirefoxDriver();
	driver.get("http://dilip:8080/login.do");
	driver.findElement(By.name("username")).sendKeys("admin");
	driver.findElement(By.name("pwd")).sendKeys("manager");
	driver.findElement(By.xpath("//input[@type='submit']")).click();
	driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	String actText=driver.findElement(By.xpath("//td[(contains(text(),'Open Tasks'))]")).getText();
	Assert.assertEquals(actText,expectedpage,"page is not verified");
	driver.quit();
	/*WebDriver driver;
	
  @Test
  public void testngAssertion() {
	  
	//Test Data
		String userName="admin";
		String password="manager";
		String user="test12345";
		String fname="ab";
		String lname="bc";
		String expOption=lname+","+" "+fname+" "+"("+user+")";
		
		//Launch Application and login to Application
		driver=new FirefoxDriver();
		driver.get("http://dilip:8080/login.do");
		driver.findElement(By.name("username")).sendKeys(userName);
		driver.findElement(By.name("pwd")).sendKeys(password);
		driver.findElement(By.xpath("//input[@type='submit']")).click();
		
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		
		//Navigate to user page
		driver.findElement(By.linkText("Users")).click();
		
		//Create user
		driver.findElement(By.xpath("//input[@value='Add New User']")).click();
		
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		
		driver.findElement(By.name("username")).sendKeys(user);
		driver.findElement(By.name("passwordText")).sendKeys("12345");
		driver.findElement(By.name("passwordTextRetype")).sendKeys("12345");
		driver.findElement(By.name("firstName")).sendKeys(fname);
		driver.findElement(By.name("lastName")).sendKeys(lname);
		driver.findElement(By.xpath("//input[@type='submit']")).click();
		
		//Navigate to user detail page and capture the details
		driver.findElement(By.partialLinkText(user)).click();
		
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		
		String userSelected= driver.findElement(By.xpath("//td[contains(text(),'You have sele')]/following-sibling::td/span")).getText();
		String actualUser =driver.findElement(By.name("username")).getAttribute("value");
		String actualfname=driver.findElement(By.name("firstName")).getAttribute("value");
		String actuallname=driver.findElement(By.name("lastName")).getAttribute("value");
		
		//verify user details
		if(user.equals(actualUser) && fname.equals(actualfname) && lname.equals(actuallname))
		{
			System.out.println("User created:  "+userSelected);
			System.out.println("Test case Passed(User detail verified)");
			System.out.println("name:  "+actualUser);
			System.out.println("firstname:  "+actualfname);
			System.out.println("lastname:  "+actuallname);
		}
		else
		{
			System.out.println("Test case Failed(User detail not match)");
		}
	  */
  }
}
